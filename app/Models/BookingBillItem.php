<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookingBillItem extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'booking_bill_id',
        'billable_type',
        'billable_id',
    ];

    public function billable()
    {
        return $this->morphTo('billable');
    }
}
