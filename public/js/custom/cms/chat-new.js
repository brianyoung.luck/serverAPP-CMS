let chatWrapperChatId = "";
const chatsWrapper = $(".chatsWrapper");
const selectBody = $(".selectpicker");
const chatGroupBodyWrapperUl = $("#chatGroupBodyWrapperUl");
const chatCount = $("#chatCount");
const imgUrlChatAdmin =
  "https://servr-public.s3-ap-southeast-1.amazonaws.com/chat/cs.png";
const imgUrlChatHotel =
  "https://servr-public.s3-ap-southeast-1.amazonaws.com/chat/hotel.png";

let channelResults = [];
const openedChats = [];
const magicNumberScroll = 250;

// $(document).ready(async function () {
//     await initSendBird();
// });
initSendBird();

$(document).ready(function() {
  // On press send chat button
  $(document).on("click", ".sendChatBtn", function() {
    sendChat($(this).data("chat_id"));
  });

  // On press enter
  $(document).on("keyup", ".textChat", function(e) {
    isEnterPressed(e, $(this).data("chat_id"));
  });

  // On click chat group item
  $(document).on("click", "#chatGroupBodyWrapperUl li", async function() {
    await showSelectedChat(
      $(this).data("chat_id"),
      $(this).data("chat_name"),
      $(this)
    );
  });

  // Outer div first and then inner div
  // On header chat clicked (minimize or show chat)
  $(document).on("click", ".topHeaderChatGuest", function(e) {
    toggleMinimizeChat($(this).data("chat_id"), $(this).data("chat_state"));
  });
  // On click close chat button
  $(document).on("click", ".headerLiveChatClose", async function(e) {
    await closeChat($(this).data("chat_id"));
    // Required
    e.stopPropagation();
  });

  $(document).on("click", ".notifyWrapper", function(e) {
    if (!$(e.target).hasClass("close")) {
      const channelUrl = $(this).data("channel_url");
      const channelName = $(this).data("channel_name");
      const message = $(this).data("message");
      const alreadyClicked = $(this).data("already_clicked");
      if (!alreadyClicked) {
        loadNewMessage(channelUrl, message, channelName);
      }
      $(this).data("already_clicked", "true");
    }
  });
});

$(document).on("click", ".btnCreateGroup", async function() {
  var users = $("#users").val();
  var message = $("#message").val();
  if (!users.length > 0) {
    $(".resMessage").html("Please select minimum one user.");
  } else if (!message) {
    $(".message").html("Message field is required");
  } else {
    const users = $("#users").val();
    const message = $("#message").val();
    $.each(users, async function(index, ids) {
      const getchannel = await getChannelByUrl(ids);
      console.log(getchannel);
      return new Promise((resolve, reject) => {
        getchannel.sendUserMessage(message, function(message, error) {
          if (error) {
            reject(error);
            return;
          }
          // console.log(message);
          resolve(message);
          $("#chatModal").modal("hide");
        });
      });
    });
  }
});
async function getChannelByUrl(ChannelUrl) {
  return new Promise((resolve, reject) => {
    sb.GroupChannel.getChannel(ChannelUrl, function(groupChannel, error) {
      if (error) {
        reject(error);
        return;
      }
      resolve(groupChannel);
    });
  });
}
$(document).on("click", "#compose", function() {
  $("#message").val("");
  $.get(activeUsersUrl, function(data, textStatus, jqXHR) {
    updateActiveUserChatList(data.data);
  });
});

/**
 * Init chat
 */
async function initChat(channels) {
  // Update chat body chat group
  updateChatBodyChatGroup(channels);

  // Update counter chat group
  await updateCounterChatGroup();
}

/**
 * Create & show chat
 * @param chatId
 * @param chatName
 * @param liElement
 */
async function showSelectedChat(chatId, chatName, liElement = null) {
  // Remove chat from chat group
  if (liElement) {
    liElement.remove();
  } else {
    $(".listChatGroup" + chatId).remove();
  }

  // Create chat details
  await createNewChat({
    chat_id: chatId,
    chat_name: chatName,
  });
  console.log(chatId);

  // Mark as read
  await markAsReadChannel(chatId);

  // Update counter chat group based on li item count
  await updateCounterChatGroup();
  // Update counter chat with magic... Dont remove function bellow
  await updateCounterChatGroup();

  // Push to array
  openedChats.push({
    chat_id: chatId,
    chat_name: chatName,
  });

  // Bind event scroll
  await bindEventScrollChat(chatId);
}

/**
 * Create new chat
 * @param chatData
 */
async function createNewChat(chatData) {
  const groupChannel = await prepareLoadPreviousMessages(chatData.chat_id);
  let messages;
  for (const channelResult of channelResults) {
    if (channelResult.chat_id === chatData.chat_id) {
      messages = await loadPreviousMessagesSendBird(
        channelResult.prevMessageListQuery
      );
    }
  }

  let chatBoxData = ``;
  for (const message of messages) {
    if (
      message.messageType === "admin" ||
      message._sender.userId !== adminHotel.sendbird_user_id
    ) {
      chatBoxData += `
                <li class='another'>
                    <div class='avatar-icon'>
                        <img src='${imgUrlChatHotel}'>
                    </div>
                    <div class='messages'>
                        <p>${message.message}</p>
                    </div>
                </li>
            `;
    } else {
      chatBoxData += `
                <li class='me'>
                    <div class='avatar-icon'>
                        <img src='${imgUrlChatAdmin}'>
                    </div>
                    <div class='messages'>
                        <p>${message.message}</p>
                    </div>
                </li>
            `;
    }
  }

  // Create chat details
  chatsWrapper.prepend(`
        <div class='chatWrapper' id="chatWrapper${chatData.chat_id}">
            <div class='topHeaderChatGuest' data-chat_id="${
              chatData.chat_id
            }" data-chat_state="show">
                <!--<span class="headerLiveChatText">Live chat</span>-->
                <i class="fa fa-close pull-right headerLiveChatClose" data-chat_id="chatWrapper${
                  chatData.chat_id
                }"></i>
                <span class="headerRoomText pull-right">${
                  chatData.chat_name.length > 20
                    ? chatData.chat_name.substring(0, 20) + "..."
                    : chatData.chat_name
                }</span>
            </div>
            <div class="bodyRoomBox">
                <ul class='chatBox'>
                   ${chatBoxData}
                </ul>
                <div class='actionFooter'>
                    <div class="row">
                        <div class="col-9 textChatWrapper">
                            <div class="form-group">
                                <input class="form-control textChat" id="name" type="text" placeholder="Enter text here" autocomplete="off" data-chat_id="chatWrapper${
                                  chatData.chat_id
                                }">
                            </div>
                        </div>
                        <div class="col-3 btnSendWrapper">
                            <button class="btn sendChatBtn" type="button" data-chat_id="chatWrapper${
                              chatData.chat_id
                            }">Send</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    `);

  // Scroll to bottom
  const elementChatBox = $("#chatWrapper" + chatData.chat_id + " .chatBox");
  scrollToBottom(elementChatBox);
}

/**
 * Update counter chat group
 */
async function updateCounterChatGroup() {
  const count = await getTotalUnreadChannelCount();
  //chatCount.text(chatGroupBodyWrapperUl.children().length);
  chatCount.text(count);
}

/**
 * Update chat body in chat group
 * @param chatData
 */
function updateChatBodyChatGroup(chatData) {
  chatGroupBodyWrapperUl.text(``);

  for (const element of chatData) {
    sb.GroupChannel.getChannel(element.chat_id, function(groupChannel, error) {
      if (error) {
        return;
      }
      // console.log(groupChannel);
      const unreadMessageCount = groupChannel.unreadMessageCount;
      if (unreadMessageCount > 0) {
        chatGroupBodyWrapperUl.append(`
                    <li class="listChatGroup${element.chat_id}" data-chat_id="${
          element.chat_id
        }" data-chat_name="${element.chat_name}"><b>${
          element.chat_name.length > 12
            ? element.chat_name.substring(0, 13) + "..."
            : element.chat_name
        }
        </b></li>
                `);
      } else {
        chatGroupBodyWrapperUl.append(`
                    <li class="listChatGroup${element.chat_id}" data-chat_id="${
          element.chat_id
        }" data-chat_name="${element.chat_name}">${
          element.chat_name.length > 15
            ? element.chat_name.substring(0, 10) + "..."
            : element.chat_name
        }</li>
                `);
      }
    });
  }
}

/**
 * Load new chat
 * @param chatId
 * @param message
 * @param channel_name
 */
function loadNewMessage(chatId, message, channel_name) {
  const inOpenedChat = isInOpenedChat(chatId);
  if (inOpenedChat) {
    // Show chat is minimize
    const chatWrapperHeader = $(
      "#chatWrapper" + chatId + " .topHeaderChatGuest"
    );
    if (chatWrapperHeader.data("chat_state") === "minimized") {
      toggleMinimizeChat(chatId, "minimize");
    }

    // Append
    $("#chatWrapper" + chatId + " .chatBox ").append(`
            <li class='another'>
                <div class='avatar-icon'>
                    <img src='${imgUrlChatHotel}'>
                </div>
                <div class='messages'>
                    <p>${message}</p>
                </div>
            </li>
        `);

    // Scroll down
    const elementChatBox = $("#chatWrapper" + chatId + " .chatBox");
    scrollToBottom(elementChatBox);
  } else {
    // Open chat and show...
    showSelectedChat(chatId, channel_name);
  }
}

function showPopupNotification(channel, message, isChatAlreadyShowed = false) {
  $.notify(
    {
      // options
      icon: "icon-user icons",
      title: channel.name,
      message: message.message,
      url: "#",
      target: "_self",
    },
    {
      // settings
      element: "body",
      position: null,
      type: "info",
      allow_dismiss: true,
      newest_on_top: true,
      showProgressbar: false,
      placement: {
        from: "top",
        align: "right",
      },
      offset: 20,
      spacing: 10,
      z_index: 1031,
      delay: 5000,
      timer: 1000,
      url_target: "_blank",
      mouse_over: "pause",
      animate: {
        enter: "animated fadeInRight",
        exit: "animated fadeOutRight",
      },
      onShow: null,
      onShown: null,
      onClose: null,
      onClosed: null,
      template:
        `<div data-notify="container" data-channel_url="${
          channel.url
        }" data-channel_name="${channel.name}" data-message="${
          message.message
        }" data-already_clicked="${isChatAlreadyShowed}" class="col-xs-11 col-sm-3 alert alert-{0} notifyWrapper" role="alert">` +
        '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
        '<span data-notify="icon" class="font-lg"></span> ' +
        '<span data-notify="title"><b>{1}</b></span><br/>' +
        '<span data-notify="message">{2}</span>' +
        '<div class="progress" data-notify="progressbar">' +
        '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
        "</div>" +
        '<a href="{3}" target="{4}" data-notify="url"></a>' +
        "</div>",
    }
  );
}

/**
 * Check if chat is open or not
 * @param chatId
 * @returns {boolean}
 */
function isInOpenedChat(chatId) {
  let inOpenedChat = false;
  for (const openedChat of openedChats) {
    if (openedChat.chat_id === chatId) {
      inOpenedChat = openedChat;
      break;
    }
  }

  return inOpenedChat;
}

/**
 * Bind scroll event for chat
 * @param chatId
 * @constructor
 * @return {boolean}
 */
async function bindEventScrollChat(chatId) {
  const element = $("#chatWrapper" + chatId + " .chatBox");

  element.bind("scroll", async function() {
    if (element.scrollTop() <= 0) {
      await getOldChat(chatId, element.get(0).scrollHeight - magicNumberScroll);
    }
  });

  return true;
}

/**
 * Get old chat
 * @param chatId
 * @param oldHeight
 */
async function getOldChat(chatId, oldHeight) {
  let messages = null;
  for (const channelResult of channelResults) {
    if (channelResult.chat_id === chatId) {
      messages = await loadPreviousMessagesSendBird(
        channelResult.prevMessageListQuery
      );
    }
  }

  let chatBoxData = ``;
  for (const message of messages) {
    if (message._sender.userId !== adminHotel.sendbird_user_id) {
      chatBoxData += `
                <li class='another'>
                    <div class='avatar-icon'>
                        <img src='${imgUrlChatHotel}'>
                    </div>
                    <div class='messages'>
                        <p>${message.message}</p>
                    </div>
                </li>
            `;
    } else {
      chatBoxData += `
                <li class='me'>
                    <div class='avatar-icon'>
                        <img src='${imgUrlChatAdmin}'>
                    </div>
                    <div class='messages'>
                        <p>${message.message}</p>
                    </div>
                </li>
            `;
    }
  }

  const element = $("#chatWrapper" + chatId + " .chatBox");
  element.prepend(`${chatBoxData}`);

  // Stay in chat
  const scrollToIndex =
    element.get(0).scrollHeight - magicNumberScroll - oldHeight;
  scrollToBottom(element, scrollToIndex);
}

/**
 * Close chat
 * @param chatId
 */
async function closeChat(chatId) {
  chatWrapperChatId = chatId;
  animateCSS("#" + chatId, "fadeOutDown", "fast", removeChat);
  $(".compose").show();
  // Remove from array open chats
  const chatIdTmp = chatId.slice(11);
  for (let i = 0; i < openedChats.length; i++) {
    if (openedChats[i].chat_id === chatIdTmp) {
      openedChats.splice(i, 1);
      break;
    }
  }

  await updateChatList();
}

/**
 * Show animation on chat removed
 * @param element
 * @param animationName
 * @param speed
 * @param callback
 */
function animateCSS(element, animationName, speed, callback) {
  const node = document.querySelector(element);
  node.classList.add("animated", animationName, speed);

  function handleAnimationEnd() {
    node.classList.remove("animated", animationName);
    node.removeEventListener("animationend", handleAnimationEnd);
    if (typeof callback === "function") callback();
  }

  node.addEventListener("animationend", handleAnimationEnd);
}

/**
 * Remove chat details
 */
function removeChat() {
  $("#" + chatWrapperChatId).remove();
  chatWrapperChatId = "";
}

/**
 * Send chat
 * @param chatId
 * @returns {boolean}
 */
async function sendChat(chatId) {
  // Get text user
  const elementChatInput = $("#" + chatId + " .textChat");

  // Send chat
  if (elementChatInput.val() === "") {
    return false;
  }

  chatWrapperChatId = chatId;
  await sendMessageSendBird(
    chatWrapperChatId.slice(11),
    elementChatInput.val()
  );

  const elementChatBox = $("#" + chatId + " .chatBox");
  elementChatBox.append(`
        <li class='me'>
            <div class='avatar-icon'>
                <img src='${imgUrlChatAdmin}'>
            </div>
            <div class='messages'>
                <p>${elementChatInput.val()}</p>
            </div>
        </li>
    `);
  scrollToBottom(elementChatBox);

  // Remove text
  elementChatInput.val("");
}

function scrollToBottom(elementChatBox, scroll = -1) {
  if (scroll === -1) {
    elementChatBox.animate(
      {
        scrollTop: elementChatBox.get(0).scrollHeight,
      },
      400
    );
  } else {
    elementChatBox.animate(
      {
        scrollTop: scroll,
      },
      0
    );
  }
}

/**
 * Send chat if user press enter
 * @param e
 * @param chatId
 */
function isEnterPressed(e, chatId) {
  if (e.which === 13) {
    sendChat(chatId);
  }
}

async function initSendBird() {
  window.sb = new SendBird({ appId: SENDBIRD_APP_ID });
  sb.connect(
    adminHotel.sendbird_user_id,
    adminHotel.sendbird_access_token,
    async (user, error) => {
      if (error) {
        return;
      }

      await updateChatList();

      //Register handler for message receive and typing status
      let channelHandler = new this.sb.ChannelHandler();

      channelHandler.onMessageReceived = function(channel, message) {
        // Play sound
        document.getElementById("audioNotificationSound").play();

        // Append id chat is open
        const chatId = channel.url;
        const messageText = message.message;
        const inOpenedChat = isInOpenedChat(chatId);
        if (inOpenedChat) {
          // Show notification
          showPopupNotification(channel, message, true);

          // Show chat is minimize
          const chatWrapperHeader = $(
            "#chatWrapper" + chatId + " .topHeaderChatGuest"
          );
          if (chatWrapperHeader.data("chat_state") === "minimized") {
            toggleMinimizeChat(chatId, "minimize");
          }

          // Append
          $("#chatWrapper" + chatId + " .chatBox ").append(`
                    <li class='another'>
                        <div class='avatar-icon'>
                            <img src='${imgUrlChatHotel}'>
                        </div>
                        <div class='messages'>
                            <p>${messageText}</p>
                        </div>
                    </li>
                `);

          // Scroll down
          const elementChatBox = $("#chatWrapper" + chatId + " .chatBox");
          scrollToBottom(elementChatBox);
        } else {
          // Show notification
          showPopupNotification(channel, message);
        }

        // Auto click on notify
        //loadNewMessage(channel.url, message.message, channel.name);
      };

      channelHandler.onTypingStatusUpdated = function(channel) {
        // let members = channel.getTypingMembers();
        // if (members.length === 0) {
        //     instance.setState({ typingInfo: null, typingChannelUrl: null });
        // } else if (members.length === 1) {
        //     instance.setState({
        //         typingInfo: members[0].nickname + " is typing ...",
        //         typingChannelUrl: channel.url
        //     });
        // } else {
        //     instance.setState({
        //         typingInfo: "Several members are typing ...",
        //         typingChannelUrl: channel.url
        //     });
        // }
      };

      sb.addChannelHandler("HANDLER", channelHandler);
    }
  );
}

async function updateChatList() {
  const channelListQuery = sb.GroupChannel.createMyGroupChannelListQuery();
  channelListQuery.includeEmpty = true;
  channelListQuery.order = "chronological";
  channelListQuery.limit = 100;

  if (channelListQuery.hasNext) {
    channelListQuery.next(async function(channelList, error) {
      if (error) {
        console.log("E: ", error);
        return;
      }

      channelResults = [];
      for (const channel of channelList) {
        let isOpen = false;
        for (const openedChat of openedChats) {
          if (openedChat.chat_id === channel.url) {
            isOpen = true;
            break;
          }
        }

        if (!isOpen) {
          channelResults.push({
            chat_id: channel.url,
            chat_name: channel.name,
          });
        }
      }
      await initChat(channelResults);
    });
  }
}
function updateActiveUserChatList(data) {
  const user_ids = data;
  var html = "";
  selectBody.html("");
  for (const element of user_ids) {
    sb.GroupChannel.getChannel(element.sendbird_channel_url, function(
      groupChannel,
      error
    ) {
      if (error) {
        return;
      }
      selectBody.append(
        '<option  value="' +
          element.sendbird_channel_url +
          '">' +
          groupChannel.name +
          "</option>"
      );
    });
  }
  $(".selectpicker").selectpicker("refresh");
}

/**
 * Load previous 1 chat
 * @param channelUrl
 * @returns {Promise<any>}
 */
async function prepareLoadPreviousMessages(channelUrl) {
  return new Promise((resolve, reject) => {
    sb.GroupChannel.getChannel(channelUrl, function(groupChannel, error) {
      if (error) {
        reject(error);
        return;
      }

      const prevMessageListQuery = groupChannel.createPreviousMessageListQuery();
      prevMessageListQuery.limit = 30;
      prevMessageListQuery.reverse = false;

      for (const channel of channelResults) {
        if (channel.chat_id === channelUrl) {
          channel.group_channel = groupChannel;
          channel.prevMessageListQuery = prevMessageListQuery;
        }
      }

      resolve(groupChannel);
    });
  });
}

function loadPreviousMessagesSendBird(prevMessageListQuery) {
  return new Promise((resolve, reject) => {
    prevMessageListQuery.load(function(messages, error) {
      if (error) {
        reject(error);
        return;
      }
      resolve(messages);
    });
  });
}

/**
 * Send message 1 chat
 * @param chatId
 * @param message
 * @returns {Promise<any>}
 */
async function sendMessageSendBird(chatId, message) {
  return new Promise((resolve, reject) => {
    const params = new sb.UserMessageParams();
    const messages = (sb.UserMessageParams.message = message);
    for (const channel of channelResults) {
      if (channel.chat_id == chatId) {
        // console.log(sendbird_channel_url);
        channel.group_channel.sendUserMessage(messages, function(
          message,
          error
        ) {
          if (error) {
            reject(error);
            return;
          }
          console.log();
          resolve(message);
        });

        break;
      }
    }
  });
}

function toggleMinimizeChat(chatId, chatState) {
  const chatWrapperHeader = $("#chatWrapper" + chatId + " .topHeaderChatGuest");
  const chatWrapperBody = $("#chatWrapper" + chatId + " .bodyRoomBox");

  // Minimize or show chat
  if (chatState === "show") {
    chatWrapperBody.addClass("d-none").removeClass("d-block");
  } else {
    chatWrapperBody.addClass("d-block").removeClass("d-none");
  }

  // Update state chat
  if (chatState === "show") {
    chatWrapperHeader.data("chat_state", "minimized");
  } else {
    chatWrapperHeader.data("chat_state", "show");
  }
}

/**
 * Get count unread channel group
 * @returns {Promise<any>}
 */
async function getTotalUnreadChannelCount() {
  return new Promise((resolve, reject) => {
    sb.getTotalUnreadChannelCount(function(count, error) {
      if (error) {
        return;
      }
      resolve(count);
    });
  });
}

/**
 * Mark as read
 * @returns {Promise<any>}
 */
async function markAsReadChannel(channelUrl) {
  return new Promise((resolve, reject) => {
    sb.GroupChannel.getChannel(channelUrl, function(groupChannel, error) {
      if (error) {
        return;
      }
      groupChannel.markAsRead();
      resolve(groupChannel);
    });
  });
}

/**
 * Get count unread channel group
 * @returns {Promise<any>}
 */
function getUnreadMessageCountInChannel(groupChannel) {
  return new Promise((resolve, reject) => {
    sb.getTotalUnreadChannelCount(function(count, error) {
      if (error) {
        return;
      }
      resolve(count);
    });
  });
}

// $(document).on("click", ".notityWrapper", function() {
//   alert();
// });
