<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\FileStorageService;

class Experience_Button_Image extends Model
{
	protected $table = "Experience_Button_Images";
	public $timestamps = false;

	/*
		Getters
	*/

	public function getPathAttribute($value)
    {
        return $this->getS3Url($value);
	}
	
	private function getS3Url($value)
    {
        $fileStorageService = new FileStorageService(env('FILESYSTEM_DRIVER'));
        $storageServiceName = $fileStorageService->getStorageServiceName($value);
        switch ($storageServiceName) {
            case 's3':
                return $fileStorageService->getTemporaryUrl($value);
                break;
            default:
                return $value;
        }
    }


}
