<?php $__env->startSection('title', __('Checkout')); ?>

<?php $__env->startSection('content'); ?>
    <div id="checkoutsWrapper" >
        <?php $helperService = app('App\Services\HelperService'); ?>
        <div class="row">
            <h4>
                Checkout time: <?php echo e($helperService::dateTimeFormat(auth('hotel')->user()->checkout_time, 'g:i A')); ?>

                <button class="btn btn-shadow ml-2" id="changeCheckoutTimeBtn">Change</button>
            </h4>
        </div>
        <div class="row">
            <h4>
                Late checkout time: <?php echo e($helperService::dateTimeFormat(auth('hotel')->user()->late_checkout_time, 'g:i A')); ?>

                <button class="btn btn-shadow ml-2" id="changeLateCheckoutTimeBtn">Change</button>
            </h4>
        </div>
        <form id="editHotelDetailsForm" method="post" action="<?php echo e(route('cms.hotels.update', ['id' => auth('hotel')->user()->id])); ?>" enctype="multipart/form-data">
            <?php echo csrf_field(); ?>
            <?php echo method_field('put'); ?>
            <input type="hidden" name="checkout_time" id="checkout_time" value="<?php echo e(auth('hotel')->user()->checkout_time); ?>">
            <input type="hidden" name="late_checkout_time" id="late_checkout_time" value="<?php echo e(auth('hotel')->user()->late_checkout_time); ?>">
        </form>
        <div class="row">
            <h4>
                Guests:
            </h4>
            <form id="checkoutHotelBookingForm" method="post" action="<?php echo e(route('cms.checkouts.update', ['hotel-booking' => 0])); ?>">
                <?php echo method_field('put'); ?>
                <?php echo csrf_field(); ?>
                <input type="hidden" name="hotelBookingId" id="hotelBookingIdCheckout" value="0">
                <input type="hidden" name="updateType" id="updateType" value="checkout">
            </form>
            <form id="lateCheckoutHotelBookingForm" method="post" action="<?php echo e(route('cms.checkouts.update', ['hotel-booking' => 0])); ?>">
                <?php echo method_field('put'); ?>
                <?php echo csrf_field(); ?>
                <input type="hidden" name="hotelBookingId" id="hotelBookingIdLateCheckout" value="0">
                <input type="hidden" name="updateType" id="updateType" value="lateCheckout">
                <input type="hidden" name="statusLateCheckout" id="statusLateCheckout" value="accepted">
            </form>
            <div class="col-12 p-2">
                <div class="card">
                    <table class="table table-responsive-sm table-striped" id="guestTable">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Booking ref#</th>
                            <th>Arrival date</th>
                            <th>Departure date</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                        </thead>
                    </table>
                    <!-- Modal -->
                    <div class="modal fade guestProfileModal" id="guestProfileModal" tabindex="-1" role="dialog" aria-labelledby="guestProfileModal" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <h4>User profile:</h4>
                                    <p>Passport entries</p>
                                    <div class="passportEntriesWrapper" id="guestPassportEntriesWrapper">
                                        <div class="passportEntryWrapper">
                                            <a href="#" target="_blank">
                                                <img src="" alt="Passport photo">
                                            </a>
                                        </div>
                                    </div>

                                    <div class="row userProfileDetails">
                                        <?php $helperService = app('App\Services\HelperService'); ?>
                                        <div class="col-12 col-md-6">
                                            <p>
                                                Name: <span id="guestName"><i class="fa fa-refresh fa-spin fa-fw"></i></span><br/>
                                                Date of Birth: -<br/>
                                                Booking reference: <span id="guestReference"><i class="fa fa-refresh fa-spin fa-fw"></i></span><br/>
                                                Arrival date: <span id="guestArrivalDate"><i class="fa fa-refresh fa-spin fa-fw"></i></span><br/>
                                                Departure date: <span id="guestDepartureDate"><i class="fa fa-refresh fa-spin fa-fw"></i></span><br/>
                                            </p>
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <p>
                                                Credit card number: <span id="guestCardNumber"><i class="fa fa-refresh fa-spin fa-fw"></i></span><br/>
                                                Exp. date: <span id="guestCardExpiryDate"><i class="fa fa-refresh fa-spin fa-fw"></i></span><br/>
                                                Name on card: <span id="guestCardholderName"><i class="fa fa-refresh fa-spin fa-fw"></i></span><br/>
                                                Address: <span id="guestCardAddress"><i class="fa fa-refresh fa-spin fa-fw"></i></span><br/>
                                            </p>
                                            <div>
                                                <button class="btn btn-pill btn-warning lateCheckoutBtn d-inline" data-id="0" type="button" aria-pressed="true">Late</button>
                                                <button class="btn btn-pill btn-success checkoutBtn" data-id="0" type="button" aria-pressed="true">Checkout</button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-3 p-3 bookingBillWrapper">
                                        <h4>Bill details</h4>
                                        <table class="table table-responsive-sm table-striped bookingBillTable">
                                            <thead>
                                            <tr>
                                                <th>Service</th>
                                                <th>From</th>
                                                <th>Price</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            </tbody>
                                        </table>

                                        <h5 class="totalBillPrice w-100 text-right">Total price 123</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script>
        const url1 = '<?php echo route('data-tables.cms.guests'); ?>';
        const url2 = '<?php echo route('cms.guests.index'); ?>';
        const url3 = '<?php echo route('data-tables.cms.guests'); ?>';
        const url4 = '<?php echo route('ajax.cms.guests.index'); ?>';
        const val1 = '<?php echo auth('hotel')->user()->checkout_time; ?>';
        const val2 = '<?php echo auth('hotel')->user()->late_checkout_time; ?>';
    </script>
    <script src="<?php echo e(asset('js/custom/cms/checkout.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('cms.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/servrhotels.com/resources/views/cms/checkout/index.blade.php ENDPATH**/ ?>