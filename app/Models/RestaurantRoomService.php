<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RestaurantRoomService extends Model
{
    protected $fillable = [
        'restaurant_id',
        'hotel_booking_id',
        'total_price',
        'status',
    ];

    protected $attributes = [
        'status' => 'pending',
    ];

    public function dishes()
    {
        return $this->belongsToMany(RestaurantDish::class)->withPivot(['price', 'note', 'qty']);
    }

    public function hotelBooking()
    {
        return $this->belongsTo(HotelBooking::class);
    }

    public function restaurant()
    {
        return $this->belongsTo(Restaurant::class);
    }

    public function scopeStatus($query, $status)
    {
        return $query->where('status', $status);
    }

    public function bookingBills()
    {
        return $this->morphToMany(BookingBill::class, 'billable');
    }
}