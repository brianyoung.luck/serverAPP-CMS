<?php

use App\Models\MobileLayout;
use Illuminate\Database\Seeder;

class MobileLayoutSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MobileLayout::create([
            'hotel_id' => 1,
            'mobile_hotel_theme_id' => 1,
            'mobile_hotel_icon_id' => 1,
        ]);
    }
}
