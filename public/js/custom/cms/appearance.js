/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 7);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/custom/cms/appearance.js":
/*!***********************************************!*\
  !*** ./resources/js/custom/cms/appearance.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var mainLogoTooltip = $("#mainLogoTooltip");
var uploadFileBtn = $("#uploadFileBtn");
var logo = $("#logo");
var editHotelDetailsForm = $("#editHotelDetailsForm");
var updateHotelNameBtn = $("#updateHotelNameBtn");
var editHotelNameBtn = $("#editHotelNameBtn");
var name = $("#name");
var mobileHotelThemeIdUpdate = $("#mobileHotelThemeIdUpdate");
var editMobileHotelThemeForm = $("#editMobileHotelThemeForm");
var mobileHotelLayoutIdUpdate = $("#mobileHotelLayoutIdUpdate");
var editMobileHotelLayoutForm = $("#editMobileHotelLayoutForm");
$(document).ready(function () {
  mainLogoTooltip.tooltip();
  uploadFileBtn.click(function () {
    logo.trigger("click");
  });
  updateHotelNameBtn.click(function () {
    editHotelLogo();
  });
  logo.change(function () {
    if (validateLogoImageSize()) {
      editHotelLogo();
    }
  });
  editHotelNameBtn.click(function () {
    editHotelName();
  });
  $(document).on("click", ".themeWrapper", function () {
    changeTheme($(this).data("id"));
  });
  $(document).on("click", ".layoutWrapper", function () {
    changeLayout($(this).data("id"));
  });
});

function editHotelLogo() {
  editHotelDetailsForm.submit();
}

function editHotelName() {
  Swal.fire({
    title: "Input hotel name",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: primary_color,
    cancelButtonColor: cancel_button_color,
    confirmButtonText: "Save",
    reverseButtons: true,
    input: "text",
    inputValue: hotelName,
    inputValidator: function inputValidator(value) {
      if (!value) {
        return "Please input hotel name!.";
      }
    }
  }).then(function (result) {
    if (result.value) {
      name.val(result.value);
      editHotelDetailsForm.submit();
    }
  });
}

function changeTheme(themeId) {
  mobileHotelThemeIdUpdate.val(themeId);
  editMobileHotelThemeForm.submit();
}

function changeLayout(layoutId) {
  mobileHotelLayoutIdUpdate.val(layoutId);
  editMobileHotelLayoutForm.submit();
  $(".file_error").hide();
}

$(document).on("click", ".btnAddCustomLayout", function () {
  $("#customLayoutModal").modal("show");
});
$(".saveLayoutImagesBtn").click(function () {
  var checkin_image = $("#checkin_image").attr("data-status");
  var checkout_image = $("#checkout_image").attr("data-status");
  var restaurant_image = $("#restaurant_image").attr("data-status");
  var spa_image = $("#spa_image").attr("data-status");
  var concierge_image = $("#concierge_image").attr("data-status");
  var experience_image = $("#experience_image").attr("data-status");
  var photo_preview_image = $("#photo_preview_image").attr("data-status");

  if ($("#checkin_image").val() && $("#checkout_image").val() && $("#restaurant_image").val() && $("#spa_image").val() && $("#concierge_image").val() && $("#experience_image").val() && $("#photo_preview_image").val() && $("#layout_name").val()) {
    if (checkin_image == "true" && checkout_image == "true" && restaurant_image == "true" && spa_image == "true" && concierge_image == "true" && experience_image == "true" && photo_preview_image == "true") {
      $(".file_error").hide();
      $("#saveLayoutImagesForm").submit();
    } else {
      $(".file_error").show();
    }
  } else {
    $(".file_error").show();
    $(".file_error").html("Please fill the all fields.");
  }
});

function validateLogoImageSize() {
  $(".logo_file_error").hide();
  var file_size = $("#logo")[0].files[0].size;

  if (file_size > 4097152) {
    $(".logo_file_error").show();
    console.log("err");
    return false;
  } else {
    $(".logo_file_error").hide();
    return true;
  }
}

/***/ }),

/***/ 7:
/*!*****************************************************!*\
  !*** multi ./resources/js/custom/cms/appearance.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! E:\Xammp\htdocs\server_hotels\resources\js\custom\cms\appearance.js */"./resources/js/custom/cms/appearance.js");


/***/ })

/******/ });