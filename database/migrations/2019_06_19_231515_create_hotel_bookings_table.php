<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotel_bookings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('hotel_id');
            $table->string('reference')->nullable()->unique();
            $table->dateTime('arrival_date');
            $table->dateTime('departure_date');
            $table->enum('late_check_out', [
                'pending',
                'accepted',
                'rejected',
            ])->nullable();
            $table->string('cardholder_name');
            $table->string('card_number');
            $table->date('card_expiry_date');
            $table->string('card_address');
            $table->string('phone_number');
            $table->string('room_number')->nullable();
            $table->string('sendbird_user_id')->nullable();
            $table->string('sendbird_access_token')->nullable();
            $table->string('sendbird_channel_url')->nullable();
            $table->string('device_token')->nullable();
            $table->enum('status', [
                'pending',
                'active',
                'checked_out',
            ]);
            $table->timestamps();

            // Foreign keys
            $table->foreign('hotel_id')
                ->references('id')
                ->on('hotels')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
