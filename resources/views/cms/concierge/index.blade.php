@extends('cms.layouts.app')

@section('title', 'Concierge')

@section('content')
<div id="conciergeWrapper">
    <div class="row">
        <div class="col-md-6 col-12">
            <!-- Modal -->
            <div class="modal fade showDetailsConciergeServiceRequestModal" tabindex="-1" role="dialog"
                aria-labelledby="showDetailsConciergeServiceRequestModal" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <form id="updateConciergeServiceRequestForm" method="post"
                                action="{{ route('cms.concierge-services-requests.update', ['conciergeServicesRequest' => 0]) }}"
                                enctype="multipart/form-data">
                                @csrf()
                                @method('put')
                                <input type="hidden" name="concierge_service_request_id"
                                    id="concierge_service_request_id" value="0">
                                <input type="hidden" name="status" id="status" value="0">
                            </form>
                            <h4 class="mb-4" id="roomNoText">Room 101 - Request details:</h4>
                            <table class="table table-responsive-sm" id="conciergeServiceRequestDetailsTable">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Item</th>
                                        <th>Qty</th>
                                        <th>Note</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Toothbrush</td>
                                        <td>1</td>
                                        <td>-</td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="row mt-4">
                                <div class="offset-lg-1 col-lg-3 col-sm-6 col-xs-12">
                                    <button class="btn btn-pill btn-block btn-danger btn-lg reject"
                                        style="margin-top:0px !important;" type="button"
                                        id="declineConciergeServiceRequestBtn">Reject</button>
                                </div>
                                {{-- <div class="col-3">
                                    <button class="btn btn-pill btn-block btn-success btn-lg" type="button"
                                        id="processConciergeServiceRequestConfirmBtn">Accept</button>
                                </div> --}}
                                <div class="col-lg-3 col-sm-6 col-xs-12 mt-xs-1 mt-sm-1">
                                    <button class="btn btn-pill btn-block btn-success btn-lg accept" type="button"
                                        id="processConciergeServiceRequestConfirmBtn">Accept</button>
                                </div>
                                {{-- <div class="col-3">
                                    <button class="btn btn-pill btn-block btn-success btn-lg" type="button"
                                        id="processConciergeServiceRequestPreparingBtn">Preparing</button>
                                </div> --}}
                                {{-- <div class="col-3">
                                    <button class="btn btn-pill btn-block btn-success btn-lg" type="button"
                                        id="processConciergeServiceRequestOnTheWayBtn">On the way</button>
                                </div> --}}
                                {{-- <div class="col-3 mt-2">
                                    <button class="btn btn-pill btn-block btn-success btn-lg" type="button"
                                        id="processConciergeServiceRequesAcceptBtn">Accept</button>
                                </div> --}}
                                <div class="col-lg-3 col-sm-6 col-xs-12 mt-xs-1 mt-sm-1">
                                    <button class="btn btn-pill btn-block btn-success btn-lg complete" type="button"
                                        id="processConciergeServiceRequestDoneBtn">Completed</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{-- New --}}
            <div class="row">
                <div class="col-12 conciergeServiceRequestPendingWrapper">
                    <div class="conciergeTextWrapper">
                        <div class="conciergeRequestTextWrapper">
                            <h3 class="mb-4">Concierge requests:</h3>
                        </div>
                        <div class="editMenuConciergeWrapper">
                            <a class="btn btn-pill btn-primary" id="editMenuConciergeBtn"
                                href="{{ route('cms.concierge-menus.index') }}">
                                Edit concierge menu
                            </a>
                        </div>
                    </div>
                    <div class="row orderItemsWrapper">
                        @foreach($conciergeServicePendingRequests as $conciergeServiceRequest)
                        <div class="column col-md-4 col-sm-4 col-6">
                            <div class="orderWrapper"
                                data-concierge_service_request_id="{{ $conciergeServiceRequest->id }}">
                                <div class="roomTextWrapper">
                                    <h4>{{ data_get($conciergeServiceRequest, 'hotelBooking.room_number', 'Room -') }}
                                    </h4>
                                </div>
                                <div class="conciergeServiceRequestWrapper">
                                    <ul>
                                        @for ($i=0, $j=1; $i<count($conciergeServiceRequest->conciergeServices); $i++,
                                            $j++)
                                            @if ($j < 4) <li>{{ $conciergeServiceRequest->conciergeServices[$i]->name }}
                                                </li>
                                                @elseif (count($conciergeServiceRequest->conciergeServices) == 4)
                                                <li>{{ $conciergeServiceRequest->conciergeServices[$i]->name }}</li>
                                                @else
                                                <li>More...</li>
                                                @break
                                                @endif
                                                @endfor
                                    </ul>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>

                <div class="col-12 laundryRequestPendingWrapper">
                    <!-- Modal -->
                    <div class="modal fade showLaundaryRequestModal" data-order-id="0" role="dialog"
                        data-backdrop="false" style="margin-top: 106px;" aria-labelledby="showLaundaryRequestModal"
                        aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <h4 class="mb-4 text-center">Are you sure?</h4>
                                    <div class="row mt-4">
                                        <div class="offset-lg-1 col-lg-3 col-sm-6 col-xs-12">
                                            <button class="btn btn-pill btn-block btn-danger btn-lg reject"
                                                style="margin-top:0px !important;" type="button"
                                                id="reject">Reject</button>
                                        </div>
                                        <div class="col-lg-3 col-sm-6 col-xs-12 mt-xs-1 mt-sm-1">
                                            <button class="btn btn-pill btn-block btn-success btn-lg accept"
                                                type="button" id="accept">Accept</button>
                                        </div>
                                        <div class="col-lg-3 col-sm-6 col-xs-12 mt-xs-1 mt-sm-1">
                                            <button class="btn btn-pill btn-block btn-success btn-lg complete"
                                                type="button" id="complete">Completed</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-8">
                            <h3 class="mb-4">Laundry requests:</h3>
                        </div>
                        <div class="col">
                            <div class="editMenuConciergeWrapper float-right">
                                <a class="btn btn-pill btn-primary"
                                    style="background-color: #e7e7e7; box-shadow: 1px 2px 10px -5px rgba(0, 0, 0, 0.63); border:none; color: #6d6d6d; padding-left: 20px; padding-right: 20px; width: 166.83px;"
                                    id="" href="{{ route('cms.laundary-menus.index') }}">
                                    Edit laundry menu
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row orderItemsWrapper">
                        <form id="updateLaundryOrderForm" method="post"
                            action="{{ route('cms.laundry-orders.update', ['laundry_order' => 0]) }}"
                            enctype="multipart/form-data">
                            @csrf()
                            @method('put')
                            <input type="hidden" name="laundry_orders_id" id="laundry_orders_id" value="0">
                            <input type="hidden" name="status" id="status" value="0">
                        </form>
                        @foreach($laundryRequests as $laundryRequest)
                        <div class="column col-md-4 col-sm-4 col-6">
                            <div class="laundryRequestWrapper" data-laundry_request_id="{{ $laundryRequest->id }}">
                                <div class="roomTextWrapper">
                                    <h4>{{ data_get($laundryRequest, 'hotelBooking.room_number', 'Room -') }}
                                    </h4>
                                </div>
                                <div class="conciergeServiceRequestWrapper">
                                    <ul>
                                        {{-- @switch($laundryRequest->type) --}}
                                        {{-- @case('light_dark') --}}
                                        {{-- <li>Light dark</li> --}}
                                        {{-- @break --}}
                                        {{-- @case('mixed_color') --}}
                                        {{-- <li>Mixed color</li> --}}
                                        {{-- @break --}}
                                        {{-- @case('ironing_service') --}}

                                        <li>{{ $laundryRequest->type }}</li>
                                        {{-- @break --}}
                                        {{-- @endswitch --}}
                                    </ul>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-12">
            <div id="laundryRoomCleaningWrapper">
                <div class="row">
                    <div class="col-12 roomCleaningRequestPendingWrapper">
                        <h3 class="mb-4">Room cleaning requests:</h3>
                        <div class="row">
                            <div class="col-12 roomCleaningOrdersWrapper">
                                <form id="updateRoomCleaningRequestForm" method="post"
                                    action="{{ route('cms.room-cleaning-requests.update', ['roomCleaningRequests' => 0]) }}"
                                    enctype="multipart/form-data">
                                    @csrf()
                                    @method('put')
                                    <input type="hidden" name="room_cleaning_request_id" id="room_cleaning_request_id"
                                        value="0">
                                    <input type="hidden" name="room_cleaning_request_status"
                                        id="room_cleaning_request_status" value="0">
                                </form>
                                @foreach($roomCleaningRequests as $roomCleaningRequest)
                                <div class="roomCleaningOrderWrapper">
                                    <div class="roomText">
                                        {{ data_get($roomCleaningRequest, 'hotelBooking.room_number', 'Room -') }}
                                    </div>
                                    <div class="btnStatusWrapper">
                                        <div class="btnWrapper">
                                            <button
                                                class="btn {{ data_get($roomCleaningRequest, 'status', 'completed') == 'pending' ? 'btn-info' : 'btn-secondary' }}"
                                                type="button">Requested</button>
                                        </div>
                                        <div class="btnWrapper border-left border-right">
                                            <button
                                                class="btn {{ data_get($roomCleaningRequest, 'status', 'completed') == 'confirmed' ? 'btn-info' : 'btn-secondary' }} btnConfirmRoomCleaningRequest"
                                                type="button"
                                                data-room_cleaning_request_id="{{ $roomCleaningRequest->id }}">Accepted</button>
                                        </div>
                                        <div class="btnWrapper">
                                            <button
                                                class="btn {{ data_get($roomCleaningRequest, 'status', 'completed') == 'completed' || data_get($roomCleaningRequest, 'status', 'completed') == 'rejected' ? 'btn-info' : 'btn-secondary' }} btnFinishRoomCleaningRequest"
                                                type="button"
                                                data-room_cleaning_request_id="{{ $roomCleaningRequest->id }}">Completed</button>
                                        </div>
                                    </div>
                                </div>
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@stop
<script>
    {{--const url1 = '{!! route('data-tables.cms.guests') !!}';--}}
    {{--const url2 = '{!! route('cms.guests.index') !!}';--}}
    {{--const url3 = '{!! route('data-tables.cms.guests') !!}';--}}
    const url4 = '{!! route('ajax.cms.concierge-services-requests.index') !!}';
    const url5 = '{!! route('ajax.cms.concierge-services.index') !!}';
</script>
@section('js')
<script src="{{ asset('js/custom/cms/concierge.js') }}"></script>
@stop