<?php

namespace App\Http\Controllers\Cms;

use App\Models\BookingBill;
use App\Models\HotelBooking;
use App\Models\Hotel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use Illuminate\Notifications\Notifiable;
use App\Mail\Invoice;

use Mail;

class GuestController extends Controller
{
    use Notifiable;
    public function index()
    {
        $newArrivals = HotelBooking::newArrivals()
            ->with([
                'passportPhotos'
            ])
            ->get();
        $lateCheckouts = HotelBooking::where('hotel_id', auth('hotel')->user()->id)
            ->where('late_check_out', 'pending')
            ->where('status', 'active')
            ->with([
                'passportPhotos'
            ])
            ->get();

        return view('cms.guest.index', [
            'newArrivals' => $newArrivals,
            'lateCheckouts' => $lateCheckouts,
        ]);
    }

    /**
     * Process data tables ajax request.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function indexDataTable(Request $request)
    {
        // Create query
        $query = HotelBooking::query();

        $status = $request->get('status', 'active');
        if ($status == 'active') {
            $query = $query->where('hotel_id', auth('hotel')->user()->id)
                ->where('status', 'active');
        } else {
            $query = $query->where('hotel_id', auth('hotel')->user()->id)
                ->whereIn('status', ['active','checked_out']);
        }

        return Datatables::of($query->orderBy('id','Desc'))
            ->addIndexColumn()
            ->make(true);
    }

    public function ajaxGetGuest(Request $request)
    {
        return [
            'status' => 'true',
        ];
    }


    public function emailBill(Request $request, $hotelBookingId, $email)
    {
        $guestDetails = HotelBooking::where('id', $hotelBookingId)
        ->with([
            'passportPhotos'
        ])
        ->first();

        // Get booking bill details
        $bookingBill = BookingBill::where('hotel_booking_id', $hotelBookingId)
            ->with([
                'items.billable',
            ])
            ->first();
        $table_body = "";
        $total_price = 0.0;
        
        if ($bookingBill) {
            foreach ($bookingBill->items as $item) {
                if ($item->billable_type == "App\Models\RestaurantRoomService") {
                    $item->billable->load('restaurant');

                    $table_body .= "<tr><td>Restaurant room service</td><td>".$item->billable->restaurant->name."</td><td>".$item->billable->total_price."</td></tr>";
                    $total_price += $item->billable->total_price;
                } elseif ($item->billable_type == "App\Models\SpaBooking") {
                    $item->billable->load('spa');
                    $table_body .= "<tr><td>Restaurant room service</td><td>".$item->billable->spa->name."</td><td>".$item->billable->total_price."</td></tr>";
                    $total_price += $item->billable->total_price;
                }
            }
        }

        //Get VAT and Service Charges
        $hotel_settings = Hotel::where('id', auth('hotel')->user()->id)->first();
        
        $currency = $hotel_settings['currency'];
        $vat = $hotel_settings['vat'];
        $service_charges = $hotel_settings['service_charges'];

        $vat = doubleval($vat);
        $service_charges = doubleval($service_charges);

        $vat_price = $total_price * ($vat / 100);
        $service_charges_price = $total_price * ($service_charges / 100);

        $total_price_after_tax = $total_price + $vat_price + $service_charges_price;

        
        $mail_html = "<h4>Bill details</h4>
        <table class=\"table table-responsive-sm table-striped bookingBillTable\">
            <thead>
            <tr>
                <th>Service</th>
                <th>From</th>
                <th>Price</th>
            </tr>
            </thead>
            <tbody>
            ".$table_body."
            </tbody>
        </table>
        <h5 class=\"totalBillPrice w-100 text-right\">VAT: ".$vat_price."</h5>
        <h5 class=\"totalBillPrice w-100 text-right\">Service Charges: ".$service_charges."</h5>
        <h5 class=\"totalBillPrice w-100 text-right\">Total price: ".$total_price_after_tax."</h5>";
        // $email = "hazhayder@gmail.com";
        // $email = "ishratali574@gmail.com";

        $hotel=$hotel_settings;
        Mail::to($email)->send(new Invoice($bookingBill, $hotel, $guestDetails, $service_charges, $vat, $vat_price, $service_charges_price, $total_price_after_tax));
        
        return $mail_html;
    }

    public function printBill(Request $request, $hotelBookingId, $type)
    {
        $guestDetails = HotelBooking::where('id', $hotelBookingId)
        ->with([
            'passportPhotos'
        ])
        ->first();

        // Get booking bill details
        $bookingBill = BookingBill::where('hotel_booking_id', $hotelBookingId)
            ->with([
                'items.billable',
            ])
            ->first();
        $table_body = "";
        $total_price = 0.0;
        
        if ($bookingBill) {
            foreach ($bookingBill->items as $item) {
                if ($item->billable_type == "App\Models\RestaurantRoomService") {
                    $item->billable->load('restaurant');
                    $item->billable->load('dishes');


                    $table_body .= "<tr><td>Restaurant room service</td><td>".$item->billable->restaurant->name."</td><td>".$item->billable->total_price."</td></tr>";
                    $total_price += $item->billable->total_price;
                } elseif ($item->billable_type == "App\Models\SpaBooking") {
                    $item->billable->load('spa');
                    $item->billable->load('treatments');
                    $table_body .= "<tr><td>Restaurant room service</td><td>".$item->billable->spa->name."</td><td>".$item->billable->total_price."</td></tr>";
                    $total_price += $item->billable->total_price;
                }
            }
        }

        //Get VAT and Service Charges
        $hotel_settings = Hotel::where('id', auth('hotel')->user()->id)->first();
        
        $currency = $hotel_settings['currency'];
        $vat = $hotel_settings['vat'];
        $service_charges = $hotel_settings['service_charges'];

        $vat = doubleval($vat);
        $service_charges = doubleval($service_charges);

        $vat_price = $total_price * ($vat / 100);
        $service_charges_price = $total_price * ($service_charges / 100);

        $total_price_after_tax = $total_price + $vat_price + $service_charges_price;

        $hotel=$hotel_settings;
        // Mail::to($email)->send(new Invoice($bookingBill, $hotel, $guestDetails));
        
        return view('cms.guest.print_bill')->with(['bookingBill'=>$bookingBill,'hotel'=>$hotel,'guestDetails'=>$guestDetails,'total_price'=>0,'service_charges'=>$service_charges,'service_charges_price'=>$service_charges_price,'vat'=>$vat,'vat_price'=>$vat_price,'total_price_after_tax'=>$total_price_after_tax,'type'=>$type]);
    }

    public function ajaxGetGuestDetails(Request $request, $hotelBookingId)
    {
        // Get guest details
        $guestDetails = HotelBooking::where('id', $hotelBookingId)
            ->with([
                'passportPhotos'
            ])
            ->first();

        // Get booking bill details
        $bookingBill = BookingBill::where('hotel_booking_id', $hotelBookingId)
            ->with([
                'items.billable',
            ])
            ->first();
            
        if ($bookingBill) {
            foreach ($bookingBill->items as $item) {
                if ($item->billable_type == "App\Models\RestaurantRoomService") {
                    $item->billable->load('restaurant');
                    $item->billable->load('dishes');
                } elseif ($item->billable_type == "App\Models\SpaBooking") {
                    $item->billable->load('spa');
                    $item->billable->load('treatments');
                }
            }
            // foreach ($bookingBill->items as $item) {
            //     foreach ($item->billable->dishes as $dish) {
            //         $item->billable['dish_name']=$dish->name;
            //     }
            // }
        }
        // return $bookingBill;
        //Get VAT and Service Charges
        $hotel_settings = Hotel::where('id', auth('hotel')->user()->id)->first();
        
        $vat = $hotel_settings['vat'];
        $service_charges = $hotel_settings['service_charges'];

        return [
            'status' => 'true',
            'guestDetails' => $guestDetails,
            'bookingBill' => $bookingBill,
            'vat' => doubleval($vat),
            "service_charges" => doubleval($service_charges)
        ];
    }

    public function getBookingBillItems(Request $request)
    {
        // Get booking bill details
        $bookingBill = BookingBill::where('hotel_booking_id', $hotelBookingId)
            ->with([
                'items.billable',
            ])
            ->first();
            
        if ($bookingBill) {
            foreach ($bookingBill->items as $item) {
                if ($item->billable_type == "App\Models\RestaurantRoomService") {
                    $item->billable->load('restaurant');
                    $item->billable->load('dishes');
                } elseif ($item->billable_type == "App\Models\SpaBooking") {
                    $item->billable->load('spa');
                    $item->billable->load('treatments');
                }
            }
        }
    }
}