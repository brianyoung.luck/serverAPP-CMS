<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\PassportPhoto;
use Faker\Generator as Faker;

$factory->define(PassportPhoto::class, function (Faker $faker) {
    $url = $faker->randomElement([
        'https://servrhotels.s3-ap-southeast-1.amazonaws.com/p/passport1.png',
        'https://servrhotels.s3-ap-southeast-1.amazonaws.com/p/passport2.jpg',
        'https://servrhotels.s3-ap-southeast-1.amazonaws.com/p/passport3.jpg',
        'https://servrhotels.s3-ap-southeast-1.amazonaws.com/p/passport4.jpg',
    ]);
    return [
        'url' => $url,
    ];
});
