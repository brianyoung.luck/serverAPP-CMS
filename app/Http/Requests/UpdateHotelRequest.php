<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateHotelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->hotel->id == auth('hotel')->user()->id ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'nullable|string|max:255',
            'logo' => 'nullable|file|max:4096',
            'restaurantImages' => 'nullable|file|max:4096',
            'mobile_hotel_theme_id' => 'nullable|numeric|exists:mobile_hotel_themes,id',
        ];
    }

    public function messages()
    {
        return [
        'logo.max'  => 'Image is too large',
    ];
    }
}