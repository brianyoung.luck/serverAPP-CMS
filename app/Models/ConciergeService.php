<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ConciergeService extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'hotel_id',
        'name',
        'description',
        'price'
    ];

    public function hotel()
    {
        return $this->belongsTo(Hotel::class);
    }

    public function requests()
    {
        return $this->belongsToMany(ConciergeServiceRequest::class);
    }
}