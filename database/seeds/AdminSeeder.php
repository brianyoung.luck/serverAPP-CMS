<?php

use App\Models\Admin;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::create([
            'email'  => 'admin@admin.com',
            'password'  => bcrypt('secret'),
            'name'  => 'Super Admin'
        ]);
        Admin::create([
            'email'  => 'agussuarya@gmail.com',
            'password'  => bcrypt('secret'),
            'name'  => 'Agus Admin'
        ]);
    }
}
