<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <?php $counterService = app('App\Services\CounterService'); ?>
            <li class="nav-item">
                <a class="nav-link <?php echo Request::is('dashboard') || Request::is('') ? 'active' : ''; ?>" href="<?php echo e(route('cms.dashboard.index')); ?>">
                    <i class="nav-icon icon-home"></i> <?php echo e(__('Home')); ?>

                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?php echo Request::is('appearance') ? 'active' : ''; ?>" href="<?php echo e(route('cms.appearances.index')); ?>">
                    <i class="nav-icon icon-eyeglass"></i> <?php echo e(__('Appearance')); ?>

                </a>
            </li>
            <?php if(auth('hotel')->user()->feature->is_restaurant_enabled): ?>
                <li class="nav-item nav-dropdown <?php echo Request::is('restaurants*') ? 'open' : ''; ?>">
                    <a class="nav-link nav-dropdown-toggle" href="#">
                        <i class="nav-icon icon-cup"></i> <?php echo e(__('Restaurant')); ?>

                        <?php
                            $pendingRestaurants = $counterService::getCountPendingRestaurant();
                        ?>
                        <?php if($pendingRestaurants > 0): ?>
                            <span class="badge badge-info badge-pill" style="position: static;"><?php echo e($pendingRestaurants); ?></span>
                        <?php endif; ?>
                    </a>
                    <ul class="nav-dropdown-items">
                        <?php $__currentLoopData = auth('hotel')->user()->restaurants; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $restaurant): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li class="nav-item">
                                <a class="nav-link <?php echo Request::is('restaurants/' . $restaurant->id . '') ? 'active' : ''; ?>" href="<?php echo e(route('cms.restaurants.show', ['restaurant' => $restaurant->id])); ?>">
                                    <i class="nav-icon icon-arrow-right"></i> <?php echo e(__('Restaurant ')); ?><?php echo e(($index + 1)); ?>

                                    <?php
                                        $pendingRestaurant = $counterService::getCountPendingRestaurant($restaurant->id);
                                    ?>
                                    <?php if($pendingRestaurant > 0): ?>
                                        <span class="badge badge-info badge-pill" style="position: static;"><?php echo e($pendingRestaurant); ?></span>
                                    <?php endif; ?>
                                </a>
                            </li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php if(count(auth('hotel')->user()->restaurants) < 3): ?>
                            <form id="createRestaurantForm" method="post" action="<?php echo e(route('cms.restaurants.store')); ?>" enctype="multipart/form-data">
                                <?php echo csrf_field(); ?>
                                <?php echo method_field('post'); ?>
                            </form>
                            <li class="nav-item">
                                <a class="nav-link" href="#" id="createNewRestaurantSidebarBtn">
                                    <i class="nav-icon icon-arrow-right"></i> <?php echo e(__('Create new')); ?>

                                </a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </li>
            <?php endif; ?>
            <?php if(auth('hotel')->user()->feature->is_spa_enabled): ?>
                <li class="nav-item">
                    <?php
                        $spa = auth('hotel')->user()->spas->first();
                    ?>
                    <a class="nav-link <?php echo Request::is('spas*') ? 'active' : ''; ?>" href="<?php echo e(route('cms.spas.show', ['spa' => $spa->id])); ?>">
                        <i class="nav-icon icon-heart"></i> <?php echo e(__('Spa')); ?>

                        <?php
                            $pendingSpa = $counterService::getCountPendingSpa($spa->id);
                        ?>
                        <?php if($pendingSpa > 0): ?>
                            <span class="badge badge-info badge-pill" style="position: static;"><?php echo e($pendingSpa); ?></span>
                        <?php endif; ?>
                    </a>
                </li>
            <?php endif; ?>
            <?php if(auth('hotel')->user()->feature->is_check_in_enabled): ?>
                <li class="nav-item">
                    <a class="nav-link <?php echo Request::is('guests') ? 'active' : ''; ?>" href="<?php echo e(route('cms.guests.index')); ?>">
                        <i class="nav-icon icon-user-follow"></i> <?php echo e(__('Guests')); ?>

                        <?php
                            $guestCount = $counterService::getCountGuest();
                        ?>
                        <?php if($guestCount > 0): ?>
                            <span class="badge badge-info badge-pill" style="position: static;"><?php echo e($guestCount); ?></span>
                        <?php endif; ?>
                    </a>
                </li>
            <?php endif; ?>
            <?php if(auth('hotel')->user()->feature->is_check_out_enabled): ?>
                <li class="nav-item">
                    <a class="nav-link <?php echo Request::is('checkout') ? 'active' : ''; ?>" href="<?php echo e(route('cms.checkouts.index')); ?>">
                        <i class="nav-icon icon-direction"></i> <?php echo e(__('Checkout')); ?>

                        <?php
                            $pendingCheckout = $counterService::getCountPendingCheckout();
                        ?>
                        <?php if($pendingCheckout > 0): ?>
                            <span class="badge badge-info badge-pill" style="position: static;"><?php echo e($pendingCheckout); ?></span>
                        <?php endif; ?>
                    </a>
                </li>
            <?php endif; ?>
            <?php if(auth('hotel')->user()->feature->is_concierge_enabled): ?>
                <li class="nav-item">
                    <a class="nav-link <?php echo Request::is('concierges') || Request::is('concierge-menus') ? 'active' : ''; ?>" href="<?php echo e(route('cms.concierges.index')); ?>">
                        <i class="nav-icon icon-emotsmile"></i> <?php echo e(__('Concierge')); ?>

                        <?php
                            $pendingConcierge = $counterService::getCountPendingConcierge();
                        ?>
                        <?php if($pendingConcierge > 0): ?>
                            <span class="badge badge-info badge-pill" style="position: static;"><?php echo e($pendingConcierge); ?></span>
                        <?php endif; ?>
                    </a>
                </li>
            <?php endif; ?>
            <li class="nav-item">
                <a class="nav-link <?php echo Request::is('settings') ? 'active' : ''; ?>" href="<?php echo e(route('cms.settings.index')); ?>">
                    <i class="nav-icon icon-settings"></i> <?php echo e(__('Settings')); ?>

                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?php echo Request::is('support') ? 'active' : ''; ?>" href="#" id="sendEmailToSupport">
                    <i class="nav-icon icon-support"></i> <?php echo e(__('Support')); ?>

                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="<?php echo e(route('cms.logout.index')); ?>">
                    <i class="nav-icon icon-logout"></i> <?php echo e(__('Logout')); ?>

                </a>
            </li>
        </ul>
    </nav>
</div><?php /**PATH /var/www/servrhotels.com/resources/views/cms/layouts/sidebar.blade.php ENDPATH**/ ?>