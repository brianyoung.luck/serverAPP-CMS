<?php

namespace App\Services;

use App\Models\Spa;
use App\Models\SpaTreatment;
use Illuminate\Database\Eloquent\Builder;
use App\Exceptions\CustomForbiddenException;
use App\Models\HotelBooking;
use App\Models\SpaBooking;
use App\Models\SpaGallery;

use Illuminate\Support\Facades\DB;

class SpaService
{
    public function getSpa($spaId)
    {
        $hotel = auth('hotel')->user();
        if (!$hotel) {
            abort(401);
        }

        $spa = Spa::where('hotel_id', $hotel->id)
            ->where('id', $spaId)
            ->with([
                'treatments' => function ($query) {
                    $query->orderBy('name', 'asc');
                }
            ])
            ->first();

        if (!$spa) {
            abort(404);
        }

        return $spa;
    }

    public function updateLogo($spaId, $file)
    {
        // Upload file
        // $fileStorageService = new FileStorageService(env('FILESYSTEM_DRIVER'));
        // $url = $fileStorageService->uploadFile($file, 'spas', $spaId . '.png');

        // // Update hotel details
        // $spa->logo_url = $url;
        // Spa::where('id', $spaId)->update(['logo_url'=>$url]);
        $fileStorageService = new FileStorageService(env('FILESYSTEM_DRIVER'));
        $ext = $fileStorageService->getExtension($file);
        $directory = 'spas';
        $filename = $spaId . '.' . $ext;
        $url = $fileStorageService->uploadFile($file, $directory, $filename);
        Spa::where('id', $spaId)
                ->update([
                    'logo_url' => $url,
                ]);
    }

    public function updateSpaImages($spaId, $files)
    {
        DB::transaction(function () use ($files, $spaId) {
            $fileStorageService = new FileStorageService(env('FILESYSTEM_DRIVER'));

            // Delete all row
            //RestaurantGallery::where('restaurant_id', $restaurant->id)->delete();

            foreach ($files as $file) {
                // Create filename
                $filename = $spaId . '_' . HelperService::generateRandomString(8) . '.' .$fileStorageService->getExtension($file);

                // Upload file
                $url = $fileStorageService->uploadFile($file, 'spa-galleries', $filename);

                // Insert to db
                SpaGallery::create([
                    'spa_id' => $spaId,
                    'image_url' => $url,
                ]);
            }
        });
    }

    public function updateSpa($spaId, $params)
    {
        // Check logged user
        $hotel = auth('hotel')->user();
        if (!$hotel) {
            abort(401);
        }

        // Get spa
        $spa = Spa::where('id', $spaId)
            ->where('hotel_id', $hotel->id)
            ->first();
        if (!$spa) {
            abort(404);
        }

        // Update spa
        $spa->fill($params);
        $spa->save();
        return $spa;
    }

    public function storeSpaTreatment($body, $image)
    {
        $spaTreatment = new SpaTreatment();
        $url = "";
        if ($image == null) {
            $url = env('CMS_URL').'/images/default.jpg';
        } else {
            $fileStorageService = new FileStorageService(env('FILESYSTEM_DRIVER'));
            $filename = $body['spa_id'] . '_' . HelperService::generateRandomString(8) . '.' .$fileStorageService->getExtension($image);
            // Upload file
            $url = $fileStorageService->uploadFile($image, 'spa-menu-galleries', $filename);
        }
        $spaTreatment->image = $url;
        $spaTreatment->fill($body);
        return $spaTreatment->save();
        // $spaTreatment = SpaTreatment::create($body);
        // return $spaTreatment;
    }

    public function updateSpaTreatment($spaTreatmentId, $params, $image)
    {
        $spaTreatment = SpaTreatment::where('id', $spaTreatmentId)
            ->whereHas('spa', function (Builder $query) {
                $query->where('hotel_id', auth('hotel')->user()->id);
            })
            ->update($params);
        if ($image != null) {
            $fileStorageService = new FileStorageService(env('FILESYSTEM_DRIVER'));
            $filename = $spaTreatmentId . '_' . HelperService::generateRandomString(8) . '.' .$fileStorageService->getExtension($image);
            // Upload file
            $url = $fileStorageService->uploadFile($image, 'spa-menu-galleries', $filename);
            $spaTreatment = SpaTreatment::where('id', $spaTreatmentId)
            ->whereHas('spa', function (Builder $query) {
                $query->where('hotel_id', auth('hotel')->user()->id);
            })
            ->update(['image'=>$url]);
        }

        return $spaTreatment;
    }

    public function destroySpaTreatment($spaTreatmentId)
    {
        $spaTreatment = SpaTreatment::where('id', $spaTreatmentId)
            ->whereHas('spa', function (Builder $query) {
                $query->where('hotel_id', auth('hotel')->user()->id);
            })
            ->delete();

        return true;
    }

    public function createBooking(HotelBooking $hotelBooking, $data)
    {
        return DB::transaction(function () use ($hotelBooking, $data) {
            $spaBooking = SpaBooking::create([
                'hotel_booking_id' => $hotelBooking->id,
                'spa_id' => $data['spa_id'],
                'people_number' => $data['people_number'],
                'type' => $data['booking_type'],
                'booking_date' => $data['booking_date'],
            ]);

            $total_price = 0;

            foreach ($data['treatments'] as $treatmentId) {
                $spaTreatment = SpaTreatment::find($treatmentId);

                if ($spaTreatment->spa->hotel_id !== $hotelBooking->hotel->id) {
                    throw new CustomForbiddenException('Forbidden Access');
                }

                // Sum total
                $total_price += $spaTreatment->price;

                $spaBooking->treatments()->attach($treatmentId, [
                    'price' => $spaTreatment->price,
                ]);
            }

            $spaBooking->total_price = $total_price;
            $spaBooking->save();

            return $spaBooking->load('treatments');
        });
    }

    public function deleteSpaGallery($spaGalleryId)
    {
        SpaGallery::where('id', $spaGalleryId)
            ->delete();
        return true;
    }
}