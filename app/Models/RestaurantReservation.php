<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RestaurantReservation extends Model
{
    protected $fillable = [
        'hotel_booking_id',
        'restaurant_id',
        'people_number',
        'booking_date',
        'status',
    ];

    public function hotelBooking()
    {
        return $this->belongsTo(HotelBooking::class);
    }
}
