<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ __('Register hotel') }} - {{ env('APP_NAME') }}</title>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom-cms.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('horizontal-select/src/ui-choose.css') }}">

    <style media="screen">
        @media (max-width: 768px) {
            .register-hotel-wrapper .container-fluid .content-wrapper .content-left .card.card-text {
                margin-top: 50px;
            }

            .card {
                margin-bottom: 0px;
            }

            .register-hotel-wrapper .container-fluid .content-wrapper .content-left .card.card-text .card-body p {
                font-size: 15px;
            }

            .register-hotel-wrapper .container-fluid .content-wrapper .content-left .card.card-register #registerForm #buttonRegister {
                margin-top: 0px;
            }
        }
    </style>
</head>

<body class="register-hotel-wrapper">
    <div class="container-fluid p-0">
        <div class="header-nav-wrapper">
            <div class="icon-nav">
                <h1>Servr</h1>
            </div>
        </div>
        <div class="content-wrapper">
            <div class="row">
                <div class="col-md-5 content-left">
                    <div class="card card-text">
                        <div class="card-body">
                            <p>
                                Start your trial and join free for a month now <i class="icon-arrow-right icons"></i>
                            </p>
                        </div>
                    </div>

                    <div class="card card-register">
                        <div class="card-body">
                            <form id="registerForm" class="form-horizontal" action="{{ route('cms.paypal.store') }}"
                                method="post">
                                @csrf()
                                @method('post')
                                <input type="hidden" name="hotel_id"
                                    value="{{ old('hotel_id') ? old('hotel_id') : session('hotel_id') }}">
                                {{-- <input type="hidden" name="hotel_id" value="8"> --}}

                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <label class="col-form-label" for="address1">Hotel Address Line 1:</label>
                                        <div class="input-group">
                                            <input
                                                class="form-control {{ $errors->has('address1') ? ' is-invalid' : '' }}"
                                                id="address1" type="text" name="address1"
                                                value="{{ old('address1') ? old('address1') : '' }}">
                                            @if ($errors->has('address1'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('address1') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="col-form-label" for="address2">Hotel Address Line 2:</label>
                                        <div class="input-group">
                                            <input
                                                class="form-control {{ $errors->has('address2') ? ' is-invalid' : '' }}"
                                                id="address2" type="text" name="address2"
                                                value="{{ old('address2') ? old('address2') : '' }}">
                                            @if ($errors->has('address2'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('address2') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <label class="col-form-label" for="post_code">Hotel post code:</label>
                                        <div class="input-group">
                                            <input
                                                class="form-control {{ $errors->has('post_code') ? ' is-invalid' : '' }}"
                                                id="post_code" type="text" name="post_code"
                                                value="{{ old('post_code') ? old('post_code') : '' }}">
                                            @if ($errors->has('post_code'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('post_code') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="col-form-label" for="country">Country:</label>
                                        <div class="input-group">
                                            <input
                                                class="form-control {{ $errors->has('country') ? ' is-invalid' : '' }}"
                                                id="country" type="text" name="country"
                                                value="{{ old('country') ? old('country') : '' }}">
                                            @if ($errors->has('country'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('country') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <label class="col-form-label" for="room">Number of Rooms In The Hotel:</label>
                                        <div class="input-group">
                                            {{-- <input class="form-control {{ $errors->has('code') ? ' is-invalid' : '' }}"
                                            id="code" type="text" name="code" placeholder="To be used by guest to login"
                                            style="text-transform:uppercase"
                                            value="{{ old('code') ? old('code') : '' }}"> --}}
                                            {{-- <select class="ui-choose" id="demo" name="room">
                                          <option value="<20"> < 20 </option>
                                          <option value="20-40">20 - 49</option>
                                          <option value="50-99">50 - 99</option>
                                          <option value="100-299">100 - 299</option>
                                          <option value="300>">300 < </option>
                                        </select> --}}
                                            <select class="form-control room-select" name="room">
                                                <option value="20">
                                                    20 </option>
                                                <option value="20-49">20 - 49
                                                </option>
                                                <option selected value="50-99">50 - 99</option>
                                                <option value="100-199">100 - 199</option>
                                                <option value="200-299">200 - 299</option>
                                                <option value="300+">300 + </option>
                                            </select> @if ($errors->
                                            has('room'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('room') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div align="center" style="width: 100%;">
                                        <p id="fee" style="margin-bottom: 0px">$150.00 per month</p>
                                    </div>
                                    <div class="col-12">
                                        <button class="btn btn-pill btn-block btn-primary buttonRegister"
                                            data-type="paypal" type="button"
                                            id="buttonRegister">{{ __('Submit') }}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-7 content-right">
                    <div class="image-wrapper">
                        <img src="{{ asset('images/register_hotel/laptop_hd.png') }}" alt="device">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
        const url1 = '{!! route('cms.dashboard.index') !!}';
    </script>
    <script src="{{ asset('js/custom/cms/hotel.js') }}"></script>
</body>
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="{{ asset('horizontal-select/src/ui-choose.js') }}"></script>
<script type="text/javascript">
    $('.ui-choose').ui_choose();
</script>
<script type="text/javascript">
    $('.room-select').change(function(event) {
    if (this.value == "20") {
      $('#fee').html('$50.00 per month')
    }else if (this.value == "20-49") {
      $('#fee').html('$80.00 per month')
    }else if (this.value == "50-99") {
      $('#fee').html('$150.00 per month')
    }else if (this.value == "100-199") {
      $('#fee').html('$220.00 per month')
    }
    else if (this.value == "200-299") {
    $('#fee').html('$280.00 per month')
    }else if (this.value == "300+") {
      $('#fee').html('$350.00 per month')
    }
  });
</script>

</html>