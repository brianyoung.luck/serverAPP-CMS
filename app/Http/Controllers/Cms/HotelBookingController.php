<?php

namespace App\Http\Controllers\Cms;

use App\Models\HotelBooking;
use App\Models\PassportPhoto;
use App\Services\HotelBookingService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class HotelBookingController extends Controller
{
    private $hotelBookingService;

    public function __construct(HotelBookingService $hotelBookingService)
    {
        $this->hotelBookingService = $hotelBookingService;
    }

    public function destroy(Request $request)
    {
        $hotelBookingId = $request->get('hotelBookingId', 0);

        PassportPhoto::where('hotel_booking_id', $hotelBookingId)->delete();
        HotelBooking::destroy($hotelBookingId);

        return redirect()->route('cms.guests.index')->with('success', 'Decline new arrival success.');
    }

    public function update(Request $request)
    {
        $hotelBookingId = $request->get('hotelBookingId', 0);
        $roomNumber = $request->get('roomNumber', 'Room -');
        $hotelBooking = HotelBooking::findOrFail($hotelBookingId);

        switch ($request->get('updateType', 'update')) {
            case 'checkout':
                $this->hotelBookingService->update($hotelBooking, [
                    'status' => 'checked_out',
                ]);
                return redirect()->route('cms.guests.index')->with('success', 'Checkout guest success.');
                break;
            case 'lateCheckout':
                $this->hotelBookingService->update($hotelBooking, [
                    'late_check_out' => $request->get('statusLateCheckout'),
                ]);
                return redirect()->route('cms.guests.index')->with('success', 'Late checkout guest success.');
                break;
            default:
                $this->hotelBookingService->update($hotelBooking, [
//                    'reference' => $this->hotelBookingService->setReference($hotelBooking),
                    'status' => 'active',
                    'room_number' => $roomNumber,
                ]);
                return redirect()->route('cms.guests.index')->with('success', 'Accept new arrival success.');
        }
    }
   
    public function ajaxIsRoomNumberNotUsed(Request $request)
    {
        $hotel = auth('hotel')->user();
        $roomNumber = strtoupper($request->get('room_number', ''));
        $hotelBooking = HotelBooking::where('hotel_id', $hotel->id)
            ->where('room_number', $roomNumber)
            ->where('status', 'active')
            ->first();

        return [
            'status' => $hotelBooking ? false : true,
        ];
    }
}
