<?php
/**
 * Created by PhpStorm.
 * User: agussuarya
 * Date: 4/3/19
 * Time: 11:21 AM
 */

namespace App\Services;

use Carbon\Carbon;

class HelperService
{
    /**
     * Generate random number
     *
     * @param int $length
     * @return string
     */
    public static function generateRandomNumber($length = 10)
    {
        if ($length < 1) {
            return 0;
        }
        $dataSource = '1234567890';

        $randomNumber = '';
        $dataSourceLength = strlen($dataSource) - 1; //put the length -1 in cache
        for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $dataSourceLength);
            $randomNumber .= $dataSource[$n];
        }

        return $randomNumber;
    }

    public static function generateRandomString($length = 8)
    {
        if ($length < 1) {
            return 0;
        }
        $dataSource = 'abcdefghijklmnopqrstuvwxyz1234567890';

        $randomNumber = '';
        $dataSourceLength = strlen($dataSource) - 1; //put the length -1 in cache
        for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $dataSourceLength);
            $randomNumber .= $dataSource[$n];
        }

        return $randomNumber;
    }

    /**
     * Thousand separator. E.g 10000, => 10.000
     *
     * @param $number
     * @param int $decimals
     * @param string $dec_point
     * @param string $thousands_sep
     * @return string
     */
    public static function numberFormat($number, $decimals = 0, $dec_point = ',', $thousands_sep = '.')
    {
        if (is_numeric($number)) {
            return number_format($number, $decimals, $dec_point, $thousands_sep);
        } else {
            return $number;
        }
    }

    /**
     * Create datetime format
     *
     * @param $dateTime
     * @param string $format
     * @param null $locale
     * @param string $default
     * @return mixed
     */
    public static function dateTimeFormat($dateTime, $format = 'M d, Y H:i', $locale = null, $default = '')
    {
        if ($locale != null) {
            Carbon::setLocale($locale);
        }

        try {
            return Carbon::parse($dateTime)->format($format);
        } catch (\Exception $err) {
            return $default;
        }
    }
}
