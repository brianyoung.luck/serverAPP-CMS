@extends('admin.layouts.auth')

@section('title', __('Reset Password'))

@section('content')
    <div id="resetPasswordWrapper" class="row justify-content-center">
        <div class="col-xl-5 col-lg-6 col-md-8 col-sm-10">
            <div class="card-group">
                <div class="card p-4">
                    <div class="card-body">
                        <div class="admin-panel-text">
                            <h2>{{ env('APP_NAME') }}</h2>
                            <h3>{{ __('Reset Password') }}</h3>
                        </div>
                        <form id="resetPasswordForm" class="form-horizontal" action="{{ route('cms.reset-password.action') }}" method="post">
                            @csrf()
                            @method('post')
                            <input type="hidden" name="token" value="{{ $token }}">
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <input class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" type="email" name="email" placeholder="Email">
                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <input class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" id="password" type="password" name="password" placeholder="New password">
                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <input class="form-control {{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" id="password_confirmation" type="password" name="password_confirmation" placeholder="New password confirm.">
                                        @if ($errors->has('password_confirmation'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </form>

                        <div class="row">
                            <div class="col-12">
                                <button class="btn btn-pill btn-block btn-primary" type="button" id="buttonReset" disabled>{{ __('Reset password') }}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>

    </script>
    <script src="{{ asset('js/custom/admin/reset-password.js') }}"></script>
@stop