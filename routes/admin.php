<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Guest area
Route::get('', 'Admin\AuthController@loginForm');
Route::get('login', 'Admin\AuthController@loginForm')->name('admin.login.index');
Route::post('login', 'Admin\AuthController@login')->name('admin.do-login');

// Reset  password
Route::get('/forgot-password', 'Admin\ForgotPasswordController@showLinkRequestForm')->name('admin.forgot-password.index');
Route::post('/forgot-password/email', 'Admin\ForgotPasswordController@sendResetLinkEmail')->name('admin.forgot-password.action');
Route::get('/reset-password/{token}', 'Admin\ResetPasswordController@showResetForm')->name('admin.reset-password.index');
Route::post('/reset-password', 'Admin\ResetPasswordController@reset')->name('admin.reset-password.action');
Route::get('/delete/{id}','Admin\AdminAccountController@DeleteStaff')->name('admin.staff.delete');
Route::get('/settings','Admin\SettingController@index')->name('settings.index');
Route::post('/settings','Admin\SettingController@store')->name('settings.store');
// Hotel admin area
Route::group(['middleware' => 'admin'], function () {
    Route::get('logout', 'Admin\AuthController@logout')->name('admin.logout.index');
    Route::get('dashboard', 'Admin\DashboardController@index')->name('admin.dashboard.index');

    Route::resource('hotels', 'Admin\HotelController', [
        'as' => 'admin',
        'only' => ['index', 'show', 'update', 'create', 'store']
    ]);
    Route::get('/deletehotel/{id}','Admin\HotelController@destroy')->name('admin.hotel.destroy');
    Route::resource('hotel-features', 'Admin\HotelFeatureController', [
        'as' => 'admin',
        'only' => ['update']
    ]);

    // Datatables
    Route::group(['prefix' => 'data-tables'], function () {
        Route::get('hotels', 'Admin\HotelController@indexDataTable')->name('data-tables.admin.hotels');
    });

    Route::get('/staff','Admin\AdminAccountController@index')->name('staff.index');
    Route::post('/staff','Admin\AdminAccountController@create')->name('staff.create');

    // Ajax
    Route::group(['prefix' => 'ajax'], function () {
        Route::get('hotel-features', 'Admin\HotelFeatureController@ajaxGetHotelFeatures')->name('ajax.admin.hotel-features.index');
        Route::put('hotel-features/{hotel}', 'Admin\HotelFeatureController@ajaxUpdateHotelFeature')->name('ajax.admin.hotel-features.update');
    });

});

