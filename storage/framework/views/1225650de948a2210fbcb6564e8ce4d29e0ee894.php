<?php $__env->startSection('title', __('Success register hotel')); ?>

<?php $__env->startSection('content'); ?>
    <div id="registerHotelResponseWrapper" class="row justify-content-center">
        <div class="col-xl-6 col-lg-8 col-md-8 col-sm-10">
            <div class="card-group">
                <div class="card p-4">
                    <div class="card-body">
                        <div class="admin-panel-text">
                            <h2><?php echo e(env('APP_NAME')); ?></h2>
                            <h3><?php echo e(__('Register hotel')); ?></h3>
                        </div>

                        <div class="row responseWrapper">
                            <div class="col-12">
                                <h4><?php echo e($response); ?></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('cms.layouts.auth', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/servrhotels.com/resources/views/cms/hotel/response.blade.php ENDPATH**/ ?>