<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Waavi\Sanitizer\Laravel\SanitizesInput;

class CreateHotelRequest extends FormRequest
{
    use SanitizesInput;

    public function filters()
    {
        return [
            'code' => 'capitalize',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required',
            'email' => 'required|email|unique:hotels,email',
            'username' => 'required|unique:hotels,username',
            'password' => 'required|confirmed|min:6',
            'name' => 'required',
            'hotel_logo_lg' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'hotel_logo_md' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'hotel_logo_sm' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'description' => 'required',
            'expired_at' => 'required|date_format:Y-m-d H:i',
        ];
    }
}
