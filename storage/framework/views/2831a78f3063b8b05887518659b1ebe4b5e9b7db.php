<?php $__env->startSection('title', __('Register Hotel')); ?>

<?php $__env->startSection('content'); ?>
    <div id="register_wrapper" class="row justify-content-center">
        <div class="col-xl-5 col-lg-6 col-md-8 col-sm-10">
            <div class="card-group">
                <div class="card p-4">
                    <div class="card-body">
                        <div class="admin-panel-text">
                            <h2><?php echo e(env('APP_NAME')); ?></h2>
                            <h3><?php echo e(__('Register hotel')); ?></h3>
                        </div>
                        <form id="registerForm" class="form-horizontal" action="<?php echo e(route('cms.hotels.store')); ?>" method="post">
                            <?php echo csrf_field(); ?>
                            <?php echo method_field('post'); ?>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <input class="form-control <?php echo e($errors->has('code') ? ' is-invalid' : ''); ?>" id="code" type="text" name="code" placeholder="Code. E.g. LUXURYHOTEL" style="text-transform:uppercase" value="<?php echo e(old('code') ? old('code') : ''); ?>">
                                        <?php if($errors->has('code')): ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('code')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <input class="form-control <?php echo e($errors->has('username') ? ' is-invalid' : ''); ?>" id="username" type="text" name="username" placeholder="Username" style="text-transform:lowercase" value="<?php echo e(old('username') ? old('username') : ''); ?>">
                                        <?php if($errors->has('username')): ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('username')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <input class="form-control <?php echo e($errors->has('name') ? ' is-invalid' : ''); ?>" id="name" type="text" name="name" placeholder="Hotel name" value="<?php echo e(old('name') ? old('name') : ''); ?>">
                                        <?php if($errors->has('name')): ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('name')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <input class="form-control <?php echo e($errors->has('description') ? ' is-invalid' : ''); ?>" id="description" type="text" name="description" placeholder="Hotel description" value="<?php echo e(old('description') ? old('description') : ''); ?>">
                                        <?php if($errors->has('description')): ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('description')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <input class="form-control <?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" id="email" type="email" name="email" placeholder="Email" value="<?php echo e(old('email') ? old('email') : ''); ?>">
                                        <?php if($errors->has('email')): ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('email')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <input class="form-control <?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>" id="password" type="password" name="password" placeholder="Password">
                                        <span class="fa fa-fw fa-eye-slash field-icon toggle-password"></span>
                                        <?php if($errors->has('password')): ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('password')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <input class="form-control <?php echo e($errors->has('password_confirmation') ? ' is-invalid' : ''); ?>" id="password_confirmation" type="password" name="password_confirmation" placeholder="Password confirm.">
                                        <span class="fa fa-fw fa-eye-slash field-icon toggle-password-confirm"></span>
                                        <?php if($errors->has('password_confirm')): ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('password_confirmation')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="row">
                            <div class="col-12">
                                <button class="btn btn-pill btn-block btn-primary" type="button" id="buttonRegister" disabled><?php echo e(__('Register')); ?></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script>
        const url1 = '<?php echo route('cms.dashboard.index'); ?>';
    </script>
    <script src="<?php echo e(asset('js/custom/cms/hotel.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('cms.layouts.auth', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/servrhotels.com/resources/views/cms/hotel/create.blade.php ENDPATH**/ ?>