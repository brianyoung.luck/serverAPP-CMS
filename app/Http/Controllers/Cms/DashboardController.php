<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index()
    {
        $hotel = auth('hotel')->user()->load('feature');
        $hotel =  $hotel->load('restaurants');

        return view('cms.dashboard.index', [
            'hotel' => $hotel,
        ]);
    }
}
