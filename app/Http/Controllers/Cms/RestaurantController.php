<?php

namespace App\Http\Controllers\Cms;

use App\Models\Restaurant;
use App\Models\RestaurantReservation;
use App\Models\RestaurantRoomService;
use App\Services\RestaurantService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Hotel;
use App\Models\RestaurantDishCategory;

class RestaurantController extends Controller
{
    private $restaurantService;

    public function __construct(RestaurantService $restaurantService)
    {
        $this->restaurantService = $restaurantService;
    }

    public function show($restaurantId)
    {
        $currency = Hotel::where('id', auth('hotel')->user()->id)->select('currency')->first()['currency'];
        $restaurant = Restaurant::where('hotel_id', auth('hotel')->user()->id)
            ->where('id', $restaurantId)
            ->with([
                'dishes' => function ($query) {
                    $query->orderBy('name', 'desc');
                }
            ])
            ->first();
        if (!$restaurant) {
            abort(404);
        }
        $categories = RestaurantDishCategory::where('hotel_id', auth('hotel')->user()->id)->get();
        $roomOrderPendings  = RestaurantRoomService::where('restaurant_id', $restaurant->id)
            ->where('status', 'pending')
            ->with([
                'dishes',
                'hotelBooking',
            ])
            ->orderBy('updated_at', 'DESC')
            ->get();
        $roomOrderCurrents = RestaurantRoomService::where('restaurant_id', $restaurant->id)
            ->whereIn('status', ['confirmed', 'preparing', 'on_the_way'])
            ->with([
                'dishes',
                'hotelBooking',
            ])
            ->orderBy('updated_at', 'DESC')
            ->get();
        $roomOrderPasts = RestaurantRoomService::where('restaurant_id', $restaurant->id)
            ->whereIn('status', ['rejected','cancelled','done'])
            ->with([
                'dishes',
                'hotelBooking',
            ])
            ->orderBy('updated_at', 'DESC')
            ->get();

//        $restaurantPendingReservations = RestaurantReservation::where('restaurant_id', $restaurant->id)
//            ->where('status', 'pending')
//            ->with([
//                'hotelBooking',
//            ])
//            ->get();
//        $restaurantPastReservations = RestaurantReservation::where('restaurant_id', $restaurant->id)
//            ->where('status', 'accepted')
//            ->with([
//                'hotelBooking',
//            ])
//            ->get();

        return view('cms.restaurant.show', [
            'restaurant' => $restaurant,
//            'restaurantPendingReservations' => $restaurantPendingReservations,
//            'restaurantPastReservations' => $restaurantPastReservations,

            'roomOrderPendings' => $roomOrderPendings,
            'roomOrderCurrents' => $roomOrderCurrents,
            'roomOrderPasts' => $roomOrderPasts,
            'currency' => $currency,
            'categories' => $categories
        ]);
    }

    public function update(Request $request, $restaurantId)
    {
        // Get data
        $logo = $request->file('logo');
        $restaurantImages = $request->file('restaurantImages');

        $restaurant = Restaurant::where('hotel_id', auth('hotel')->user()->id)
            ->where('id', $restaurantId)
            ->first();
        if (!$restaurant) {
            abort(404);
        }

        if ($logo) {
            // Validate Restaurant Logo
            $request->validate(
                [ 'logo' =>  'nullable|file|mimes:jpg,png,jpeg|max:4096'],
                [
                         'logo.*.mimes' => 'Please upload only png or jpg file.',
                 'logo.max' => 'Image is too large.'
                 ]
            );
            // Update logo
            $this->restaurantService->updateLogo($restaurant, $logo);
        }
        if ($restaurantImages) {
            // Validate Restaurant Image
            $request->validate(
                [ 'restaurantImages.*' =>  'nullable|file|mimes:jpg,png,jpeg|max:4096'],
                [
                    'restaurantImages.*.mimes' =>'Please upload only png or jpg file.',
                 'restaurantImages.*.max' => 'Image is too large.'
                 ]
            );

            $this->restaurantService->updateRestaurantImages($restaurant, $restaurantImages);
        }

        $type = $request->get('updateType');
        switch ($type) {
            case 'changeRestaurantName':
                $this->restaurantService->updateRestaurant($restaurant, [
                    'name' => $request->get('restaurantNameUpdate', ''),
                ]);
                break;
            default:
        }

        return redirect()->route('cms.restaurants.show', ['restaurant' => $restaurant->id])->with('success', 'Update restaurant success.');
    }

    public function store(Request $request)
    {
        $restaurant = $this->restaurantService->createRestaurant(auth('hotel')->user()->id);

        return redirect()->route('cms.restaurants.show', ['restaurant' => $restaurant->id])
            ->with('success', 'Create new restaurant success.');
    }

    public function destroy($restaurantId)
    {
        $restaurant = Restaurant::where('hotel_id', auth('hotel')->user()->id)
            ->where('id', $restaurantId)
            ->first();
        $restaurant->delete();
        return redirect()->route('cms.dashboard.index')->with('success', 'Delete restaurant success');
    }
    public function storeCategory(Request $request)
    {
        $request->validate(['name' => 'required']);
        $name = $request->input('name');
        $hotel_id = auth('hotel')->user()->id;
        $category = new RestaurantDishCategory;
        $category->name = $name;
        $category->hotel_id = $hotel_id;
        $category->save();
        return back();
    }
    public function ajaxDeleteCategory()
    {
        $id = $_GET['id'];
        RestaurantDishCategory::where('id', '=', $id)->delete();
        return  "done";
    }
    public function delete($id)
    {
        Restaurant::where('id', $id)->delete();
        return redirect()->back()
        ->with('success', 'Restaurant deleted successfull');
    }
    public function newRestaurant(Request $request)
    {
        $restaurant = new Restaurant;
        $restaurant->hotel_id = auth('hotel')->user()->id;
        $restaurant->name = $request->input('res_name');
        $restaurant->logo_url = 'https://marketplace.canva.com/MAC5oKacMGY/1/0/thumbnail_large-5/canva-black-with-utensils-icon-restaurant-logo-MAC5oKacMGY.jpg';
        $restaurant->save();
        return redirect()->back()
        ->with('success', 'Restaurant deleted successfull');
    }
}