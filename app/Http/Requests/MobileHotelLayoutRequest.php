<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MobileHotelLayoutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'checkin_image' => 'required',
           'checkout_image' => 'required',
           'restaurant_image' => 'required',
           'spa_image' => 'required',
           'concierge_image' => 'required',
           'experience_image' => 'required',
           'photo_preview_image' => 'required',
           'layout_name' => 'required',
        ];
    }
}