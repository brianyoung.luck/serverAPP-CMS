<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelFeaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotel_features', function (Blueprint $table) {
            $table->unsignedBigInteger('hotel_id');
            $table->boolean('is_check_in_enabled')->default(true);
            $table->boolean('is_check_out_enabled')->default(true);
            $table->boolean('is_spa_enabled')->default(true);
            $table->boolean('is_restaurant_enabled')->default(true);
            $table->boolean('is_concierge_enabled')->default(true);
            $table->boolean('is_cleaning_enabled')->default(true);
            $table->boolean('is_experience')->default(true);
            $table->timestamps();

            // Foreign Key
            $table->foreign('hotel_id')
                ->references('id')
                ->on('hotels')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotel_features');
    }
}