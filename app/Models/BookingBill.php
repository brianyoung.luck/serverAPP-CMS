<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookingBill extends Model
{
    protected $fillable = [
        'hotel_booking_id',
        'hotel_booking_price',
        'total',
    ];

    public function items()
    {
        return $this->hasMany(BookingBillItem::class);
    }
}
