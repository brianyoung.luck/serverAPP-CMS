<?php

namespace App\Http\Controllers\Cms;

use App\Models\Hotel;
use App\Http\Controllers\Controller;

use App\Models\Hotel_Staff;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Hash;

class SettingController extends Controller
{
    public function index()
    {
        $hotel = Hotel::where('id', auth('hotel')->user()->id)
            ->with([
                'hotelFeature'
            ])
            ->first();

        if (!$hotel) {
            abort(404);
        }
        $users = Hotel_Staff::where('hotel_id', auth('hotel')->user()->id)->get();
        $hotel_settings = Hotel::where('id', auth('hotel')->user()->id)->first();
        
        $currency = $hotel_settings['currency'];
        $vat = $hotel_settings['vat'];
        $service_charges = $hotel_settings['service_charges'];

        return view('cms.settings.index', [
            'hotel' => $hotel,
            'users' => $users,
            'currency' => $currency,
            'vat' => $vat,
            'service_charges' => $service_charges

        ]);
    }
    public function save_staff(Request $request)
    {
        $email = $request->input('email');
        $isUnique = Hotel_Staff::where('email', $email)->exists();
        if ($isUnique) {
            return Redirect::back()->withInput()->with('error', 'Email is not unique.');
        }
        $staff_mem = new Hotel_Staff;
        $staff_mem->name = $request->input('name');
        $staff_mem->email = $email;
        $staff_mem->password = $request->input('password');
        $staff_mem->hotel_id = auth('hotel')->user()->id;
        $staff_mem->save();
        return Redirect::back()->with('success', 'Staff member added successfully.');
    }
    public function delete_staff($id)
    {
        Hotel_Staff::where('id', $id)->delete();
        return Redirect::back()->with('success', 'Staff member deleted successfull.');
    }
}