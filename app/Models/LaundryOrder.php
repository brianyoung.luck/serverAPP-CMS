<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LaundryOrder extends Model
{
    protected $fillable = [
        'hotel_booking_id',
        'type',
        'status',
    ];

    protected $attributes = [
        'status' => 'pending',
    ];

    public function hotelBooking()
    {
        return $this->belongsTo(HotelBooking::class);
    }
    public function laundaryServices()
    {
        return $this->belongsToMany(LaundaryService::class)->withPivot(['qty', 'note']);
    }
}