<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantRoomServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_room_services', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('restaurant_id');
            $table->unsignedBigInteger('hotel_booking_id');
            $table->double('total_price', 10, 2)->default(0);
            $table->enum('status', [
                'pending',
                'confirmed',
                'preparing',
                'on_the_way',
                'rejected',
                'cancelled',
                'done',
            ])->default('pending');
            $table->text('signature')->nullable();
            $table->timestamps();

            // Foreign Keys
            $table->foreign('restaurant_id')
                ->references('id')
                ->on('restaurants')
                ->onDelete('restrict');
            $table->foreign('hotel_booking_id')
                ->references('id')
                ->on('hotel_bookings')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant_room_services');
    }
}
