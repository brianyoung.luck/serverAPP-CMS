<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class Hotel_Staff extends Authenticatable
{
    use HasRoles;
	protected $table = "Hotels_Staff";
    protected $fillable = [
    	'name', 'email', 'password'
    ];
    protected $guard_name = 'hotel_staff';
    protected $guard = 'hotel_staff';
}
