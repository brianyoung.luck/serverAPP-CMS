<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\LoginAdminRequest;
use App\Http\Requests\LoginHotelAdminRequest;
use App\Models\Admin;
use App\Http\Controllers\Controller;
use App\Models\Hotel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;

class AuthController extends Controller
{
    public function loginForm()
    {
        if (auth('admin')->check()) {
            return redirect()->route('admin.dashboard.index');
        }
        return view('admin.auth.login.index');
    }

    public function login(LoginAdminRequest $request)
    {

        // check hotel membership expired or not
        $hotels = Hotel::where('expired_at', '<=', Carbon::now())->get();

        foreach ($hotels as $key => $hotel) {
          $hotel->is_active = 0;
          $hotel->save();
        }
        //

        $credentials = [
            'email' => $request->email,
            'password' => $request->password,
        ];

        if (Auth::guard('admin')->attempt($credentials)) {
            return redirect()->route('admin.dashboard.index');
        }

        return Redirect::back()->withInput()->with('error', 'Incorrect credentials!.');
    }

    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect()->route('admin.login.index');
    }

}
