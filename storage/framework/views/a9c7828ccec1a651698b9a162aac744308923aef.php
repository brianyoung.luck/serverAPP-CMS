<?php $__env->startSection('title', __('Staff')); ?>

<?php $__env->startSection('content'); ?>

    <div id="dashboardWrapper">
    	<div class="row">
    		<div class="col-12">
    			<div class="card-header"><?php echo e(__('Staff Members')); ?></div>
                 <div class="card-body">
                    <div class="row">
                        <div class="col-5">
                            <h3>Add New Staff Member</h3>
                            <form method="post" action="<?php echo e(route('staff.create')); ?>">
                                <?php echo csrf_field(); ?>
                                <?php echo method_field('post'); ?>
                                <div class="form-group">
                                    <label for="name">Type</label>
                                    
                                    <select name="name" id="name" class="form-control" required="true">
                                    	<option value="Normal">Normal Admin</option>
                                    	<option value="Super Admin">Super Admin</option>
                                    </select>

                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" name="email" id="email" placeholder="Enter email" class="form-control" required="true" autocomplete="off">

                                </div>
                                <div class="form-group">
                                    <label for="name">Password</label>
                                    <input type="password" name="password" id="password" placeholder="Enter password" class="form-control" required="true" autocomplete="off">
                                </div>
                                <input type="submit" name="submit" value="Save" class="btn btn-info pull-right">
                            </form>
                        </div>
                        <div class="col">
                            <table class="table table-striped">
                                <thead class="thead-light">
                                    <th>Type</th>
                                    <th>Email</th>
                                    <th>Action</th>
                                </thead>
                                <tbody>
                                    <?php $__currentLoopData = $staff; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e($user->name); ?></td>
                                        <td><?php echo e($user->email); ?></td>
                                        <td><a href="<?php echo e(route('admin.staff.delete',['id' => $user->id])); ?>" class="btn btn-sm btn-danger">Delete</a></td>
                                    </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>        
                        </div>
                    </div>
                    
               
    	</div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Syed Hasnain Haider\Desktop\backup\resources\views/admin/staff/index.blade.php ENDPATH**/ ?>