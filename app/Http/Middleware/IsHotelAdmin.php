<?php

namespace App\Http\Middleware;

use Closure;

class IsHotelAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Check if hotel admin logged in or not
        if (!auth('hotel')->check()) {
            return redirect()->route('cms.login.index');
        }

        // Check expiry
        if (!auth('hotel')->user()->is_active) {
            auth('hotel')->logout();
            return redirect()->route('cms.login.index')->with('error', 'Your account is inactive!.');
        }

        return $next($request);
    }
}
