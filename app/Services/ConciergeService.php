<?php

namespace App\Services;

use App\Models\ConciergeService as ConciergeServiceModel;
use App\Models\ConciergeServiceRequest;


class ConciergeService
{
    public function getConciergeServices($hotelId)
    {
        $conciergeServices = ConciergeServiceModel::where('hotel_id', $hotelId)
            ->get();

        return $conciergeServices;
    }

    public function storeConciergeService($data)
    {
        $conciergeService = new ConciergeServiceModel();
        $conciergeService->fill($data);
        return $conciergeService->save();
    }

    public function updateConciergeService(ConciergeServiceModel $conciergeService, $data)
    {
        $conciergeService->fill($data);
        return $conciergeService->save();
    }

    public function deleteConciergeService(ConciergeServiceModel $conciergeService)
    {
        $conciergeService->delete();
        return true;
    }

    public function processConciergeServiceRequest(ConciergeServiceRequest $conciergeServiceRequest, $newStatus)
    {
        if ($newStatus == 'done') {
            // Update status
            $conciergeServiceRequest->status = $newStatus;

            // Send chat to guest @todo

        } else {
            // Update status
            $conciergeServiceRequest->status = 'rejected';

            // Send chat to guest @todo
        }
        return $conciergeServiceRequest->save();
    }
}