@extends('admin.layouts.auth')

@section('title', __('Forgot Password'))

@section('content')
    <div id="forgotPasswordWrapper" class="row justify-content-center">
        <div class="col-xl-5 col-lg-6 col-md-8 col-sm-10">
            <div class="card-group">
                <div class="card p-4">
                    <div class="card-body">
                        <div class="admin-panel-text">
                            <h2>{{ env('APP_NAME') }}</h2>
                            <h3>{{ __('Forgot Password') }}</h3>
                        </div>
                        @include('admin.layouts.flash-message')
                        <form id="forgotPasswordForm" class="form-horizontal" action="{{ route('admin.forgot-password.action') }}" method="post">
                            @csrf()
                            @method('post')
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <input class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" type="email" name="email" placeholder="Email">
                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </form>

                        <div class="row">
                            <div class="col-12">
                                <button class="btn btn-pill btn-block btn-primary" type="button" id="buttonForgot" disabled>{{ __('Send reset link') }}</button>
                            </div>
                        </div>
                        <div id="loginLinkWrapper">
                            <a href="{{ route('admin.login.index') }}">
                                <i class="icons cui-arrow-left"></i>
                                {{ __('Login') }}
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>

    </script>
    <script src="{{ asset('js/custom/admin/forgot-password.js') }}"></script>
@stop