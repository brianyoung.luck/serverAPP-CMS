@extends('cms.layouts.app')

@section('title', __('Guests'))

@section('content')
<style>
    .animated {
        background: gainsboro !important;
        padding: 10px 10px 30px 10px;
        border-radius: 5px;
    }
</style>
<div class="divToPrint m-auto" style="width: 400px;">
    <div class="row text-center">
        {{-- <h1 class="">{{ $hotel['name'] }}</h1> --}}
        <img src="{{ asset('images/logo/servr.png') }}" class="" alt="logo"
            style="margin-left: 35%;width: 31%!important;">
        <hr>
    </div>
    <div class="row mt-2">
        <p>
            Room: <span id="guestCardNumber">{{ $guestDetails->room_number }}</span><br />
            Name: <span id="guestCardholderName">{{ $guestDetails->cardholder_name }}</span><br />
            Date: <span id="guestCardholderName">{{ $guestDetails->created_at->format('d-m-Y') }}</span><br />
        </p>
    </div>
    <div class="row">
        <p class="">Thank you for using Servr, please see below for your order details</p>
    </div>
    <div class="row">
        <table class="table table-responsive-sm">
            <thead>
                <tr>
                    <th>Service</th>
                    <th>From</th>
                    <th>Name</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($bookingBill->items as $item)
                @if ($item->billable_type == "App\Models\RestaurantRoomService")
                @foreach($item->billable->dishes as $dish)
                <tr>

                    @php
                    $item->billable->load('restaurant');
                    $item->billable->load('dishes');
                    @endphp
                    <td>Restaurant Room Service</td>
                    <td>{{$item->billable->restaurant->name}}</td>

                    <td>

                        {{ $dish->name }}
                    </td>
                    <td>{{ $dish->pivot->qty }}</td>
                    <td>{{$dish->pivot->price}}</td>
                    <td>{{$t_price=$dish->pivot->price*$dish->pivot->qty}}</td>
                    <?php
                                                  $total_price += $t_price;
                                            ?>

                </tr>
                @endforeach
                @else
                @foreach($item->billable->treatments as $treatment)
                <tr>
                    @php
                    $item->billable->load('spa');
                    $item->billable->load('treatments');
                    @endphp

                    <td>Spa Service</td>
                    <td>{{$item->billable->spa->name}}</td>
                    <td>
                        {{ $treatment->name }}
                    </td>
                    <td>{{ $treatment->pivot->qty }}</td>
                    <td>{{$treatment->pivot->price}}</td>
                    <td>{{$t_price=$treatment->pivot->price}}</td>
                    <?php
                                                  $total_price += $t_price;
                                            ?>
                    @endforeach
                    @endif
                    {{-- <td>1</td>
                    <td>{{$item->billable->total_price}}</td>
                    <td>{{$item->billable->total_price}}</td> --}}
                </tr>
                @endforeach
        </table>
    </div>
    <hr>
    <div class="row mt-2">
        <div class="col-4">
            VAT: {{ number_format((float)$total_price*$vat/100, 0, '.', '') }} ({{ $vat }}%)
        </div>
        <div class="col-4">
            Service Charges: {{ number_format((float)$total_price*$service_charges/100, 0, '.', '') }}
            ({{ $service_charges }}%)
        </div>
        <div class="col-4">
            Total price:
            {{ number_format((float)$total_price+($total_price*$service_charges/100+$total_price*$vat/100), 0, '.', '') }}
        </div>
    </div>
    <p class="text-center mt-2">www.servrhotels.com</p>
</div>
@stop
@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/printThis/1.15.0/printThis.min.js"></script>
<script>
</script>
<script>
    $(document).ready(function(){
   window.print();
var type='{{ $type }}';
   window.onafterprint = function (event) {
       if(type=='guests'){
window.location = '{{url('guests')}}';
       }else{
         window.location = '{{url('checkouts')}}';  
       }
};

window.close = function (event) {
    if(type=='guests'){
window.location = '{{url('guests')}}';
    }else{
        window.location = '{{url('checkouts')}}';
    }
};
});
</script>
@stop