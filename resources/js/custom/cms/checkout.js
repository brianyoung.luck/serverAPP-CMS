import SignaturePad from "signature_pad";
const guestTable = $("#guestTable");
const optionsDateTime = {
  year: "numeric",
  month: "short",
  day: "numeric",
  hour: "2-digit",
  minute: "2-digit",
};
const deleteHotelBookingForm = $("#deleteHotelBookingForm");
const hotelBookingId = $("#hotelBookingId");
const hotelBookingIdUpdate = $("#hotelBookingIdUpdate");
const editHotelBookingForm = $("#editHotelBookingForm");
const guestProfileModal = $("#guestProfileModal");
const DishesListModal = $("#DishesListModal");

const guestName = $("#guestName");
const guestReference = $("#guestReference");
const guestArrivalDate = $("#guestArrivalDate");
const guestDepartureDate = $("#guestDepartureDate");
const guestCardExpiryDate = $("#guestCardExpiryDate");
const guestCardholderName = $("#guestCardholderName");
const guestCardAddress = $("#guestCardAddress");
const guestPassportEntriesWrapper = $("#guestPassportEntriesWrapper");

const declineBtn = $(".declineBtn");
const acceptBtn = $(".acceptBtn");
const checkoutBtn = $(".checkoutBtn");

const checkoutHotelBookingForm = $("#checkoutHotelBookingForm");
const hotelBookingIdCheckout = $("#hotelBookingIdCheckout");
const roomNumber = $("#roomNumber");
const lateCheckoutHotelBookingForm = $("#lateCheckoutHotelBookingForm");
const hotelBookingIdLateCheckout = $("#hotelBookingIdLateCheckout");
const statusLateCheckout = $("#statusLateCheckout");
const lateCheckoutBtn = $(".lateCheckoutBtn");
const guestCardNumber = $("#guestCardNumber");
const changeCheckoutTimeBtn = $("#changeCheckoutTimeBtn");
const changeLateCheckoutTimeBtn = $("#changeLateCheckoutTimeBtn");
const checkoutTime = $("#checkout_time");
const lateCheckoutTime = $("#late_checkout_time");
const editHotelDetailsForm = $("#editHotelDetailsForm");
var canvas = document.querySelector("canvas");
var clearSig = $("#clearSig");

$(document).ready(function() {
  var signaturePad = new SignaturePad(canvas);
  clearSig.click(function() {
    signaturePad.clear();
  });
  guestTable.DataTable({
    lengthMenu: [[10, 25, 50, -1], ["10", "25", "50", "All"]],
    order: [[0, "desc"]],
    language: {
      searchPlaceholder: "Name/booking ref",
    },
    processing: true,
    serverSide: true,
    ajax: {
      url: url1,
      data: function(d) {
        d.status = "all";
      },
    },
    columns: [
      {
        data: "cardholder_name",
        name: "cardholder_name",
        render: function(data) {
          return data;
        },
      },
      {
        data: "reference",
        name: "reference",
        render: function(data) {
          return data;
        },
      },
      {
        data: "room_number",
        name: "room_number",
        render: function(data) {
          return data;
        },
      },
      {
        data: "arrival_date",
        name: "arrival_date",
        render: function(data) {
          return new Date(data).toLocaleDateString("en-US", optionsDateTime);
        },
      },
      {
        data: "departure_date",
        name: "departure_date",
        render: function(data) {
          return new Date(data).toLocaleDateString("en-US", optionsDateTime);
        },
      },
      {
        data: "status",
        name: "status",
        render: function(data) {
          if (data === "active") {
            return `<span class="badge badge-success">Active</span>`;
          } else {
            return `<span class="badge badge-warning">Checked out</span>`;
          }
        },
      },
      {
        data: "id",
        name: "id",
        orderable: false,
        render: function(data) {
          return `
                        <a class="btn-action-table" data-id="${data}"><i class="icon-info icons" ></i></a>
                    `;
        },
      },
    ],
  });
  $(document).on("click", ".checkoutBtn", function() {
    checkout($(this).data("id"));
  });
  $(document).on("click", ".lateCheckoutBtn", function() {
    lateCheckout($(this).data("id"));
  });
  $(document).on("click", ".btn-action-table", async function() {
    await showModalGuestDetails($(this).data("id"));
  });

  $(document).on("mouseover", ".price", function() {
    $(this).popover("show");
  });
  // $(document).on("click", ".icon-more", async function() {
  //   $(this).popover("show");
  // console.log($(".btn-action-table").attr("data-id"));
  // const guestDetails = await getGuestDetails(hotelBookingId);
  // const bookingBill = guestDetails.bookingBill;
  // console.log(guestDetails);
  // });

  // $(document).on("click", ".icon-more", function() {
  //   $(".exampleModal").modal("show");
  // });

  // Append modal to body
  guestProfileModal.appendTo("body");
  guestProfileModal.on("hidden.bs.modal", function(e) {
    // Change guest details to loading
    resetGuestDetails();
  });

  changeCheckoutTimeBtn.click(function() {
    changeCheckoutTime();
  });
  changeLateCheckoutTimeBtn.click(function() {
    changeLateCheckoutTime();
  });
});

function accept(id) {
  Swal.fire({
    title: "Are you sure?",
    type: "question",
    showCancelButton: true,
    confirmButtonColor: success_button_color,
    cancelButtonColor: cancel_button_color,
    confirmButtonText: "Accept",
    reverseButtons: true,
    input: "text",
    inputPlaceholder: "Enter room number",
    inputValue: "",
    inputValidator: (value) => {
      if (!value) {
        return "You need to set room number.";
      }
    },
  }).then((result) => {
    if (result.value) {
      roomNumber.val(result.value);
      hotelBookingIdUpdate.val(id);
      editHotelBookingForm.submit();
    }
  });
}

async function getGuestDetails(hotelBookingId) {
  let promise = new Promise(function(resolve, reject) {
    $.ajax({
      headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
      },
      type: "GET",
      url: url4 + "/" + hotelBookingId,
      contentType: "application/json",
      dataType: "json",
    })
      .done(function(response) {
        resolve(response);
      })
      .fail(function(error) {
        console.log(error);
        reject();
      });
  });

  try {
    return await promise;
  } catch (err) {
    return {
      status: false,
      error: err,
    };
  }
}

function resetGuestDetails() {
  guestName.html(`<i class="fa fa-refresh fa-spin fa-fw"></i>`);
  guestReference.html(`<i class="fa fa-refresh fa-spin fa-fw"></i>`);
  guestArrivalDate.html(`<i class="fa fa-refresh fa-spin fa-fw"></i>`);
  guestDepartureDate.html(`<i class="fa fa-refresh fa-spin fa-fw"></i>`);
  guestCardExpiryDate.html(`<i class="fa fa-refresh fa-spin fa-fw"></i>`);
  guestCardholderName.html(`<i class="fa fa-refresh fa-spin fa-fw"></i>`);
  guestCardAddress.html(`<i class="fa fa-refresh fa-spin fa-fw"></i>`);

  guestPassportEntriesWrapper.html(``);
}

async function showModalGuestDetails(hotelBookingId) {
  // Show modal
  guestProfileModal.modal("show");

  // Call ajax to get guest details
  const guestDetails = await getGuestDetails(hotelBookingId);
  console.log("Guest Details: ", guestDetails);
  // Show guest details
  fillGuestDetails(guestDetails.guestDetails);

  // Show bill details
  $(".bookingBillWrapper").html("");
  fillBillsDetails(
    guestDetails.guestDetails.room_number,
    guestDetails.bookingBill,
    guestDetails.vat,
    guestDetails.service_charges
  );
  // if (guestDetails.guestDetails.status === 'checked_out') {
  //     fillBillsDetails(guestDetails.bookingBill);
  // }
}

function fillGuestDetails(guestDetails) {
  guestName.html(guestDetails.cardholder_name);
  guestReference.html(guestDetails.reference);
  guestArrivalDate.html(dateTimeFormat(guestDetails.arrival_date));
  guestDepartureDate.html(dateTimeFormat(guestDetails.departure_date));
  guestCardExpiryDate.html(
    dateTimeFormat(guestDetails.card_expiry_date, "card_expiry_date")
  );
  guestCardholderName.html(guestDetails.cardholder_name);
  guestCardAddress.html(guestDetails.card_address);
  guestCardNumber.html(guestDetails.card_number);

  guestPassportEntriesWrapper.html(``);
  for (const passport_photo of guestDetails.passport_photos) {
    guestPassportEntriesWrapper.append(`
            <div class="passportEntryWrapper">
                <a href="${
                  passport_photo.url
                }" target="_blank" class="copyImage">
                    <img src="${passport_photo.url}" alt="Passport photo">
                </a>
            </div>
        `);
  }
  checkoutBtn.attr("data-id", guestDetails.id);
  lateCheckoutBtn.attr("data-id", guestDetails.id);

  if (guestDetails.status === "active") {
    checkoutBtn.addClass("d-inline");
    checkoutBtn.removeClass("d-none");
    if (guestDetails.late_check_out === "pending") {
      lateCheckoutBtn.addClass("d-inline");
      lateCheckoutBtn.removeClass("d-none");
    } else {
      lateCheckoutBtn.addClass("d-none");
      lateCheckoutBtn.removeClass("d-inline");
    }
  } else {
    checkoutBtn.removeClass("d-inline");
    checkoutBtn.addClass("d-none");
    lateCheckoutBtn.addClass("d-none");
    lateCheckoutBtn.removeClass("d-inline");
  }
}

function fillBillsDetails(room_number, bookingBill, vat, service_charges) {
  $(".roomNumberText").html("");
  $(".roomNumberText").html("Room No : " + room_number);
  if (bookingBill === null) {
    return;
  }

  $(".bookingBillWrapper").html(`
       
        <div class="row">
        <div class="col-12">
        <div class="row">
        <div class="col-md-8">

           <h4>Bill details</h4>
          </div>
          <div class="col-md-2 col-sm-6 col-xs-6 print-col">
              <button href="" class="btn h-booking-id-print btn-pill btn-info btn-block" data-hotel-booking-id="0">Print</button>
          </div>
          <div class="col-md-2 col-sm-6 col-xs-6 email-row email-col">
              <button href="" class="btn btn-pill h-booking-id-email btn-info btn-block" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Email</button>
              </div>
              <div class="collapse email-card" id="collapseExample">
  <div class="card card-body email-card-body shadow-lg ">
  <p class="msg text-success text-center"></p>
    <input type="email" required class="form-control mb-1 billEmail" name="billEmail" id="billEmail" placeholder="Enter email">
    <input type="submit" class="btn btn-info btn-snd-bill" value="Send" >
  </div>
</div>
        <table class="table table-responsive-sm table-striped bookingBillTable">
            <thead>
            <tr>
                <th>Service</th>
                <th>From</th>
                <th>Name</th>
                <th>Price</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        </div>
        <h5 class="totalVatPrice w-100 text-right">VAT:  123</h5>
        <h5 class="totalServiceCharges w-100 text-right">Service Charges: 123</h5>
        <h5 class="totalBillPrice w-100 text-right">Total price 123</h5>
    `);
  const bookingBillTableTbody = $(".bookingBillTable tbody");
  var i = 0;

  for (const item of bookingBill.items) {
    $(".h-booking-id-email").attr(
      "data-hotel-booking-id",
      item.billable.hotel_booking_id
    );

    $(".h-booking-id-print").attr(
      "data-hotel-booking-id",
      item.billable.hotel_booking_id
    );
    $("#getHotelId").attr("data-id", item.billable.hotel_booking_id);
    if (item.billable_type === "App\\Models\\RestaurantRoomService") {
      bookingBillTableTbody.append(`
                <tr  data-toggle="collapse" onclick=openItemPopUp(this) data-target="#restaurantRoomService${
                  item.id
                }">
                    <td>Restaurant room service</td>
                    <td>${item.billable.restaurant.name}</td>
                      <td class="dish_name">
                      ${item.billable.dishes[0].name} 
                   
                      <i class="icon-more icons text-success text-bold" data-dish='${
                        item.billable.dishes
                      }' >${item.billable.dishes.length} More.. 
      </i></td>
                    <td class="price"  data-id='${item.billable.id}'
      data-incr='${i + 1}'
                     data-hotel_booking_id='${
                       item.billable.hotel_booking_id
                     }'>${
        item.billable.total_price
      } <i class="icon-info icons text-success" ></i></td>
      </tr>
      <tr>
      <td colspan="12" class="p-0 ">
          <div id="restaurantRoomService${
            item.id
          }" class="collapse  icon-inner billdetailtr">
            <div class="row ">
              <div class="col-md-6">
                <h5>Name</h5> 
              </div>
              <div class="col-md-6">
                  <h5>Price</h5> 
              </div>
            </div>
            <div class="row itemDeatil${item.id}">
            </div>
            <div class="row">
              <div class="col-md-8">
             
              </div>
              <div class="col-md-4">
               <h5 class='signhead'>Customer Signature</h5>
                   <div class='signature'>${
                     item.billable.signature
                       ? '<img src="' + item.billable.signature + '" />'
                       : ""
                   }</div>
              </div>
            </div>
        </div>
      </td>
      </tr>
      
            `);
    } else if (item.billable_type === "App\\Models\\SpaBooking") {
      bookingBillTableTbody.append(`
                <tr data-toggle="collapse" onclick=openItemPopUp(this) data-target="#spaRoomService${
                  item.id
                }">
                    <td>Spa service</td>
                    <td>${item.billable.spa.name}</td>
                               <td class="dish_name">
                      ${
                        item.billable.treatments != ""
                          ? item.billable.treatments[0].name
                          : ""
                      }  
                      <i class="icon-more icons text-success text-bold" data-dish='${
                        item.billable.treatments
                      }'  data-trigger="manual">${
        item.billable.treatments.length
      } More.. 
      </i>  
   </td>
                <td class="price"  data-id='${item.billable.id}'
      data-incr='${i + 1}'
                     data-hotel_booking_id='${
                       item.billable.hotel_booking_id
                     }'>${
        item.billable.total_price
      } <i class="icon-info icons text-success" ></i>
      </td>
                </tr>
                <tr>
                    <td colspan="12" class="p-0">
          <div id="spaRoomService${
            item.id
          }" class="collapse  icon-inner billdetailtr">
            <div class="row ">
              <div class="col-md-6">
                <h5>Name</h5> 
              </div>
              <div class="col-md-6">
                  <h5>Price</h5> 
              </div>
            </div>
            <div class="row itemDeatil${item.id}">
            </div>
            <div class="row">
              <div class="col-md-8">
             
              </div>
              <div class="col-md-4">
               <h5 class='signhead'>Customer Signature</h5>
                   <div class='signature'>${
                     item.billable.signature
                       ? '<img src="' + item.billable.signature + '" />'
                       : ""
                   }</div>
              </div>
            </div>
        </div>
      </td>

                
                </tr>
            `);
    }
    i++;
  }
  // for (const item of bookingBill.items) {
  //   for (const dish of item.billable.dishes) {
  //     $(".dish_name").html(dish.name);
  //   }
  // }
  // debugger;
  const totalBillPrice = $(".totalBillPrice");
  const totalVatPrice = $(".totalVatPrice");
  const totalServiceCharges = $(".totalServiceCharges");

  let totalVat = bookingBill.total * (vat / 100);
  let totalServiceChargesPrice = bookingBill.total * (service_charges / 100);
  totalVatPrice.html("Total VAT: " + Math.round(totalVat) + " ( " + vat + "%)");
  totalServiceCharges.html(
    "Total Service Charges: " +
      Math.round(totalServiceChargesPrice) +
      " ( " +
      service_charges +
      "%)"
  );
  totalBillPrice.html(
    `Total price ${Math.round(
      bookingBill.total + totalVat + totalServiceChargesPrice
    )}`
  );
}

function checkout(id) {
  Swal.fire({
    title: "Are you sure?",
    type: "question",
    showCancelButton: true,
    confirmButtonColor: success_button_color,
    cancelButtonColor: cancel_button_color,
    confirmButtonText: "Checkout",
    reverseButtons: true,
  }).then((result) => {
    if (result.value && result.value === true) {
      hotelBookingIdCheckout.val(id);
      checkoutHotelBookingForm.submit();
    }
  });
}

function lateCheckout(id) {
  Swal.fire({
    title: "Approve late checkout?",
    type: "question",
    showCancelButton: true,
    confirmButtonColor: success_button_color,
    cancelButtonColor: cancel_button_color,
    confirmButtonText: "Accept",
    cancelButtonText: "Reject",
    reverseButtons: true,
  }).then((result) => {
    if (result.dismiss && result.dismiss === "cancel") {
      // Reject
      hotelBookingIdLateCheckout.val(id);
      statusLateCheckout.val("rejected");
      lateCheckoutHotelBookingForm.submit();
    } else if (result.value && result.value === true) {
      // Accept
      hotelBookingIdLateCheckout.val(id);
      statusLateCheckout.val("accepted");
      lateCheckoutHotelBookingForm.submit();
    }
  });
}

function changeCheckoutTime() {
  Swal.fire({
    title: "Choose checkout time",
    type: "warning",
    html: `<input type="time" id="checkoutTime" class="swal2-input" placeholder="Choose time" value="${val1}">`,
    showCancelButton: true,
    confirmButtonColor: primary_color,
    cancelButtonColor: cancel_button_color,
    confirmButtonText: "Save",
    reverseButtons: true,
    preConfirm: function() {
      return new Promise(function(resolve) {
        resolve([document.getElementById("checkoutTime").value]);
      });
    },
  }).then((result) => {
    if (result.value) {
      checkoutTime.val(result.value[0]);
      editHotelDetailsForm.submit();
    }
  });
}

function changeLateCheckoutTime() {
  Swal.fire({
    title: "Choose late checkout time",
    type: "warning",
    html: `<input type="time" id="lateCheckoutTime" class="swal2-input" placeholder="Choose time" value="${val2}">`,
    showCancelButton: true,
    confirmButtonColor: primary_color,
    cancelButtonColor: cancel_button_color,
    confirmButtonText: "Save",
    reverseButtons: true,
    preConfirm: function() {
      return new Promise(function(resolve) {
        resolve([document.getElementById("lateCheckoutTime").value]);
      });
    },
  }).then((result) => {
    if (result.value) {
      lateCheckoutTime.val(result.value[0]);
      editHotelDetailsForm.submit();
    }
  });
}

$(document).on("click", ".btn-snd-bill", function(e) {
  e.preventDefault();
  $(".msg").html("Sending...");

  var hotel_booking_id = $(".h-booking-id-email").attr("data-hotel-booking-id");
  var email = $(".billEmail").val();
  if (email == "") {
    $(".msg").html("Enter email please");
    return false;
  }
  $.ajax({
    url: "ajax/guests/mailbill/" + hotel_booking_id + "/" + email,
    type: "get", //send it through get method
    data: {},
    success: function(response) {
      $(".msg").html("Sent");
      $(".billEmail").val("");
      $("#collapseExample").collapse("hide");
    },
    error: function(xhr) {
      $(".msg").html("Email not sent ,please try again");
    },
  });
});

// Print
$(document).on("click", ".h-booking-id-print", function() {
  var url = printUrl;
  var hotel_booking_id = $(".h-booking-id-email").attr("data-hotel-booking-id");
  window.location = url + "/" + hotel_booking_id + "/" + redirectType;
});

$(document).on("click", ".copyText", function() {
  let element = document.getElementById($(this).attr("id")); //select the element
  let elementText = element.textContent; //get the text content from the element
  // copyText(elementText);
  navigator.clipboard.writeText(elementText);
  $(".msgText").html("Text copied to clipboard.");
});

$(document).on("mouseover", ".copyImage", function(e) {
  var copyUrl = $(".copyImage img").attr("src");
  navigator.clipboard.writeText(copyUrl);
  console.log(copyUrl);
  $(".msgText").html("Image url copied to clipboard.");
});
function SelectText(element) {
  var doc = document;
  if (doc.body.createTextRange) {
    var range = document.body.createTextRange();
    range.moveToElementText(element);
    range.select();
  } else if (window.getSelection) {
    var selection = window.getSelection();
    var range = document.createRange();
    range.selectNodeContents(element);
    selection.removeAllRanges();
    selection.addRange(range);
  }
}
$(document).on("mouseover", ".copyText", function() {
  $(".msgText").html("Click on text to copy.");
});
