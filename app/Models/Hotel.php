<?php

namespace App\Models;

use App\Notifications\HotelResetPasswordNotification;
use App\Services\FileStorageService;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hotel extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    protected $fillable = [
        'code',
        'email',
        'username',
        'password',
        'name',
        'is_active',
        'hotel_logo_ori',
        'hotel_logo_lg',
        'hotel_logo_md',
        'hotel_logo_sm',
        'mobile_hotel_theme_id',
        'mobile_hotel_layout_id',
        'address1',
        'address2',
        'post_code',
        'country',
        'room',
        'description',
        'sendbird_user_id',
        'sendbird_access_token',
        'checkout_time',
        'late_checkout_time',
    ];

    public function mobileLayout()
    {
        return $this->hasOne(MobileLayout::class);
    }

    public function mobileHotelTheme()
    {
        return $this->belongsTo(MobileHotelTheme::class);
    }
    public function mobileHotelLayout()
    {
        return $this->belongsTo(MobileHotelLayout::class);
    }

    public function hotelFeature()
    {
        return $this->hasOne(HotelFeature::class);
    }

    public function feature()
    {
        return $this->hasOne(HotelFeature::class, 'hotel_id', 'id');
    }

    public function restaurants()
    {
        return $this->hasMany(Restaurant::class, 'hotel_id', 'id');
    }

    public function spas()
    {
        return $this->hasMany(Spa::class, 'hotel_id', 'id');
    }

    public function conciergeServices()
    {
        return $this->hasMany(ConciergeService::class);
    }

    //Send password reset notification
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new HotelResetPasswordNotification($token));
    }

    /**
     * Getter
     */

    public function getHotelLogoOriAttribute($value)
    {
        return $this->getLogoUrl($value);
    }

    public function getHotelLogoLgAttribute($value)
    {
        return $this->getLogoUrl($value);
    }

    public function getHotelLogoMdAttribute($value)
    {
        return $this->getLogoUrl($value);
    }

    public function getHotelLogoSmAttribute($value)
    {
        return $this->getLogoUrl($value);
    }

    /**
     * Create presign url
     * @param $value
     * @return string|null
     */
    private function getLogoUrl($value)
    {
        $fileStorageService = new FileStorageService(env('FILESYSTEM_DRIVER_SERVR_FILES'));
        $storageServiceName = $fileStorageService->getStorageServiceName($value);
        switch ($storageServiceName) {
            case 's3':
                return $fileStorageService->getTemporaryUrl($value);
                break;
            default:
                return $value;
        }
    }
}