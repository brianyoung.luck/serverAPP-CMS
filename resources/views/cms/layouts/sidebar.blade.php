@php
$user = auth('hotel_staff')->user();
@endphp
<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            @inject('counterService', 'App\Services\CounterService')
            <li class="nav-item">
                <a class="nav-link {!! Request::is('dashboard') || Request::is('') ? 'active' : '' !!}"
                    href="{{ route('cms.dashboard.index') }}">
                    <i class="nav-icon icon-home"></i> {{ __('Home') }}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {!! Request::is('appearance') ? 'active' : '' !!}"
                    href="{{ route('cms.appearances.index') }}">
                    <i class="nav-icon icon-eyeglass"></i> {{ __('Appearance') }}
                </a>
            </li>
            @if (auth('hotel')->user()->feature->is_restaurant_enabled && (!$user || $user->can('restaurant')))
            <li class="nav-item nav-dropdown {!! Request::is('restaurants*') ? 'open' : '' !!}">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="nav-icon icon-cup"></i> {{ __('Restaurant') }}
                    @php
                    $pendingRestaurants = $counterService::getCountPendingRestaurant();
                    @endphp
                    @if ($pendingRestaurants > 0)
                    <span class="badge badge-info badge-pill" style="position: static;">{{ $pendingRestaurants }}</span>
                    @endif
                </a>
                <ul class="nav-dropdown-items">
                    @foreach(auth('hotel')->user()->restaurants as $index => $restaurant)
                    <li class="nav-item">
                        <a class="nav-link {!! Request::is('restaurants/' . $restaurant->id . '') ? 'active' : '' !!}"
                            href="{{ route('cms.restaurants.show', ['restaurant' => $restaurant->id]) }}">
                            <i class="nav-icon icon-arrow-right"></i> {{ __('Restaurant ') }}{{ ($index + 1) }}
                            @php
                            $pendingRestaurant = $counterService::getCountPendingRestaurant($restaurant->id);
                            @endphp
                            @if ($pendingRestaurant > 0)
                            <span class="badge badge-info badge-pill"
                                style="position: static;">{{ $pendingRestaurant }}</span>
                            @endif
                        </a>
                    </li>
                    @endforeach
                    @if (count(auth('hotel')->user()->restaurants) < 3) <form id="createRestaurantForm" method="post"
                        action="{{ route('cms.restaurants.store') }}" enctype="multipart/form-data">
                        @csrf()
                        @method('post')
                        </form>
                        <li class="nav-item">
                            <a class="nav-link" href="#" id="createNewRestaurantSidebarBtn">
                                <i class="nav-icon icon-arrow-right"></i> {{ __('Create new') }}
                            </a>
                        </li>
                        @endif
                </ul>
            </li>
            @endif
            @if (auth('hotel')->user()->feature->is_spa_enabled && (!$user || $user->can('spa')))

            <li class="nav-item">
                @php
                $spa = auth('hotel')->user()->spas->first();
                @endphp
                <a class="nav-link {!! Request::is('spas*') ? 'active' : '' !!}"
                    href="{{ route('cms.spas.show', ['spa' => $spa->id]) }}">
                    <i class="nav-icon icon-heart"></i> {{ __('Spa') }}
                    @php
                    $pendingSpa = $counterService::getCountPendingSpa($spa->id);
                    @endphp
                    @if ($pendingSpa > 0)
                    <span class="badge badge-info badge-pill" style="position: static;">{{ $pendingSpa }}</span>
                    @endif
                </a>
            </li>
            @endif
            @if (auth('hotel')->user()->feature->is_check_in_enabled && (!$user || $user->can('checkin_in')))
            <li class="nav-item">
                <a class="nav-link {!! Request::is('guests') ? 'active' : '' !!}"
                    href="{{ route('cms.guests.index') }}">
                    <i class="nav-icon icon-user-follow"></i> {{ __('Guests') }}
                    @php
                    $guestCount = $counterService::getCountGuest();
                    @endphp
                    @if ($guestCount > 0)
                    <span class="badge badge-info badge-pill" style="position: static;">{{ $guestCount }}</span>
                    @endif
                </a>
            </li>
            @endif
            @if (auth('hotel')->user()->feature->is_experience &&(!$user || $user->can('experience')))
            <li class="nav-item">
                <a class="nav-link {!! Request::is('experience') ? 'active' : '' !!}"
                    href="{{ route('cms.experience.index') }}">
                    <i class="nav-icon icon-direction"></i> {{ __('Experience') }}

                </a>
            </li>
            @endif
            @if (auth('hotel')->user()->feature->is_concierge_enabled && (!$user || $user->can('concierge')))
            <li class="nav-item">
                <a class="nav-link {!! Request::is('concierges') || Request::is('concierge-menus') ? 'active' : '' !!}"
                    href="{{ route('cms.concierges.index') }}">
                    <i class="nav-icon icon-emotsmile"></i> {{ __('Concierge') }}
                    @php
                    $pendingConcierge = $counterService::getCountPendingConcierge();
                    @endphp
                    @if ($pendingConcierge > 0)
                    <span class="badge badge-info badge-pill" style="position: static;">{{ $pendingConcierge }}</span>
                    @endif
                </a>
            </li>
            @endif
            @if ((!$user || $user->can('settings')))
            <li class="nav-item">
                <a class="nav-link {!! Request::is('settings') ? 'active' : '' !!}"
                    href="{{ route('cms.settings.index') }}">
                    <i class="nav-icon icon-settings"></i> {{ __('Settings') }}
                </a>
            </li>
            @endif
            <li class="nav-item">
                <a class="nav-link {!! Request::is('support') ? 'active' : '' !!}" href="#" id="sendEmailToSupport">
                    <i class="nav-icon icon-support"></i> {{ __('Support') }}
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{ route('cms.logout.index') }}">
                    <i class="nav-icon icon-logout"></i> {{ __('Logout') }}
                </a>
            </li>
        </ul>
    </nav>
</div>