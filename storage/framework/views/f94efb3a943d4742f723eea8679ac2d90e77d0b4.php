<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <meta name="description" content="<?php echo e(env('APP_DESCRIPTION')); ?>">
    <meta name="author" content="<?php echo e(env('APP_AUTHOR')); ?>">
    <meta name="keyword" content="<?php echo e(env('APP_KEYWORD')); ?>">
    <title><?php echo $__env->yieldContent('title'); ?> - <?php echo e(env('APP_NAME')); ?></title>

    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/custom-cms.css')); ?>" rel="stylesheet">
</head>
<body class="app flex-row align-items-center auth-body">
    <div class="container">
        <?php echo $__env->yieldContent('content'); ?>
    </div>
    <script src="<?php echo e(asset('js/app.js')); ?>"></script>
    <?php echo $__env->yieldContent('js'); ?>
</body>
</html><?php /**PATH C:\Users\Syed Hasnain Haider\Desktop\backup\resources\views/cms/layouts/auth.blade.php ENDPATH**/ ?>