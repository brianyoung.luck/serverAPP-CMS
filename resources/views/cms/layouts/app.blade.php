<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="{{ env('APP_DESCRIPTION') }}">
    <meta name="author" content="{{ env('APP_AUTHOR') }}">
    <meta name="keyword" content="{{ env('APP_KEYWORD') }}">
    <title>@yield('title') - {{ env('APP_NAME') }}</title>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom-cms.css') }}" rel="stylesheet">
    <link rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
    <style>
        .messageAlert {
            padding-top: 11px;
            padding-bottom: 0px;
        }
    </style>
      @yield('styles')
</head>

<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
    @include('cms.layouts.header')
    <div class="app-body">
        @include('cms.layouts.sidebar')
        <main class="main">
            @yield('breadcrumb')
            <div class="container-fluid">
                @include('cms.layouts.flash-message')
                <div class="animated fadeIn">
                    <div class="top-action-wrapper">
                        @yield('top-action')
                    </div>
                    @yield('content')
                </div>
            </div>
            <div class="chatsWrapper">
                <div class="chatGroupWrapper">
                    <span class="circle compose" id="compose" data-toggle="modal" data-target="#chatModal">
                        <div class="vertical"></div>
                        <div class="horizontal"></div>
                    </span>
                    <div class="chatGroupTextWrapper">
                        <span class="notify-bubble" id="chatCount">0</span>
                        <span class="chatGroupText" data-toggle="collapse" data-target="#collapseChatBodyGroup"
                            aria-expanded="false" aria-controls="collapseChatBodyGroup" role="button">Chats</span>
                    </div>
                    <div class="chatGroupBodyWrapper collapse" id="collapseChatBodyGroup">
                        <ul id="chatGroupBodyWrapperUl">
                            <li class="listChatGroup301" data-chat_id="301" data-chat_name="Room 301">Room 301</li>
                        </ul>
                    </div>
                </div>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="chatModal" tabindex="-1" role="dialog" aria-labelledby="chatModal"
                aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Send Group Message</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body" id="SelectBody">

                            <div class="form-group">
                                <label>Select Guest Room</label>
                                <select class="selectpicker my-select form-control" multiple data-actions-box="true"
                                    name="users[]" id="users">>
                                    <option value=''>Select Room</option>

                                </select>
                                <p class="resMessage text-danger"></p>
                            </div>
                            <div class="form-group">
                                <label>Message</label>
                                <textarea id="message" required rows="5" class="form-control"></textarea>
                                <p class="message text-danger"></p>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary btnCreateGroup">Send</button>
                        </div>
                    </div>
                </div>
            </div>
        </main>

        {{--Audio notification--}}
        <audio controls class="d-none" id="audioNotificationSound">
            <source src="{{ asset('sounds/light.ogg') }}" type="audio/ogg">
            <source src="{{ asset('sounds/light.mp3') }}" type="audio/mp3">
        </audio>
    </div>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha256-pasqAKBDmFT4eHoN2ndd6lN370kFiGUFyTiUHWhU7k8=" crossorigin="anonymous">
    </script>
    <script src="{{ asset('js/vendor/SendBird.min.js') }}"></script>
    <script>
        const adminHotel = {!! auth('hotel')->user(); !!};
        const activeUsersUrl = '{!! route('hotel.activeUser') !!}';
        const SENDBIRD_APP_ID = '{!! env('SENDBIRD_APP_ID') !!}';
    </script>
    <!-- Latest compiled and minified JavaScript -->

    <script src="{{ asset('js/custom/cms/chat-new.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
    <script>
        $('#sendEmailToSupport').click(function () {
        window.open('mailto:support@servrhotels.com');
    });
    </script>
    @yield('js')
</body>

</html>