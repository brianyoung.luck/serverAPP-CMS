<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantDishesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_dishes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('restaurant_id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->double('price', 10,  2);
            $table->enum('category', [
                'starter',
                'main',
                'dessert',
                'drink',
                'salad',
                'meat_and_fish',
            ]);
            $table->timestamps();
            $table->softDeletes();

            // Foreign Key
            $table->foreign('restaurant_id')
                ->references('id')
                ->on('restaurants')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant_dishes');
    }
}
