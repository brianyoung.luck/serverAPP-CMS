let chatWrapperChatId = "";
const chatsWrapper = $(".chatsWrapper");
const chatGroupBodyWrapperUl = $("#chatGroupBodyWrapperUl");
const chatCount = $("#chatCount");
const imgUrlChatAdmin =
  "https://servr-public.s3-ap-southeast-1.amazonaws.com/chat/cs.png";
const imgUrlChatHotel =
  "https://servr-public.s3-ap-southeast-1.amazonaws.com/chat/hotel.png";

$(document).ready(function() {
  // On click close chat button
  $(document).on("click", ".headerLiveChatClose", function() {
    closeChat($(this).data("chat_id"));
  });

  // On press send chat button
  $(document).on("click", ".sendChatBtn", function() {
    sendChat($(this).data("chat_id"));
  });

  // On press enter
  $(document).on("keyup", ".textChat", function(e) {
    isEnterPressed(e, $(this).data("chat_id"));
  });

  // On click chat group item
  $(document).on("click", "#chatGroupBodyWrapperUl li", function() {
    showSelectedChat(
      $(this).data("chat_id"),
      $(this).data("chat_name"),
      $(this)
    );
  });

  initChat();
});
/**
 * Init chat
 */
function initChat() {
  const chatData = [
    {
      chat_id: "301",
      chat_name: "Room 301",
    },
    {
      chat_id: "302",
      chat_name: "Room 302",
    },
    {
      chat_id: "303",
      chat_name: "Room 303",
    },
    {
      chat_id: "304",
      chat_name: "Room 304",
    },
    {
      chat_id: "305",
      chat_name: "Room 305",
    },
  ];

  // Update chat body chat group
  updateChatBodyChatGroup(chatData);

  // Update counter chat group
  updateCounterChatGroup();
}

/**
 * Create & show chat
 * @param chatId
 * @param chatName
 * @param liElement
 */
function showSelectedChat(chatId, chatName, liElement) {
  // Remove chat from chat group
  liElement.remove();
  // Update counter chat group
  updateCounterChatGroup();

  // Create chat details
  createNewChat({
    chat_id: chatId,
    chat_name: chatName,
  });

  // Bind event scroll
  bindEventScrollChat(chatId);
}

/**
 * Update counter chat group
 */
function updateCounterChatGroup() {
  chatCount.text(chatGroupBodyWrapperUl.children().length);
}

/**
 * Update chat body in chat group
 * @param chatData
 */
console.log(chatData);
function updateChatBodyChatGroup(chatData) {
  chatGroupBodyWrapperUl.text(``);
  for (const element of chatData) {
    chatGroupBodyWrapperUl.append(`
            <li data-chat_id="${element.chat_id}" data-chat_name="${
      element.chat_name
    }">${element.chat_name}</li>
        `);
  }
}

/**
 * Create new chat
 * @param chatData
 */
function createNewChat(chatData) {
  // Create chat details
  chatsWrapper.append(`
        <div class='chatWrapper' id="chatWrapper${chatData.chat_id}">
            <div class='topHeaderChatGuest'>
                <span class="headerLiveChatText">Live chat</span>
                <i class="fa fa-close pull-right headerLiveChatClose" data-chat_id="chatWrapper${
                  chatData.chat_id
                }"></i>
                <span class="headerRoomText pull-right">${
                  chatData.chat_name
                }</span>
            </div>
            <div class="bodyRoomBox">
                <ul class='chatBox'>
                    <li class='me'>
                        <div class='avatar-icon'>
                            <img src='${imgUrlChatAdmin}'>
                        </div>
                        <div class='messages'>
                            <p>Hi fine bro.</p>
                        </div>
                    </li>
                    <li class='another'>
                        <div class='avatar-icon'>
                            <img src='${imgUrlChatHotel}'>
                        </div>
                        <div class='messages'>
                            <p>
                                <div class="ticontainer">
                                    <div class="tiblock">
                                        <div class="tidot"></div>
                                        <div class="tidot"></div>
                                        <div class="tidot"></div>
                                    </div>
                                </div>
                            </p>
                        </div>
                    </li>
                </ul>
                <div class='actionFooter'>
                    <div class="row">
                        <div class="col-9 textChatWrapper">
                            <div class="form-group">
                                <input class="form-control textChat" id="name" type="text" placeholder="Enter text here" autocomplete="off" data-chat_id="chatWrapper${
                                  chatData.chat_id
                                }">
                            </div>
                        </div>
                        <div class="col-3 btnSendWrapper">
                            <button class="btn sendChatBtn" type="button" data-chat_id="chatWrapper${
                              chatData.chat_id
                            }">Send</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    `);
}

/**
 * Bind scroll event for chat
 * @param chatId
 * @constructor
 * @return {boolean}
 */
function bindEventScrollChat(chatId) {
  const element = $("#chatWrapper" + chatId + " .chatBox");
  element.bind("scroll", function() {
    if (element.scrollTop() <= 0) {
      getOldChat(chatId);
    }
  });

  return true;
}

/**
 * Get old chat
 * @param chatId
 */
function getOldChat(chatId) {
  const element = $("#chatWrapper" + chatId + " .chatBox");
  element.prepend(`
        <li class='me'>
            <div class='avatar-icon'>
                <img src='${imgUrlChatAdmin}'>
            </div>
            <div class='messages'>
                <p>Hi fine bro.</p>
            </div>
        </li>
        <li class='another'>
            <div class='avatar-icon'>
                <img src='${imgUrlChatHotel}'>
            </div>
            <div class='messages'>
                <p>
                    <div class="ticontainer">
                        <div class="tiblock">
                            <div class="tidot"></div>
                            <div class="tidot"></div>
                            <div class="tidot"></div>
                        </div>
                    </div>
                </p>
            </div>
        </li>
    `);
}

/**
 * Close chat
 * @param chatId
 */
function closeChat(chatId) {
  chatWrapperChatId = chatId;
  animateCSS("#" + chatId, "fadeOutDown", "fast", removeChat);
}

/**
 * Show animation on chat removed
 * @param element
 * @param animationName
 * @param speed
 * @param callback
 */
function animateCSS(element, animationName, speed, callback) {
  const node = document.querySelector(element);
  node.classList.add("animated", animationName, speed);

  function handleAnimationEnd() {
    node.classList.remove("animated", animationName);
    node.removeEventListener("animationend", handleAnimationEnd);
    if (typeof callback === "function") callback();
  }

  node.addEventListener("animationend", handleAnimationEnd);
}

/**
 * Remove chat details
 */
function removeChat() {
  $("#" + chatWrapperChatId).remove();
  chatWrapperChatId = "";
}

/**
 * Send chat
 * @param chatId
 * @returns {boolean}
 */
function sendChat(chatId) {
  // Get text user
  const elementChatInput = $("#" + chatId + " .textChat");

  // Send chat
  if (elementChatInput.val() === "") {
    return false;
  }
  chatWrapperChatId = chatId;
  const elementChatBox = $("#" + chatId + " .chatBox");
  elementChatBox.append(`
        <li class='me'>
            <div class='avatar-icon'>
                <img src='${imgUrlChatAdmin}'>
            </div>
            <div class='messages'>
                <p>${elementChatInput.val()}</p>
            </div>
        </li>
    `);

  elementChatBox.animate(
    {
      scrollTop: elementChatBox.get(0).scrollHeight,
    },
    400
  );

  // Remove text
  elementChatInput.val("");
}

/**
 * Send chat if user press enter
 * @param e
 * @param chatId
 */
function isEnterPressed(e, chatId) {
  if (e.which === 13) {
    sendChat(chatId);
  }
}
