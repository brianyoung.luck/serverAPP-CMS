<?php

namespace App\Models;

use App\Services\FileStorageService;
use Illuminate\Database\Eloquent\Model;

class MobileHotelTheme extends Model
{
    /**
     * Getter
     */

    public function getPhotoPreviewUrlAttribute($value)
    {
        $fileStorageService = new FileStorageService(env('FILESYSTEM_DRIVER'));
        $storageServiceName = $fileStorageService->getStorageServiceName($value);
        switch ($storageServiceName) {
            case 's3':
                return $fileStorageService->getTemporaryUrl($value);
                break;
            default:
                return $value;
        }
    }
}
