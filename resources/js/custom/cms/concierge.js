const showDetailsConciergeServiceRequestModal = $(
  ".showDetailsConciergeServiceRequestModal"
);
const addConciergeServiceModal = $(".addConciergeServiceModal");
const showLaundaryRequestModal = $(".showLaundaryRequestModal");
const editConciergeServiceModal = $(".editConciergeServiceModal");
const addConciergeServiceBtn = $("#addConciergeServiceBtn");
const storeNewConciergeServiceBtn = $("#storeNewConciergeServiceBtn");
const addConciergeServiceForm = $("#addConciergeServiceForm");
const updateConciergeServiceBtn = $("#updateConciergeServiceBtn");
const editConciergeServiceForm = $("#editConciergeServiceForm");
const deleteConciergeServiceBtn = $("#deleteConciergeServiceBtn");
const deleteConciergeServiceForm = $("#deleteConciergeServiceForm");
const declineConciergeServiceRequestBtn = $(
  "#declineConciergeServiceRequestBtn"
);
const processConciergeServiceRequestBtn = $(
  "#processConciergeServiceRequestBtn"
);
const updateConciergeServiceRequestForm = $(
  "#updateConciergeServiceRequestForm"
);

const processConciergeServiceRequestConfirmBtn = $(
  "#processConciergeServiceRequestConfirmBtn"
);
const processConciergeServiceRequestPreparingBtn = $(
  "#processConciergeServiceRequestPreparingBtn"
);
const processConciergeServiceRequestOnTheWayBtn = $(
  "#processConciergeServiceRequestOnTheWayBtn"
);
const processConciergeServiceRequestDoneBtn = $(
  "#processConciergeServiceRequestDoneBtn"
);
const priceField = $("#price");

let currentConciergeServiceRequestId = 0;

$(document).ready(function() {
  // Append modal to body
  showDetailsConciergeServiceRequestModal.appendTo("body");
  addConciergeServiceModal.appendTo("body");
  editConciergeServiceModal.appendTo("body");

  addConciergeServiceBtn.click(function() {
    addConciergeService();
  });
  storeNewConciergeServiceBtn.click(function() {
    storeConciergeService();
  });
  $(document).on("click", ".menuConciergeServiceTr", async function() {
    await showModalConciergeService($(this).data("concierge_service_id"));
  });

  $(document).on("click", ".orderWrapper", async function() {
    await showModalConciergeServiceRequest(
      $(this).data("concierge_service_request_id")
    );
  });
  $(document).on("click", ".laundryRequestWrapper", async function() {
    // showLaundaryRequestModal.show();
    showLaundaryRequestModal.modal("show");
    $(".showLaundaryRequestModal").attr(
      "order-id",
      $(this).data("laundry_request_id")
    );

    // await processLaundryOrderRequest($(this).data("laundry_request_id"));
  });
  updateConciergeServiceBtn.click(function() {
    updateConciergeService();
  });
  deleteConciergeServiceBtn.click(function() {
    confirmDeleteConciergeService();
  });
  declineConciergeServiceRequestBtn.click(function() {
    declineConciergeServiceRequest();
  });
  processConciergeServiceRequestBtn.click(function() {
    acceptConciergeServiceRequest();
  });
  processConciergeServiceRequestConfirmBtn.click(function() {
    acceptConciergeServiceRequest("confirmed", "Confirm");
  });

  processConciergeServiceRequestPreparingBtn.click(function() {
    acceptConciergeServiceRequest("preparing", "Preparing");
  });
  processConciergeServiceRequestOnTheWayBtn.click(function() {
    acceptConciergeServiceRequest("on_the_way", "On the way");
  });
  processConciergeServiceRequestDoneBtn.click(function() {
    acceptConciergeServiceRequest("done", "Accept");
  });

  // Room cleaning request
  $(document).on("click", ".btnConfirmRoomCleaningRequest", function() {
    changeStatusRoomCleaningRequest(
      $(this).data("room_cleaning_request_id"),
      "confirmed"
    );
  });
  $(document).on("click", ".btnFinishRoomCleaningRequest", function() {
    changeStatusRoomCleaningRequest(
      $(this).data("room_cleaning_request_id"),
      "completed"
    );
  });
});
window.addEventListener("click", function() {
  $(".showLaundaryRequestModal").modal("hide");
});

function addConciergeService() {
  // Show modal
  addConciergeServiceModal.modal("show");
}

function storeConciergeService() {
  storeNewConciergeServiceBtn.attr("disabled", true);
  addConciergeServiceForm.submit();
}

async function showModalConciergeService(conciergeServiceId) {
  // Show modal
  editConciergeServiceModal.modal("show");

  // Call ajax to get data
  const conciergeService = await getConciergeServiceDetails(conciergeServiceId);

  if (conciergeService.status) {
    const data = conciergeService.conciergeService;

    // Show details
    $(".editConciergeServiceModal #concierge_service_id").val(data.id);
    $(".deleteConciergeServiceForm #concierge_service_id").val(data.id);
    $(".editConciergeServiceModal #nameUpdate").val(data.name);
    $(".editConciergeServiceModal #descriptionUpdate").html(data.description);
    console.log(data);
    $(".editConciergeServiceModal #priceUpdate").val(data.price);
  }
}

async function getConciergeServiceDetails(conciergeServiceId) {
  let promise = new Promise(function(resolve, reject) {
    $.ajax({
      headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
      },
      type: "GET",
      url: url5 + "/" + conciergeServiceId,
      contentType: "application/json",
      dataType: "json",
    })
      .done(function(response) {
        resolve(response);
      })
      .fail(function(error) {
        console.log(error);
        reject();
      });
  });

  try {
    return await promise;
  } catch (err) {
    return {
      status: false,
      error: err,
    };
  }
}

function updateConciergeService() {
  updateConciergeServiceBtn.attr("disabled", true);
  editConciergeServiceForm.submit();
}

function confirmDeleteConciergeService(conciergeServiceId) {
  Swal.fire({
    title: "Are you sure?",
    type: "question",
    showCancelButton: true,
    confirmButtonColor: delete_button_color,
    cancelButtonColor: cancel_button_color,
    confirmButtonText: "Delete",
    reverseButtons: true,
  }).then((result) => {
    if (result.value && result.value === true) {
      deleteConciergeServiceForm.submit();
    }
  });
}

async function showModalConciergeServiceRequest(conciergeServiceRequestId) {
  // Show modal
  showDetailsConciergeServiceRequestModal.modal("show");

  // Call ajax to get data
  const conciergeServiceRequest = await getConciergeServiceRequestDetails(
    conciergeServiceRequestId
  );
  console.log(conciergeServiceRequest);
  // Clear table
  $(
    ".showDetailsConciergeServiceRequestModal #conciergeServiceRequestDetailsTable tbody"
  ).html("");

  if (conciergeServiceRequest.status) {
    const data = conciergeServiceRequest.conciergeServiceRequest;
    // Save id
    currentConciergeServiceRequestId = data.id;
    console.log(data);
    // Clear room no text
    $("#roomNoText").html("");
    //assign room no
    $("#roomNoText").html(
      "Room " + data.hotel_booking.room_number + " - Request details:"
    );
    // Show order details
    console.log("room" + data.hotel_booking.room_number);
    $(".showDetailsConciergeServiceRequestModal #roomNoText").val(
      `${data.hotel_booking.room_number} - Request details:`
    );

    data.concierge_services.forEach((conciergeService, index) => {
      $(
        ".showDetailsConciergeServiceRequestModal #conciergeServiceRequestDetailsTable tbody"
      ).append(`
                <tr>
                    <td>${index + 1}</td>
                    <td>${conciergeService.name}</td>
                    <td>${conciergeService.pivot.qty}</td>
                    <td>${
                      conciergeService.description
                        ? conciergeService.description
                        : "-"
                    }</td>
                </tr>
            `);
    });
    // <td>${
    //   conciergeService.pivot.note
    //     ? conciergeService.pivot.note
    //     : "-"
    // }</td>
    // show button action
    switch (data.status) {
      case "pending":
        declineConciergeServiceRequestBtn.removeClass("d-none");
        processConciergeServiceRequestBtn.addClass("d-none");
        // processConciergeServiceRequestConfirmBtn.addClass("d-none");
        processConciergeServiceRequestPreparingBtn.addClass("d-none");
        processConciergeServiceRequestOnTheWayBtn.addClass("d-none");
        processConciergeServiceRequestDoneBtn.removeClass("d-none");
        break;
      case "confirmed":
        declineConciergeServiceRequestBtn.removeClass("d-none");
        processConciergeServiceRequestBtn.addClass("d-none");
        processConciergeServiceRequestConfirmBtn.addClass("d-none");
        processConciergeServiceRequestPreparingBtn.removeClass("d-none");
        processConciergeServiceRequestOnTheWayBtn.addClass("d-none");
        processConciergeServiceRequestDoneBtn.addClass("d-none");
        break;
      case "preparing":
        declineConciergeServiceRequestBtn.removeClass("d-none");
        processConciergeServiceRequestBtn.addClass("d-none");
        processConciergeServiceRequestConfirmBtn.addClass("d-none");
        processConciergeServiceRequestPreparingBtn.addClass("d-none");
        processConciergeServiceRequestOnTheWayBtn.removeClass("d-none");
        processConciergeServiceRequestDoneBtn.addClass("d-none");
        break;
      case "on_the_way":
        declineConciergeServiceRequestBtn.removeClass("d-none");
        processConciergeServiceRequestBtn.addClass("d-none");
        processConciergeServiceRequestConfirmBtn.addClass("d-none");
        processConciergeServiceRequestPreparingBtn.addClass("d-none");
        processConciergeServiceRequestOnTheWayBtn.addClass("d-none");
        processConciergeServiceRequestDoneBtn.removeClass("d-none");
        break;
      default:
        processConciergeServiceRequestBtn.addClass("d-none");
        processConciergeServiceRequestConfirmBtn.addClass("d-none");
        processConciergeServiceRequestPreparingBtn.addClass("d-none");
        processConciergeServiceRequestOnTheWayBtn.addClass("d-none");
        processConciergeServiceRequestDoneBtn.addClass("d-none");
        declineConciergeServiceRequestBtn.addClass("d-none");
    }
  } else {
    currentConciergeServiceRequestId = 0;
  }
}

function processLaundryOrderRequest(laundryOrderRequestId) {
  Swal.fire({
    title: "Are you sure?",
    // type: "question",
    // showCancelButton: true,
    showConfirmButton: false,
    // confirmButtonColor: success_button_color,
    // cancelButtonColor: delete_button_color,
    // confirmButtonText: "Completed",
    // cancelButtonText: "Reject",
    // reverseButtons: true,
  }).then((result) => {
    if (result.value && result.value === true) {
      $("#updateLaundryOrderForm #laundry_orders_id").val(
        laundryOrderRequestId
      );
      $("#updateLaundryOrderForm #status").val("confirmed");
      $("#updateLaundryOrderForm").submit();
    } else if (result.dismiss && result.dismiss === "cancel") {
      $("#updateLaundryOrderForm #laundry_orders_id").val(
        laundryOrderRequestId
      );
      $("#updateLaundryOrderForm #status").val("rejected");
      $("#updateLaundryOrderForm").submit();
    }
  });
}
// Accept laundary request
$(document).on("click", "#accept", function(e) {
  e.preventDefault();
  Swal.fire({
    title: "Are you sure?",
    type: "question",
    showCancelButton: true,
    confirmButtonColor: success_button_color,
    cancelButtonColor: cancel_button_color,
    confirmButtonText: "Accept",
    reverseButtons: true,
  }).then((result) => {
    if (result.value && result.value === true) {
      $("#updateLaundryOrderForm #laundry_orders_id").val(
        $(".showLaundaryRequestModal").attr("order-id")
      );
      $("#updateLaundryOrderForm #status").val("confirmed");
      $("#updateLaundryOrderForm").submit();
      location.reload(true);
    }
  });
});
// Reject laundary request
$(document).on("click", "#reject", function(e) {
  e.preventDefault();
  Swal.fire({
    title: "Are you sure?",
    type: "question",
    showCancelButton: true,
    confirmButtonColor: delete_button_color,
    cancelButtonColor: cancel_button_color,
    confirmButtonText: "Reject",
    reverseButtons: true,
  }).then((result) => {
    if (result.value && result.value === true) {
      $("#updateLaundryOrderForm #laundry_orders_id").val(
        $(".showLaundaryRequestModal").attr("order-id")
      );
      $("#updateLaundryOrderForm #status").val("rejected");
      $("#updateLaundryOrderForm").submit();
      location.reload(true);
    }
  });
});
// Completed laundary request
$(document).on("click", "#complete", function(e) {
  e.preventDefault();
  Swal.fire({
    title: "Are you sure?",
    type: "question",
    showCancelButton: true,
    confirmButtonColor: success_button_color,
    cancelButtonColor: cancel_button_color,
    confirmButtonText: "Complete",
    reverseButtons: true,
  }).then((result) => {
    if (result.value && result.value === true) {
      $("#updateLaundryOrderForm #laundry_orders_id").val(
        $(".showLaundaryRequestModal").attr("order-id")
      );
      $("#updateLaundryOrderForm #status").val("completed");
      $("#updateLaundryOrderForm").submit();
      location.reload(true);
    }
  });
});

async function getConciergeServiceRequestDetails(conciergeServiceRequestId) {
  let promise = new Promise(function(resolve, reject) {
    $.ajax({
      headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
      },
      type: "GET",
      url: url4 + "/" + conciergeServiceRequestId,
      contentType: "application/json",
      dataType: "json",
    })
      .done(function(response) {
        resolve(response);
      })
      .fail(function(error) {
        console.log(error);
        reject();
      });
  });

  try {
    return await promise;
  } catch (err) {
    return {
      status: false,
      error: err,
    };
  }
}

function declineConciergeServiceRequest() {
  Swal.fire({
    title: "Are you sure?",
    type: "question",
    showCancelButton: true,
    confirmButtonColor: delete_button_color,
    cancelButtonColor: cancel_button_color,
    confirmButtonText: "Reject",
    reverseButtons: true,
  }).then((result) => {
    if (result.value && result.value === true) {
      $("#updateConciergeServiceRequestForm #concierge_service_request_id").val(
        currentConciergeServiceRequestId
      );
      $("#updateConciergeServiceRequestForm #status").val("rejected");
      updateConciergeServiceRequestForm.submit();
    }
  });
}

function acceptConciergeServiceRequest(status, text) {
  Swal.fire({
    title: "Are you sure?",
    type: "question",
    showCancelButton: true,
    confirmButtonColor: success_button_color,
    cancelButtonColor: cancel_button_color,
    confirmButtonText: text,
    reverseButtons: true,
  }).then((result) => {
    if (result.value && result.value === true) {
      $("#updateConciergeServiceRequestForm #concierge_service_request_id").val(
        currentConciergeServiceRequestId
      );
      $("#updateConciergeServiceRequestForm #status").val(status);
      updateConciergeServiceRequestForm.submit();
    }
  });
}

function changeStatusRoomCleaningRequest(roomCleaningRequestId, newStatus) {
  let btnText = "Finish";
  if (newStatus === "confirmed") {
    btnText = "Confirm";
  }

  Swal.fire({
    title: "Are you sure?",
    type: "question",
    showCancelButton: true,
    confirmButtonColor: success_button_color,
    cancelButtonColor: cancel_button_color,
    confirmButtonText: btnText,
    reverseButtons: true,
  }).then((result) => {
    if (result.value && result.value === true) {
      $("#updateRoomCleaningRequestForm #room_cleaning_request_id").val(
        roomCleaningRequestId
      );
      $("#updateRoomCleaningRequestForm #room_cleaning_request_status").val(
        newStatus
      );
      $("#updateRoomCleaningRequestForm").submit();
    }
  });
}
