<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoomCleaningOrder extends Model
{
    protected $fillable = [
        'hotel_booking_id',
        'status',
    ];

    protected $attributes = [
        'status' => 'pending',
    ];

    public function hotelBooking()
    {
        return $this->belongsTo(HotelBooking::class);
    }
}
