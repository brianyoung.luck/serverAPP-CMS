<?php

namespace App\Http\Controllers\Cms;

use App\Http\Requests\StoreRestaurantDishRequest;
use App\Http\Requests\UpdateRestaurantDishRequest;
use App\Models\RestaurantDish;
use App\Services\RestaurantService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RestaurantDishController extends Controller
{
    private $restaurantService;

    public function __construct(RestaurantService $restaurantService)
    {
        $this->restaurantService = $restaurantService;
    }

    public function store(StoreRestaurantDishRequest $request)
    {
        // Get data
        $data = $request->only([
            'restaurant_id',
            'category',
            'name',
            'description',
            'price',
        ]);
        $image = $request->file('image');

        // Store restaurant menu
        $this->restaurantService->storeRestaurantDish($data, $image);


        // Return
        return redirect()->route('cms.restaurants.show', ['restaurant' => $data['restaurant_id']])->with('success', 'Add new menu success.');
    }

    public function ajaxGetDishes(Request $request)
    {
        $category = $request->get('category', 'all');

        // Get guest details
        $dishes = RestaurantDish::where('restaurant_id', $request->get('restaurant_id', 0));
        if ($category != 'all') {
            $dishes = $dishes->where('category', $category);
        }
        $dishes = $dishes->orderBy('name', 'desc')
            ->get();

        return [
            'status' => true,
            'dishes' => $dishes,
        ];
    }
    

    public function ajaxGetDishDetails(Request $request, $dishId)
    {
        // Get guest details
        $dishDetails = RestaurantDish::where('id', $dishId)
//            ->where('restaurant_id', auth('hotel')->user()->id)
            ->first();

        return [
            'status' => true,
            'dishDetails' => $dishDetails,
        ];
    }

    public function update(UpdateRestaurantDishRequest $request)
    {
        // Get data
        $data = $request->only([
            'dish_id',
            'restaurant_id',
            'categoryUpdate',
            'nameUpdate',
            'descriptionUpdate',
            'priceUpdate',
        ]);

        // Get dish
        $restaurantDish = RestaurantDish::where('id', data_get($data, 'dish_id', 0))
//            ->where('restaurant_id', auth('hotel')->user()->id)
            ->first();
        if (!$restaurantDish) {
            abort(404);
        }

        // Update restaurant menu
        $restaurantDish->category = $request->input('categoryUpdate');
        $image = $request->file('image');
        $restaurantDish = $this->restaurantService->updateRestaurantDish($restaurantDish, [
            'restaurant_id' => data_get($data, 'restaurant_id', 0),
            //'category' => data_get($data, 'categoryUpdate', ''),
            'name' => data_get($data, 'nameUpdate', ''),
            'description' => data_get($data, 'descriptionUpdate'),
            'price' => data_get($data, 'priceUpdate', 0),
        ], $image);

        // Return
        return redirect()->route('cms.restaurants.show', ['restaurant' => $data['restaurant_id']])->with('success', 'Update menu success.');
    }

    public function destroy(Request $request)
    {
        // Get data
        $data = $request->only([
            'dish_id',
            'restaurant_id',
        ]);

        // Get dish
        $restaurantDish = RestaurantDish::where('id', data_get($data, 'dish_id', 0))
//            ->where('restaurant_id', auth('hotel')->user()->id)
            ->first();
        if (!$restaurantDish) {
            abort(404);
        }

        // Destroy
        $restaurantDish->delete();

        // Return
        return redirect()->route('cms.restaurants.show', ['restaurant' => $data['restaurant_id']])->with('success', 'Delete menu success.');
    }
}