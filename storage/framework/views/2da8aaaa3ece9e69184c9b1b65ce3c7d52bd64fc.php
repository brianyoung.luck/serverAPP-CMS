<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <meta name="description" content="<?php echo e(env('APP_DESCRIPTION')); ?>">
    <meta name="author" content="<?php echo e(env('APP_AUTHOR')); ?>">
    <meta name="keyword" content="<?php echo e(env('APP_KEYWORD')); ?>">
    <title><?php echo $__env->yieldContent('title'); ?> - <?php echo e(env('APP_NAME')); ?></title>

    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/custom-cms.css')); ?>" rel="stylesheet">
    <link rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
    <style>
        .messageAlert {
            padding-top: 11px;
            padding-bottom: 0px;
        }
    </style>
      <?php echo $__env->yieldContent('styles'); ?>
</head>

<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
    <?php echo $__env->make('cms.layouts.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <div class="app-body">
        <?php echo $__env->make('cms.layouts.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <main class="main">
            <?php echo $__env->yieldContent('breadcrumb'); ?>
            <div class="container-fluid">
                <?php echo $__env->make('cms.layouts.flash-message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                <div class="animated fadeIn">
                    <div class="top-action-wrapper">
                        <?php echo $__env->yieldContent('top-action'); ?>
                    </div>
                    <?php echo $__env->yieldContent('content'); ?>
                </div>
            </div>
            <div class="chatsWrapper">
                <div class="chatGroupWrapper">
                    <span class="circle compose" id="compose" data-toggle="modal" data-target="#chatModal">
                        <div class="vertical"></div>
                        <div class="horizontal"></div>
                    </span>
                    <div class="chatGroupTextWrapper">
                        <span class="notify-bubble" id="chatCount">0</span>
                        <span class="chatGroupText" data-toggle="collapse" data-target="#collapseChatBodyGroup"
                            aria-expanded="false" aria-controls="collapseChatBodyGroup" role="button">Chats</span>
                    </div>
                    <div class="chatGroupBodyWrapper collapse" id="collapseChatBodyGroup">
                        <ul id="chatGroupBodyWrapperUl">
                            <li class="listChatGroup301" data-chat_id="301" data-chat_name="Room 301">Room 301</li>
                        </ul>
                    </div>
                </div>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="chatModal" tabindex="-1" role="dialog" aria-labelledby="chatModal"
                aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Send Group Message</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body" id="SelectBody">

                            <div class="form-group">
                                <label>Select Guest Room</label>
                                <select class="selectpicker my-select form-control" multiple data-actions-box="true"
                                    name="users[]" id="users">>
                                    <option value=''>Select Room</option>

                                </select>
                                <p class="resMessage text-danger"></p>
                            </div>
                            <div class="form-group">
                                <label>Message</label>
                                <textarea id="message" required rows="5" class="form-control"></textarea>
                                <p class="message text-danger"></p>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary btnCreateGroup">Send</button>
                        </div>
                    </div>
                </div>
            </div>
        </main>

        
        <audio controls class="d-none" id="audioNotificationSound">
            <source src="<?php echo e(asset('sounds/light.ogg')); ?>" type="audio/ogg">
            <source src="<?php echo e(asset('sounds/light.mp3')); ?>" type="audio/mp3">
        </audio>
    </div>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha256-pasqAKBDmFT4eHoN2ndd6lN370kFiGUFyTiUHWhU7k8=" crossorigin="anonymous">
    </script>
    <script src="<?php echo e(asset('js/vendor/SendBird.min.js')); ?>"></script>
    <script>
        const adminHotel = <?php echo auth('hotel')->user();; ?>;
        const activeUsersUrl = '<?php echo route('hotel.activeUser'); ?>';
        const SENDBIRD_APP_ID = '<?php echo env('SENDBIRD_APP_ID'); ?>';
    </script>
    <!-- Latest compiled and minified JavaScript -->

    <script src="<?php echo e(asset('js/custom/cms/chat-new.js')); ?>"></script>
    <script src="<?php echo e(asset('js/app.js')); ?>"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
    <script>
        $('#sendEmailToSupport').click(function () {
        window.open('mailto:support@servrhotels.com');
    });
    </script>
    <?php echo $__env->yieldContent('js'); ?>
</body>

</html><?php /**PATH E:\Xammp\htdocs\server_hotels\resources\views/cms/layouts/app.blade.php ENDPATH**/ ?>