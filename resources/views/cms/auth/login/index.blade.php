@extends('cms.layouts.auth')

@section('title', __('Login CMS'))

@section('content')
<div id="login_wrapper" class="row justify-content-center">
    <div class="col-xl-5 col-lg-6 col-md-8 col-sm-10">
        <div class="card-group">
            <div class="card p-4">
                <div class="card-body " style="width: 430px !important;">
                    <div class="admin-panel-text">
                        <h2>{{ env('APP_NAME') }}</h2>
                        <h3>{{ __('Admin Hotel') }}</h3>
                    </div>
                    @include('cms.layouts.flash-message')
                    <form id="loginForm" class="form-horizontal" action="
                        @if($for == 'hotel_admin')
                        {{ route('cms.do-login') }}
                        @else
                        {{ route ('cms.do-staff-login')}}
                        @endif
                        " method="post">
                        @csrf()
                        @method('post')
                        <div class="form-group row">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <input class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}"
                                        id="email" type="email" name="email" placeholder="Email">
                                    @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <input class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}"
                                        id="password" type="password" name="password" placeholder="Password">
                                    <span class="fa fa-fw fa-eye-slash field-icon toggle-password"></span>
                                    @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="row">
                        <div class="col-12">
                            <button class="btn btn-pill btn-block btn-primary" type="button" id="buttonLogin"
                                disabled>{{ __('Login') }}</button>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-12">
                            <a href="{{ route('cms.hotels.create') }}" class="btn btn-pill btn-block btn-primary mt-2"
                                style="font-size: 0.975rem;" id="buttonRegister" disabled>{{ __('Register here') }}</a>
                        </div>
                    </div>
                    <div id="forgotPasswordLinkWrapper">
                        <a href="{{ route('cms.forgot-password.index') }}">{{ __('Forgot password?') }}</a>
                    </div>
                    <div class="row mt-2">
                        <div class="col-6">
                            <img class="btnPlayStore" style="cursor: pointer;"
                                src="https://static.wixstatic.com/media/d9af4b_2b80996b943f4eca80991da9bf14c841~mv2.png/v1/fill/w_154,h_52,al_c,q_85,usm_0.66_1.00_0.01/google%20play%20store%20button%201.webp"
                                alt="https://www.servrhotels.com">
                        </div>
                        <div class="col-6">
                            <img class="btnAppStore" style="cursor: pointer;"
                                src="https://static.wixstatic.com/media/d9af4b_ed82a02f095748c9b2cb9999c0a6eb11~mv2.png/v1/fill/w_146,h_52,al_c,q_85,usm_0.66_1.00_0.01/app%20store%20button%201.webp"
                                alt="https://www.servrhotels.com">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('js')
<script>
    const url1 = '{!! route('cms.dashboard.index') !!}';
</script>
<script src="{{ asset('js/custom/cms/login.js') }}"></script>
@stop