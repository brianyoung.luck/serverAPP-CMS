<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Srmklive\PayPal\Services\ExpressCheckout;
use Illuminate\Support\Facades\Mail;
use App\Mail\SubscribeConfirmation;
use App\Mail\RenewalConfirmation;
use App\Models\Hotel;
use App\Models\HotelFeature;
use App\Models\Restaurant;
use App\Models\Spa;
use Carbon\Carbon;

class PaypalController extends Controller
{
    public function search(Request $req)
    {
        $req->validate([
        'code' => 'required|max:255',
      ]);

        try {
            $hotel = Hotel::where('code', $req->code)->firstOrFail();
            return view('cms.paypal.search', compact('hotel'));
        } catch (\Exception $e) {
            return redirect()->back()->with('not_found', 'Hotel Code Not Found');
        }
    }

    public function store(Request $req)
    {
        switch ($req->room) {
        case '20':
          $amount = 50;
          break;

        case '20-49':
          $amount = 80;
          break;

        case '50-99':
          $amount = 150;
          break;

        case '100-199':
          $amount = 220;
          break;

           case '200-299':
          $amount = 280;
          break;

        case '300+':
          $amount = 350;
          break;

        default:
          $amount = 0;
          break;
      }

        $response = $this->checkout($req, $amount);

        // This will redirect user to PayPal
        return redirect($response['paypal_link']);
    }

    public function checkout($req, $amount)
    {
        $req->validate([
          'address1' => 'required|max:255',
          'address2' => 'required|max:255',
          'post_code' => 'required|max:255',
          'country' => 'required|max:255',
          'room' => 'required|max:255',
      ]);

        $provider = new ExpressCheckout;

        $hotel = Hotel::findOrFail($req->hotel_id);
        $hotel->fill($req->all());

        // set data for paypal
        $data = [];
        $data['items'] = [
          [
              'name'  => "Monthly Subscription",
              'price' => $amount,
              'qty'   => 1,
          ],
      ];
        $data['subscription_desc'] = "Monthly Subscription ".$hotel->name;
        $data['invoice_id'] = uniqId();
        $data['invoice_description'] = "Monthly Subscription ".$hotel->name;
        $data['return_url'] = route('cms.paypal.success');
        $data['cancel_url'] = route('cms.code.search');
        $total = 0;
        foreach ($data['items'] as $item) {
            $total += $item['price'] * $item['qty'];
        }
        $data['total'] = $total;

        // Use the following line when creating recurring payment profiles (subscriptions)
        $response = $provider->setExpressCheckout($data, true);

        // simpan token ke database untuk tracking pembayaran
        $hotel->token = $response['TOKEN'];
        $hotel->save();

        // create HotelFeature
        $hotel_feature = HotelFeature::create([
        'hotel_id' => $hotel->id,
        'is_check_in_enabled' => '1',
        'is_check_out_enabled' => '1',
        'is_spa_enabled' => '1',
        'is_restaurant_enabled' => '1',
        'is_concierge_enabled' => '1',
        'is_cleaning_enabled' => '1',
        'is_experience' => '1',
      ]);

        // create restaurant
        $restaurant = Restaurant::create([
        'hotel_id' => $hotel->id,
        'name' => 'Restaurant',
        'logo_url' => 'https://marketplace.canva.com/MAC5oKacMGY/1/0/thumbnail_large-5/canva-black-with-utensils-icon-restaurant-logo-MAC5oKacMGY.jpg',
      ]);

        // create spa
        $spa = Spa::create([
        'hotel_id' => $hotel->id,
        'name' => 'Spa',
        'logo_url' => 'http://lorempixel.com/200/200',
      ]);

        return $response;
    }

    public function success(Request $req)
    {
        // get express checkout detail
        $provider = new ExpressCheckout;
        $response = $provider->getExpressCheckoutDetails($req->token);

        // create recurring payment profile
        $startdate = Carbon::now()->toAtomString();
        $profile_desc = $response['DESC'];
        $data = [
          'PROFILESTARTDATE' => $startdate,
          'DESC' => $profile_desc,
          'BILLINGPERIOD' => 'Month', // Can be 'Day', 'Week', 'SemiMonth', 'Month', 'Year'
          'BILLINGFREQUENCY' => 1, //
          'AMT' => $response['AMT'], // Billing amount for each billing cycle
          'CURRENCYCODE' => 'USD', // Currency code
          'TRIALBILLINGPERIOD' => 'Month',  // (Optional) Can be 'Day', 'Week', 'SemiMonth', 'Month', 'Year'
          'TRIALBILLINGFREQUENCY' => 1, // (Optional) set 12 for monthly, 52 for yearly
          'TRIALTOTALBILLINGCYCLES' => 1, // (Optional) Change it accordingly
          'TRIALAMT' => 0, // (Optional) Change it accordingly
      ];
        $response = $provider->createRecurringPaymentsProfile($data, $req->token);

        // simpan token ke database untuk tracking pembayaran
        $hotel = Hotel::where('token', $req->token)->first();
        $hotel->correlation_id = $response['CORRELATIONID'];
        $hotel->save();

        return redirect('https://www.servrhotels.com/subscription-success-page');
    }

    public function notification(Request $req)
    {
        $provider = new ExpressCheckout;

        $req->merge(['cmd' => '_notify-validate']);
        $post = $req->all();
        $response = (string) $provider->verifyIPN($post);
        if ($response === 'VERIFIED') {
            $hotel = Hotel::where('correlation_id', $req->ipn_track_id)
        ->orWhere('recurring_payment_id', $req->recurring_payment_id)
        ->first();

            // check first payment or not
            if ($hotel->recurring_payment_id == null || $hotel->recurring_payment_id == "") {
                $hotel->recurring_payment_id = $req->recurring_payment_id;
                $hotel->save();

                Mail::to($hotel->email)->bcc('mavindraadiyasa@gmail.com')->send(new SubscribeConfirmation($hotel));
            } else {
                Mail::to($hotel->email)->bcc('mavindraadiyasa@gmail.com')->send(new RenewalConfirmation($hotel));
            }

            // Make hotel status active and renewal membership
            $hotel->is_active = 1;
            $hotel->expired_at = Carbon::now()->addMonth();
            $hotel->save();
        }
    }

    public function test()
    {
        $hotel = Hotel::findOrFail(17);
        Mail::to($hotel->email)->bcc('mavindraadiyasa@gmail.com')->send(new RenewalConfirmation($hotel));
    }
}