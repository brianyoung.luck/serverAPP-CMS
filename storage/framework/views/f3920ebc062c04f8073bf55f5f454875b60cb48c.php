<?php $__env->startSection('title', __('Dashboard')); ?>
<?php 
$user = auth('hotel_staff')->user();
?>

<?php $__env->startSection('content'); ?>
    <div id="dashboardWrapper" >
        <div class="row">
            <div class="col-12 col-sm-4 col-md-3 col-lg-3 col-xl-3 colBtnMenuList">
                <a class="btn btn-block btn-secondary btnMenuList" href="<?php echo e(route('cms.appearances.index')); ?>">
                    <img src="<?php echo e(asset('images/hotel/appearance_icon@3x.png')); ?>" alt="appearance img">
                    <p>Appearance</p>
                </a>
            </div>
            <?php if(auth('hotel')->user()->feature->is_restaurant_enabled && (!$user || $user->can('restaurant'))): ?>
                <div class="col-12 col-sm-4 col-md-3 col-lg-3 col-xl-3 colBtnMenuList">
                    <a class="btn btn-block btn-secondary btnMenuList" data-toggle="modal" data-target="#restaurantSelectModal">
                        <img src="<?php echo e(asset('images/hotel/restaurant_icon@3x.png')); ?>" alt="restaurant img">
                        <p>Restaurant</p>
                    </a>
                </div>
            <?php endif; ?>
            <?php if(auth('hotel')->user()->feature->is_spa_enabled && (!$user || $user->can('spa'))): ?>
                <div class="col-12 col-sm-4 col-md-3 col-lg-3 col-xl-3 colBtnMenuList">
                    <?php
                        $spa = auth('hotel')->user()->spas->first();
                    ?>
                    <a class="btn btn-block btn-secondary btnMenuList" href="<?php echo e(route('cms.spas.show', ['spa' => $spa->id])); ?>">
                        <img src="<?php echo e(asset('images/hotel/spa_icon@3x.png')); ?>" alt="spa img">
                        <p>Spa</p>
                    </a>
                </div>
            <?php endif; ?>
            <?php if(auth('hotel')->user()->feature->is_check_in_enabled && (!$user || $user->can('checkin_in'))): ?>
                <div class="col-12 col-sm-4 col-md-3 col-lg-3 col-xl-3 colBtnMenuList">
                    <a class="btn btn-block btn-secondary btnMenuList" href="<?php echo e(route('cms.guests.index')); ?>">
                        <img src="<?php echo e(asset('images/hotel/check_in_icon@3x.png')); ?>" alt="guests img">
                        <p>Guests</p>
                    </a>
                </div>
            <?php endif; ?>
            <?php if((!$user || $user->can('experience'))): ?>
                <div class="col-12 col-sm-4 col-md-3 col-lg-3 col-xl-3 colBtnMenuList">
                    <a class="btn btn-block btn-secondary btnMenuList" href="<?php echo e(route('cms.experience.index')); ?>">
                        <img src="<?php echo e(asset('images/hotel/check_out_icon@3x.png')); ?>" alt="checkout img">
                        <p>Experience</p>
                    </a>
                </div>
            <?php endif; ?>
            <?php if($hotel->feature->is_concierge_enabled && (!$user || $user->can('concierge'))): ?>
                <div class="col-12 col-sm-4 col-md-3 col-lg-3 col-xl-3 colBtnMenuList">
                    <a class="btn btn-block btn-secondary btnMenuList" href="<?php echo e(route('cms.concierges.index')); ?>">
                        <img src="<?php echo e(asset('images/hotel/concierge_icon@3x.png')); ?>" alt="concierge img">
                        <p>Concierge</p>
                    </a>
                </div>
            <?php endif; ?>
            <?php if((!$user || $user->can('settings'))): ?>
            <div class="col-12 col-sm-4 col-md-3 col-lg-3 col-xl-3 colBtnMenuList">
                <a class="btn btn-block btn-secondary btnMenuList" href="<?php echo e(route('cms.settings.index')); ?>">
                    <img src="<?php echo e(asset('images/hotel/settings_icon@3x.png')); ?>" alt="settings img">
                    <p>Settings</p>
                </a>
            </div>
            <?php endif; ?>
            <div class="col-12 col-sm-4 col-md-3 col-lg-3 col-xl-3 colBtnMenuList">
                <a class="btn btn-block btn-secondary btnMenuList" id="btnSupportMailTo">
                    <img src="<?php echo e(asset('images/hotel/help_icon@3x.png')); ?>" alt="support img">
                    <p>Support</p>
                </a>
            </div>
        </div>
    </div>

    <div class="modal fade restaurantSelectModal" id="restaurantSelectModal" tabindex="-1" role="dialog" aria-labelledby="restaurantSelectModal" aria-hidden="true">
        <div class="modal-dialog modal-md modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <?php $__currentLoopData = $hotel->restaurants; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $restaurant): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <a href="<?php echo e(route('cms.restaurants.show', ['restaurant' => $restaurant->id])); ?>" class="btn btn-pill btn-block btn-primary"><?php echo e($restaurant->name); ?></a>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php if(count($hotel->restaurants) < 3): ?>
                       <button class="btn btn-pill btn-block btn-secondary mt-2" id="addNewRestaurantBtn"><?php echo e(__('Create new')); ?></button>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('js/custom/cms/dashboard.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('cms.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\sevr_hotel\resources\views/cms/dashboard/index.blade.php ENDPATH**/ ?>