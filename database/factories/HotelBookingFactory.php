<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\HotelBooking;
use Faker\Generator as Faker;

$factory->define(HotelBooking::class, function (Faker $faker) {
    return [
        'reference' => $faker->toUpper($faker->bothify('??#####')),
        'arrival_date' => $faker->dateTimeInInterval('+15 days, +25 days'),
        'departure_date' => $faker->dateTimeInInterval('+30 days, +40 days'),
        'cardholder_name' => $faker->firstName . ' ' . $faker->lastName,
        'card_expiry_date' => $faker->dateTimeInInterval('+3 years, +4 years'),
        'card_address' => $faker->streetAddress,
        'phone_number' => $faker->numerify('+628##########'),
        'card_number' => 'XXXX-XXXX-XXXX-XXXX',
    ];
});
