/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

// Jquery
require("jquery");

// Popper
require("popper.js/dist/umd/popper.min.js");

// Bootstrap
require("bootstrap/dist/js/bootstrap.min.js");

// Pace
require("pace-progress/pace.min.js");

// Perfect scrollbar
require("perfect-scrollbar/dist/perfect-scrollbar.min.js");

// Core ui
require("@coreui/coreui/dist/js/coreui.min.js");

// Sweet alert 2
window.Swal = require("sweetalert2");

// Yajra Datatables
require("datatables.net/js/jquery.dataTables.min");
require("datatables.net-bs4/js/dataTables.bootstrap4.js");

// Chart js
require("chart.js/dist/Chart.min");

// Notif
require("./vendor/bootstrap-notify-3.1.3/bootstrap-notify.min");

/**
 * Color
 */
window.primary_color = "#102133";
window.cancel_button_color = "#BDBDBD";
window.primary_color = "#3085d6";
window.delete_button_color = "#FF5050";
window.success_button_color = "#38c172";

/**
 * Thousand separator
 * e.g. 100000 => 100.000
 *
 * @param n
 * @returns {*}
 */
window.thousandSeparator = function thousand_separator(n) {
  const tmp = Number(n);
  if (typeof tmp === "number") {
    n += "";
    const x = n.split(".");
    let x1 = x[0];
    const x2 = x.length > 1 ? "." + x[1] : "";
    const rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
      x1 = x1.replace(rgx, "$1" + "." + "$2");
    }
    return x1 + x2;
  } else {
    return n;
  }
};

function pad(n) {
  return n < 10 ? "0" + n : n;
}

window.dateTimeFormat = function dateTimeFormat(dateTime, format = "datetime") {
  const dateTmp = new Date(dateTime);
  const month = dateTmp.getMonth();
  const year = dateTmp.getFullYear();

  if (format === "card_expiry_date") {
    return month + 1 + "/" + year.toString().substr(-2);
  }

  const monthNames = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];

  const date = dateTmp.getDate();

  const hour = dateTmp.getHours();
  const minute = dateTmp.getMinutes();

  return (
    monthNames[month] +
    " " +
    pad(date) +
    ", " +
    year +
    " (" +
    hour +
    ":" +
    minute +
    ")"
  );
};

$(document).ready(function() {
  $("#createNewRestaurantSidebarBtn").click(function() {
    $("#createRestaurantForm").submit();
  });
});
