<?php

use App\Models\HotelBooking;
use App\Models\PassportPhoto;
use Illuminate\Database\Seeder;

class PassportPhotoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $hotelBookings = HotelBooking::all();
        foreach ($hotelBookings as $hotelBooking)
        {
            factory(PassportPhoto::class, rand(1, 3))->create([
                'hotel_booking_id' => $hotelBooking->id,
            ]);
        }
    }
}
