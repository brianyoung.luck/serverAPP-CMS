<?php

namespace App\Http\Controllers\Cms;

use App\Models\RestaurantRoomService;
use App\Services\SendbirdService;
use App\Services\BookingBillService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class RestaurantRoomServiceController extends Controller
{
    public function update(Request $request, SendbirdService $sendbirdService, BookingBillService $bookingBillService)
    {
       
        $restaurantRoomService = RestaurantRoomService::where('id', $request->get('restaurant_room_services_id', 0))
            ->first();
        if (!$restaurantRoomService) {
            abort(404);
        }

        DB::transaction(function () use ($request, $restaurantRoomService, $bookingBillService) {
            $restaurantRoomService->status = $request->get('status', 'done');
            $restaurantRoomService->signature = $request->get('signature',$request->get('signature'));
            $restaurantRoomService->save();

            if ($restaurantRoomService->status == 'done') {
                $bookingBillService->addToBill($restaurantRoomService);
            }
        });
        // Notification via sendbird
        $sendbirdService->sendAdminMessage($restaurantRoomService->hotelBooking, [
            'text' => 'Your restaurant order is ' . ucwords(str_replace("_", " ", $restaurantRoomService->status)),
            'payload' => [
                'type' => 'restaurant_room_order',
                'id' => $restaurantRoomService->id,
            ],
        ]);

        return redirect()->route('cms.restaurants.show', ['restaurant' => $restaurantRoomService->restaurant_id])->with('success', 'Success');
    }

    public function ajaxGetRestaurantRoomServices(Request $request)
    {
        return [
            'status' => true,
        ];
    }

    public function ajaxGetRestaurantRoomService(Request $request, $restaurantRoomServiceId)
    {
        // Get guest details
        $restaurantRoomService = RestaurantRoomService::where('id', $restaurantRoomServiceId)
            ->with([
                'dishes',
                'hotelBooking',
            ])
            ->first();

        return [
            'status' => true,
            'restaurantRoomService' => $restaurantRoomService,
        ];
    }
}