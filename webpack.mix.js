const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('resources/sass/app.scss', 'public/css')
    .sass('resources/sass/custom-cms.scss', 'public/css')
    .sass('resources/sass/custom-admin.scss', 'public/css')
    .sass('resources/sass/custom-web.scss', 'public/css')
;
mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/custom/cms/login.js', 'public/js/custom/cms')
    .js('resources/js/custom/cms/dashboard.js', 'public/js/custom/cms')
    .js('resources/js/custom/cms/forgot-password.js', 'public/js/custom/cms')
    .js('resources/js/custom/cms/guest.js', 'public/js/custom/cms')
    .js('resources/js/custom/cms/appearance.js', 'public/js/custom/cms')
    .js('resources/js/custom/cms/chat.js', 'public/js/custom/cms')
    .js('resources/js/custom/cms/hotel.js', 'public/js/custom/cms')
    .js('resources/js/custom/cms/restaurant.js', 'public/js/custom/cms')
    .js('resources/js/custom/cms/concierge.js', 'public/js/custom/cms')
    .js('resources/js/custom/cms/checkout.js', 'public/js/custom/cms')
    .js('resources/js/custom/cms/spa.js', 'public/js/custom/cms')
    .js('resources/js/custom/cms/setting.js', 'public/js/custom/cms')

    .js('resources/js/custom/admin/login.js', 'public/js/custom/admin')
    .js('resources/js/custom/admin/forgot-password.js', 'public/js/custom/admin')
    .js('resources/js/custom/admin/reset-password.js', 'public/js/custom/admin')
    .js('resources/js/custom/admin/hotel.js', 'public/js/custom/admin')
    .js('resources/js/custom/cms/experience.js', 'public/js/custom/cms')
;
