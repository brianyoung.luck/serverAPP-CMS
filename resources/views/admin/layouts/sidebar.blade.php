<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link {!! Request::is('dashboard') || Request::is('') ? 'active' : '' !!}" href="{{ route('admin.dashboard.index') }}">
                    <i class="nav-icon icon-home"></i> {{ __('Home') }}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {!! Request::is('hotels*') ? 'active' : '' !!}" href="{{ route('admin.hotels.index') }}">
                    <i class="nav-icon icon-grid"></i> {{ __('Hotel') }}
                </a>
            </li>
            @if(auth('admin')->user()->name == "Super Admin")
            <li class="nav-item">
                <a class="nav-link {!! Request::is('staff*') ? 'active' : '' !!}" href="{{ route('staff.index') }}">
                    <i class="nav-icon icon-grid"></i> {{ __('Staff Members') }}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {!! Request::is('settings*') ? 'active' : '' !!}" href="{{ route('settings.index') }}">
                    <i class="nav-icon icon-grid"></i> {{ __('Settings') }}
                </a>
            </li>
            @endif
            
            <li class="nav-item">
                <a class="nav-link" href="{{ route('admin.logout.index') }}">
                    <i class="nav-icon icon-logout"></i> {{ __('Logout') }}
                </a>
            </li>
        </ul>
    </nav>
</div>