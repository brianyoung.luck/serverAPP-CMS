<?php

namespace App\Services;


use App\Models\HotelBooking;
use App\Models\RoomCleaningOrder;

class RoomCleaningOrderService
{
    public function createOrder(HotelBooking $booking)
    {
        return RoomCleaningOrder::create([
            'hotel_booking_id' => $booking->id,
        ]);
    }
}