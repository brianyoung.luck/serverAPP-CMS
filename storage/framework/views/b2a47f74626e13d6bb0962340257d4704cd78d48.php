<?php $__env->startSection('title', __('Experience')); ?>

<?php $__env->startSection('content'); ?>
<div id="guestsWrapper">
	<div class="buttons_group">

		<div class="scrollable row newArrivalsWrapper">
			<div class="col-4">
				<button type="button" class="btn addBtn" id="addButton">Add Button +</button>
				<?php $__currentLoopData = $buttons->slice(0,2)->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $button): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<div class="btn d-block existing_btn" data-id="<?php echo e($button->id); ?>">
					<img src="/icons/<?php echo e($button->icon); ?>.png" width="50" height="50" class="d-block">
					<span class="btn_title ellipsis" title="<?php echo e($button->title); ?>"><?php echo e($button->title); ?></span>
				</div>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</div>

			<?php
			$i = 1;
			?>
			<?php $__currentLoopData = $buttons->slice(2)->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $button): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<?php if($i == 1): ?>
			<div class="col-4">
				<?php endif; ?>


				<div class="btn d-block existing_btn" data-id="<?php echo e($button->id); ?>">
					<img src="/icons/<?php echo e($button->icon); ?>.png" width="50" height="50" class="d-block">
					<span class="btn_title"><?php echo e($button->title); ?></span>
				</div>

				<?php if($i == 3): ?>
			</div>
			<?php
			$i = 0;
			?>
			<?php endif; ?>
			<?php
			$i++
			?>

			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

		</div>
	</div>
</div>
<!-- Modals -->
<div class="modal fade addButtonModal modal-addBtn" tabindex="-1" role="dialog" aria-labelledby="addButtonModal"
	aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<form id="addBtnForm" method="post" action="<?php echo e(route('cms.experience.store')); ?>"
					enctype="multipart/form-data">
					<?php echo csrf_field(); ?>
					<?php echo method_field('post'); ?>
					<input type="hidden" name="button_id" id="button_id" value="-1" />
					<input type="hidden" name="images_id" id="images_id" value="" />
					<div class="row">
						<div class="col-6">
							<div class="form-group">
								<input type="text" name="btnTitle" id="btnTitle" placeholder="Button title" required
									class="input_text form-control">
							</div>
							<div class="form-group">
								
								<div class="alert alert-danger alert-dismissible fade show restImagesErrors text-center messageAlert"
									role="alert" style="display: none;">
									<span></span>
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="btn btn-d no-radius" id="selectBtnIconWrapper">
									<input type="hidden" name="icon" required id="selectedIcon_Name">
									<div id="selectedImageText">
										<img id="btnSelectedImg" />
									</div>
									<span class="btn_title absolute">Choose button icon</span>
								</div>
							</div>
							<div class="form-group">
								<div class="btn btn-d no-radius" style="height: 176px;">
									<div id="selectBtnIconWrapper">
										
										<div id="selectedImgWrapper">
											<div class="imageWrapper" data-index="0" data-id="6">

											</div>
										</div>
									</div>

									<span class="btn_title absolute b-top" id="imageUploadBtn" data-status="0">Upload
										Photo</span>
								</div>
							</div>
							<div class="form-group">
								<input type="file" name="btnImages[]" id="btnImages" multiple="" class="d-none">

							</div>
						</div>
						<div class="col-6">
							<div class="form-group">
								<select class="form-control input_text" name="btnType" id="btnType">
									<option value="">---Select Button Option---</option>
									<option value="Promotion">Promotion</option>
									<option value="Shuttle">Shuttle bus service</option>
									<option value="Map">Map</option>
									<option value="Custom">Custom Text</option>
									<option value="Custom_Image">Custom Image</option>
								</select>
							</div>
							<div class="from-group option" id="promotion_data" style="display: none;">
								<div class="form-group">

									<input type="text" name="promotion_title" id="promotion_title"
										class="form-control input_text" placeholder="Promotion Title">
								</div>
								<div class="form-group">
									<input type="text" name="promotion_amount" id="promotion_amount"
										class="form-control input_text" placeholder="Promotion Amount">
								</div>
								<div class="form-group">
									<textarea class="form-control" rows="10" style="width: 100%;"
										id="promotion_description" name="promotion_description input_text"
										placeholder="Promotion Description"></textarea>
								</div>
								<div class="form-group">
									<input type="text" name="promotion_code" id="promotion_code"
										class="form-control input_text" placeholder="Promotion Code">
								</div>
								<div class="form-group">
									<input type="text" name="promotion_url" id="promotion_url"
										class="form-control input_text" placeholder="Redeem Link">
								</div>
							</div>
							<div class="option" id="custom_data" style="display: none;">
								<div class="form-group">
									<input type="text" name="custom_button_title" id="custom_button_title"
										class="form-control input_text" placeholder="Enter Button Subtitle">
								</div>
								<div class="form-group">
									<textarea id="custom_data_txt">

									</textarea>
								</div>
								<div class="form-group">
									<input type="text" name="custom_button_link" id="custom_button_link"
										placeholder="Enter Button Link" class="form-control input_text" />
								</div>
							</div>
							
							<div class="option" id="custom_image_data" style="display: none;">
								<div class="form-group">
									<input type="text" name="custom_image_title" id="custom_image_title"
										class="form-control input_text" placeholder="Enter Button Subtitle">
								</div>
								<div class="form-group">
									<input type="text" name="custom_image_link" id="custom_image_link"
										placeholder="Enter Button Link" class="form-control input_text" />
								</div>
							</div>
							
							<div class="form-group option" id="map_image_upload" style="display: none;">
								<div class="form-group">
									<label>File Upload: </label>
									<input type="file" name="shuttle_image">
								</div>
							</div>
							<div class="form-group option" id="map_image_2" style="display: none;">
								<div class="form-group">
									<label>File Upload: </label>
									<input type="file" name="map_image">
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col">
							<span class="pull-right">
								<input type="hidden" name="encoded" value="">
								<input type="button" name="" value="Clear" class="btn btn-pill btn-primary mb-2 mt-2"
									id="clearBtn">
								<input type="button" name="" value="Cancel" class="btn btn-pill btn-primary mb-2 mt-2"
									id="cancelBtn">
								<input type="button" name="save" value="Save"
									class="btn btn-pill btn-primary  mb-2 mt-2" id="saveBtn">
							</span>
						</div>
					</div>

				</form>
			</div>
		</div>
	</div>
</div>

<div class="modal fade chooseIconModal" tabindex="-1" role="dialog" aria-labelledby="iconMenuModal" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<h4>Chose Icon:</h4>
				<hr />
				<?php for($i = 1;$i<=30;$i++): ?> <img src="/icons/<?php echo e($i); ?>.png" width="50" height="50" alt="" title=""
					class="selectable_image" data-imgname="<?php echo e($i); ?>">
					<?php endfor; ?>

			</div>
		</div>
	</div>
</div>

<div class="modal fade addNewPromotionModal" tabindex="-1" role="dialog" aria-labelledby="addNewPromotionModal"
	aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<form id="promotion_item_form" enctype="multipart/form-data">
					<input type="hidden" name="promotion_id" id="promotion_item_id">

				</form>

			</div>
		</div>
	</div>
</div>



<style type="text/css">
	.modal-lg.modal-addBtn {
		max-width: 950px !important;
		width: 950px !important;
	}

	.addBtn {
		font-size: 24px;
	}

	.btn {
		padding: 30px 10px;
		border-radius: 45px;
		width: 220px;
		margin: 10px;
		display: inline-block;
		transition: -webkit-transform 0.2s;
		transition: transform 0.2s;
		transition: transform 0.2s, -webkit-transform 0.2s;
		overflow: hidden;
		color: #6d6d6d;
		box-shadow: 1px 2px 10px -5px rgba(0, 0, 0, 0.63);
		background-color: white;
		border: none;
		text-align: center;
		cursor: pointer;
	}

	.btn img {
		margin: auto;
	}

	.btn .btn_title {
		font-size: 20px;
		margin-top: 5px;
	}

	.btn:hover {
		box-shadow: 1px 2px 10px -5px rgba(1, 1, 1, 1);
	}

	.buttons_group>.row {
		overflow-x: auto;
		white-space: nowrap;
	}

	.buttons_group-group>.row>.col-4 {
		display: inline-block;
		float: none;
	}

	.input_text {
		padding: 3px;
		font-size: 20px;
		border-radius: 20px;
		transition: -webkit-transform 0.2s;
		transition: transform 0.2s;
		transition: transform 0.2s, -webkit-transform 0.2s;
		overflow: hidden;
		color: #6d6d6d;
		box-shadow: 1px 2px 10px -5px rgba(0, 0, 0, 0.63);
		background-color: white;
		border: none;
		text-align: center;
	}

	.form-group.relative {
		position: relative;
	}

	.input_text option {
		text-align: center;
	}

	.btn.no-radius {
		margin: 0px !important;
		width: 100% !important;
		border-radius: 20px !important;
		position: relative;
		height: 140px;
		padding: 10px !important;
	}

	.btn_title.absolute {
		position: absolute;
		bottom: 5px;
		text-align: center;
		right: 50%;
		left: 50%;
		width: 100%;
		transform: translateX(-50%);
	}

	.b-top {
		border-top: 1px solid grey;
	}

	.imagesWrapper {
		white-space: nowrap;
		overflow-x: scroll;
		overflow-y: hidden;
		height: 95px;
	}

	.imageWrapper {
		height: 110px;
		margin: 10px 5px;
		display: inline !important;
		transition: -webkit-transform 0.2s;
		transition: transform 0.2s;
		transition: transform 0.2s, -webkit-transform 0.2s;
	}

	.selectable_image {
		padding: 10px;
	}

	#btnSelectedImg {
		margin-top: 25px;
		width: 50px !important;
		height: 50px !important;
		display: block;
	}

	#custom_data_txt {
		width: 100%;
		height: 250px;
		background: lightgrey;
	}

	#saveBtn,
	#cancelBtn {

		color: #fff !important;
		background-color: #20a8d8 !important;
		border-color: #20a8d8 !important;
		border-radius: 50em !important;
		padding: 10px !important;
		width: 100px !important;
	}

	#btnType {
		text-align: center;
		text-align-last: center;
	}

	.ellipsis {
		text-overflow: ellipsis;
		white-space: nowrap;
		overflow: hidden;
		display: inline-block;
		max-width: 180px;

	}
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script type="text/javascript">
	const endPoint = "<?php echo e(route('cms.experience.index')); ?>";
		const savePromotionItem = "<?php echo e(route('cms.experience.upload_promotion_item_image')); ?>"
		const maxPromotions = 5;
		const deleteBtnImg = "<?php echo e(route('cms.experience.delete_image', ['id'=>''])); ?>";
</script>
<script type="text/javascript" src="<?php echo e(asset('js/custom/cms/experience.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('cms.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\XAMP\htdocs\sevr_hotel\resources\views/cms/experience/index.blade.php ENDPATH**/ ?>