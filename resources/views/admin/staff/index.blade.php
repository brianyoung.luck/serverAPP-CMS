@extends('admin.layouts.app')

@section('title', __('Staff'))

@section('content')

    <div id="dashboardWrapper">
    	<div class="row">
    		<div class="col-12">
    			<div class="card-header">{{ __('Staff Members') }}</div>
                 <div class="card-body">
                    <div class="row">
                        <div class="col-5">
                            <h3>Add New Staff Member</h3>
                            <form method="post" action="{{route('staff.create')}}">
                                @csrf()
                                @method('post')
                                <div class="form-group">
                                    <label for="name">Type</label>
                                    
                                    <select name="name" id="name" class="form-control" required="true">
                                    	<option value="Normal">Normal Admin</option>
                                    	<option value="Super Admin">Super Admin</option>
                                    </select>

                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" name="email" id="email" placeholder="Enter email" class="form-control" required="true" autocomplete="off">

                                </div>
                                <div class="form-group">
                                    <label for="name">Password</label>
                                    <input type="password" name="password" id="password" placeholder="Enter password" class="form-control" required="true" autocomplete="off">
                                </div>
                                <input type="submit" name="submit" value="Save" class="btn btn-info pull-right">
                            </form>
                        </div>
                        <div class="col">
                            <table class="table table-striped">
                                <thead class="thead-light">
                                    <th>Type</th>
                                    <th>Email</th>
                                    <th>Action</th>
                                </thead>
                                <tbody>
                                    @foreach($staff as $user)
                                    <tr>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td><a href="{{route('admin.staff.delete',['id' => $user->id])}}" class="btn btn-sm btn-danger">Delete</a></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>        
                        </div>
                    </div>
                    
               
    	</div>
    </div>
@stop

@section('js')

@stop