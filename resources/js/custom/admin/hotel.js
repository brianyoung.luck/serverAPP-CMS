const hotelTable = $('#hotelTable');
const optionsDateTime = {year: 'numeric', month: 'short', day: 'numeric', hour: '2-digit', minute: '2-digit'};
const renewBtn = $('#renewBtn');
const extendmonthTime = $('#extendmonthTime');
const updateHotelForm = $('#updateHotelForm');
const deleteHotelBtn = $('#deleteHotelBtn');
const typeUpdateHotel = $('#typeUpdateHotel');
const buttonCreateHotel = $('#buttonCreateHotel');
const createHotelForm = $('#createHotelForm');

const extendTrial = $("#extendTrial")

const isCheckInEnabled = $('#is_check_in_enabled');
const isCheckOutEnabled = $('#is_check_out_enabled');
const isSpaEnabled = $('#is_spa_enabled');
const isRestaurantEnabled = $('#is_restaurant_enabled');
const isConciergeEnabled = $('#is_concierge_enabled');
const isCleaningEnabled = $('#is_cleaning_enabled');
const spa_treatment_enable = $("#spa_treament");
const spa_room_service = $("#spa_room_service");
$(document).ready(function () {
    hotelTable.DataTable({
        lengthMenu: [[ -1, 10, 25, 50 ], [ 'All', '10', '25', '50' ]],
        order: [[ 0, "desc" ]],
        language: {
            searchPlaceholder: 'Name or desc...'
        },
        processing: true,
        serverSide: true,
        ajax: {
            url: url1,
            data: function (d) {

            }
        },
        columns: [
            {
                data: 'id',
                name: 'id',
                render: function (data) {
                    return data;
                }
            },
            {
                data: 'name',
                name: 'name',
                render: function (data) {
                    return data;
                }
            },
            {
                data: 'description',
                name: 'description',
                render: function (data) {
                    return data;
                }
            },
            {
                data: 'is_active',
                name: 'is_active',
                render: function (data) {
                    return data ? `<span class="badge badge-pill badge-success">Active</span>`:
                        `<span class="badge badge-pill badge-warning">Inactive</span>`;
                }
            },
            {
                data: 'expired_at',
                name: 'expired_at',
                render: function (data) {
                    return new Date(data).toLocaleDateString("en-US", optionsDateTime);
                }
            },
            {
                data: 'id',
                name: 'id',
                orderable: false,
                render: function (data) {
                    return `
                        <a href="${url2}/${data}" class="btn-action-table"><i class="icon-info icons" ></i></a>
                    `;
                }
            },
        ]
    });

    extendTrial.click(function(){
        renewHotel();
    });

    renewBtn.click(function () {
        renewHotel();
    });


    deleteHotelBtn.click(function () {
        deleteHotel();
    });
    buttonCreateHotel.click(function () {
        createHotel();
    });

    isCheckInEnabled.change(async function () {
        await updateHotelFeature();
    });
    spa_treatment_enable.change(async function(){
        console.log("SPA Room Service: ",$(spa_room_service).is(":checked"));
        console.log("SPA Treatment: ",$(spa_treatment_enable).is(":checked"));
        if((!spa_room_service.is(":checked") && !spa_treatment_enable.is(":checked"))){
            if($(is_spa_enabled).is(":checked")){
                $(is_spa_enabled).trigger('click');
            }
        }else{
            if(!$(is_spa_enabled).is(":checked")){
                $(is_spa_enabled).trigger('click');
            }
        }

        await updateHotelFeature();
    });
    spa_room_service.change(async function(){
        if((!spa_room_service.is(":checked") && !spa_treatment_enable.is(":checked"))){
            if($(is_spa_enabled).is(":checked")){
                $(is_spa_enabled).trigger('click');
            }
        }else{
            if(!$(is_spa_enabled).is(":checked")){
                $(is_spa_enabled).trigger('click');
            }
        }
        await updateHotelFeature();
    });
    isCheckOutEnabled.change(async function () {

        await updateHotelFeature();
    });
    isSpaEnabled.change(async function () {
        await updateHotelFeature();
    });
    isRestaurantEnabled.change(async function () {
        await updateHotelFeature();
    });
    isConciergeEnabled.change(async function () {
        await updateHotelFeature();
    });
    isCleaningEnabled.change(async function () {
        await updateHotelFeature();
    });
});
         
function renewHotel()
{
    Swal.fire({
        title: 'Are you sure?',
        type: 'question',
        showCancelButton: true,
        html:'<div class="form-group">\
        <label for="sel1">Select Month:</label>\
        <select class="form-control" id="monthTime">\
          <option value="1">1 Month</option>\
          <option value="2">2 Month</option>\
          <option value="3">3 Month</option>\
          <option value="4">4 Month</option>\
          <option value="5">5 Month</option>\
          <option value="6">6 Month</option>\
          <option value="7">7 Month</option>\
          <option value="8">8 Month</option>\
          <option value="9">9 Month</option>\
          <option value="10">10 Month</option>\
          <option value="11">11 Month</option>\
          <option value="12">12 Month</option>\
        </select>\
      </div>',
        confirmButtonColor: success_button_color,
        cancelButtonColor: cancel_button_color,
        confirmButtonText: 'Renew',
        reverrenewHotelseButtons: true,
    }).then((result) => {
        if (result.value && result.value === true) {
            renewBtn.html(`<i class="fa fa-refresh fa-spin fa-fw"></i> Please wait...`).attr('disabled', 'disabled');
          
            var  monthvalue = $('#monthTime').val();
            extendmonthTime.val(monthvalue);
            typeUpdateHotel.val('renew');
            updateHotelForm.submit();
        }
    });
}


function deleteHotel()
{
    Swal.fire({
        title: 'Are you sure?',
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: delete_button_color,
        cancelButtonColor: cancel_button_color,
        confirmButtonText: 'Inactive',
        reverseButtons: true,
    }).then((result) => {
        if (result.value && result.value === true) {
            deleteHotelBtn.html(`<i class="fa fa-refresh fa-spin fa-fw"></i> Please wait...`).attr('disabled', 'disabled');
            typeUpdateHotel.val('inactive');
            updateHotelForm.submit();
        }
    });
}

function createHotel()
{
    buttonCreateHotel.html(`<i class="fa fa-refresh fa-spin fa-fw"></i> Please wait...`).attr('disabled', 'disabled');
    createHotelForm.submit();
}

async function updateHotelFeature()
{
    let promise = new Promise(function(resolve, reject) {
        $.ajax({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
            },
            type: "PUT",
            url: url3 + '/' + id1,
            contentType: "application/json",
            dataType: 'json',
            data: JSON.stringify({
                'is_check_in_enabled': isCheckInEnabled.is(":checked"),
                'is_check_out_enabled': isCheckOutEnabled.is(":checked"),
                'is_spa_enabled': isSpaEnabled.is(":checked"),
                'is_restaurant_enabled': isRestaurantEnabled.is(":checked"),
                'is_concierge_enabled': isConciergeEnabled.is(":checked"),
                'is_cleaning_enabled': isCleaningEnabled.is(":checked"),
                'spa_treament' : spa_treatment_enable.is(":checked"),
                'spa_room_service' : spa_room_service.is(":checked"),
            }),
        })
        .done(function (response) {
            resolve(response);
        })
        .fail(function (error) {
            console.log(error);
            reject();
        });
    });

    try {
        return await promise;
    } catch(err) {
        return {
            'status': false,
            'error': err
        };
    }
}