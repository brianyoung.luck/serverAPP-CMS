const guestTable = $("#guestTable");
const optionsDateTime = {
  year: "numeric",
  month: "short",
  day: "numeric",
  hour: "2-digit",
  minute: "2-digit",
};
const deleteHotelBookingForm = $("#deleteHotelBookingForm");
const hotelBookingId = $("#hotelBookingId");
const hotelBookingIdUpdate = $("#hotelBookingIdUpdate");
const editHotelBookingForm = $("#editHotelBookingForm");
const guestProfileModal = $("#guestProfileModal");
const userProfileModal = $(".userProfileModal");

const guestName = $("#guestName");
const guestReference = $("#guestReference");
const guestArrivalDate = $("#guestArrivalDate");
const guestDepartureDate = $("#guestDepartureDate");
const guestCardExpiryDate = $("#guestCardExpiryDate");
const guestCardholderName = $("#guestCardholderName");
const guestCardAddress = $("#guestCardAddress");
const guestPassportEntriesWrapper = $("#guestPassportEntriesWrapper");

const newGuestName = $("#newGuestName");
const newGuestReference = $("#newGuestReference");
const newGuestArrivalDate = $("#newGuestArrivalDate");
const newGuestDepartureDate = $("#newGuestDepartureDate");
const newGuestCardExpiryDate = $("#newGuestCardExpiryDate");
const newGuestCardholderName = $("#newGuestCardholderName");
const newGuestCardAddress = $("#newGuestCardAddress");
const newGuestPassportEntriesWrapper = $("#newGuestPassportEntriesWrapper");

const declineBtn = $(".declineBtn");
const acceptBtn = $(".acceptBtn");
const checkoutBtn = $(".checkoutBtn");

const checkoutHotelBookingForm = $("#checkoutHotelBookingForm");
const hotelBookingIdCheckout = $("#hotelBookingIdCheckout");
const roomNumber = $("#roomNumber");
const lateCheckoutHotelBookingForm = $("#lateCheckoutHotelBookingForm");
const hotelBookingIdLateCheckout = $("#hotelBookingIdLateCheckout");
const statusLateCheckout = $("#statusLateCheckout");
const lateCheckoutBtn = $(".lateCheckoutBtn");
const guestCardNumber = $("#guestCardNumber");
const newGuestCardCardNumber = $("#newGuestCardCardNumber");
const userProfileText = $(".userProfileText");

let showModalType;

$(document).ready(function() {
  // guestTable.DataTable({
  //   lengthMenu: [[10, 25, 50, -1], ["10", "25", "50", "All"]],
  //   order: [[0, "desc"]],
  //   language: {
  //     searchPlaceholder: "Name/booking ref",
  //   },
  //   processing: true,
  //   serverSide: true,
  //   ajax: {
  //     url: url1,
  //     data: function(d) {},
  //   },
  //   columns: [
  //     {
  //       data: "cardholder_name",
  //       name: "cardholder_name",
  //       render: function(data) {
  //         return data;
  //       },
  //     },
  //     {
  //       data: "reference",
  //       name: "reference",
  //       render: function(data) {
  //         return data;
  //       },
  //     },
  //     {
  //       data: "room_number",
  //       name: "room_number",
  //       render: function(data) {
  //         return data ? data : "-";
  //       },
  //     },
  //     {
  //       data: "arrival_date",
  //       name: "arrival_date",
  //       render: function(data) {
  //         return new Date(data).toLocaleDateString("en-US", optionsDateTime);
  //       },
  //     },
  //     {
  //       data: "departure_date",
  //       name: "departure_date",
  //       render: function(data) {
  //         return new Date(data).toLocaleDateString("en-US", optionsDateTime);
  //       },
  //     },
  //     {
  //       data: "status",
  //       name: "status",
  //       render: function(data) {
  //         if (data == "active") {
  //           return '<span class="badge badge-success">Active</span>';
  //         } else {
  //           return '<span class="badge badge-warning">Checked out</span>';
  //         }
  //       },
  //     },
  //     {
  //       data: "id",
  //       name: "id",
  //       orderable: false,
  //       render: function(data) {
  //         return `
  //                     <a class="btn-action-table" data-id="${data}"><i class="icon-info icons" ></i></a>
  //                 `;
  //       },
  //     },
  //     {
  //       data: "reference",
  //       name: "reference",
  //       visible: false,
  //       orderable: false,
  //     },
  //   ],
  // });

  $(document).on("click", ".acceptBtn", function() {
    accept($(this).data("id"));
  });
  $(document).on("click", ".declineBtn", function() {
    decline($(this).data("id"));
  });
  $(document).on("click", ".checkoutBtn", function() {
    checkout($(this).data("id"));
  });
  $(document).on("click", ".lateCheckoutBtn", function() {
    lateCheckout($(this).data("id"));
  });
  // $(document).on("click", ".btn-action-table", async function() { await showModalGuestDetails($(this).data('id')); });
  $(document).on("click", ".newArrivalWrapper", async function() {
    await showModalNewGuestDetails($(this).data("id"), "new_arrival");
  });
  $(document).on("click", ".lateCheckoutWrapper", async function() {
    await showModalNewGuestDetails($(this).data("id"), "late_checkout");
  });

  // Append modal to body
  userProfileModal.appendTo("body");
  guestProfileModal.appendTo("body");
  userProfileModal.on("shown.bs.modal", function() {
    $(document).off("focusin.modal");
  });

  guestProfileModal.on("hidden.bs.modal", function(e) {
    // Change guest details to loading
    resetGuestDetails();
  });
  userProfileModal.on("hidden.bs.modal", function(e) {
    // Change guest details to loading
    resetNewGuestDetails();
  });
});

function accept(id) {
  if (showModalType === "late_checkout") {
    Swal.fire({
      title: "Are you sure?",
      type: "question",
      showCancelButton: true,
      confirmButtonColor: success_button_color,
      cancelButtonColor: cancel_button_color,
      confirmButtonText: "Accept",
      reverseButtons: true,
    }).then((result) => {
      if (result.value) {
        // Accept
        hotelBookingIdLateCheckout.val(id);
        statusLateCheckout.val("accepted");
        lateCheckoutHotelBookingForm.submit();
      }
    });
  } else {
    Swal.fire({
      title: "Are you sure?",
      type: "question",
      showCancelButton: true,
      confirmButtonColor: success_button_color,
      cancelButtonColor: cancel_button_color,
      confirmButtonText: "Accept",
      reverseButtons: true,
      input: "text",
      inputPlaceholder: "Enter room number",
      inputValue: "",
      inputValidator: (value) => {
        if (!value) {
          return "You need to set room number.";
        }
      },
      showLoaderOnConfirm: true,
      preConfirm: (roomNumber) => {
        return fetch(`/ajax/is-room-number-not-used/?room_number=${roomNumber}`)
          .then((response) => {
            if (!response.ok) {
              throw new Error(response.statusText);
            }
            return response.json();
          })
          .then(function(myJson) {
            if (!myJson.status) {
              Swal.showValidationMessage("Room already checked in.");
            }
          })
          .catch((error) => {
            console.log(error);
          });
      },
      allowOutsideClick: () => !Swal.isLoading(),
    }).then((result) => {
      if (result.value) {
        roomNumber.val(result.value);
        hotelBookingIdUpdate.val(id);
        editHotelBookingForm.submit();
      }
    });
  }
}

function decline(id) {
  if (showModalType === "late_checkout") {
    Swal.fire({
      title: "Are you sure?",
      type: "question",
      showCancelButton: true,
      confirmButtonColor: delete_button_color,
      cancelButtonColor: cancel_button_color,
      confirmButtonText: "Decline",
      reverseButtons: true,
    }).then((result) => {
      if (result.value && result.value === true) {
        // Reject
        hotelBookingIdLateCheckout.val(id);
        statusLateCheckout.val("rejected");
        lateCheckoutHotelBookingForm.submit();
      }
    });
  } else {
    Swal.fire({
      title: "Are you sure?",
      type: "question",
      showCancelButton: true,
      confirmButtonColor: delete_button_color,
      cancelButtonColor: cancel_button_color,
      confirmButtonText: "Decline",
      reverseButtons: true,
    }).then((result) => {
      if (result.value && result.value === true) {
        hotelBookingId.val(id);
        deleteHotelBookingForm.submit();
      }
    });
  }
}
async function getGuestDetails(hotelBookingId) {
  let promise = new Promise(function(resolve, reject) {
    $.ajax({
      headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
      },
      type: "GET",
      url: url4 + "/" + hotelBookingId,
      contentType: "application/json",
      dataType: "json",
    })
      .done(function(response) {
        resolve(response);
      })
      .fail(function(error) {
        console.log(error);
        reject();
      });
  });

  try {
    return await promise;
  } catch (err) {
    return {
      status: false,
      error: err,
    };
  }
}

function fillGuestDetails(guestDetails) {
  $("#guestProfileModal .roomNumberText").html(guestDetails.room_number);
  guestName.html(guestDetails.cardholder_name);
  guestReference.html(guestDetails.reference);
  guestArrivalDate.html(dateTimeFormat(guestDetails.arrival_date));
  guestDepartureDate.html(dateTimeFormat(guestDetails.departure_date));
  guestCardExpiryDate.html(
    dateTimeFormat(guestDetails.card_expiry_date, "card_expiry_date")
  );
  guestCardholderName.html(guestDetails.cardholder_name);
  guestCardAddress.html(guestDetails.card_address);
  guestCardNumber.html(guestDetails.card_number);

  guestPassportEntriesWrapper.html(``);
  for (const passport_photo of guestDetails.passport_photos) {
    guestPassportEntriesWrapper.append(`
            <div class="passportEntryWrapper">
                <a href="${passport_photo.url}" target="_blank">
                    <img src="${passport_photo.url}" alt="Passport photo">
                </a>
            </div>
        `);
  }
  checkoutBtn.attr("data-id", guestDetails.id);
  lateCheckoutBtn.attr("data-id", guestDetails.id);

  if (guestDetails.late_check_out === "pending") {
    lateCheckoutBtn.addClass("d-inline");
    lateCheckoutBtn.removeClass("d-none");
  } else {
    lateCheckoutBtn.addClass("d-none");
    lateCheckoutBtn.removeClass("d-inline");
  }
}

function resetGuestDetails() {
  guestName.html(`<i class="fa fa-refresh fa-spin fa-fw"></i>`);
  guestReference.html(`<i class="fa fa-refresh fa-spin fa-fw"></i>`);
  guestArrivalDate.html(`<i class="fa fa-refresh fa-spin fa-fw"></i>`);
  guestDepartureDate.html(`<i class="fa fa-refresh fa-spin fa-fw"></i>`);
  guestCardExpiryDate.html(`<i class="fa fa-refresh fa-spin fa-fw"></i>`);
  guestCardholderName.html(`<i class="fa fa-refresh fa-spin fa-fw"></i>`);
  guestCardAddress.html(`<i class="fa fa-refresh fa-spin fa-fw"></i>`);

  guestPassportEntriesWrapper.html(``);
}

async function showModalGuestDetails(hotelBookingId) {
  // Show modal
  guestProfileModal.modal("show");

  // Call ajax to get guest details
  const guestDetails = await getGuestDetails(hotelBookingId);

  // Show guest details
  fillGuestDetails(guestDetails.guestDetails);
}

async function showModalNewGuestDetails(hotelBookingId, type) {
  // Show modal
  userProfileModal.modal("show");

  // Call ajax to get guest details
  const guestDetails = await getGuestDetails(hotelBookingId);

  // Change show modal type
  showModalType = type;

  // Show guest details
  fillNewGuestDetails(guestDetails.guestDetails);
}

function fillNewGuestDetails(guestDetails) {
  if (showModalType === "late_checkout") {
    userProfileText.html("Late checkout:");
  } else {
    userProfileText.html("New arrival:");
  }
  newGuestName.html(guestDetails.cardholder_name);
  newGuestReference.html(guestDetails.reference ? guestDetails.reference : "-");
  newGuestArrivalDate.html(dateTimeFormat(guestDetails.arrival_date));
  newGuestDepartureDate.html(dateTimeFormat(guestDetails.departure_date));
  newGuestCardExpiryDate.html(
    dateTimeFormat(guestDetails.card_expiry_date, "card_expiry_date")
  );
  newGuestCardholderName.html(guestDetails.cardholder_name);
  newGuestCardAddress.html(guestDetails.card_address);
  newGuestCardCardNumber.html(guestDetails.card_number);

  newGuestPassportEntriesWrapper.html(``);
  for (const passport_photo of guestDetails.passport_photos) {
    newGuestPassportEntriesWrapper.append(`
            <div class="passportEntryWrapper">
                <a href="${passport_photo.url}" target="_blank">
                    <img src="${passport_photo.url}" alt="Passport photo">
                </a>
            </div>
        `);
  }

  declineBtn.attr("data-id", guestDetails.id);
  acceptBtn.attr("data-id", guestDetails.id);
}

function resetNewGuestDetails() {
  newGuestName.html(`<i class="fa fa-refresh fa-spin fa-fw"></i>`);
  newGuestReference.html(`<i class="fa fa-refresh fa-spin fa-fw"></i>`);
  newGuestArrivalDate.html(`<i class="fa fa-refresh fa-spin fa-fw"></i>`);
  newGuestDepartureDate.html(`<i class="fa fa-refresh fa-spin fa-fw"></i>`);
  newGuestCardExpiryDate.html(`<i class="fa fa-refresh fa-spin fa-fw"></i>`);
  newGuestCardholderName.html(`<i class="fa fa-refresh fa-spin fa-fw"></i>`);
  newGuestCardAddress.html(`<i class="fa fa-refresh fa-spin fa-fw"></i>`);

  newGuestPassportEntriesWrapper.html(``);
}

function checkout(id) {
  Swal.fire({
    title: "Are you sure?",
    type: "question",
    showCancelButton: true,
    confirmButtonColor: success_button_color,
    cancelButtonColor: cancel_button_color,
    confirmButtonText: "Checkout",
    reverseButtons: true,
  }).then((result) => {
    if (result.value && result.value === true) {
      hotelBookingIdCheckout.val(id);
      checkoutHotelBookingForm.submit();
    }
  });
}

function lateCheckout(id) {
  Swal.fire({
    title: "Approve late checkout?",
    type: "question",
    showCancelButton: true,
    confirmButtonColor: success_button_color,
    cancelButtonColor: cancel_button_color,
    confirmButtonText: "Accept",
    cancelButtonText: "Reject",
    reverseButtons: true,
  }).then((result) => {
    if (result.dismiss && result.dismiss === "cancel") {
      // Reject
      hotelBookingIdLateCheckout.val(id);
      statusLateCheckout.val("rejected");
      lateCheckoutHotelBookingForm.submit();
    } else if (result.value && result.value === true) {
      // Accept
      hotelBookingIdLateCheckout.val(id);
      statusLateCheckout.val("accepted");
      lateCheckoutHotelBookingForm.submit();
    }
  });
}
