<?php

namespace App\Services;

use App\Exceptions\CustomForbiddenException;
use App\Models\Hotel;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Models\Experience_Button_Image;

class ExperienceService
{
    public function uploadImage($files, $promotion_item_index, $button_id)
    {
        $fileStorageService = new FileStorageService(env('FILESYSTEM_DRIVER'));
        $images = [];

        foreach ($files as  $image) {
            $filename = HelperService::generateRandomString(8) . '.' .$fileStorageService->getExtension($image);
            // Upload file
            $url = $fileStorageService->uploadFile($image, 'restaurant-dish-galleries', $filename);
            $image = new Experience_Button_Image;
            $image->experience_button_id = $button_id;
            $image->path = $url;
            $image->type = "Item_Related";
            $image->reference_id = $promotion_item_index;
            $image->save();
            array_push($images, $image);
        }
        return $image;
    }

    public function uploadBtnImages($request, $button_id)
    {
        $fileStorageService = new FileStorageService(env('FILESYSTEM_DRIVER'));

        $images = [];
        foreach ($request->file('btnImages') as  $image) {
            $filename = HelperService::generateRandomString(8) . '.' .$fileStorageService->getExtension($image);
            // Upload file
            $url = $fileStorageService->uploadFile($image, 'experience_buttons', $filename);
            $image = new Experience_Button_Image;
            $image->experience_button_id = $button_id;
            $image->path = $url;
            $image->type = "Button";
            $image->reference_id = $button_id;
            $image->save();
            array_push($images, $image);
        }
        return $images;
    }

    public function uploadBtnImage($image, $button_id)
    {
        $filename = HelperService::generateRandomString(8) . '.' .$fileStorageService->getExtension($image);
        // Upload file
        $url = $fileStorageService->uploadFile($image, 'experience_buttons', $filename);
        $image = new Experience_Button_Image;
        $image->experience_button_id = $button_id;
        $image->path = $url;
        $image->type = "Item_Related";
        $image->reference_id = $button_id;
        $image->save();
        return $image;
    }
}