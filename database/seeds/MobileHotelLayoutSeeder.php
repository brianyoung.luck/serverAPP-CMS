<?php

use Illuminate\Database\Seeder;
use App\Models\MobileHotelLayout;

class MobileHotelLayoutSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Default
        MobileHotelLayout::create([
            'checkin_image' =>public_path() . '/images/hotel/check_in_icon@3x.png',
            'checkout_image' =>public_path() . '/images/hotel/check_out_icon@3x.png',
            'restaurant_image' =>public_path() . '/images/hotel/restaurant_icon@3x.png',
            'spa_image' => public_path() . '/images/hotel/spa_icon@3x.png',
            'concierge_image' => public_path() . '/images/hotel/concierge_icon@3x.png',
            'experience_image' =>public_path() . '/images/hotel/setting_icon@3x.png',
            'photo_preview_url' => 'https://servrhotels.s3.ap-southeast-1.amazonaws.com/appearances/themes/1.png?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAUEWTMAAIH52ANYWP%2F20200706%2Fap-southeast-1%2Fs3%2Faws4_request&X-Amz-Date=20200706T095601Z&X-Amz-SignedHeaders=host&X-Amz-Expires=7200&X-Amz-Signature=b89288ec876aca139550a4bbc87d7f61866afe125fe2f5c27b0a1bc74f1902f5',
            'layout_name'=>'default',
            'hotel_id'=>0
        ]);
    }
}