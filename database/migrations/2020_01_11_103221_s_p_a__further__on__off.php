<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SPAFurtherOnOff extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hotel_features',function(BluePrint $table){
            $table->tinyInteger('spa_treatment')->default(1);
            $table->tinyInteger('spa_room_service')->default(1);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table->dropColumn('spa_treatment');
        $table->dropColumn('spa_room_service');
    }
}
