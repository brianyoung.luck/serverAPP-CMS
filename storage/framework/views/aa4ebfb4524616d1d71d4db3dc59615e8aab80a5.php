<?php $__env->startSection('title', 'Concierge'); ?>

<?php $__env->startSection('content'); ?>
<div id="conciergeWrapper">
    <div class="row">
        <div class="col-md-6 col-12">
            
            <h3 class="mb-4"><a href="<?php echo e(url('concierges')); ?>"><i class="fa fa-arrow-left text-dark"
                        style="font-size: 18px !important;"></i></a>
                &nbspConcierge menu:</h3>
            <div class="row">
                <div class="col-12 editConciergeServiceWrapper">
                    <div class="row">
                        <div class="col-12 editConciergeServiceLabelWrapper mb-1">
                            <label>Edit items:</label>
                        </div>
                        <div class="col-12">
                            <div class="card">
                                <div class="starterWrapper d-inline-block">
                                    <div class="d-inline-block">
                                        
                                    </div>
                                    <div class="d-inline-block pull-right">
                                        <button class="btn btn-pill btn-primary" type="button"
                                            id="addConciergeServiceBtn">
                                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                </div>

                                <!-- Modal -->
                                <div class="modal fade addConciergeServiceModal" tabindex="-1" role="dialog"
                                    aria-labelledby="addConciergeServiceModal" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <h4>Add menu:</h4>
                                                <hr />
                                                <form id="addConciergeServiceForm" method="post"
                                                    action="<?php echo e(route('cms.concierges.store')); ?>"
                                                    enctype="multipart/form-data">
                                                    <?php echo csrf_field(); ?>
                                                    <?php echo method_field('post'); ?>

                                                    <div class="form-group">
                                                        <label for="name">Name</label>
                                                        <input class="form-control" id="name" name="name" type="text">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="description">Description</label>
                                                        <textarea class="form-control" id="description"
                                                            name="description" rows="4" placeholder=""></textarea>
                                                    </div>
                                                </form>
                                                <button class="btn btn-pill btn-block btn-secondary btn-lg"
                                                    type="button" id="storeNewConciergeServiceBtn">Add</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade editConciergeServiceModal" tabindex="-1" role="dialog"
                                    aria-labelledby="editConciergeServiceModal" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <h4>Edit menu:</h4>
                                                <hr />
                                                <form id="editConciergeServiceForm" method="post"
                                                    action="<?php echo e(route('cms.concierges.update', ['concierge' => 0])); ?>"
                                                    enctype="multipart/form-data">
                                                    <?php echo csrf_field(); ?>
                                                    <?php echo method_field('put'); ?>
                                                    <input type="hidden" name="concierge_service_id"
                                                        id="concierge_service_id" value="0">

                                                    <div class="form-group">
                                                        <label for="nameUpdate">Name</label>
                                                        <input class="form-control" id="nameUpdate" name="nameUpdate"
                                                            type="text">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="descriptionUpdate">Description</label>
                                                        <textarea class="form-control" id="descriptionUpdate"
                                                            name="descriptionUpdate" rows="4" placeholder=""></textarea>
                                                    </div>
                                                </form>
                                                <form id="deleteConciergeServiceForm" method="post"
                                                    action="<?php echo e(route('cms.concierges.destroy', ['concierge' => 0])); ?>"
                                                    enctype="multipart/form-data">
                                                    <?php echo csrf_field(); ?>
                                                    <?php echo method_field('delete'); ?>
                                                    <input type="hidden" name="concierge_service_id"
                                                        id="concierge_service_id" value="0">
                                                </form>

                                                <div class="row mt-4">
                                                    <div class="col-6">
                                                        <button class="btn btn-pill btn-block btn-danger btn-lg"
                                                            type="button" id="deleteConciergeServiceBtn">Delete</button>
                                                    </div>
                                                    <div class="col-6">
                                                        <button class="btn btn-pill btn-block btn-secondary btn-lg"
                                                            type="button" id="updateConciergeServiceBtn">Update</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="conciergeServicesTableWrapper">
                                    <table class="table table-responsive-sm conciergeServicesTable">
                                        <tbody>
                                            <?php $__currentLoopData = $conciergeServices; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $conciergeService): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr data-concierge_service_id="<?php echo e($conciergeService->id); ?>"
                                                class="menuConciergeServiceTr">
                                                <td><?php echo e($conciergeService->name); ?></td>

                                            </tr>

                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<script>
    const url4 = '<?php echo route('ajax.cms.concierge-services-requests.index'); ?>';
    const url5 = '<?php echo route('ajax.cms.concierge-services.index'); ?>';
</script>
<?php $__env->startSection('js'); ?>
<script src="<?php echo e(asset('js/custom/cms/concierge.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('cms.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\XAMP\htdocs\sevr_hotel\resources\views/cms/concierge/concierge-menu.blade.php ENDPATH**/ ?>