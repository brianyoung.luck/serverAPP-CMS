const buttonForgot = $('#buttonForgot');
const forgotPasswordForm = $('#forgotPasswordForm');
const email = $('#email');

$(document).ready(function() {
    buttonForgot.click(login);
    email.keyup(function(e) { checkFormValidation(e) });
});

function login()
{
    // Show loading
    showLoadingButton(true);

    // Submit
    forgotPasswordForm.submit();
}

function showLoadingButton(is_show = true)
{
    if (is_show === true) {
        buttonForgot.html(`<i class="fa fa-refresh fa-spin fa-fw"></i> Please wait...`).attr('disabled', 'disabled');
    } else {
        buttonForgot.html(`Send reset link`).attr('disabled', false);
    }
}

function isAllFormFilled()
{
    let current = 0;
    const complete = 1;

    if (email.val() !== '') {
        current += 1;
    }

    return current === complete;
}

function checkFormValidation(e)
{
    if (isAllFormFilled() === true) {
        buttonForgot.prop('disabled', false);
        if(e.which === 13) {
            buttonForgot.click();
        }
    } else {
        buttonForgot.prop('disabled', true);
    }
}