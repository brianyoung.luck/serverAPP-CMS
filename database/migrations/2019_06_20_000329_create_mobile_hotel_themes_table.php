<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMobileHotelThemesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mobile_hotel_themes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('primary_color');
            $table->string('secondary_color');
            $table->string('check_in_color');
            $table->string('check_out_color');
            $table->string('restaurant_color');
            $table->string('spa_color');
            $table->string('concierge_color');
            $table->string('cleaning_color');
            $table->string('photo_preview_url');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mobile_hotel_themes');
    }
}
