<?php

use App\Models\Spa;
use Illuminate\Database\Seeder;

class SpaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Spa::create([
            'hotel_id' => 1,
            'name' => 'SPA 1',
            'logo_url' => 'http://lorempixel.com/200/200',
        ]);
        Spa::create([
            'hotel_id' => 1,
            'name' => 'SPA 2',
            'logo_url' => 'http://lorempixel.com/200/200',
        ]);
        Spa::create([
            'hotel_id' => 1,
            'name' => 'SPA 3',
            'logo_url' => 'http://lorempixel.com/200/200',
        ]);
    }
}
