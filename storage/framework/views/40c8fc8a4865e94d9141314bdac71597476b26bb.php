<?php $__env->startSection('title', __('Login CMS')); ?>

<?php $__env->startSection('content'); ?>
<div id="login_wrapper" class="row justify-content-center">
    <div class="col-xl-5 col-lg-6 col-md-8 col-sm-10">
        <div class="card-group">
            <div class="card p-4">
                <div class="card-body " style="width: 430px !important;">
                    <div class="admin-panel-text">
                        <h2><?php echo e(env('APP_NAME')); ?></h2>
                        <h3><?php echo e(__('Admin Hotel')); ?></h3>
                    </div>
                    <?php echo $__env->make('cms.layouts.flash-message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    <form id="loginForm" class="form-horizontal" action="
                        <?php if($for == 'hotel_admin'): ?>
                        <?php echo e(route('cms.do-login')); ?>

                        <?php else: ?>
                        <?php echo e(route ('cms.do-staff-login')); ?>

                        <?php endif; ?>
                        " method="post">
                        <?php echo csrf_field(); ?>
                        <?php echo method_field('post'); ?>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <input class="form-control <?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>"
                                        id="email" type="email" name="email" placeholder="Email">
                                    <?php if($errors->has('email')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <input class="form-control <?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>"
                                        id="password" type="password" name="password" placeholder="Password">
                                    <span class="fa fa-fw fa-eye-slash field-icon toggle-password"></span>
                                    <?php if($errors->has('password')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('password')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="row">
                        <div class="col-12">
                            <button class="btn btn-pill btn-block btn-primary" type="button" id="buttonLogin"
                                disabled><?php echo e(__('Login')); ?></button>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-12">
                            <a href="<?php echo e(route('cms.hotels.create')); ?>" class="btn btn-pill btn-block btn-primary mt-2"
                                style="font-size: 0.975rem;" id="buttonRegister" disabled><?php echo e(__('Register here')); ?></a>
                        </div>
                    </div>
                    <div id="forgotPasswordLinkWrapper">
                        <a href="<?php echo e(route('cms.forgot-password.index')); ?>"><?php echo e(__('Forgot password?')); ?></a>
                    </div>
                    <div class="row mt-2">
                        <div class="col-6">
                            <img class="btnPlayStore" style="cursor: pointer;"
                                src="https://static.wixstatic.com/media/d9af4b_2b80996b943f4eca80991da9bf14c841~mv2.png/v1/fill/w_154,h_52,al_c,q_85,usm_0.66_1.00_0.01/google%20play%20store%20button%201.webp"
                                alt="https://www.servrhotels.com">
                        </div>
                        <div class="col-6">
                            <img class="btnAppStore" style="cursor: pointer;"
                                src="https://static.wixstatic.com/media/d9af4b_ed82a02f095748c9b2cb9999c0a6eb11~mv2.png/v1/fill/w_146,h_52,al_c,q_85,usm_0.66_1.00_0.01/app%20store%20button%201.webp"
                                alt="https://www.servrhotels.com">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script>
    const url1 = '<?php echo route('cms.dashboard.index'); ?>';
</script>
<script src="<?php echo e(asset('js/custom/cms/login.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('cms.layouts.auth', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\XAMP\htdocs\sevr_hotel\resources\views/cms/auth/login/index.blade.php ENDPATH**/ ?>