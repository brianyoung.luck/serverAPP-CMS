<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use File;

class Invoice extends Mailable
{
    use Queueable, SerializesModels;

    private $bookingBill;
    private $hotel;
    private $total_price = 0;
    private $service_charges=0;
    private $vat=0;
    private $vat_price=0;
    private $service_charges_price=0;
    private $total_price_after_tax=0;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($bookingBill, $hotel, $guestDetails, $service_charges, $vat, $vat_price, $service_charges_price, $total_price_after_tax)
    {
        $this->bookingBill = $bookingBill;
        $this->hotel = $hotel;
        $this->guestDetails= $guestDetails;
        $this->service_charges= $service_charges;
        $this->vat= $vat;
        $this->vat_price= $vat_price;
        $this->service_charges_price= $service_charges_price;
        $this->total_price_after_tax= $total_price_after_tax;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('cms.checkout.bill')->
        with(['bookingBill'=>$this->bookingBill,'hotel'=>$this->hotel,'total_price'=>'0','guestDetails'=>$this->guestDetails,'service_charges'=>$this->service_charges,'vat'=>$this->vat,'url'=>url('public/images/logo/servr.png')]);
    }
}