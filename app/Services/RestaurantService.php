<?php

namespace App\Services;

use App\Exceptions\CustomForbiddenException;
use App\Models\Hotel;
use App\Models\HotelBooking;
use App\Models\Restaurant;
use App\Models\RestaurantDish;
use App\Models\RestaurantDishCategory;
use App\Models\RestaurantGallery;
use App\Models\RestaurantReservation;
use App\Models\RestaurantRoomService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class RestaurantService
{
    /**
     * Update restaurant logo
     * @param Restaurant $restaurant
     * @param $file
     */
    public function updateLogo(Restaurant $restaurant, $file)
    {
        // Upload file
        $fileStorageService = new FileStorageService(env('FILESYSTEM_DRIVER'));
        $url = $fileStorageService->uploadFile($file, 'restaurants', $restaurant->id . '.png');

        // Update hotel details
        $restaurant->logo_url = $url;
        $restaurant->save();
    }

    /**
     * Update restaurant logo
     * @param Restaurant $restaurant
     * @param $files
     */
    public function updateRestaurantImages(Restaurant $restaurant, $files)
    {
        DB::transaction(function () use ($files, $restaurant) {
            $fileStorageService = new FileStorageService(env('FILESYSTEM_DRIVER'));

            // Delete all row
            //RestaurantGallery::where('restaurant_id', $restaurant->id)->delete();

            foreach ($files as $file) {
                // Create filename
                $filename = $restaurant->id . '_' . HelperService::generateRandomString(8) . '.' .$fileStorageService->getExtension($file);

                // Upload file
                $url = $fileStorageService->uploadFile($file, 'restaurant-galleries', $filename);

                // Insert to db
                RestaurantGallery::create([
                    'restaurant_id' => $restaurant->id,
                    'image_url' => $url,
                ]);
            }
        });
    }

    public function getRestaurantDishes(Restaurant $restaurant)
    {
        $dishes = RestaurantDish::findByRestaurantId($restaurant->id)->get();

        $grouped = $dishes->mapToGroups(function ($item) {
            return [
                $item['category'] => $item
            ];
        });

        $mappedGroup = collect([]);
        foreach ($grouped as $key => $value) {
            $mappedGroup->push([
                'key' => $key,
                'dishes' => $value->makeHidden([
                    'restaurant_id',
                    'created_at',
                    'updated_at',
                    'deleted_at',
                ]),
            ]);
        }

        // Arrange categories
        $starters = $mappedGroup->firstWhere('key', 'starter') ?? ['dishes' => []];
        $salads = $mappedGroup->firstWhere('key', 'salad') ?? ['dishes' => []];
        $meatsAndFish = $mappedGroup->firstWhere('key', 'meat_and_fish') ?? ['dishes' => []];
        $mains = $mappedGroup->firstWhere('key', 'main') ?? ['dishes' => []];
        $desserts = $mappedGroup->firstWhere('key', 'dessert') ?? ['dishes' => []];
        $drinks = $mappedGroup->firstWhere('key', 'drink') ?? ['dishes' => []];

        $restaurantMenu = collect([]);
        $restaurantMenu->push(collect(['name' => 'Starters'])->merge(collect($starters)->except('key')));
        $restaurantMenu->push(collect(['name' => 'Salads'])->merge(collect($salads)->except('key')));
        $restaurantMenu->push(collect(['name' => 'Meats and Fish'])->merge(collect($meatsAndFish))->except('key'));
        $restaurantMenu->push(collect(['name' => 'Mains'])->merge(collect($mains))->except('key'));
        $restaurantMenu->push(collect(['name' => 'Desserts'])->merge(collect($desserts))->except('key'));
        $restaurantMenu->push(collect(['name' => 'Drinks'])->merge(collect($drinks))->except('key'));

        return $restaurantMenu;
    }

   public function storeRestaurantDish($data,$image)
    {
        $restaurantDish = new RestaurantDish();
        $url = "";
        if($image == null){
            $url = env('CMS_URL').'/images/default.jpg';
        }else{
             $fileStorageService = new FileStorageService(env('FILESYSTEM_DRIVER'));
            $filename = $data['restaurant_id'] . '_' . HelperService::generateRandomString(8) . '.' .$fileStorageService->getExtension($image);
                // Upload file
                $url = $fileStorageService->uploadFile($image, 'restaurant-dish-galleries', $filename);
        }
        $restaurantDish->image = $url;
        $restaurantDish->fill($data);
        return $restaurantDish->save();
    }

    public function updateRestaurantDish(RestaurantDish $restaurantDish, $data, $image)
    {
        $restaurantDish->fill($data);
        if($image != null){
            $fileStorageService = new FileStorageService(env('FILESYSTEM_DRIVER'));
            $filename = $data['restaurant_id'] . '_' . HelperService::generateRandomString(8) . '.' .$fileStorageService->getExtension($image);
                // Upload file
                $url = $fileStorageService->uploadFile($image, 'restaurant-dish-galleries', $filename);
            $restaurantDish->image = $url;
        }
        $restaurantDish->save();
        return $restaurantDish;
    }

    public function createRestaurantReservation(HotelBooking $booking, $data)
    {
        $data['hotel_booking_id'] = $booking->id;
        return RestaurantReservation::create($data);
    }

    public function createRestaurantRoomOrder(HotelBooking $booking, $data)
    {
        return DB::transaction(function () use ($booking, $data) {
            $roomService = RestaurantRoomService::create([
                'hotel_booking_id' => $booking->id,
                'restaurant_id' => $data['restaurant_id'],
            ]);

            $total_price = 0;

            foreach ($data['orders'] as $order) {
                $dish = RestaurantDish::with('restaurant')->find($order['dish_id']);

                if ($dish->restaurant->hotel_id !== $booking->hotel->id) {
                    throw new CustomForbiddenException('Forbidden Access');
                }

                // Sum total
                $total_price += $dish->price * $order['qty'];

                $roomService->dishes()->attach($order['dish_id'], [ 'qty' => $order['qty'], 'price' => $dish->price, 'note' => $order['note'] ]);
            }

            $roomService->total_price = $total_price;
            $roomService->save();

            return $roomService->load('dishes');
        });
    }

    public function getRestaurantRoomOrder(HotelBooking $booking)
    {
        return $booking->restaurantRoomServices->load('');
    }

    public function updateRestaurant(Restaurant $restaurant, $data)
    {
        $restaurant->fill($data);
        return $restaurant->save();
    }

    public function createRestaurant($hotelId)
    {
        $restaurant = Restaurant::create([
            'hotel_id' => $hotelId,
            'name' => 'Your restaurant',
            'logo_url' => 'https://servrhotels.s3-ap-southeast-1.amazonaws.com/restaurants/default.jpeg',
        ]);

        return $restaurant;
    }

    public function deleteRestaurantGallery($restaurantGalleryId)
    {
        RestaurantGallery::where('id', $restaurantGalleryId)
            ->delete();
        return true;
    }
}