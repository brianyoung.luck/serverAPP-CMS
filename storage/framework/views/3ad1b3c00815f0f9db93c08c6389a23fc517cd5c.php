<?php $__env->startSection('title', $restaurant->name); ?>

<?php $__env->startSection('content'); ?>
<div id="restaurantWrapper">
    <h3 class="mb-4">Restaurant: <span id="restaurantNameText"><?php echo e($restaurant->name); ?>

        </span> &nbsp <i id="changeRestaurantName" class="icon-pencil icons"></i>
        &nbsp <i id="deleteRestaurantBtn" class="icon-trash icons"></i>
    </h3>
    <form id="deleteRestaurantForm" method="post"
        action="<?php echo e(route('cms.restaurants.destroy', ['restaurant' => $restaurant->id])); ?>"
        enctype="multipart/form-data">
        <?php echo csrf_field(); ?>
        <?php echo method_field('delete'); ?>
    </form>
    <form id="deleteRestaurantGalleryForm" method="post"
        action="<?php echo e(route('cms.restaurant-galleries.destroy', ['restaurant-gallery' => 0])); ?>"
        enctype="multipart/form-data">
        <?php echo csrf_field(); ?>
        <?php echo method_field('delete'); ?>
        <input type="hidden" name="restaurantGalleryId" id="restaurantGalleryId" value="0">
        <input type="hidden" name="restaurantId" id="restaurantId" value="<?php echo e($restaurant->id); ?>">
    </form>
    <div class="row">
        <div class="col-lg-4 col-md-5 col-12 restaurantLogoWrapper">
            <div id="restaurantLogoFormWrapper">
                
                <div class="alert alert-danger alert-dismissible fade show logo_file_error messageAlert" role="alert"
                    style="display: none;">
                    <p></p>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="form-group row">

                    <label class="col-md-5 col-form-label">

                        Main logo
                    </label>
                    <div class="col-md-7">

                        <button class="btn btn-shadow" id="uploadFileRestaurantLogoBtn">Upload file</button>
                        <p class="text-muted mt-3">maximum size :4 MB</p>
                        <form id="editRestaurantForm" method="post"
                            action="<?php echo e(route('cms.restaurants.update', ['id' => $restaurant->id])); ?>"
                            enctype="multipart/form-data">
                            <?php echo csrf_field(); ?>
                            <?php echo method_field('put'); ?>
                            <input type="file" name="logo" id="logo" class="d-none" accept="image/*">
                            <input type="hidden" name="updateType" id="updateType" value="">
                            <input type="hidden" name="restaurantNameUpdate" id="restaurantNameUpdate" value="">
                        </form>
                    </div>
                </div>
            </div>
            <div class="previewRestaurantLogo">
                <div class="row">
                    <div class="col-md-5">
                        <label>Preview</label>
                    </div>
                    <div class="col-md-7">
                        <div class="imgWrapper">
                            <?php if(!$restaurant->logo_url): ?>
                            <img src="<?php echo e(asset('images/hotel/img-placeholder.png')); ?>" alt="Restaurant gallery">
                            <?php else: ?>
                            <img src="<?php echo e($restaurant->logo_url); ?>" alt="Logo">

                            <?php endif; ?>

                        </div>
                    </div>
                </div>
            </div>

            <div id="restaurantImagesFormWrapper">
                
                <div class="alert alert-danger alert-dismissible fade show restImagesErrors messageAlert" role="alert"
                    style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="form-group row">
                    <label class="col-md-5 col-form-label">
                        Restaurant images:
                    </label>
                    <div class="col-md-7">
                        <button class="btn btn-shadow" id="uploadFileRestaurantImagesBtn">Upload file</button>

                        <p class="text-muted mt-3">maximum size :4 MB</p>
                        <form id="editRestaurantImagesForm" method="post"
                            action="<?php echo e(route('cms.restaurants.update', ['id' => $restaurant->id])); ?>"
                            enctype="multipart/form-data">
                            <?php echo csrf_field(); ?>
                            <?php echo method_field('put'); ?>
                            <input type="file" name="restaurantImages[]" id="restaurantImages" class="d-none"
                                accept="image/*" multiple>
                        </form>
                    </div>
                </div>
            </div>
            <div class="previewRestaurantImages">
                <div class="row">
                    <div class="col-md-5">
                        <label>Preview</label>
                    </div>
                    <div class="col-md-7">
                        <div class="imagesWrapper">
                            <?php $__currentLoopData = $restaurant->restaurantGalleries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gallery): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="imageWrapper" data-sort_id="<?php echo e($gallery->sort_id); ?>"
                                data-restaurant_gallery_id="<?php echo e($gallery->id); ?>"
                                data-restaurant_gallery_url="<?php echo e($gallery->image_url); ?>" id="<?php echo e($gallery->id); ?>">
                                <img src="<?php echo e($gallery->image_url); ?>" alt="Restaurant gallery">
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($restaurant->restaurantGalleries) == 0): ?>
                            <div class="imageWrapper">
                                <img src="<?php echo e(asset('images/hotel/img-placeholder.png')); ?>" alt="Restaurant gallery">
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-8 col-md-7 col-12 editMenuWrapper">
            <div class="row">
                <div class="col-lg-2 col-md-3 editMenuLabelWrapper">
                    <label>Edit menu:</label>
                </div>
                <div class="col-lg-10 col-md-9">
                    <div class="card">
                        <div class="starterWrapper d-inline-block">
                            <div class="d-inline-block mb-2 mt-2">
                                
                                <select class="form-control selectCategoryDish" id="selectCategoryDish">
                                    <option value="all">All</option>
                                    <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($category->id); ?>"><?php echo e($category->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                            <div class="d-inline-block pull-right">
                                <button class="btn btn-pill btn-primary  mb-2 mt-2" type="button"
                                    onclick="$('#addCategoryModal').modal('show');">
                                    <i class="fa fa-plus"></i> Add Category
                                </button>&nbsp;&nbsp;
                                <button class="btn btn-pill btn-primary  mb-2 mt-2" type="button"
                                    id="addMenuRestaurantBtn">
                                    <i class="fa fa-plus"></i> Add Dish
                                </button>
                            </div>
                        </div>

                        <!-- Modal -->
                        <div class="modal fade addRestaurantMenuModal" tabindex="-1" role="dialog"
                            aria-labelledby="addRestaurantMenuModal" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <h4>Add menu:</h4>
                                        <hr />
                                        <form id="addRestaurantMenuForm" method="post"
                                            action="<?php echo e(route('cms.restaurant-dishes.store')); ?>"
                                            enctype="multipart/form-data">
                                            <?php echo csrf_field(); ?>
                                            <?php echo method_field('post'); ?>
                                            <input type="hidden" name="restaurant_id" value="<?php echo e($restaurant->id); ?>">

                                            <div class="form-group">
                                                <label for="category">Category</label>
                                                <select class="form-control" id="category" name="category"
                                                    data-cat-id="0">
                                                    <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($category->id); ?>"><?php echo e($category->name); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="name">Name</label>
                                                <input class="form-control" id="name" name="name" type="text">
                                            </div>
                                            <div class="form-group">
                                                <label for="description">Description</label>
                                                <textarea class="form-control" id="description" name="description"
                                                    rows="4" placeholder=""></textarea>
                                            </div>
                                            <div class="form-group">
                                                <div class="alert alert-danger alert-dismissible fade show add_file_error messageAlert"
                                                    role="alert" style="display: none;">
                                                    <p></p>
                                                    <button type="button" class="close" data-dismiss="alert"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <label for="image">Image</label>
                                                <input type="file" name="image" id="image"
                                                    class="form-control restaurantAddMenuImage">
                                            </div>
                                            <p class="text-muted mt-3">maximum size :4 MB</p>
                                            <div class="form-group">
                                                <label for="price">Price</label>
                                                <div class="input-prepend input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><?php echo e($currency); ?></span>
                                                    </div>
                                                    <input class="form-control" id="price" name="price" size="16"
                                                        type="number" step="0.01" min="0">
                                                </div>
                                            </div>


                                            <button class="btn btn-pill btn-block btn-secondary btn-lg" type="button"
                                                id="storeNewRestaurantMenuBtn">Add</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal addCategoryModal" id="addCategoryModal" tabindex="-1" role="dialog"
                            aria-labelledby="addCategoryModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <h4>Add Category:</h4>
                                        <hr />
                                        <form id="addCategoryForm" method="post"
                                            action="<?php echo e(route('cms.restaurants.storecategory')); ?>"
                                            enctype="multipart/form-data">
                                            <?php echo csrf_field(); ?>
                                            <?php echo method_field('post'); ?>

                                            <div class="form-group">
                                                <label for="name">Name</label>
                                                <input class="form-control" id="name" name="name" type="text" required>
                                            </div>
                                        </form>
                                        <button class="btn btn-pill btn-block btn-secondary btn-lg"
                                            id="storeNewCategory">Add</button>
                                        <table class="table">
                                            <thead>
                                                <th>Name</th>
                                                <th>Action</th>
                                            </thead>
                                            <tbody>
                                                <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                    <td><?php echo e($category->name); ?></td>
                                                    <td><button type="button"
                                                            class="btn btn-pill btn-danger btn-sm del-cat"
                                                            data-catid="<?php echo e($category->id); ?>">Delete</button></td>
                                                </tr>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade editRestaurantMenuModal" tabindex="-1" role="dialog"
                            aria-labelledby="editRestaurantMenuModal" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <h4>Edit menu:</h4>
                                        <hr />
                                        <form id="editRestaurantMenuForm" method="post"
                                            action="<?php echo e(route('cms.restaurant-dishes.update', ['restaurant_dish' => 0])); ?>"
                                            enctype="multipart/form-data">
                                            <?php echo csrf_field(); ?>
                                            <?php echo method_field('put'); ?>
                                            <input type="hidden" name="restaurant_id" value="<?php echo e($restaurant->id); ?>">
                                            <input type="hidden" name="dish_id" id="dish_id" value="0">

                                            <div class="form-group">
                                                <label for="categoryUpdate">Category</label>
                                                <select class="form-control" id="categoryUpdate" name="categoryUpdate">
                                                    <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($category->id); ?>"><?php echo e($category->name); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="nameUpdate">Name</label>
                                                <input class="form-control" id="nameUpdate" name="nameUpdate"
                                                    type="text" >
                                            </div>
                                            <div class="form-group">
                                                <label for="descriptionUpdate">Description</label>
                                                <textarea class="form-control" id="descriptionUpdate"
                                                    name="descriptionUpdate" rows="4" placeholder=""></textarea>
                                            </div>

                                            <div class="form-group">
                                                <div class="alert alert-danger alert-dismissible fade show edit_file_error messageAlert"
                                                    role="alert" style="display: none;">
                                                    <p></p>
                                                    <button type="button" class="close" data-dismiss="alert"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <label for="image">Image</label>
                                                <input type="file" name="image" id="image"
                                                    class="form-control restaurantEditMenuImage">
                                            </div>
                                            <p class="text-muted mt-3">maximum size :4 MB</p>
                                            <div class="form-group">
                                                <label for="priceUpdate">Price</label>
                                                <div class="input-prepend input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">$</span>
                                                    </div>
                                                    <input class="form-control" id="priceUpdate" name="priceUpdate"
                                                        size="16" type="number" step="0.01" min="0">
                                                </div>
                                            </div>
                                        </form>
                                        <form id="deleteRestaurantMenuForm" method="post"
                                            action="<?php echo e(route('cms.restaurant-dishes.destroy', ['restaurant-dish' => 0])); ?>"
                                            enctype="multipart/form-data">
                                            <?php echo csrf_field(); ?>
                                            <?php echo method_field('delete'); ?>
                                            <input type="hidden" name="restaurant_id" value="<?php echo e($restaurant->id); ?>">
                                            <input type="hidden" name="dish_id" id="dish_id_delete" value="0">
                                        </form>

                                        <div class="row mt-4">
                                            <div class="col-6">
                                                <button class="btn btn-pill btn-block btn-danger btn-lg" type="button"
                                                    id="deleteRestaurantMenuBtn">Delete</button>
                                            </div>
                                            <div class="col-6">
                                                <button class="btn btn-pill btn-block btn-secondary btn-lg"
                                                    type="button" id="updateRestaurantMenuBtn">Update</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="menusTableWrapper">
                            <table class="table table-responsive-sm menusTable">
                                <tbody>
                                    <?php $__currentLoopData = $restaurant->dishes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dish): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr data-dish_id="<?php echo e($dish->id); ?>" class="menuDishTr">
                                        <td><?php echo e($dish->name); ?> <span class="pull-right"><?php echo e($currency); ?>

                                                <?php echo e($dish->price); ?></span></td>
                                    </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade showDetailsOrderModal" tabindex="-1" role="dialog"
            aria-labelledby="showDetailsOrderModal" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <h4 class="mb-4" id="roomText">Room 101 - Order details:</h4>
                        <form id="updateRestaurantRoomServiceForm" method="post"
                            action="<?php echo e(route('cms.restaurant-room-services.update', ['restaurantRoomService' => 0])); ?>"
                            enctype="multipart/form-data">
                            <?php echo csrf_field(); ?>
                            <?php echo method_field('put'); ?>
                            <input type="hidden" name="restaurant_room_services_id" id="restaurant_room_services_id"
                                value="0">
                            <input type="hidden" name="status" id="status" value="0">
                            <input type="hidden" name="signature" id="signature" value="0">
                        </form>
                        <div class="row">
                            <div class="col-7">

                                <table class="table table-responsive-sm" id="ordersTable">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Dish</th>
                                            <th>Qty</th>
                                            <th>Note</th>
                                            <th>Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Nasi goreng</td>
                                            <td>1</td>
                                            <td>-</td>
                                            <td>$12.01</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-5">
                                <div class="sigWrapper" style="margin-top: -77px;">
                                    <h4 class="d-inline">Customer Signature</h4>
                                    <button class="btn btn-pill btn-primary mb-3 float-right" id="clearSig" data-id="0"
                                        type="button" aria-pressed="true">Clear</button>
                                    <canvas id="mySign" style="border: 3px solid #5CB9EA;"></canvas>
                                    <div class="sigDev"></div>

                                </div>




                            </div>
                        </div>
                    </div>
                    <div class="row rowBtns">
                        <div class="col">
                            <button class="btn btn-pill btn-success btn-block" type="button"
                                id="processRestaurantServiceRequestConfirmBtn">Confirm</button>
                        </div>
                        <div class="col">
                            <button class="btn btn-pill btn-success btn-block" type="button"
                                id="processRestaurantServiceRequestPreparingBtn">Preparing</button>
                        </div>
                        <div class="col">
                            <button class="btn btn-pill btn-success btn-block" type="button"
                                id="processRestaurantServiceRequestOnTheWayBtn">On the way</button>
                        </div>
                        <div class="col">
                            <button class="btn btn-pill btn-success btn-block" type="button"
                                id="processRestaurantServiceRequestDoneBtn">Done</button>
                        </div>
                        <div class="col">
                            <button class="btn btn-pill btn-secondary btn-block" style="margin-top: 3px;"
                                id="declineRestaurantOrderRoomBtn" type="button">Reject</button>
                        </div>

                    </div>
                </div>


            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 pendingOrdersWrapper">
            <div class="row">
                <div class="col-lg-2 col-md-3 ordersLabelWrapper">
                    <label>Pending orders:</label>
                </div>
                <div class="col-lg-10 col-md-9 orderItemsWrapper">
                    <?php $__currentLoopData = $roomOrderPendings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $roomOrder): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="orderWrapper" data-order_details_id="<?php echo e($roomOrder->id); ?>">
                        <div class="roomTextWrapper">
                            <?php if($roomOrder->hotelBooking->room_number): ?>
                            <h4><?php echo e($roomOrder->hotelBooking->room_number); ?></h4>
                            <?php else: ?>
                            <h4>Room -</h4>
                            <?php endif; ?>
                        </div>
                        <div class="orderDishWrapper">
                            <ul>
                                <?php for($i=0, $j=1; $i<count($roomOrder->dishes); $i++, $j++): ?>
                                    <?php if($j < 3): ?> <li><?php echo e($roomOrder->dishes[$i]->name); ?></li>
                                        <?php elseif(count($roomOrder->dishes) == 3): ?>
                                        <li><?php echo e($roomOrder->dishes[$i]->name); ?></li>
                                        <?php else: ?>
                                        <li>More...</li>
                                        <?php break; ?>
                                        <?php endif; ?>
                                        <?php endfor; ?>
                            </ul>
                        </div>
                        <div class="totalPriceWrapper">
                            <p>Total $<?php echo e($roomOrder->total_price); ?></p>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 currentOrdersWrapper">
            <div class="row">
                <div class="col-lg-2 col-md-3 ordersLabelWrapper">
                    <label>Current orders:</label>
                </div>
                <div class="col-lg-10 col-md-9 orderItemsWrapper">
                    <?php $__currentLoopData = $roomOrderCurrents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $roomOrder): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="orderWrapper" data-order_details_id="<?php echo e($roomOrder->id); ?>">
                        <div class="roomTextWrapper">
                            <?php if($roomOrder->hotelBooking->room_number): ?>
                            <h4><?php echo e($roomOrder->hotelBooking->room_number); ?></h4>
                            <?php else: ?>
                            <h4>Room -</h4>
                            <?php endif; ?>
                        </div>
                        <div class="orderDishWrapper">
                            <ul>
                                <?php for($i=0, $j=1; $i<count($roomOrder->dishes); $i++, $j++): ?>
                                    <?php if($j < 3): ?> <li><?php echo e($roomOrder->dishes[$i]->name); ?></li>
                                        <?php elseif(count($roomOrder->dishes) == 3): ?>
                                        <li><?php echo e($roomOrder->dishes[$i]->name); ?></li>
                                        <?php else: ?>
                                        <li>More...</li>
                                        <?php break; ?>
                                        <?php endif; ?>
                                        <?php endfor; ?>
                            </ul>
                        </div>
                        <div class="totalPriceWrapper">
                            <p>Total <?php echo e($currency); ?> <?php echo e($roomOrder->total_price); ?></p>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 pastOrdersWrapper">
            <div class="row">
                <div class="col-lg-2 col-md-3 ordersLabelWrapper">
                    <label>Past orders:</label>
                </div>
                <div class="col-lg-10 col-md-9 orderItemsWrapper">
                    <?php $__currentLoopData = $roomOrderPasts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $roomOrder): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="orderWrapper" data-order_details_id="<?php echo e($roomOrder->id); ?>">
                        <div class="roomTextWrapper">
                            <?php if($roomOrder->hotelBooking->room_number): ?>
                            <h4><?php echo e($roomOrder->hotelBooking->room_number); ?></h4>
                            <?php else: ?>
                            <h4>Room -</h4>
                            <?php endif; ?>
                        </div>
                        <div class="orderDishWrapper">
                            <ul>
                                <?php for($i=0, $j=1; $i<count($roomOrder->dishes); $i++, $j++): ?>
                                    <?php if($j < 3): ?> <li><?php echo e($roomOrder->dishes[$i]->name); ?></li>
                                        <?php elseif(count($roomOrder->dishes) == 3): ?>
                                        <li><?php echo e($roomOrder->dishes[$i]->name); ?></li>
                                        <?php else: ?>
                                        <li>More...</li>
                                        <?php break; ?>
                                        <?php endif; ?>
                                        <?php endfor; ?>
                            </ul>
                        </div>
                        <div class="totalPriceWrapper">
                            <p>Total <?php echo e($currency); ?> <?php echo e($roomOrder->total_price); ?></p>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade showDetailsReservationModal" tabindex="-1" role="dialog"
        aria-labelledby="showDetailsReservationModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <h4 class="mb-4" id="roomText">Room 101 - Reservation details:</h4>
                    <form id="updateRestaurantReservationForm" method="post"
                        action="<?php echo e(route('cms.restaurant-reservations.update', ['restaurantReservation' => 0])); ?>"
                        enctype="multipart/form-data">
                        <?php echo csrf_field(); ?>
                        <?php echo method_field('put'); ?>
                        <input type="hidden" name="restaurant_reservation_id" id="restaurant_reservation_id" value="0">
                        <input type="hidden" name="status" id="status" value="0">
                    </form>
                    <table class="table table-responsive-sm" id="reservationTable">
                        <tbody>
                            <tr>
                                <td>Room</td>
                                <td>Room 101</td>
                            </tr>
                            <tr>
                                <td>People number</td>
                                <td>10</td>
                            </tr>
                            <tr>
                                <td>Booking date</td>
                                <td>1 august 2019 16:00</td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="row mt-4">
                        <div class="col-6">
                            <button class="btn btn-pill btn-block btn-danger btn-lg" type="button"
                                id="rejectRestaurantReservationBtn">Reject</button>
                        </div>
                        <div class="col-6">
                            <button class="btn btn-pill btn-block btn-success btn-lg" type="button"
                                id="acceptRestaurantReservationBtn">Accept</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    
    
    
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    <div class="col-12 reservationsCalendarWrapper mb-5">
        <h3>Reservations</h3>
        <div class="legendsWrapper">
            <div class="legendWrapper">
                <div class="color grey">
                </div>
                <div class="textWrapper">
                    <p>Requested</p>
                </div>
            </div>
            <div class="legendWrapper">
                <div class="color blue">
                </div>
                <div class="textWrapper">
                    <p>Accepted</p>
                </div>
            </div>
        </div>
        <div id='reservationRestaurantCalendar'></div>
    </div>
</div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script>
    const url4 = '<?php echo route('ajax.cms.restaurant-dishes.index'); ?>';
        const url5 = '<?php echo route('ajax.cms.restaurant-room-services.index'); ?>';
        const url6 = '<?php echo route('ajax.cms.restaurant-reservations.index'); ?>';
        const id1 = '<?php echo $restaurant->id; ?>';
        const delCatUrl = '<?php echo route('ajax.cms.restaurant-category.delete'); ?>';
        const urlRoomServiceUpdate = "<?php echo route('cms.restaurant-room-services.update', ['restaurantRoomService' => 0]); ?>";
        const sortGallery = "<?php echo route('ajax.cms.sort-gallery'); ?>";
</script>
<script src="<?php echo e(asset('js/custom/cms/restaurant.js')); ?>"></script>
<script type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js">

</script>
<script type="text/javascript">
    $(document).ready(function(){
            
            $(".imagesWrapper").sortable({
                update: function(event, ui){
                    var finalUrl = sortGallery+"?gallery_id="+$(ui.item).attr('data-restaurant_gallery_id')+"&sort_id="+ui.item.index();
                    console.log(ui.item.index());
                    $.ajax({
                        type: 'POST',
                        data: {sorted: $(".imagesWrapper").sortable('toArray')},
                        url: finalUrl,
                        headers: {

                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                        },
                        success: function(){
                            console.log("Done");
                        }
                    });
                }
            });
        });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('cms.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\Xammp\htdocs\server_hotels\resources\views/cms/restaurant/show.blade.php ENDPATH**/ ?>