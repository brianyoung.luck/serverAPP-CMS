@extends('admin.layouts.auth')

@section('title', __('Forgot Password'))

@section('content')
    <div id="forgotPasswordResponseWrapper" class="row justify-content-center">
        <div class="col-xl-6 col-lg-8 col-md-8 col-sm-10">
            <div class="card-group">
                <div class="card p-4">
                    <div class="card-body">
                        <div class="admin-panel-text">
                            <h2>{{ env('APP_NAME') }}</h2>
                            <h3>{{ __('Forgot Password') }}</h3>
                        </div>

                        <div class="row responseWrapper">
                            <div class="col-12">
                                <h4>{{ $response }}</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop