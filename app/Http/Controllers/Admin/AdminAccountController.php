<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\LoginAdminRequest;
use App\Http\Requests\LoginHotelAdminRequest;
use App\Models\Admin;
use App\Http\Controllers\Controller;
use App\Models\Hotel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;
use Illuminate\Http\Request;



class AdminAccountController extends Controller
{

    public function index(){
        $staff = Admin::all();
        return view('admin.staff.index')->with([
            'staff' => $staff
        ]);
    }

    public function create(Request $request){
        $email = $request->input('email');
        $isUnique = Admin::where('email',$email)->exists();
        if($isUnique){
            return Redirect::back()->withInput()->with('error', 'Email is not unique.');
        }
        $staff_mem = new Admin;
        $staff_mem->name = $request->input('name');
        $staff_mem->email = $email;
        $staff_mem->password = $request->input('password');
        $staff_mem->save();
        return Redirect::back()->with('success','Staff member added successfully.');
    }
    public function DeleteStaff(Request $request){
        Admin::where('id',$request->id)->delete();
        return Redirect::back()->with('success','Staff member added successfully.');   
    }
}