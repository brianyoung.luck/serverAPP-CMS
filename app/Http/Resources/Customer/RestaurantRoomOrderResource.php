<?php

namespace App\Http\Resources\Customer;

use Illuminate\Http\Resources\Json\JsonResource;

class RestaurantRoomOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'restaurant' => $this->whenLoaded('restaurant', function () {
                return [
                    'id' => $this->restaurant->id,
                    'name' => $this->restaurant->name,
                    'logo_url' => $this->restaurant->logo_url,
                ];
            }),
            'total_price' => $this->total_price,
            'dishes' => RestaurantDishResource::collection($this->whenLoaded('dishes')),
            'datetime' => $this->created_at,
            'status' => $this->status,
        ];
    }
}
