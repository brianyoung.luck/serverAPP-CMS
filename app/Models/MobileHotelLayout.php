<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\FileStorageService;

class MobileHotelLayout extends Model
{
    protected $guarded=[];
    public function getCheckinImageAttribute($value)
    {
        return $this->getS3Url($value);
    }
    public function getCheckoutImageAttribute($value)
    {
        return $this->getS3Url($value);
    }
    public function getRestaurantImageAttribute($value)
    {
        return $this->getS3Url($value);
    }
    public function getSpaImageAttribute($value)
    {
        return $this->getS3Url($value);
    }
    public function getConciergeImageAttribute($value)
    {
        return $this->getS3Url($value);
    }
    public function getExperienceImageAttribute($value)
    {
        return $this->getS3Url($value);
    }
    public function getPhotoPreviewUrlAttribute($value)
    {
        return $this->getS3Url($value);
    }

    /**
     * Create presign url
     * @param $value
     * @return string|null
     */
    private function getS3Url($value)
    {
        $fileStorageService = new FileStorageService(env('FILESYSTEM_DRIVER'));
        $storageServiceName = $fileStorageService->getStorageServiceName($value);
        switch ($storageServiceName) {
            case 's3':
                return $fileStorageService->getTemporaryUrl($value);
                break;
            default:
                return $value;
        }
    }
}