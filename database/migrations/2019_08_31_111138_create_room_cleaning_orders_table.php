<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomCleaningOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room_cleaning_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('hotel_booking_id');
            $table->enum('status', [
                'pending',
                'confirmed',
                'completed',
                'rejected',
            ])->default('pending');
            $table->timestamps();

            // Hotel Booking
            $table->foreign('hotel_booking_id')
                ->references('id')
                ->on('hotel_bookings')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('room_cleaning_orders');
    }
}
