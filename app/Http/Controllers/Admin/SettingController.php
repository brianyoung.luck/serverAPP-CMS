<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\LoginAdminRequest;
use App\Http\Requests\LoginHotelAdminRequest;
use App\Models\Admin;
use App\Http\Controllers\Controller;
use App\Models\Hotel;
use App\Models\Setting;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;
use Illuminate\Http\Request;



class SettingController extends Controller
{
    public function index(){
        $labels = array(
            "max_promotions_basic" => "Max Promotion For Basic Package",
            "max_promotions_premium" => "Max Promotion For Premium Package"
        );
        $settings = Setting::all();
        return view('admin.setting')->with([
            'labels' => $labels,
            'settings' => $settings,
        ]);
    }

    public function store(Request $request){
        foreach($request->all() as $key => $value){
            $setting = new Setting;
            $setting->id = $key;
            $setting->exists = true;
            $setting->value = $value;
            $setting->save();
        }
        return redirect()->back()->with([
            'success' => 'Setting save successfull.'
        ]);
    }
}