<?php

namespace App\Exceptions;

use RuntimeException;

class CustomForbiddenException extends RuntimeException
{
    /**
     * Any extra data to send with the response.
     *
     * @var array
     */
    public $error_message = 'This action is forbidden';

    /**
     * The status code to use for the response.
     *
     * @var integer
     */
    public $status = 403;

    public function render()
    {
        $response = [
            'message' => $this->getMessage(),
        ];

        return response()->json($response, $this->status);
    }
}
