const togglePassword = $('.toggle-password');
const password = $('#password');
const buttonReset = $('#buttonReset');
const resetPasswordForm = $('#resetPasswordForm');
const email = $('#email');
const passwordConfirmation = $('#password_confirmation');

$(document).ready(function() {
    buttonReset.click(reset);
    togglePassword.click(changeIconPassword);
    email.keyup(function(e) { checkFormValidation(e) });
    password.keyup(function(e) { checkFormValidation(e) });
    passwordConfirmation.keyup(function(e) { checkFormValidation(e) });
});

function reset()
{
    // Show loading
    showLoadingButton(true);

    // Submit
    resetPasswordForm.submit();
}

function showLoadingButton(is_show = true)
{
    if (is_show === true) {
        buttonReset.html(`<i class="fa fa-refresh fa-spin fa-fw"></i> Please wait...`).attr('disabled', 'disabled');
    } else {
        buttonReset.html(`Reset password`).attr('disabled', false);
    }
}

function changeIconPassword()
{
    if (password.attr('type') === 'text'){
        password.attr('type', 'password');
        togglePassword.addClass( 'fa-eye-slash' );
        togglePassword.removeClass( 'fa-eye' );
    } else if(password.attr('type') === 'password'){
        password.attr('type', 'text');
        togglePassword.removeClass( 'fa-eye-slash' );
        togglePassword.addClass( 'fa-eye' );
    }
}

function isAllFormFilled()
{
    let current = 0;
    const complete = 3;

    if (email.val() !== '') {
        current += 1;
    }
    if (password.val() !== '') {
        current += 1;
    }
    if (passwordConfirmation.val() !== '') {
        current += 1;
    }

    return current === complete;
}

function checkFormValidation(e)
{
    if (isAllFormFilled() === true) {
        buttonReset.prop('disabled', false);
        if(e.which === 13) {
            buttonReset.click();
        }
    } else {
        buttonReset.prop('disabled', true);
    }
}