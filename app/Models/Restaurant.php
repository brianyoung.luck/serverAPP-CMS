<?php

namespace App\Models;

use App\Services\FileStorageService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Restaurant extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'hotel_id',
        'name',
        'logo_url',
    ];


    /**
     * Getter
     */

    public function getLogoUrlAttribute($value)
    {
        return $this->getLogoUrl($value);
    }

    /**
     * Create presign url
     * @param $value
     * @return string|null
     */
    private function getLogoUrl($value)
    {
        $fileStorageService = new FileStorageService(env('FILESYSTEM_DRIVER'));
        $storageServiceName = $fileStorageService->getStorageServiceName($value);
        switch ($storageServiceName) {
            case 's3':
                return $fileStorageService->getTemporaryUrl($value);
                break;
            default:
                return $value;
        }
    }

    /**
     * Relations
     */

    public function restaurantGalleries()
    {
        return $this->hasMany(RestaurantGallery::class, 'restaurant_id', 'id')->orderBy('sort_id');;
    }

    public function dishes()
    {
        return $this->hasMany(RestaurantDish::class, 'restaurant_id', 'id');
    }

}
