@extends('admin.layouts.auth')

@section('title', __('Login Admin'))

@section('content')
    <div id="loginWrapper" class="row justify-content-center">
        <div class="col-xl-5 col-lg-6 col-md-8 col-sm-10">
            <div class="card-group">
                <div class="card p-4">
                    <div class="card-body">
                        <div class="admin-panel-text">
                            <h2>{{ env('APP_NAME') }}</h2>
                            <h3>{{ __('Admin panel') }}</h3>
                        </div>
                        @include('cms.layouts.flash-message')
                        <form id="loginForm" class="form-horizontal" action="{{ route('admin.do-login') }}" method="post">
                            @csrf()
                            @method('post')
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <input class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" type="email" name="email" placeholder="Email">
                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <input class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" id="password" type="password" name="password" placeholder="Password">
                                        <span class="fa fa-fw fa-eye-slash field-icon toggle-password"></span>
                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </form>

                        <div class="row">
                            <div class="col-12">
                                <button class="btn btn-pill btn-block btn-primary" type="button" id="buttonLogin" disabled>{{ __('Login') }}</button>
                            </div>
                        </div>
                        <div id="forgotPasswordLinkWrapper">
                            <a href="{{ route('admin.forgot-password.index') }}">{{ __('Forgot password?') }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>
        const url1 = '{!! route('cms.dashboard.index') !!}';
    </script>
    <script src="{{ asset('js/custom/cms/login.js') }}"></script>
@stop