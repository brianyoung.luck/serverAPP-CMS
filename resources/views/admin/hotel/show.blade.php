@extends('admin.layouts.app')

@section('title', __('Hotel details'))

@section('content')
    <div class="row hotelDetailsWrapper">
        <div class="col-12">
            <div class="card">
                <div class="card-header">{{ __('Hotel details') }}</div>
                <div class="card-body">
                    <form id="updateHotelForm" method="post" action="{{ route('admin.hotels.update', ['hotel' => $hotel->id]) }}">
                        @method('put')
                        @csrf()
                        <input type="hidden" name="type" id="typeUpdateHotel" value="renew">
                        <input type="hidden" name="extendmonthTime" id="extendmonthTime" value="">
                    </form>
                    <table class="table table-responsive-sm table-striped" id="hotelDetailsTable">
                        <tbody>
                        <tr>
                            <td><b>Hotel name</b></td>
                            <td>{{ $hotel->name }}</td>
                        </tr>
                        <tr>
                            <td><b>Description</b></td>
                            <td>{{ $hotel->description }}</td>
                        </tr>
                        <tr>
                            <td><b>Code</b></td>
                            <td>{{ $hotel->code }}</td>
                        </tr>
                        <tr>
                            <td><b>Email</b></td>
                            <td>{{ $hotel->email }}</td>
                        </tr>
                        <tr>
                            <td><b>Checkout time</b></td>
                            <td>{{ $hotel->checkout_time }}</td>
                        </tr>
                        <tr>
                            <td><b>Late checkout time</b></td>
                            <td>{{ $hotel->late_checkout_time }}</td>
                        </tr>

                        <tr>
                            <td><b>Created at</b></td>
                            <td>{{ $hotel->created_at }}</td>
                        </tr>
                        <tr>
                            <td><b>Status</b></td>
                            <td>
                                @if ($hotel->is_active)
                                    <span class="badge badge-pill badge-success">Active</span>
                                @else
                                    <span class="badge badge-pill badge-warning">Inactive</span>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td><b>Expired at</b></td>
                            <td>
                                {{ \App\Services\HelperService::dateTimeFormat($hotel->expired_at) }}
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <button class="btn btn-success" type="button" id="renewBtn">
                                    <i class="nav-icon icon-refresh"></i>&nbsp
                                    {{ __('Renew') }}
                                </button>
                                <button class="btn btn-success" type="button" id="extendTrial">
                                    <i class="nav-icon icon-refresh"></i>&nbsp
                                    {{ __('Extend Trial') }}
                                </button>
                                @if ($hotel->is_active)
                                    <button class="btn btn-danger" type="button" id="deleteHotelBtn">
                                        <i class="nav-icon icon-trash"></i>&nbsp
                                        {{ __('Inactive') }}
                                    </button>
                                @endif
                                <a class="btn btn-danger" type="button" id="deleteHotelButton" href="{{route('admin.hotel.destroy',['id'=>$hotel->id])}}">
                                        <i class="nav-icon icon-trash"></i>&nbsp
                                        {{ __('Delete') }}
                                </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-12">
            <div class="card">
                <div class="card-header">{{ __('Hotel features') }}</div>
                <div class="card-body">
                    <form id="updateHotelFeatureForm" method="post" action="{{ route('admin.hotel-features.update', ['hotel' => $hotel->id]) }}">
                        @method('put')
                        @csrf()
                    </form>
                    <table class="table table-responsive-sm table-striped" id="hotelDetailsTable">
                        <tbody>
                        <tr>
                            <td><b>Check in</b></td>
                            <td>
                                <label class="switch switch-pill switch-success">
                                    <input type="checkbox" class="switch-input" name="is_check_in_enabled" id="is_check_in_enabled" {{ $hotel->hotelFeature->is_check_in_enabled ? 'checked' : '' }}>
                                    <span class="switch-slider"></span>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Check out</b></td>
                            <td>
                                <label class="switch switch-pill switch-success">
                                    <input type="checkbox" class="switch-input" name="is_check_out_enabled" id="is_check_out_enabled" {{ $hotel->hotelFeature->is_check_out_enabled ? 'checked' : '' }}>
                                    <span class="switch-slider"></span>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Spa</b></td>
                            <td>
                                <label class="switch switch-pill switch-success">
                                    <input type="checkbox" class="switch-input" name="is_spa_enabled" id="is_spa_enabled" {{ $hotel->hotelFeature->is_spa_enabled || $hotel->hotelFeature->spa_treament || $hotel->hotelFeature->spa_room_service? 'checked' : '' }}>
                                    <span class="switch-slider"></span>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><b stlye="padding-left: 20px !important;">Spa Room Service</b></td>
                            <td>
                                <label class="switch switch-pill switch-success">
                                    <input type="checkbox" class="switch-input" name="spa_room_service" id="spa_room_service" {{$hotel->hotelFeature->spa_room_service ? 'checked' : '' }}>
                                    <span class="switch-slider"></span>
                                </label>
                            </td>
                        </tr>

                        <tr>
                            <td><b >Spa Treatment</b></td>
                            <td>
                                <label class="switch switch-pill switch-success">
                                    <input type="checkbox" class="switch-input" name="spa_treament" id="spa_treament" {{$hotel->hotelFeature->spa_treatment ? 'checked' : '' }}>
                                    <span class="switch-slider"></span>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Restaurant</b></td>
                            <td>
                                <label class="switch switch-pill switch-success">
                                    <input type="checkbox" class="switch-input" name="is_restaurant_enabled" id="is_restaurant_enabled" {{ $hotel->hotelFeature->is_restaurant_enabled ? 'checked' : '' }}>
                                    <span class="switch-slider"></span>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Concierge</b></td>
                            <td>
                                <label class="switch switch-pill switch-success">
                                    <input type="checkbox" class="switch-input" name="is_concierge_enabled" id="is_concierge_enabled" {{ $hotel->hotelFeature->is_concierge_enabled ? 'checked' : '' }}>
                                    <span class="switch-slider"></span>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Cleaning service</b></td>
                            <td>
                                <label class="switch switch-pill switch-success">
                                    <input type="checkbox" class="switch-input" name="is_cleaning_enabled" id="is_cleaning_enabled" {{ $hotel->hotelFeature->is_cleaning_enabled ? 'checked' : '' }}>
                                    <span class="switch-slider"></span>
                                </label>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>
        const url1 = '{!! route('data-tables.admin.hotels') !!}';
        const url2 = '{!! route('data-tables.admin.hotels') !!}';
        const url3 = '{!! route('ajax.admin.hotel-features.index') !!}';
        const id1 = '{!! $hotel->id !!}';

    </script>
    <script src="{{ asset('js/custom/admin/hotel.js') }}"></script>
@stop