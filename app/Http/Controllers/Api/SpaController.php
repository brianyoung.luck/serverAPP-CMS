<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\CustomForbiddenException;
use App\Http\Requests\CreateSpaBookingRequest;
use App\Http\Resources\Customer\SpaBookingResource;
use App\Http\Resources\Customer\SpaBookingResourceCollection;
use App\Http\Resources\Customer\SpaResource;
use App\Http\Resources\Customer\SpaTreatmentResourceCollection;
use App\Models\Spa;
use App\Models\SpaGallery;
use App\Services\SendbirdService;
use App\Services\SpaService;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\HotelFeature;

class SpaController extends Controller
{
    protected $spaService;

    public function __construct(SpaService $spaService)
    {
        $this->spaService = $spaService;
    }

    public function getAll()
    {
        $result = Auth::user()->hotel->spas()->first();

        $services_enabled = HotelFeature::where('hotel_id', Auth::user()->hotel->id)->first();
        $result['spa_treatment'] = $services_enabled->spa_treatment;
        $result['spa_room_service'] = $services_enabled->spa_room_service;
        $spas= Auth::user()->hotel->spas;
        foreach ($spas as $spa) {
            $spaGalleries=SpaGallery::where('spa_id', $spa->id)->get();
        }
        $result['images']=$spaGalleries->prepend(['image_url'=>$result->logo_url]);
        // return $result;
        return (new SpaResource($result))
            ->additional([
                'message' => 'Get spa success'
            ])
            ->response()
            ->setStatusCode(200);
    }

    public function getTreatments(Spa $spa)
    {
        if ($spa->hotel_id !== Auth::user()->hotel->id) {
            throw new CustomForbiddenException('Forbidden Access');
        }

        $result = $spa->treatments;

        return (new SpaTreatmentResourceCollection($result))
            ->additional([
                'message' => 'Get spa treatments success'
            ])
            ->response()
            ->setStatusCode(200);
    }

    public function createBooking(Spa $spa, CreateSpaBookingRequest $request, SendbirdService $sendbirdService)
    {
        if ($spa->hotel_id !== Auth::user()->hotel->id) {
            throw new CustomForbiddenException('Forbidden Access');
        }
        $hotelBooking = Auth::user();

        $validated = $request->validated();
        $validated['spa_id'] = $spa->id;

        $result = $this->spaService->createBooking($hotelBooking, $validated);

        // Notification to admin
        if ($validated['booking_type'] == 'normal_reservation') {
            $notificationText = 'reservation';
        } else {
            $notificationText = 'room service';
        }
        $sendbirdService->sendAdminMessage($hotelBooking, [
            'text' => "Spa $notificationText has been requested",
            'payload' => [
                'type' => 'spa_booking',
                'id' => $result->id,
            ],
        ]);

        return (new SpaBookingResource($result))
            ->additional([
                'message' => 'Success, your booking request has been sent'
            ])
            ->response()
            ->setStatusCode(201);
    }

    public function getAllRequests()
    {
        $bookings = Auth::user()->spaBookings()->with(['treatments'])->get();

        return (new SpaBookingResourceCollection($bookings))
            ->additional([
                'message' => 'Get spa bookings success'
            ])
            ->response()
            ->setStatusCode(200);
    }
}