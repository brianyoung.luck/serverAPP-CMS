<?php

namespace App\Http\Controllers\Cms;

use App\Models\MobileHotelTheme;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Hotel_Staff;
class HotelStaffPermissionController extends Controller
{
    public function get($id){
        $staff = Hotel_Staff::where('id', $id)->first();
        if(!$staff){
            abort(404);
        }
        
        $permissions = $staff->getPermissionNames()->all();
        
        
        return view('cms.hotel_staff.permission')->with([
            'permissions' => $permissions,
            'user_id' => $id
        ]);
    }

    public function store($id, Request $request){
        $staff_member = Hotel_Staff::where('id', $id)->first();
        
        $permission_set = ["check_in","checkout","spa","spa_room_service","spa_treatment","restaurant","concierge","settings","experience"];
        $selected_permissions = [];
        if($request->input('permission')){
            $selected_permissions = $request->input('permission');
        }
        $permission_diff = array_diff($permission_set, $selected_permissions);
        
        foreach($permission_diff as $perm_to_verify){
            if($staff_member->hasPermissionTo($perm_to_verify)){
                $staff_member->revokePermissionTo($perm_to_verify);
            }
        }
        
        foreach($selected_permissions as $perm_to_verify){
            if(!$staff_member->hasPermissionTo($perm_to_verify)){
                $staff_member->givePermissionTo($perm_to_verify);
            }else{
            }
        }
        return redirect()->back()->with(['success' => 'Permission updated successfull.']);
    }
}
