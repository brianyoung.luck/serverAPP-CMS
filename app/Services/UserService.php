<?php

namespace App\Services;

use App\Models\User;

class UserService
{
    public function create($userData)
    {
        $user = User::create($userData);

        return $user;
    }
}