<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpaBookingSpaTreatment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spa_booking_spa_treatment', function (Blueprint $table) {
            $table->unsignedBigInteger('spa_booking_id');
            $table->unsignedBigInteger('spa_treatment_id');
            $table->double('price', 10, 2);
            $table->timestamps();

            // Foreign Keys
            $table->foreign('spa_booking_id')
                ->references('id')
                ->on('spa_bookings')
                ->onDelete('restrict');
            $table->foreign('spa_treatment_id')
                ->references('id')
                ->on('spa_treatments')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spa_booking_spa_treatment');
    }
}
