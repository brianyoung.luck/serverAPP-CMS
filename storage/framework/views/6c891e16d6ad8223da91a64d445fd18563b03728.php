<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link <?php echo Request::is('dashboard') || Request::is('') ? 'active' : ''; ?>" href="<?php echo e(route('admin.dashboard.index')); ?>">
                    <i class="nav-icon icon-home"></i> <?php echo e(__('Home')); ?>

                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?php echo Request::is('hotels*') ? 'active' : ''; ?>" href="<?php echo e(route('admin.hotels.index')); ?>">
                    <i class="nav-icon icon-grid"></i> <?php echo e(__('Hotel')); ?>

                </a>
            </li>
            <?php if(auth('admin')->user()->name == "Super Admin"): ?>
            <li class="nav-item">
                <a class="nav-link <?php echo Request::is('staff*') ? 'active' : ''; ?>" href="<?php echo e(route('staff.index')); ?>">
                    <i class="nav-icon icon-grid"></i> <?php echo e(__('Staff Members')); ?>

                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?php echo Request::is('settings*') ? 'active' : ''; ?>" href="<?php echo e(route('settings.index')); ?>">
                    <i class="nav-icon icon-grid"></i> <?php echo e(__('Settings')); ?>

                </a>
            </li>
            <?php endif; ?>
            
            <li class="nav-item">
                <a class="nav-link" href="<?php echo e(route('admin.logout.index')); ?>">
                    <i class="nav-icon icon-logout"></i> <?php echo e(__('Logout')); ?>

                </a>
            </li>
        </ul>
    </nav>
</div><?php /**PATH E:\Xammp\htdocs\server_hotels\resources\views/admin/layouts/sidebar.blade.php ENDPATH**/ ?>