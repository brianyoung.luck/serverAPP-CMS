<?php

namespace App\Http\Middleware;

use App\Exceptions\CustomAuthorizationException;
use Closure;
use Auth;

class IsBookingActive
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (now()->addHour() > Auth::user()->departure_date || Auth::user()->status == 'checked_out') {
            throw new CustomAuthorizationException('Unauthenticated.');
        }

        return $next($request);
    }
}
