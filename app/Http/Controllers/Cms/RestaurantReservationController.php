<?php

namespace App\Http\Controllers\Cms;

use App\Models\RestaurantReservation;
use App\Services\SendbirdService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RestaurantReservationController extends Controller
{
    public function update(Request $request, SendbirdService $sendbirdService)
    {
        $restaurantReservation = RestaurantReservation::where('id', $request->get('restaurant_reservation_id', 0))
            ->first();
        if (!$restaurantReservation) {
            abort(404);
        }
        $restaurantReservation->status = $request->get('status', 'accepted');
        $restaurantReservation->save();

        // Notification via sendbird
        if ($restaurantReservation->status == 'accepted') {
            $sendbirdService->sendAdminMessage($restaurantReservation->hotelBooking, [
                'text' => 'Your restaurant reservation has been accepted.',
                'payload' => [
                    'type' => 'restaurant_reservation_accepted',
                    'id' => $restaurantReservation->id,
                ],
            ]);
        }

        return redirect()->route('cms.restaurants.show', ['restaurant' => $restaurantReservation->restaurant_id])->with('success', 'Success');
    }

    public function ajaxGetRestaurantReservations(Request $request)
    {
        return [
            'status' => true,
        ];
    }

    public function ajaxGetRestaurantReservation(Request $request, $restaurantReservationId)
    {
        // Get details
        $restaurantReservation = RestaurantReservation::where('id', $restaurantReservationId)
            ->with([
                'hotelBooking',
            ])
            ->first();

        return [
            'status' => true,
            'restaurantReservation' => $restaurantReservation,
        ];
    }

    public function ajaxGetRestaurantReservation2(Request $request)
    {
        $status = $request->query('status');
        $restaurantId = $request->query('restaurant_id');
        $startDate = Carbon::parse($request->query('start'));
        $endDate = Carbon::parse($request->query('end'));

        $restaurantReservations = RestaurantReservation::where('restaurant_id', $restaurantId)
            ->where('status', $status)
            ->where('booking_date', '>=', $startDate->toDateTimeString())
            ->where('booking_date', '<=', $endDate->toDateTimeString())
            ->with([
                'hotelBooking',
            ])
            ->get();

        $result = [];
        foreach ($restaurantReservations as $restaurantReservation) {
            array_push($result, [
                'id' => $restaurantReservation->id,
                'title' => data_get($restaurantReservation->hotelBooking, 'room_number', ''),
                'start' => Carbon::create($restaurantReservation->booking_date),
                'end' => Carbon::create($restaurantReservation->booking_date)->addSecond(),
                'allDay' => false
            ]);
        }
        return $result;
    }
}
