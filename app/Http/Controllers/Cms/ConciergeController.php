<?php

namespace App\Http\Controllers\Cms;

use App\Models\ConciergeService;
use App\Models\ConciergeServiceRequest;
use App\Models\LaundryOrder;
// use App\Models\LaundaryServiceRequest;
use App\Models\RoomCleaningOrder;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\LaundaryService;
use App\Models\Hotel;

class ConciergeController extends Controller
{
    public function index()
    {
        $conciergeServicePendingRequests = ConciergeServiceRequest::where('status', 'pending')
            ->whereHas('hotelBooking', function (Builder $query) {
                $query->where('hotel_id', auth('hotel')->user()->id);
            })
            ->with([
                'conciergeServices',
                'hotelBooking'
            ])
            ->orderBy('id', 'asc')
            ->get();
        $conciergeServicePastRequests = ConciergeServiceRequest::whereIn('status', ['rejected','cancelled','done'])
            ->whereHas('hotelBooking', function (Builder $query) {
                $query->where('hotel_id', auth('hotel')->user()->id);
            })
            ->with([
                'conciergeServices',
                'hotelBooking'
            ])
            ->orderBy('id', 'desc')
            ->get();

        $roomCleaningRequests = RoomCleaningOrder::whereHas('hotelBooking', function (Builder $query) {
            $query->where('hotel_id', auth('hotel')->user()->id);
        })
            ->with([
                'hotelBooking'
            ])
            ->orderBy('updated_at', 'asc')
            ->get();

        $laundryRequests = LaundryOrder::whereHas('hotelBooking', function (Builder $query) {
            $query->where('hotel_id', auth('hotel')->user()->id);
        })
            ->where('status', 'pending')
            ->with([
                'hotelBooking',
                    // 'laundaryServices',
            ])
            ->orderBy('updated_at', 'asc')
            ->get();

        return view('cms.concierge.index', [
            'conciergeServicePendingRequests' => $conciergeServicePendingRequests,
            'conciergeServicePastRequests' => $conciergeServicePastRequests,
            'roomCleaningRequests' => $roomCleaningRequests,
            'laundryRequests' => $laundryRequests,
        ]);
    }

    public function store(Request $request)
    {
        $name = $request->get('name', '-');
        ConciergeService::create([
            'hotel_id' => auth('hotel')->user()->id,
            'name' => $name == null ? '-' : $name,
            'description' => $request->get('description'),
        ]);

        return redirect()->route('cms.concierge-menus.index')->with('success', 'Add concierge success.');
    }
    public function insertLaundary(Request $request)
    {
        $name = $request->get('name', '-');
        LaundaryService::create([
            'hotel_id' => auth('hotel')->user()->id,
            'name' => $name == null ? '-' : $name,
            'description' => $request->get('description'),
              'price' => $request->get('price')
        ]);
        return redirect()->route('cms.laundary-menus.index')->with('success', 'Add laundary success.');
    }
    public function updateLaundary(Request $request)
    {
        LaundaryService::where('id', $request->get('concierge_service_id', 0))
            ->update([
                'name' => $request->get('nameUpdate', '-'),
                'description' => $request->get('descriptionUpdate'),
                'price'=>$request->get('priceUpdate')
            ]);
        return redirect()->route('cms.laundary-menus.index')->with('success', 'Update laundary service success.');
    }
    public function destroyLaundary(Request $request)
    {
        LaundaryService::where('id', $request->get('concierge_service_id', 0))
            ->delete();
        return redirect()->route('cms.laundary-menus.index')->with('success', 'Delete laundary service success.');
    }

    public function ajaxGetLaundaryServiceDetails(Request $request, $laundaryServiceId)
    {
        // Get details
        $conciergeService = LaundaryService::where('id', $laundaryServiceId)
            ->first();
        return [
            'status' => true,
            'conciergeService' => $conciergeService,
        ];
    }

    public function update(Request $request)
    {
        $a=  ConciergeService::where('id', $request->get('concierge_service_id', 0))
            ->update([
                'name' => $request->get('nameUpdate', '-'),
                'description' => $request->get('descriptionUpdate')
            ]);

        return redirect()->route('cms.concierge-menus.index')->with('success', 'Update concierge service success.');
    }

    public function destroy(Request $request)
    {
        ConciergeService::where('id', $request->get('concierge_service_id', 0))
            ->delete();

        return redirect()->route('cms.concierge-menus.index')->with('success', 'Delete concierge service success.');
    }

    public function ajaxGetConciergeService(Request $request)
    {
        return [
            'status' => true,
        ];
    }

    public function ajaxGetConciergeServiceDetails(Request $request, $conciergeServiceId)
    {
        // Get details
        $conciergeService = ConciergeService::where('id', $conciergeServiceId)
            ->first();

        return [
            'status' => true,
            'conciergeService' => $conciergeService,
        ];
    }

    public function ajaxGetConciergeServiceRequest(Request $request)
    {
        return [
            'status' => true,
        ];
    }

    public function ajaxGetConciergeServiceRequestDetails(Request $request, $conciergeServiceRequestId)
    {
        // Get details
        $conciergeServiceRequests = ConciergeServiceRequest::where('id', $conciergeServiceRequestId)
            ->with([
                'conciergeServices',
                'hotelBooking'
            ])
            ->first();

        return [
            'status' => true,
            'conciergeServiceRequest' => $conciergeServiceRequests,
        ];
    }

    public function showConciergeMenus(Request $request)
    {
        $conciergeServices = ConciergeService::where('hotel_id', auth('hotel')->user()->id)
            ->orderBy('id', 'desc')
            ->get();

        return view('cms.concierge.concierge-menu', [
            'conciergeServices' => $conciergeServices,

        ]);
    }
    public function showLaundaryMenus(Request $request)
    {
        $conciergeServices = LaundaryService::where('hotel_id', auth('hotel')->user()->id)
            ->orderBy('name', 'asc')
            ->get();
        $currency = Hotel::where('id', auth('hotel')->user()->id)->select('currency')->first()['currency'];
        return view('cms.concierge.laundary-menu', [
            'conciergeServices' => $conciergeServices,
            'currency' => $currency
        ]);
    }
}