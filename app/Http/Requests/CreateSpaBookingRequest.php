<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateSpaBookingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'treatments' => 'array|required',
            'treatments.*' => 'required|integer',
            'booking_type' => 'required|in:normal_reservation,room_service',
            'booking_date' => 'required|date_format:Y-m-d H:i|after:now',
            'people_number' => 'required|integer',
        ];
    }
}
