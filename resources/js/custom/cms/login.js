const togglePassword = $(".toggle-password");
const password = $("#password");
const buttonLogin = $("#buttonLogin");
const loginForm = $("#loginForm");
const username = $("#username");
const btnAppStore = $(".btnAppStore");
const btnPlayStore = $(".btnPlayStore");
$(document).ready(function() {
  buttonLogin.click(login);
  togglePassword.click(changeIconPassword);
  username.keyup(function(e) {
    checkFormValidation(e);
  });
  password.keyup(function(e) {
    checkFormValidation(e);
  });
});
btnAppStore.click(function() {
  window.location.href =
    "https://apps.apple.com/us/app/servr/id1473118642?ls=1";
});
btnPlayStore.click(function() {
  window.location.href =
    "https://play.google.com/store/apps/details?id=com.servr";
});

function login() {
  // Show loading
  showLoadingButton(true);

  // Submit
  loginForm.submit();
}

function showLoadingButton(is_show = true) {
  if (is_show === true) {
    buttonLogin
      .html(`<i class="fa fa-refresh fa-spin fa-fw"></i> Please wait...`)
      .attr("disabled", "disabled");
  } else {
    buttonLogin.html(`Login`).attr("disabled", false);
  }
}

function changeIconPassword() {
  if (password.attr("type") === "text") {
    password.attr("type", "password");
    togglePassword.addClass("fa-eye-slash");
    togglePassword.removeClass("fa-eye");
  } else if (password.attr("type") === "password") {
    password.attr("type", "text");
    togglePassword.removeClass("fa-eye-slash");
    togglePassword.addClass("fa-eye");
  }
}

function isAllFormFilled() {
  let current = 0;
  const complete = 2;

  if (username.val() !== "") {
    current += 1;
  }
  if (password.val() !== "") {
    current += 1;
  }

  return current === complete;
}

function checkFormValidation(e) {
  if (isAllFormFilled() === true) {
    buttonLogin.prop("disabled", false);
    if (e.which === 13) {
      buttonLogin.click();
    }
  } else {
    buttonLogin.prop("disabled", true);
  }
}
