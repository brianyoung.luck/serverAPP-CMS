<?php $__env->startSection('title', __('Hotels')); ?>

<?php $__env->startSection('content'); ?>
    <div id="hotelsWrapper" >
        <div class="row">
            <h4>
                New hotel
            </h4>
            <div class="col-12 p-2">
                <a class="btn mb-4 btn-shadow" href="<?php echo e(route('admin.hotels.create')); ?>">Create new hotel</a>
            </div>
        </div>
        <div class="row">
            <h4>
                Hotels:
            </h4>
            <div class="col-12 p-2">
                <div class="card">
                    <table class="table table-responsive-sm table-striped" id="hotelTable">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Status</th>
                            <th>Expired at</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script>
        const url1 = '<?php echo route('data-tables.admin.hotels'); ?>';
        const url2 = '<?php echo route('admin.hotels.index'); ?>';

    </script>
    <script src="<?php echo e(asset('js/custom/admin/hotel.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/servrhotels.com/resources/views/admin/hotel/index.blade.php ENDPATH**/ ?>