<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="{{ env('APP_DESCRIPTION') }}">
    <meta name="author" content="{{ env('APP_AUTHOR') }}">
    <meta name="keyword" content="{{ env('APP_KEYWORD') }}">
    <title>@yield('title') - {{ env('APP_NAME') }}</title>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom-admin.css') }}" rel="stylesheet">
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
@include('admin.layouts.header')
<div class="app-body">
    @include('admin.layouts.sidebar')
    <main class="main">
        @yield('breadcrumb')
        <div class="container-fluid">
            @include('admin.layouts.flash-message')
            <div class="animated fadeIn">
                <div class="top-action-wrapper">
                    @yield('top-action')
                </div>
                @yield('content')
            </div>
        </div>
    </main>
</div>

<footer class="app-footer">
    <div class="ml-auto">
        <a href="#">{{ env('APP_NAME') }}</a>
        <span>&copy; {{ __('2019') }}</span>
    </div>
</footer>
<script src="{{ asset('js/app.js') }}"></script>
@yield('js')
</body>
</html>