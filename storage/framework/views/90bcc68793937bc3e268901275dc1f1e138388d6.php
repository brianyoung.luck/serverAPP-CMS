<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link <?php echo Request::is('dashboard') || Request::is('') ? 'active' : ''; ?>" href="<?php echo e(route('admin.dashboard.index')); ?>">
                    <i class="nav-icon icon-home"></i> <?php echo e(__('Home')); ?>

                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?php echo Request::is('hotels*') ? 'active' : ''; ?>" href="<?php echo e(route('admin.hotels.index')); ?>">
                    <i class="nav-icon icon-grid"></i> <?php echo e(__('Hotel')); ?>

                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="<?php echo e(route('admin.logout.index')); ?>">
                    <i class="nav-icon icon-logout"></i> <?php echo e(__('Logout')); ?>

                </a>
            </li>
        </ul>
    </nav>
</div><?php /**PATH /var/www/servrhotels.com/resources/views/admin/layouts/sidebar.blade.php ENDPATH**/ ?>