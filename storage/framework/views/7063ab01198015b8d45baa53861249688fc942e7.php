<?php $__env->startSection('title', __('Hotel settings')); ?>

<?php $__env->startSection('content'); ?>
    <div class="row hotelSettingsWrapper">
        <div class="col-12">
            <div class="card mb-5">
                <div class="card-header"><?php echo e(__('Change password')); ?></div>
                <div class="card-body">
                    <form id="changePasswordForm" method="post" action="<?php echo e(route('cms.hotels.reset-password', ['id' => auth('hotel')->user()->id])); ?>" enctype="multipart/form-data">
                        <?php echo method_field('put'); ?>
                        <?php echo csrf_field(); ?>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group col-sm-12 col-md-4">
                                    <label for="password">Old password</label>
                                    <input class="form-control <?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>" id="password" name="password" type="password" placeholder="">
                                    <?php if($errors->has('password')): ?>
                                        <div class="invalid-feedback">*<?php echo e($errors->first('password')); ?></div>
                                    <?php endif; ?>
                                </div>
                                <div class="form-group col-sm-12 col-md-4">
                                    <label for="new_password">New password</label>
                                    <input class="form-control <?php echo e($errors->has('new_password') ? ' is-invalid' : ''); ?>" id="new_password" name="new_password" type="password" placeholder="">
                                    <?php if($errors->has('new_password')): ?>
                                        <div class="invalid-feedback">*<?php echo e($errors->first('new_password')); ?></div>
                                    <?php endif; ?>
                                </div>
                                <div class="form-group col-sm-12 col-md-4">
                                    <label for="new_password_confirmation">New password confirmation</label>
                                    <input class="form-control <?php echo e($errors->has('new_password_confirmation') ? ' is-invalid' : ''); ?>" id="new_password_confirmation" name="new_password_confirmation" type="password" placeholder="">
                                    <?php if($errors->has('new_password_confirmation')): ?>
                                        <div class="invalid-feedback">*<?php echo e($errors->first('new_password_confirmation')); ?></div>
                                    <?php endif; ?>
                                </div>
                                <div class="form-group col-sm-12 col-md-4">
                                    <button type="submit" class="btn btn-info pull-right">Change password</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="card">
                <div class="card-header"><?php echo e(__('Hotel features')); ?></div>
                <div class="card-body">
                    <table class="table table-responsive-sm">
                        <tbody>
                        <tr>
                            <td><b>Check in</b></td>
                            <td>
                                <label class="switch switch-pill switch-success">
                                    <input type="checkbox" class="switch-input" name="is_check_in_enabled" id="is_check_in_enabled" <?php echo e($hotel->hotelFeature->is_check_in_enabled ? 'checked' : ''); ?>>
                                    <span class="switch-slider"></span>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Check out</b></td>
                            <td>
                                <label class="switch switch-pill switch-success">
                                    <input type="checkbox" class="switch-input" name="is_check_out_enabled" id="is_check_out_enabled" <?php echo e($hotel->hotelFeature->is_check_out_enabled ? 'checked' : ''); ?>>
                                    <span class="switch-slider"></span>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Spa</b></td>
                            <td>
                                <label class="switch switch-pill switch-success">
                                    <input type="checkbox" class="switch-input" name="is_spa_enabled" id="is_spa_enabled" <?php echo e($hotel->hotelFeature->is_spa_enabled ? 'checked' : ''); ?>>
                                    <span class="switch-slider"></span>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Restaurant</b></td>
                            <td>
                                <label class="switch switch-pill switch-success">
                                    <input type="checkbox" class="switch-input" name="is_restaurant_enabled" id="is_restaurant_enabled" <?php echo e($hotel->hotelFeature->is_restaurant_enabled ? 'checked' : ''); ?>>
                                    <span class="switch-slider"></span>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Concierge</b></td>
                            <td>
                                <label class="switch switch-pill switch-success">
                                    <input type="checkbox" class="switch-input" name="is_concierge_enabled" id="is_concierge_enabled" <?php echo e($hotel->hotelFeature->is_concierge_enabled ? 'checked' : ''); ?>>
                                    <span class="switch-slider"></span>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Cleaning service</b></td>
                            <td>
                                <label class="switch switch-pill switch-success">
                                    <input type="checkbox" class="switch-input" name="is_cleaning_enabled" id="is_cleaning_enabled" <?php echo e($hotel->hotelFeature->is_cleaning_enabled ? 'checked' : ''); ?>>
                                    <span class="switch-slider"></span>
                                </label>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script>
        const url3 = '<?php echo route('ajax.cms.hotel-features.index'); ?>';
        const id1 = '<?php echo auth('hotel')->user()->id; ?>';
    </script>
    <script src="<?php echo e(asset('js/custom/cms/setting.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('cms.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/servrhotels.com/resources/views/cms/settings/index.blade.php ENDPATH**/ ?>