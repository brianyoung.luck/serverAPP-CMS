<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingBillItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_bill_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('booking_bill_id');
            $table->string('billable_type');
            $table->unsignedBigInteger('billable_id');

            // Foreign Key
            $table->foreign('booking_bill_id')
                ->references('id')
                ->on('booking_bills')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_bill_items');
    }
}
