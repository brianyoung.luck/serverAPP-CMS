<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\HotelCheckIn;
use App\Services\AuthService;
use App\Services\HotelBookingService;
use App\Http\Controllers\Controller;
use App\Services\SendbirdService;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Models\Experience_Button;
use App\Models\Experience_Button_Image;
class ExperienceController extends Controller
{
    protected $hotelBookingService;

    public function __construct(HotelBookingService $hotelBookingService)
    {
        $this->hotelBookingService = $hotelBookingService;
    }

    public function getAll($hotel_id){
        $buttons = Experience_Button::where([['type','!=','Promotion'],['hotel_id','=',$hotel_id]])->get();
        return $buttons;
    }

    public function getPromotions($hotel_id){
        $buttons = Experience_Button::where([['type','=','Promotion'],['hotel_id','=',$hotel_id]])->get();
        return $buttons;
    }
    public function getButton($hotel_id,$button_id){
        $experience_button = Experience_Button::where('id',$button_id)->get();
        if(!$experience_button){
            abort(404);
        }

        $experience_button_images = Experience_Button_Image::where('experience_button_id',$button_id)->get();
        
        $experience_button["images"] = $experience_button_images;
        return $experience_button;
    }
}
