<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SpaTreatment extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'spa_id',
        'name',
        'description',
        'duration',
        'price',
        'image'
    ];

    public function spa()
    {
        return $this->belongsTo(Spa::class);
    }
}