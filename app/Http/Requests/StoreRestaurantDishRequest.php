<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreRestaurantDishRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category' => 'required|string|max:255',
            'name' => 'required|string|max:255',
            'description' => 'nullable',
            'price' => 'required|numeric|regex:/^(-)?[0-9]+(\.[0-9]{1,3})?$/',
              'image' => 'nullable|file|mimes:png,jpg,jpeg|max:4096',
        ];
    }

    public function messages()
    {
        return[
                   'image.mimes'=>'Please upload only png or jpg file.',
             'image.max'=>'Image is too large',
        'price.regex'=>"The price value should not be greater than three decimal points"
        ];
    }
}