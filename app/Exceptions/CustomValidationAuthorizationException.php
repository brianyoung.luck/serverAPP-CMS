<?php

namespace App\Exceptions;

use RuntimeException;

/**
 * Class CustomValidationAuthorizationException
 *
 * Source: https://laracasts.com/discuss/channels/general-discussion/using-custom-exception-for-handling-json-responses
 *
 * @package App\Exceptions
 */
class CustomValidationAuthorizationException extends RuntimeException
{
    /**
     * Any extra data to send with the response.
     *
     * @var array
     */
    public $error_message;

    /**
     * The status code to use for the response.
     *
     * @var integer
     */
    public $status = 422;

    /**
     * Create a new exception instance.
     *
     * @param string        $message
     * @param $error_message
     */
    public function __construct($message, $error_message)
    {
        parent::__construct($message);

        $this->error_message = $error_message;
    }

    public function render()
    {
        return response()->json(
            [
                'message' => $this->getMessage(),
                'errors'   => $this->error_message,
            ],
            $this->status
        );
    }
}
