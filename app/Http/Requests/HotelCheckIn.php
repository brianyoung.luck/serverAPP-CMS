<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Waavi\Sanitizer\Laravel\SanitizesInput;

class HotelCheckIn extends FormRequest
{
    use SanitizesInput;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function filters()
    {
        return [
            'cardholder_name' => 'capitalize|escape',
            'phone_number' => 'trim',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'hotel_id' => 'required|integer',
            'arrival_date' => 'date_format:Y-m-d H:i|after:yesterday|before:departure_date',
            'departure_date' => 'date_format:Y-m-d H:i|after:now,arrival_date',
            'cardholder_name' => 'required',
            'card_expiry_date' => 'date_format:m/y|after:now',
            'card_number' => 'required|size:16',
            'card_address' => 'required',
            'phone_number' => ['required', 'regex:/^\+\d+$/'],
            'passport_photos' => 'required',
            'passport_photos.*' => 'image|mimes:jpeg,png,jpg|max:2048',
            'reference' => 'required',
        ];
    }
}
