<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Experience_Button extends Model
{

	protected $table = "experience_buttons";
	public $timestamps = false;
    
    public function images(){
        return $this->hasMany('App\Models\Experience_Button_Image');
    }

}
