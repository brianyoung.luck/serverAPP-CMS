<?php

use App\Models\Hotel;
use Illuminate\Database\Seeder;

class ConciergeServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $hotels = Hotel::all();

        foreach ($hotels as $hotel)
        {
            $conciergeServices = [
                [
                    'hotel_id' => $hotel->id,
                    'name' => 'AC power adapter',
                ],
                [
                    'hotel_id' => $hotel->id,
                    'name' => 'Toothbrush',
                ],
                [
                    'hotel_id' => $hotel->id,
                    'name' => 'Tooth paste',
                ],
                [
                    'hotel_id' => $hotel->id,
                    'name' => 'Mouth wash',
                ],
                [
                    'hotel_id' => $hotel->id,
                    'name' => 'Body soap',
                ],
                [
                    'hotel_id' => $hotel->id,
                    'name' => 'Shampoo',
                ],
                [
                    'hotel_id' => $hotel->id,
                    'name' => 'Conditioner',
                ],
                [
                    'hotel_id' => $hotel->id,
                    'name' => 'Slippers',
                ],
                [
                    'hotel_id' => $hotel->id,
                    'name' => 'Towels',
                ],
                [
                    'hotel_id' => $hotel->id,
                    'name' => 'Water',
                ],
                [
                    'hotel_id' => $hotel->id,
                    'name' => 'Hair comb',
                ],
                [
                    'hotel_id' => $hotel->id,
                    'name' => 'Fresh fruit',
                ],
                [
                    'hotel_id' => $hotel->id,
                    'name' => 'Bed linen',
                ],
            ];

            DB::table('concierge_services')->insert($conciergeServices);
        }
    }
}
