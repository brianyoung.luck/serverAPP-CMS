<?php

namespace App\Exceptions;

use RuntimeException;

/**
 * Class CustomValidationAuthorizationException
 *
 * Source: https://laracasts.com/discuss/channels/general-discussion/using-custom-exception-for-handling-json-responses
 *
 * @package App\Exceptions
 */
class CustomAuthorizationException extends RuntimeException
{
    /**
     * Any extra data to send with the response.
     *
     * @var array
     */
    public $error_message;

    /**
     * The status code to use for the response.
     *
     * @var integer
     */
    public $status = 401;

    /**
     * Create a new exception instance.
     *
     * @param string        $message
     * @param $error_message
     */
    public function __construct($message, $error_message = null)
    {
        parent::__construct($message);

        $this->error_message = $error_message;
    }

    public function render()
    {
        $data = [
            'message' => $this->getMessage(),
        ];

        if ($this->error_message) {
            $data['errors'] = $this->error_message;
        }

        return response()->json($data, $this->status);
    }
}
