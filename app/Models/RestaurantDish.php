<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Services\FileStorageService;

class RestaurantDish extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'restaurant_id',
        'category',
        'name',
        'description',
        'price',
    ];

    public function categories()
    {
        return $this->belongsToMany(RestaurantDishCategory::class);
    }

    public function restaurant()
    {
        return $this->belongsTo(Restaurant::class);
    }

    public function roomService()
    {
        return $this->belongsToMany(RestaurantRoomService::class)->withPivot(['price', 'note', 'qty']);
    }

    /**
     * Scope section
     */

    public function scopeFindByRestaurantId($query, $restaurant_id)
    {
        return $query->where('restaurant_id', $restaurant_id);
    }

    /**
     * Getters
     */
    public function getImageAttribute($value)
    {
        return $this->getS3Url($value);
	}
	
	private function getS3Url($value)
    {
        $fileStorageService = new FileStorageService(env('FILESYSTEM_DRIVER'));
        $storageServiceName = $fileStorageService->getStorageServiceName($value);
        switch ($storageServiceName) {
            case 's3':
                return $fileStorageService->getTemporaryUrl($value);
                break;
            default:
                return $value;
        }
    }
}

