<?php $__env->startSection('title', __('Guests')); ?>

<?php $__env->startSection('content'); ?>
<div id="guestsWrapper">
    <div class="row">
        <h4>
            New arrivals: <span class="badge badge-primary badge-pill"><?php echo e($newArrivals->count()); ?></span>
        </h4>
        <?php $helperService = app('App\Services\HelperService'); ?>

        <form id="deleteHotelBookingForm" method="post"
            action="<?php echo e(route('cms.hotel-bookings.destroy', ['hotel-booking' => 0])); ?>">
            <?php echo method_field('delete'); ?>
            <?php echo csrf_field(); ?>
            <input type="hidden" name="hotelBookingId" id="hotelBookingId" value="0">
        </form>
        <form id="editHotelBookingForm" method="post"
            action="<?php echo e(route('cms.hotel-bookings.update', ['hotel-booking' => 0])); ?>">
            <?php echo method_field('put'); ?>
            <?php echo csrf_field(); ?>
            <input type="hidden" name="hotelBookingId" id="hotelBookingIdUpdate" value="0">
            <input type="hidden" name="roomNumber" id="roomNumber" value="0">
        </form>
        <div class="col-12 newArrivalsWrapper">
            <?php $__currentLoopData = $newArrivals; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $newArrival): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="newArrivalWrapper" data-id="<?php echo e($newArrival->id); ?>">
                <div>
                    <div class="userFullNameWrapper">
                        <p class="userFullName"><?php echo e($newArrival->cardholder_name); ?></p>
                        <?php if($newArrival->passportPhotos->count() > 1): ?>
                        <p class="counter">+<?php echo e($newArrival->passportPhotos->count() - 1); ?></p>
                        <?php endif; ?>
                    </div>
                    <p class="bookingRefText">
                        Booking ref#:
                    </p>
                    <p class="bookingRef">
                        <?php echo e($newArrival->reference ? $newArrival->reference : '-'); ?>

                    </p>
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
        <!-- Modal -->
        <div class="modal fade userProfileModal" tabindex="-1" role="dialog" aria-labelledby="userProfileModal"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <h4 class="userProfileText">User profile:</h4>
                        <p>Passport entries</p>
                        <div class="passportEntriesWrapper" id="newGuestPassportEntriesWrapper">
                            <div class="passportEntryWrapper">
                                <a href="#" target="_blank">
                                    <img src="" alt="Passport photo" class="copyImage">
                                </a>
                            </div>
                        </div>

                        <div class="row userProfileDetails">
                            <?php $helperService = app('App\Services\HelperService'); ?>

                            <div class="col-12 col-md-6">
                                <p>
                                    Name: <span id="newGuestName" class="copyText" data-toggle="tooltip"
                                        data-placement="left" title="Click on it to copy"><i
                                            class="fa fa-refresh fa-spin fa-fw"></i></span><br />
                                    Date of Birth: -<br />
                                    Booking reference: <span id="newGuestReference" class="copyText"
                                        data-toggle="tooltip" data-placement="left" title="Click on it to copy"><i
                                            class=" fa fa-refresh fa-spin fa-fw"></i></span><br />
                                    Arrival date: <span id="newGuestArrivalDate" class="copyText" data-toggle="tooltip"
                                        data-placement="left" title="Click on it to copy"><i
                                            class="fa fa-refresh fa-spin fa-fw"></i></span><br />
                                    Departure date: <span id="newGuestDepartureDate" class="copyText"
                                        data-toggle="tooltip" data-placement="left" title="Click on it to copy"
                                        data-id="newGuestDepartureDate"><i
                                            class="fa fa-refresh fa-spin fa-fw"></i></span><br />
                                </p>
                            </div>
                            <div class="col-12 col-md-6">
                                <p>
                                    Credit card number: <span id="newGuestCardCardNumber" class="copyText"
                                        data-toggle="tooltip" data-placement="left" title="Click on it to copy"
                                        data-id="newGuestCardCardNumber"><i
                                            class="fa fa-refresh fa-spin fa-fw"></i></span><br />
                                    Exp. date: <span id="newGuestCardExpiryDate" class="copyText" data-toggle="tooltip"
                                        data-placement="left" title="Click on it to copy"
                                        data-id="newGuestCardExpiryDate"><i
                                            class="fa fa-refresh fa-spin fa-fw"></i></span><br />
                                    Name on card: <span id="newGuestCardholderName" class="copyText"
                                        data-toggle="tooltip" data-placement="left" title="Click on it to copy"
                                        data-id="newGuestCardholderName"><i
                                            class="fa fa-refresh fa-spin fa-fw"></i></span><br />
                                    Address: <span id="newGuestCardAddress" class="copyText" data-toggle="tooltip"
                                        data-placement="left" title="Click on it to copy"
                                        data-id="newGuestCardAddress"><i
                                            class="fa fa-refresh fa-spin fa-fw"></i></span><br />
                                </p>
                                <div>
                                    <div>
                                        <button class="btn btn-pill btn-outline-danger declineBtn" data-id="0"
                                            type="button">Decline</button>
                                        <button class="btn btn-pill btn-success acceptBtn" data-id="0" type="button"
                                            aria-pressed="true">Accept</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <h4>
            Checkout time: <?php echo e($helperService::dateTimeFormat(auth('hotel')->user()->checkout_time, 'g:i A')); ?>

            <button class="btn btn-shadow ml-2" id="changeCheckoutTimeBtn">Change</button>
        </h4>
    </div>
    <div class="row">
        <h4>
            Late checkout time: <?php echo e($helperService::dateTimeFormat(auth('hotel')->user()->late_checkout_time, 'g:i A')); ?>

            <button class="btn btn-shadow ml-2" id="changeLateCheckoutTimeBtn">Change</button>
        </h4>
    </div>
</div>
<form id="editHotelDetailsForm" method="post"
    action="<?php echo e(route('cms.hotels.update', ['id' => auth('hotel')->user()->id])); ?>" enctype="multipart/form-data">
    <?php echo csrf_field(); ?>
    <?php echo method_field('put'); ?>
    <input type="hidden" name="checkout_time" id="checkout_time" value="<?php echo e(auth('hotel')->user()->checkout_time); ?>">
    <input type="hidden" name="late_checkout_time" id="late_checkout_time" class="copyText" data-id="late_checkout_time"
        value="<?php echo e(auth('hotel')->user()->late_checkout_time); ?>">
</form>
<div class="row">
    <h4>
        Late checkouts: <span class="badge badge-primary badge-pill copyText" id="late_checkout_time"
            data-id="late_checkout_time"><?php echo e($lateCheckouts->count()); ?></span>
    </h4>
    

<div class="col-lg-10 col-md-9 orderItemsWrapper">
    <?php $__currentLoopData = $lateCheckouts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lateCheckout): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <div class="orderWrapper lateCheckoutWrapper" data-id="<?php echo e($lateCheckout->id); ?>">
        <div class="roomTextWrapper">
            

        </div>
        <div class="userFullNameWrapper">
            <p> Name: <span class="userFullName copyText" id="myTooltip" data-toggle="tooltip"
                    title="Click to copy"><?php echo e($lateCheckout->cardholder_name); ?></span></p>
            <?php if($lateCheckout->passportPhotos->count() > 1): ?>
            <p> Images: <span class="counter copyText">+<?php echo e($lateCheckout->passportPhotos->count() - 1); ?></span></p>
            <?php endif; ?>
        </div>
        <p class="bookingRefText">
            Booking ref#: <span class="bookingRef copyText" data-toggle="tooltip" title="Click to copy">
                <?php echo e($lateCheckout->reference ? $lateCheckout->reference : '-'); ?>

            </span>
        </p>


    </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>

</div>

<div class="row mt-2">
    <h4>
        Guests:
    </h4>
    <form id="checkoutHotelBookingForm" method="post"
        action="<?php echo e(route('cms.hotel-bookings.update', ['hotel-booking' => 0])); ?>">
        <?php echo method_field('put'); ?>
        <?php echo csrf_field(); ?>
        <input type="hidden" name="hotelBookingId" id="hotelBookingIdCheckout" value="0">
        <input type="hidden" name="updateType" id="updateType" value="checkout">
    </form>
    <form id="lateCheckoutHotelBookingForm" method="post"
        action="<?php echo e(route('cms.hotel-bookings.update', ['hotel-booking' => 0])); ?>">
        <?php echo method_field('put'); ?>
        <?php echo csrf_field(); ?>
        <input type="hidden" name="hotelBookingId" id="hotelBookingIdLateCheckout" value="0">
        <input type="hidden" name="updateType" id="updateType" value="lateCheckout">
        <input type="hidden" name="statusLateCheckout" id="statusLateCheckout" value="accepted">
    </form>
    <div class="col-12 p-2">
        <div class="card">
            <table class="table table-responsive-sm table-striped" id="guestTable">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Booking Ref#</th>
                        <th>Room number</th>
                        <th>Arrival date</th>
                        <th>Departure date</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
            </table>
            <!-- Modal -->
            <div class="modal fade guestProfileModal focus" style="" id="guestProfileModal" tabindex="-1" role="dialog"
                aria-labelledby="guestProfileModal" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content guestProfileModal" id="guestProfileModal">
                        <div class="modal-body">
                            <h4 class="roomNumberText cl copyText">Room x</h4>
                            <p>Passport entries</p>
                            <div class="passportEntriesWrapper" id="guestPassportEntriesWrapper">
                                <div class="passportEntryWrapper">
                                    <a href="#" target="_blank">
                                        <img src="" alt="Passport photo">
                                    </a>
                                </div>
                            </div>
                            <p class="msgText text-white mt-3 text-center shadow"></p>
                            <div class="row userProfileDetails">
                                <?php $helperService = app('App\Services\HelperService'); ?>
                                <div class="col-12 col-md-6">
                                    <p>
                                        Name: <span id="guestName" class="copyText"><i
                                                class="fa fa-refresh fa-spin fa-fw"></i></span><br />
                                        Date of Birth: -<br />
                                        Booking reference: <span id="guestReference" class="copyText"><i
                                                class="fa fa-refresh fa-spin fa-fw"></i></span><br />
                                        Arrival date: <span id="guestArrivalDate" class="copyText"><i
                                                class="fa fa-refresh fa-spin fa-fw"></i></span><br />
                                        Departure date: <span id="guestDepartureDate" class="copyText"><i
                                                class="fa fa-refresh fa-spin fa-fw"></i></span><br />
                                    </p>
                                </div>
                                <div class="col-12 col-md-6">
                                    <p>
                                        Credit card number: <span id="guestCardNumber" class="copyText"><i
                                                class="fa fa-refresh fa-spin fa-fw"></i></span><br />
                                        Exp. date: <span id="guestCardExpiryDate" class="copyText"><i
                                                class="fa fa-refresh fa-spin fa-fw"></i></span><br />
                                        Name on card: <span id="guestCardholderName" class="copyText"><i
                                                class="fa fa-refresh fa-spin fa-fw"></i></span><br />
                                        Address: <span id="guestCardAddress" class="copyText"><i
                                                class="fa fa-refresh fa-spin fa-fw"></i></span><br />
                                    </p>
                                    <div>
                                        <button class="btn btn-pill btn-warning lateCheckoutBtn d-inline" data-id="0"
                                            type="button" aria-pressed="true">Late</button>
                                        <button class="btn btn-pill btn-success checkoutBtn" data-id="0" type="button"
                                            aria-pressed="true">Checkout</button>
                                    </div>
                                </div>
                                <div class="row mt-3 p-3">
                                    
                                    <div class="col">
                                        <div class="sigWrapper">
                                            <h4 style="color: #5CB9EA; display:none;">Customer Signature Optional</h4>
                                            <canvas style="border: 3px dotted #5CB9EA; display:none;"></canvas>

                                        </div>
                                        <button class="btn btn-pill btn-primary d-none" id="clearSig" data-id="0"
                                            type="button" aria-pressed="true">Clear</button>

                                    </div>
                                </div>
                                <div class="row mt-3 p-3 bookingBillWrapper">
                                    <h4>Bill details</h4>

                                    <table class="table table-responsive-sm table-striped bookingBillTable">
                                        <thead>
                                            <tr>
                                                <th>Service</th>
                                                <th>From</th>
                                                <th>Price</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </tbody>

                                    </table>

                                    <h5 class="totalVatPrice w-100 text-right">VAT: 123</h5>
                                    <h5 class="totalServiceCharges w-100 text-right">Service Charges: 123</h5>
                                    <h5 class="totalBillPrice w-100 text-right">Total price 123</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>






<input type="hidden" name="getHotelId" value="0" data-id="0" class="getHotelId" id="getHotelId">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.0/clipboard.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/printThis/1.15.0/printThis.min.js"></script>
<script>
    const url1 = '<?php echo route('data-tables.cms.guests'); ?>';
        const url2 = '<?php echo route('cms.guests.index'); ?>';
        const url3 = '<?php echo route('data-tables.cms.guests'); ?>';
        const url4 = '<?php echo route('ajax.cms.guests.index'); ?>';
</script>
<script>
    const curl1 = '<?php echo route('data-tables.cms.guests'); ?>';
        const curl2 = '<?php echo route('cms.guests.index'); ?>';
        const curl3 = '<?php echo route('data-tables.cms.guests'); ?>';
        const curl4 = '<?php echo route('ajax.cms.guests.index'); ?>';
        const printUrl = '<?php echo url('ajax/guests/printbill/'); ?>';
        const redirectType='guests';
        const val1 = '<?php echo auth('hotel')->user()->checkout_time; ?>';
        const val2 = '<?php echo auth('hotel')->user()->late_checkout_time; ?>';
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
<script src="<?php echo e(asset('js/custom/cms/checkout.js')); ?>"></script>
<script src="<?php echo e(asset('js/custom/cms/guest.js')); ?>"></script>
<script>
    $( document ).ready(function() {
    
    new ClipboardJS('.copy');
    
    });
    async function openItemPopUp(type) {
    const hotelBookingId=$('#getHotelId').attr('data-id');
    const guestDetails =await getGuestDetails(hotelBookingId);
    const bookingBill=guestDetails.bookingBill;
   
for (const item of bookingBill.items) {

if (item.billable_type === "App\\Models\\RestaurantRoomService") {
    var html='';
    $('.itemDeatil'+item.id).html('');
    for(const dish of item.billable.dishes){
         html+=`<div class="col-md-6">
            <span>${dish.name}</span>
        </div>
        <div class="col-md-6">
            <span> ${dish.pivot.price*dish.pivot.qty}</span>
        </div>`;

//   $('#restaurantRoomService').collapse();
  }

        $('.itemDeatil'+item.id).html(html);
} else if (item.billable_type === "App\\Models\\SpaBooking") {
    $('.itemDeatil'+item.id).html('');
for(const treatment of item.billable.treatments){
            $('.itemDeatil'+item.id).append(`
            <div class="col-md-6">
                <span>${treatment.name}</span> 
            </div>
            <div class="col-md-6">
                <span>${treatment.pivot.price}</span>
            </div>`);
        // $('#spaRoomService' +item.id).collapse();
}
}
// i++;
}

$(type).addClass("show");
}

async function getGuestDetails(hotelBookingId) {
let promise = new Promise(function(resolve, reject) {
$.ajax({
headers: {
"X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
},
type: "GET",
url: url4 + "/" + hotelBookingId,
contentType: "application/json",
dataType: "json",
})
.done(function(response) {
resolve(response);
})
.fail(function(error) {
console.log(error);
reject();
});
});

try {
return await promise;
} catch (err) {
return {
status: false,
error: err,
};
}
}
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('cms.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\XAMP\htdocs\sevr_hotel\resources\views/cms/guest/index.blade.php ENDPATH**/ ?>