<?php

namespace App\Http\Controllers\Cms;

use App\Http\Requests\LoginAdminRequest;
use App\Http\Requests\LoginHotelAdminRequest;
use App\Models\Admin;
use App\Http\Controllers\Controller;
use App\Models\Hotel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Hotel_Staff;
use App\Models\User;

class AuthController extends Controller
{
    public function loginForm()
    {
        if (auth('hotel')->check()) {
            return redirect()->route('cms.dashboard.index');
        }
        $for = "hotel_admin";
        if (\Request::route()->getName() === "cms.staff_login.index") {
            $for = "hotel_staff";
        }
        return view('cms.auth.login.index', [
            'for' => $for]);
        return view('cms.auth.login.index');
    }

    public function login(LoginHotelAdminRequest $request)
    {
        // check hotel membership expired or not
        $hotels = Hotel::where('expired_at', '<=', Carbon::now())->get();

        foreach ($hotels as $key => $hotel) {
            $hotel->is_active = 0;
            $hotel->save();
        }
        //

        $credentials = [
            'email' => $request->email,
            'password' => $request->password,
        ];

        if (Auth::guard('hotel')->attempt($credentials)) {
            $hotel=Hotel::where('id', Auth::guard('hotel')->user()->id)->first();
            return redirect()->route('cms.dashboard.index');
        }

        return Redirect::back()->withInput()->with('error', 'Incorrect credentials!.');
    }

    public function staff_login(Request $request)
    {
        $hotels = Hotel::where('expired_at', '<=', Carbon::now())->get();

        foreach ($hotels as $key => $hotel) {
            $hotel->is_active = 0;
            $hotel->save();
        }
        
        $email = $request->input('email');
        $password = $request->input('password');
        $validUser = Hotel_Staff::where([['email','=',$email],['password','=',$password]])->first();
        
        //dd(DB::getQueryLog()); // Show results of log
        if (!$validUser) {
            return Redirect::back()->withInput()->with('error', 'Incorrect credentials!.');
        }
        $hotel_account = Hotel::where('id', $validUser->hotel_id)->first();
        Auth::guard('hotel')->login($hotel_account);
        Auth::guard('hotel_staff')->login($validUser);
        return redirect()->route('cms.dashboard.index');
    }

    public function logout()
    {
        Auth::guard('hotel')->logout();
        if (auth('hotel_staff')->user()) {
            Auth::guard('hotel_staff')->logout();
        }
        return redirect()->route('cms.login.index');
    }
}