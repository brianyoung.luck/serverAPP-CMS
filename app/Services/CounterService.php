<?php

namespace App\Services;

use App\Models\ConciergeServiceRequest;
use App\Models\HotelBooking;
use App\Models\Restaurant;
use App\Models\RestaurantReservation;
use App\Models\RestaurantRoomService;
use App\Models\SpaBooking;
use Illuminate\Database\Eloquent\Builder;

class CounterService
{
    public static function getCountPendingConcierge()
    {
        $conciergeServicePendingRequests = ConciergeServiceRequest::whereIn('status', ['pending', 'confirmed', 'preparing', 'on_the_way'])
            ->whereHas('hotelBooking', function (Builder $query) {
                $query->where('hotel_id', auth('hotel')->user()->id);
            })
            ->count();

        return $conciergeServicePendingRequests > 9 ? '9+' : $conciergeServicePendingRequests;
    }

    public static function getCountPendingRestaurant($restaurantId = null)
    {
        if ($restaurantId != null) {
            $count1 = RestaurantRoomService::where('restaurant_id', $restaurantId)
                ->whereIn('status', ['pending', 'confirmed', 'preparing', 'on_the_way'])
                ->count();
            $count2 = RestaurantReservation::where('restaurant_id', $restaurantId)
                ->where('status', 'pending')
                ->count();
        } else {
            $hotel = auth('hotel')->user();
            $restaurantIds = Restaurant::where('hotel_id', $hotel->id)
                ->get()
                ->pluck('id')
                ->toArray();
            $count1 = RestaurantRoomService::whereIn('restaurant_id', $restaurantIds)
                ->whereIn('status', ['pending', 'confirmed', 'preparing', 'on_the_way'])
                ->count();
            $count2 = RestaurantReservation::whereIn('restaurant_id', $restaurantIds)
                ->where('status', 'pending')
                ->count();
        }

        $count3 = $count1 + $count2;

        return $count3 > 9 ? '9+' : $count3;
    }

    public static function getCountPendingCheckout()
    {
        $count1 = HotelBooking::where('hotel_id', auth('hotel')->user()->id)
            ->whereDate('departure_date', '<=', now())
            ->where('status', 'active')
            ->count();

        return $count1 > 9 ? '9+' : $count1;
    }

    public static function getCountGuest()
    {
        $newArrivalsCounter = HotelBooking::newArrivals()
            ->count();
        $lateCheckoutsCounter = HotelBooking::where('hotel_id', auth('hotel')->user()->id)
            ->where('late_check_out', 'pending')
            ->where('status', 'active')
            ->count();
        $count1 = $newArrivalsCounter + $lateCheckoutsCounter;

        return $count1 > 9 ? '9+' : $count1;
    }

    public static function getCountPendingSpa($spaId)
    {
        $count1 = SpaBooking::where('spa_id', $spaId)
            ->whereIn('status', ['pending'])
            ->count();

        return $count1 > 9 ? '9+' : $count1;
    }

}