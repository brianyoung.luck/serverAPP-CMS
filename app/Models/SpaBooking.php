<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SpaBooking extends Model
{
    protected $fillable = [
        'spa_id',
        'hotel_booking_id',
        'type',
        'people_number',
        'booking_date',
        'total_price',
        'status',
    ];

    protected $attributes = [
        'status' => 'pending',
    ];

    public function treatments()
    {
        return $this->belongsToMany(SpaTreatment::class)->withPivot(['price']);
    }

    public function hotelBooking()
    {
        return $this->belongsTo(HotelBooking::class);
    }

    public function spa()
    {
        return $this->belongsTo(Spa::class);
    }

    public function spaTreatments()
    {
        return $this->belongsToMany(SpaTreatment::class)->withPivot(['price']);
    }

    public function scopeStatus($query, $status)
    {
        return $query->where('status', $status);
    }

    public function bookingBills()
    {
        return $this->morphToMany(BookingBill::class, 'billable')->groupBy('id');
    }
}