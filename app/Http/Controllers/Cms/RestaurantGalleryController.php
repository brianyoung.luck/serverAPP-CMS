<?php

namespace App\Http\Controllers\Cms;

use App\Services\RestaurantService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\RestaurantGallery;
use Response;
use Illuminate\Support\Facades\Input;

class RestaurantGalleryController extends Controller
{
    private $restaurantService;

    public function __construct(RestaurantService $restaurantService)
    {
        $this->restaurantService = $restaurantService;
    }

    public function destroy(Request $request)
    {
        $this->restaurantService->deleteRestaurantGallery($request->get('restaurantGalleryId', 0));
        return redirect()->route('cms.restaurants.show', ['restaurant' => $request->get('restaurantId', 0)])->with('success', 'Delete gallery success.');
    }

    public function sortGallery(){
        $val = Input::get('sorted');
        $i = 0;
        foreach ($val as $id) {
            $gallery = RestaurantGallery::where('id',$id)->first();
            $gallery->sort_id = $i++;
            $gallery->save();
        }
        
        return "done";
    }
}
