<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantDishRestaurantRoomServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_dish_restaurant_room_service', function (Blueprint $table) {
            $table->unsignedBigInteger('restaurant_room_service_id');
            $table->unsignedBigInteger('restaurant_dish_id');
            $table->double('price', 10, 2);
            $table->unsignedInteger('qty');
            $table->string('note')->nullable();
            $table->timestamps();

            // Foreign Keys
            $table->foreign('restaurant_dish_id', 'dish_room_service_dish_id_foreign')
                ->references('id')
                ->on('restaurant_dishes')
                ->onDelete('restrict');
            $table->foreign('restaurant_room_service_id', 'dish_room_service_room_service_id_foreign')
                ->references('id')
                ->on('restaurant_room_services')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant_room_service_details');
    }
}
