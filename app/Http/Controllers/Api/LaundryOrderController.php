<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\CreateLaundryOrderRequest;
use App\Http\Resources\Customer\LaundryOrderResource;
use App\Http\Resources\Customer\LaundryOrderResourceCollection;
use App\Services\LaundryOrderService;
use App\Services\SendbirdService;
use App\Http\Controllers\Controller;
use Auth;

class LaundryOrderController extends Controller
{
    protected $laundryOrderService;

    public function __construct(LaundryOrderService $laundryOrderService)
    {
        $this->laundryOrderService = $laundryOrderService;
    }

    public function create(CreateLaundryOrderRequest $request, SendbirdService $sendbirdService)
    {
        $booking = Auth::user();

        $validated = $request->validated();
        $result = $this->laundryOrderService->createOrder($booking, $validated);

        // Notification to admin
        $sendbirdService->sendAdminMessage($booking, [
            'text' => 'Laundry service has been requested',
            'payload' => [
                'type' => 'laundry_order',
                'id' => $result->id,
            ],
        ]);

        return (new LaundryOrderResource($result))
            ->additional([
                'message' => 'Create laundry order success'
            ])
            ->response()
            ->setStatusCode(201);
    }

    public function getAllOrders()
    {
        $result = Auth::user()->laundryOrders;

        return (new LaundryOrderResourceCollection($result))
            ->additional([
                'message' => 'Get all laundry orders success'
            ])
            ->response()
            ->setStatusCode(201);
    }
}
