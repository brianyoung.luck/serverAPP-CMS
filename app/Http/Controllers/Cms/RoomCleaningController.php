<?php

namespace App\Http\Controllers\Cms;

use App\Models\RoomCleaningOrder;
use App\Services\SendbirdService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RoomCleaningController extends Controller
{
    public function update(Request $request, SendbirdService $sendbirdService) {
        $roomCleaningOrder = RoomCleaningOrder::where('id', $request->get('room_cleaning_request_id', 0))
            ->whereHas('hotelBooking', function (Builder $query) {
                $query->where('hotel_id', auth('hotel')->user()->id);
            })
            ->first();
        if (!$roomCleaningOrder) {
            abort(404);
        }

        $roomCleaningOrder->status = $request->get('room_cleaning_request_status');
        $roomCleaningOrder->save();

        // Notification via sendbird
        $sendbirdService->sendAdminMessage($roomCleaningOrder->hotelBooking, [
            'text' => 'Your room cleaning order is ' . ucwords(str_replace("_", " ", $roomCleaningOrder->status)),
            'payload' => [
                'type' => 'room_cleaning_order',
                'id' => $roomCleaningOrder->id,
            ],
        ]);

        return redirect()->route('cms.concierges.index')->with('success', 'Success.');
    }
}
