/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 8);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/custom/cms/chat.js":
/*!*****************************************!*\
  !*** ./resources/js/custom/cms/chat.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var chatWrapperChatId = "";
var chatsWrapper = $(".chatsWrapper");
var chatGroupBodyWrapperUl = $("#chatGroupBodyWrapperUl");
var chatCount = $("#chatCount");
var imgUrlChatAdmin = "https://servr-public.s3-ap-southeast-1.amazonaws.com/chat/cs.png";
var imgUrlChatHotel = "https://servr-public.s3-ap-southeast-1.amazonaws.com/chat/hotel.png";
$(document).ready(function () {
  // On click close chat button
  $(document).on("click", ".headerLiveChatClose", function () {
    closeChat($(this).data("chat_id"));
  }); // On press send chat button

  $(document).on("click", ".sendChatBtn", function () {
    sendChat($(this).data("chat_id"));
  }); // On press enter

  $(document).on("keyup", ".textChat", function (e) {
    isEnterPressed(e, $(this).data("chat_id"));
  }); // On click chat group item

  $(document).on("click", "#chatGroupBodyWrapperUl li", function () {
    showSelectedChat($(this).data("chat_id"), $(this).data("chat_name"), $(this));
  });
  initChat();
});
/**
 * Init chat
 */

function initChat() {
  var chatData = [{
    chat_id: "301",
    chat_name: "Room 301"
  }, {
    chat_id: "302",
    chat_name: "Room 302"
  }, {
    chat_id: "303",
    chat_name: "Room 303"
  }, {
    chat_id: "304",
    chat_name: "Room 304"
  }, {
    chat_id: "305",
    chat_name: "Room 305"
  }]; // Update chat body chat group

  updateChatBodyChatGroup(chatData); // Update counter chat group

  updateCounterChatGroup();
}
/**
 * Create & show chat
 * @param chatId
 * @param chatName
 * @param liElement
 */


function showSelectedChat(chatId, chatName, liElement) {
  // Remove chat from chat group
  liElement.remove(); // Update counter chat group

  updateCounterChatGroup(); // Create chat details

  createNewChat({
    chat_id: chatId,
    chat_name: chatName
  }); // Bind event scroll

  bindEventScrollChat(chatId);
}
/**
 * Update counter chat group
 */


function updateCounterChatGroup() {
  chatCount.text(chatGroupBodyWrapperUl.children().length);
}
/**
 * Update chat body in chat group
 * @param chatData
 */


console.log(chatData);

function updateChatBodyChatGroup(chatData) {
  chatGroupBodyWrapperUl.text("");
  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    for (var _iterator = chatData[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var element = _step.value;
      chatGroupBodyWrapperUl.append("\n            <li data-chat_id=\"".concat(element.chat_id, "\" data-chat_name=\"").concat(element.chat_name, "\">").concat(element.chat_name, "</li>\n        "));
    }
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator["return"] != null) {
        _iterator["return"]();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }
}
/**
 * Create new chat
 * @param chatData
 */


function createNewChat(chatData) {
  // Create chat details
  chatsWrapper.append("\n        <div class='chatWrapper' id=\"chatWrapper".concat(chatData.chat_id, "\">\n            <div class='topHeaderChatGuest'>\n                <span class=\"headerLiveChatText\">Live chat</span>\n                <i class=\"fa fa-close pull-right headerLiveChatClose\" data-chat_id=\"chatWrapper").concat(chatData.chat_id, "\"></i>\n                <span class=\"headerRoomText pull-right\">").concat(chatData.chat_name, "</span>\n            </div>\n            <div class=\"bodyRoomBox\">\n                <ul class='chatBox'>\n                    <li class='me'>\n                        <div class='avatar-icon'>\n                            <img src='").concat(imgUrlChatAdmin, "'>\n                        </div>\n                        <div class='messages'>\n                            <p>Hi fine bro.</p>\n                        </div>\n                    </li>\n                    <li class='another'>\n                        <div class='avatar-icon'>\n                            <img src='").concat(imgUrlChatHotel, "'>\n                        </div>\n                        <div class='messages'>\n                            <p>\n                                <div class=\"ticontainer\">\n                                    <div class=\"tiblock\">\n                                        <div class=\"tidot\"></div>\n                                        <div class=\"tidot\"></div>\n                                        <div class=\"tidot\"></div>\n                                    </div>\n                                </div>\n                            </p>\n                        </div>\n                    </li>\n                </ul>\n                <div class='actionFooter'>\n                    <div class=\"row\">\n                        <div class=\"col-9 textChatWrapper\">\n                            <div class=\"form-group\">\n                                <input class=\"form-control textChat\" id=\"name\" type=\"text\" placeholder=\"Enter text here\" autocomplete=\"off\" data-chat_id=\"chatWrapper").concat(chatData.chat_id, "\">\n                            </div>\n                        </div>\n                        <div class=\"col-3 btnSendWrapper\">\n                            <button class=\"btn sendChatBtn\" type=\"button\" data-chat_id=\"chatWrapper").concat(chatData.chat_id, "\">Send</button>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    "));
}
/**
 * Bind scroll event for chat
 * @param chatId
 * @constructor
 * @return {boolean}
 */


function bindEventScrollChat(chatId) {
  var element = $("#chatWrapper" + chatId + " .chatBox");
  element.bind("scroll", function () {
    if (element.scrollTop() <= 0) {
      getOldChat(chatId);
    }
  });
  return true;
}
/**
 * Get old chat
 * @param chatId
 */


function getOldChat(chatId) {
  var element = $("#chatWrapper" + chatId + " .chatBox");
  element.prepend("\n        <li class='me'>\n            <div class='avatar-icon'>\n                <img src='".concat(imgUrlChatAdmin, "'>\n            </div>\n            <div class='messages'>\n                <p>Hi fine bro.</p>\n            </div>\n        </li>\n        <li class='another'>\n            <div class='avatar-icon'>\n                <img src='").concat(imgUrlChatHotel, "'>\n            </div>\n            <div class='messages'>\n                <p>\n                    <div class=\"ticontainer\">\n                        <div class=\"tiblock\">\n                            <div class=\"tidot\"></div>\n                            <div class=\"tidot\"></div>\n                            <div class=\"tidot\"></div>\n                        </div>\n                    </div>\n                </p>\n            </div>\n        </li>\n    "));
}
/**
 * Close chat
 * @param chatId
 */


function closeChat(chatId) {
  chatWrapperChatId = chatId;
  animateCSS("#" + chatId, "fadeOutDown", "fast", removeChat);
}
/**
 * Show animation on chat removed
 * @param element
 * @param animationName
 * @param speed
 * @param callback
 */


function animateCSS(element, animationName, speed, callback) {
  var node = document.querySelector(element);
  node.classList.add("animated", animationName, speed);

  function handleAnimationEnd() {
    node.classList.remove("animated", animationName);
    node.removeEventListener("animationend", handleAnimationEnd);
    if (typeof callback === "function") callback();
  }

  node.addEventListener("animationend", handleAnimationEnd);
}
/**
 * Remove chat details
 */


function removeChat() {
  $("#" + chatWrapperChatId).remove();
  chatWrapperChatId = "";
}
/**
 * Send chat
 * @param chatId
 * @returns {boolean}
 */


function sendChat(chatId) {
  // Get text user
  var elementChatInput = $("#" + chatId + " .textChat"); // Send chat

  if (elementChatInput.val() === "") {
    return false;
  }

  chatWrapperChatId = chatId;
  var elementChatBox = $("#" + chatId + " .chatBox");
  elementChatBox.append("\n        <li class='me'>\n            <div class='avatar-icon'>\n                <img src='".concat(imgUrlChatAdmin, "'>\n            </div>\n            <div class='messages'>\n                <p>").concat(elementChatInput.val(), "</p>\n            </div>\n        </li>\n    "));
  elementChatBox.animate({
    scrollTop: elementChatBox.get(0).scrollHeight
  }, 400); // Remove text

  elementChatInput.val("");
}
/**
 * Send chat if user press enter
 * @param e
 * @param chatId
 */


function isEnterPressed(e, chatId) {
  if (e.which === 13) {
    sendChat(chatId);
  }
}

/***/ }),

/***/ 8:
/*!***********************************************!*\
  !*** multi ./resources/js/custom/cms/chat.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! E:\Xammp\htdocs\server_hotels\resources\js\custom\cms\chat.js */"./resources/js/custom/cms/chat.js");


/***/ })

/******/ });