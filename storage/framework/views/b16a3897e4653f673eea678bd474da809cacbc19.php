<?php $__env->startSection('title', __('Guests')); ?>

<?php $__env->startSection('content'); ?>
<style>
    .animated {
        background: gainsboro !important;
        padding: 10px 10px 30px 10px;
        border-radius: 5px;
    }
</style>
<div class="divToPrint m-auto" style="width: 400px;">
    <div class="row text-center">
        
        <img src="<?php echo e(asset('images/logo/servr.png')); ?>" class="" alt="logo"
            style="margin-left: 35%;width: 31%!important;">
        <hr>
    </div>
    <div class="row mt-2">
        <p>
            Room: <span id="guestCardNumber"><?php echo e($guestDetails->room_number); ?></span><br />
            Name: <span id="guestCardholderName"><?php echo e($guestDetails->cardholder_name); ?></span><br />
            Date: <span id="guestCardholderName"><?php echo e($guestDetails->created_at->format('d-m-Y')); ?></span><br />
        </p>
    </div>
    <div class="row">
        <p class="">Thank you for using Servr, please see below for your order details</p>
    </div>
    <div class="row">
        <table class="table table-responsive-sm">
            <thead>
                <tr>
                    <th>Service</th>
                    <th>From</th>
                    <th>Name</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                <?php $__currentLoopData = $bookingBill->items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php if($item->billable_type == "App\Models\RestaurantRoomService"): ?>
                <?php $__currentLoopData = $item->billable->dishes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dish): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>

                    <?php
                    $item->billable->load('restaurant');
                    $item->billable->load('dishes');
                    ?>
                    <td>Restaurant Room Service</td>
                    <td><?php echo e($item->billable->restaurant->name); ?></td>

                    <td>

                        <?php echo e($dish->name); ?>

                    </td>
                    <td><?php echo e($dish->pivot->qty); ?></td>
                    <td><?php echo e($dish->pivot->price); ?></td>
                    <td><?php echo e($t_price=$dish->pivot->price*$dish->pivot->qty); ?></td>
                    <?php
                                                  $total_price += $t_price;
                                            ?>

                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php else: ?>
                <?php $__currentLoopData = $item->billable->treatments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $treatment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <?php
                    $item->billable->load('spa');
                    $item->billable->load('treatments');
                    ?>

                    <td>Spa Service</td>
                    <td><?php echo e($item->billable->spa->name); ?></td>
                    <td>
                        <?php echo e($treatment->name); ?>

                    </td>
                    <td><?php echo e($treatment->pivot->qty); ?></td>
                    <td><?php echo e($treatment->pivot->price); ?></td>
                    <td><?php echo e($t_price=$treatment->pivot->price); ?></td>
                    <?php
                                                  $total_price += $t_price;
                                            ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                    
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </table>
    </div>
    <hr>
    <div class="row mt-2">
        <div class="col-4">
            VAT: <?php echo e(number_format((float)$total_price*$vat/100, 0, '.', '')); ?> (<?php echo e($vat); ?>%)
        </div>
        <div class="col-4">
            Service Charges: <?php echo e(number_format((float)$total_price*$service_charges/100, 0, '.', '')); ?>

            (<?php echo e($service_charges); ?>%)
        </div>
        <div class="col-4">
            Total price:
            <?php echo e(number_format((float)$total_price+($total_price*$service_charges/100+$total_price*$vat/100), 0, '.', '')); ?>

        </div>
    </div>
    <p class="text-center mt-2">www.servrhotels.com</p>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/printThis/1.15.0/printThis.min.js"></script>
<script>
</script>
<script>
    $(document).ready(function(){
   window.print();
var type='<?php echo e($type); ?>';
   window.onafterprint = function (event) {
       if(type=='guests'){
window.location = '<?php echo e(url('guests')); ?>';
       }else{
         window.location = '<?php echo e(url('checkouts')); ?>';  
       }
};

window.close = function (event) {
    if(type=='guests'){
window.location = '<?php echo e(url('guests')); ?>';
    }else{
        window.location = '<?php echo e(url('checkouts')); ?>';
    }
};
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('cms.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\XAMP\htdocs\sevr_hotel\resources\views/cms/guest/print_bill.blade.php ENDPATH**/ ?>