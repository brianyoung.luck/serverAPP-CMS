<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExperienceTabInitial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('experience_buttons',function(Blueprint $table){
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('icon');
            $table->string('type');
            $table->bigInteger('hotel_id')->unsigned();
            $table->foreign('hotel_id')
                  ->references('id')->on('hotels')
                  ->onDelete('cascade');
        });
        Schema::create('experience_button_images',function(Blueprint $table){
            $table->bigIncrements('id');
            $table->bigInteger('experience_button_id')->unsigned();
            $table->foreign('experience_button_id')
                  ->references('id')->on('experience_buttons')
                  ->onDelete('cascade');
            $table->string('path');
            $table->string('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('experience_button_images');
        Schema::dropIfExists('experience_buttons');
    }
}
