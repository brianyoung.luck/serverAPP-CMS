<?php

namespace App\Models;

use App\Services\FileStorageService;
use Illuminate\Database\Eloquent\Model;

class PassportPhoto extends Model
{
    protected $fillable = [
        'hotel_booking_id',
        'url',
    ];

    /**
     * Getter
     */

    public function getUrlAttribute($value)
    {
        $fileStorageService = new FileStorageService();
        $storageServiceName = $fileStorageService->getStorageServiceName($value);
        switch ($storageServiceName) {
            case 's3':
                return $fileStorageService->getTemporaryUrl($value);
                break;
            default:
                return $value;
        }
    }
}
