<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConciergeServiceConciergeServiceRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('concierge_service_concierge_service_request', function (Blueprint $table) {
            $table->unsignedBigInteger('concierge_service_request_id');
            $table->unsignedBigInteger('concierge_service_id');
            $table->unsignedInteger('qty');
            $table->string('note')->nullable();

            // Foreign Keys
            $table->foreign('concierge_service_request_id', 'cs_id_cs_request_id_foreign')
                ->references('id')
                ->on('concierge_service_requests')
                ->onDelete('cascade');
            $table->foreign('concierge_service_id', 'cs_request_id_cs_id_foreign')
                ->references('id')
                ->on('concierge_services')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('concierge_service_concierge_service_request');
    }
}
