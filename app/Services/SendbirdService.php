<?php

namespace App\Services;

use App\Models\Hotel;
use App\Models\HotelBooking;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Str;
use Auth;

class SendbirdService
{
    protected $client;
    protected $headers;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => env('SENDBIRD_APP_URL'),
        ]);

        $this->headers = [
            'Api-Token' => env('SENDBIRD_API_TOKEN'),
        ];
    }

    public function createHotelUser(Hotel $hotel)
    {
        $code = Str::lower($hotel->code);

        $body = [
            'user_id' => "admin_hotel_" . $hotel->id . '_' . strtolower($hotel->code),
            'profile_url' => "",
            'nickname' => "Admin Hotel - ". $hotel->name,
            'issue_access_token' => true,
        ];

        $response = $this->client->request('POST', '/v3/users', [
            'headers' => $this->headers,
            'json' => $body
        ]);

        return json_decode($response->getBody()->getContents());
    }

    public function createBookingUser(HotelBooking $booking)
    {
        $body = [
            'user_id' => "booking_" . $booking->id,
            'profile_url' => "",
            'nickname' => "Booking #".$booking->id,
            'issue_access_token' => true,
        ];

        $response = $this->client->request('POST', '/v3/users', [
            'headers' => $this->headers,
            'json' => $body
        ]);

        return json_decode($response->getBody()->getContents());
    }
    public function getChatListChannel($activeUsers)
    {
        foreach ($activeUsers as $users) {
            $user_ids[] =$users->sendbird_channel_url;
        }

        $response = $this->client->request('GET', '/v3/users/{live_chat_booking_694}/my_group_channels', [
            'headers' => $this->headers
        ]);

        return json_decode($response->getBody()->getContents());
    }
    public function createChatChannel(HotelBooking $booking)
    {
        $body = [
            'name' => "Booking #$booking->id",
            'channel_url' => "live_chat_booking_$booking->id",
            'user_ids' => ["booking_$booking->id", $booking->hotel->sendbird_user_id],
        ];

        $response = $this->client->request('POST', '/v3/group_channels', [
            'headers' => $this->headers,
            'json' => $body
        ]);

        return json_decode($response->getBody()->getContents());
    }
    public function updateChatChannel(HotelBooking $booking)
    {
        $body = [
            'name' => "Room $booking->room_number",
        ];

        $response = $this->client->request('PUT', "/v3/group_channels/$booking->sendbird_channel_url", [
            'headers' => $this->headers,
            'json' => $body
        ]);

        return json_decode($response->getBody()->getContents());
    }

    public function freezeChannel(HotelBooking $booking)
    {
        $this->client->request('PUT', '/v3/group_channels/' . $booking->sendbird_channel_url . '/freeze', [
            'headers' => $this->headers,
        ]);

        return true;
    }

    public function deleteChannel(HotelBooking $booking)
    {
        $this->client->request('DELETE', '/v3/group_channels/' . $booking->sendbird_channel_url, [
            'headers' => $this->headers,
        ]);

        return true;
    }

    public function sendAdminMessage(HotelBooking $booking, $message)
    {
        $body = [
            'message_type' => "ADMM",
            'message' => $message['text'],
            'data' => json_encode($message['payload']),
            'send_push' => true,
        ];

        $response = $this->client->request('POST', "/v3/group_channels/$booking->sendbird_channel_url/messages", [
            'headers' => $this->headers,
            'json' => $body
        ]);

        return json_decode($response->getBody()->getContents());
    }
    public function sendWebAdminMessage($sendBirdIds, $message)
    {
        $body = [
            'message_type' => "ADMM",
            'message' => $message['text'],
            'data' => json_encode($message['payload']),
            'send_push' => true,
        ];
      
            $response = $this->client->request('POST', "/v3/group_channels/$sendBirdIds/messages", [
                'headers' => $this->headers,
                'json' => $body
            ]);
        
        

        return json_decode($response->getBody()->getContents());
    }
}