<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\CustomAuthorizationException;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateConciergeServiceRequest;
use App\Http\Resources\Customer\ConciergeServiceRequestResourceCollection;
use App\Models\ConciergeService;
use App\Models\ConciergeServiceRequest;
use App\Services\SendbirdService;
use App\Models\LaundaryService;

use Auth;
use DB;

class ConciergeServiceController extends Controller
{
    public function getAll()
    {
        $conciergeServices = Auth::user()->hotel->conciergeServices;

        return $conciergeServices->makeHidden([
            'created_at',
            'updated_at',
            'hotel_id',
        ]);
    }

    public function getAllLaundary()
    {
        $laundries = LaundaryService::where('hotel_id', Auth::user()->hotel->id)->get();
        return $laundries;
    }

    public function createRequest(CreateConciergeServiceRequest $request, SendbirdService $sendbirdService)
    {
        $data = $request->validated();
        $booking = Auth::user();

        $conciergeServiceRequest = DB::transaction(function () use ($data, $booking) {
            $conciergeServiceRequest = ConciergeServiceRequest::create([
                'hotel_booking_id' => $booking->id,
            ]);

            foreach ($data['concierge_services'] as $conciergeService) {
                $conciergeServiceData = ConciergeService::findOrFail($conciergeService['service_id']);

                if ($conciergeServiceData->hotel_id != Auth::user()->hotel->id) {
                    throw new CustomAuthorizationException('Invalid concierge service');
                }

                $conciergeServiceRequest->services()->attach($conciergeServiceData, [ 'qty' => $conciergeService['qty'], 'note' => $conciergeService['note'] ]);
            }

            return $conciergeServiceRequest;
        });

        // Send to sendbird
        $sendbirdService->sendAdminMessage($booking, [
            'text' => "A Concierge Service has been requested.",
            'payload' => [
                'type' => 'concierge_service',
                'id' => $conciergeServiceRequest->id,
            ],
        ]);

        return response()->json([
            'message' => 'Request concierge services success',
        ]);
    }

    public function getAllRequests()
    {
        $conciergeServiceRequests = Auth::user()->conciergeServiceRequests->load('services');

        return (new ConciergeServiceRequestResourceCollection($conciergeServiceRequests))
            ->additional([
                'message' => 'Get concierge service requests success'
            ])
            ->response()
            ->setStatusCode(200);
    }
}