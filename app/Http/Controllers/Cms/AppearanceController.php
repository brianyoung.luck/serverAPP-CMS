<?php

namespace App\Http\Controllers\Cms;

use App\Models\MobileHotelTheme;
use App\Models\MobileHotelLayout;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AppearanceController extends Controller
{
    public function index()
    {
        $hotel = auth('hotel')->user();
        $mobileHotelThemes = MobileHotelTheme::all();
        $mobileHotelLayout = MobileHotelLayout::where(function ($query) use ($hotel) {
            $query->where('hotel_id', '=', $hotel->id)
          ->orWhere('hotel_id', '=', 0);
        })->get();


        return view('cms.appearance.index', [
            'hotel' => $hotel,
            'mobileHotelThemes' => $mobileHotelThemes,
            'mobileHotelLayouts'=>$mobileHotelLayout
        ]);
    }
}