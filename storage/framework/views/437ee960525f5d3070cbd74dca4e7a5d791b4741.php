<?php $__env->startSection('title', __('Guests')); ?>

<?php $__env->startSection('content'); ?>
    <div id="guestsWrapper" >
        <div class="row">
            <h4>
                New arrivals: <span class="badge badge-primary badge-pill"><?php echo e($newArrivals->count()); ?></span>
            </h4>
            <form id="deleteHotelBookingForm" method="post" action="<?php echo e(route('cms.hotel-bookings.destroy', ['hotel-booking' => 0])); ?>">
                <?php echo method_field('delete'); ?>
                <?php echo csrf_field(); ?>
                <input type="hidden" name="hotelBookingId" id="hotelBookingId" value="0">
            </form>
            <form id="editHotelBookingForm" method="post" action="<?php echo e(route('cms.hotel-bookings.update', ['hotel-booking' => 0])); ?>">
                <?php echo method_field('put'); ?>
                <?php echo csrf_field(); ?>
                <input type="hidden" name="hotelBookingId" id="hotelBookingIdUpdate" value="0">
                <input type="hidden" name="roomNumber" id="roomNumber" value="0">
            </form>
            <div class="col-12 newArrivalsWrapper">
                <?php $__currentLoopData = $newArrivals; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $newArrival): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="newArrivalWrapper" data-id="<?php echo e($newArrival->id); ?>">
                        <div>
                            <div class="userFullNameWrapper">
                                <p class="userFullName"><?php echo e($newArrival->cardholder_name); ?></p>
                                <?php if($newArrival->passportPhotos->count() > 1): ?>
                                    <p class="counter">+<?php echo e($newArrival->passportPhotos->count() - 1); ?></p>
                                <?php endif; ?>
                            </div>
                            <p class="bookingRefText">
                                Booking ref#:
                            </p>
                            <p class="bookingRef">
                                <?php echo e($newArrival->reference ? $newArrival->reference : '-'); ?>

                            </p>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
            <!-- Modal -->
            <div class="modal fade userProfileModal" tabindex="-1" role="dialog" aria-labelledby="userProfileModal" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <h4 class="userProfileText">User profile:</h4>
                            <p>Passport entries</p>
                            <div class="passportEntriesWrapper" id="newGuestPassportEntriesWrapper">
                                <div class="passportEntryWrapper">
                                    <a href="#" target="_blank">
                                        <img src="" alt="Passport photo">
                                    </a>
                                </div>
                            </div>

                            <div class="row userProfileDetails">
                                <?php $helperService = app('App\Services\HelperService'); ?>
                                <div class="col-12 col-md-6">
                                    <p>
                                        Name: <span id="newGuestName"><i class="fa fa-refresh fa-spin fa-fw"></i></span><br/>
                                        Date of Birth: -<br/>
                                        Booking reference: <span id="newGuestReference"><i class="fa fa-refresh fa-spin fa-fw"></i></span><br/>
                                        Arrival date: <span id="newGuestArrivalDate"><i class="fa fa-refresh fa-spin fa-fw"></i></span><br/>
                                        Departure date: <span id="newGuestDepartureDate"><i class="fa fa-refresh fa-spin fa-fw"></i></span><br/>
                                    </p>
                                </div>
                                <div class="col-12 col-md-6">
                                    <p>
                                        Credit card number: <span id="newGuestCardCardNumber"><i class="fa fa-refresh fa-spin fa-fw"></i></span><br/>
                                        Exp. date: <span id="newGuestCardExpiryDate"><i class="fa fa-refresh fa-spin fa-fw"></i></span><br/>
                                        Name on card: <span id="newGuestCardholderName"><i class="fa fa-refresh fa-spin fa-fw"></i></span><br/>
                                        Address: <span id="newGuestCardAddress"><i class="fa fa-refresh fa-spin fa-fw"></i></span><br/>
                                    </p>
                                    <div>
                                        <div>
                                            <button class="btn btn-pill btn-outline-danger declineBtn" data-id="0" type="button">Decline</button>
                                            <button class="btn btn-pill btn-success acceptBtn" data-id="0" type="button" aria-pressed="true">Accept</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <h4>
                Late checkouts: <span class="badge badge-primary badge-pill"><?php echo e($lateCheckouts->count()); ?></span>
            </h4>
            <div class="col-12 lateCheckoutsWrapper">
                <?php $__currentLoopData = $lateCheckouts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lateCheckout): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="lateCheckoutWrapper" data-id="<?php echo e($lateCheckout->id); ?>">
                        <div>
                            <div class="userFullNameWrapper">
                                <p class="userFullName"><?php echo e($lateCheckout->cardholder_name); ?></p>
                                <?php if($lateCheckout->passportPhotos->count() > 1): ?>
                                    <p class="counter">+<?php echo e($lateCheckout->passportPhotos->count() - 1); ?></p>
                                <?php endif; ?>
                            </div>
                            <p class="bookingRefText">
                                Booking ref#:
                            </p>
                            <p class="bookingRef">
                                <?php echo e($lateCheckout->reference ? $lateCheckout->reference : '-'); ?>

                            </p>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>

        <div class="row mt-2">
            <h4>
                Guests:
            </h4>
            <form id="checkoutHotelBookingForm" method="post" action="<?php echo e(route('cms.hotel-bookings.update', ['hotel-booking' => 0])); ?>">
                <?php echo method_field('put'); ?>
                <?php echo csrf_field(); ?>
                <input type="hidden" name="hotelBookingId" id="hotelBookingIdCheckout" value="0">
                <input type="hidden" name="updateType" id="updateType" value="checkout">
            </form>
            <form id="lateCheckoutHotelBookingForm" method="post" action="<?php echo e(route('cms.hotel-bookings.update', ['hotel-booking' => 0])); ?>">
                <?php echo method_field('put'); ?>
                <?php echo csrf_field(); ?>
                <input type="hidden" name="hotelBookingId" id="hotelBookingIdLateCheckout" value="0">
                <input type="hidden" name="updateType" id="updateType" value="lateCheckout">
                <input type="hidden" name="statusLateCheckout" id="statusLateCheckout" value="accepted">
            </form>
            <div class="col-12 p-2">
                <div class="card">
                    <table class="table table-responsive-sm table-striped" id="guestTable">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Room number</th>
                            <th>Arrival date</th>
                            <th>Departure date</th>
                            <th></th>
                        </tr>
                        </thead>
                    </table>
                    <!-- Modal -->
                    <div class="modal fade guestProfileModal" id="guestProfileModal" tabindex="-1" role="dialog" aria-labelledby="guestProfileModal" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <h4 class="roomNumberText">Room x</h4>
                                    <p>Passport entries</p>
                                    <div class="passportEntriesWrapper" id="guestPassportEntriesWrapper">
                                        <div class="passportEntryWrapper">
                                            <a href="#" target="_blank">
                                                <img src="" alt="Passport photo">
                                            </a>
                                        </div>
                                    </div>

                                    <div class="row userProfileDetails">
                                        <?php $helperService = app('App\Services\HelperService'); ?>
                                        <div class="col-12 col-md-6">
                                            <p>
                                                Name: <span id="guestName"><i class="fa fa-refresh fa-spin fa-fw"></i></span><br/>
                                                Date of Birth: -<br/>
                                                Booking reference: <span id="guestReference"><i class="fa fa-refresh fa-spin fa-fw"></i></span><br/>
                                                Arrival date: <span id="guestArrivalDate"><i class="fa fa-refresh fa-spin fa-fw"></i></span><br/>
                                                Departure date: <span id="guestDepartureDate"><i class="fa fa-refresh fa-spin fa-fw"></i></span><br/>
                                            </p>
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <p>
                                                Credit card number: <span id="guestCardNumber"><i class="fa fa-refresh fa-spin fa-fw"></i></span><br/>
                                                Exp. date: <span id="guestCardExpiryDate"><i class="fa fa-refresh fa-spin fa-fw"></i></span><br/>
                                                Name on card: <span id="guestCardholderName"><i class="fa fa-refresh fa-spin fa-fw"></i></span><br/>
                                                Address: <span id="guestCardAddress"><i class="fa fa-refresh fa-spin fa-fw"></i></span><br/>
                                            </p>
                                            <div>
                                                <button class="btn btn-pill btn-warning lateCheckoutBtn d-inline" data-id="0" type="button" aria-pressed="true">Late</button>
                                                <button class="btn btn-pill btn-success checkoutBtn" data-id="0" type="button" aria-pressed="true">Checkout</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script>
        const url1 = '<?php echo route('data-tables.cms.guests'); ?>';
        const url2 = '<?php echo route('cms.guests.index'); ?>';
        const url3 = '<?php echo route('data-tables.cms.guests'); ?>';
        const url4 = '<?php echo route('ajax.cms.guests.index'); ?>';
    </script>
    <script src="<?php echo e(asset('js/custom/cms/guest.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('cms.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/servrhotels.com/resources/views/cms/guest/index.blade.php ENDPATH**/ ?>