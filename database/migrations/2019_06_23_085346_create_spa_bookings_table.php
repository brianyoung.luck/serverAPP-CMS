<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpaBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spa_bookings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('spa_id');
            $table->unsignedBigInteger('hotel_booking_id');
            $table->enum('type', [
                'normal_reservation',
                'room_service',
            ]);
            $table->enum('status', [
                'pending',
                'accepted',
                'rejected',
            ])->default('pending');
            $table->unsignedInteger('people_number');
            $table->dateTime('booking_date');
            $table->double('total_price')->nullable();
            $table->timestamps();

            // Foreign Keys
            $table->foreign('spa_id')
                ->references('id')
                ->on('spas')
                ->onDelete('restrict');
            $table->foreign('hotel_booking_id')
                ->references('id')
                ->on('hotel_bookings')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spa_bookings');
    }
}
