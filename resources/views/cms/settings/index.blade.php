@extends('cms.layouts.app')

@section('title', __('Hotel settings'))

@section('content')
<div class="row hotelSettingsWrapper">
    <div class="col-6">
        <div class="card mb-5">
            <div class="card-header">{{ __('Hotel Settings') }}</div>
            <div class="card-body">
                <form id="changeSettingForm" method="post" action="{{ route('cms.hotels.save-setting') }}"
                    enctype="multipart/form-data">
                    @method('put')
                    @csrf()
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group col-sm-12 col-md-12">
                                <label for="currency">Currency</label>
                                <input class="form-control" id="currency" name="currency" type="text"
                                    placeholder="Input currency as text here (e.g Type: $)" value="{{$currency}}">
                            </div>
                            <div class="form-group col-sm-12 col-md-12">
                                <label for="vat">VAT</label>
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control" name="vat" id="vat"
                                        aria-label="Input the vat value" value="{{$vat}}">
                                    <div class="input-group-append">
                                        <span class="input-group-text">%</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-sm-12 col-md-12">
                                <label for="service_charges">Service Charges</label>
                                <div class="input-group mb-3">
                                    <input class="form-control" id="service_charges" name="service_charges" type="text"
                                        placeholder="Input service charge value" value="{{$service_charges}}">
                                    <div class="input-group-append">
                                        <span class="input-group-text">%</span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-sm-12 col-md-12">
                                <button type="submit" class="btn btn-info pull-right">Save settings</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-6">
        <div class="card mb-5">
            <div class="card-header">{{ __('Restaurants') }}</div>
            <div class="card-body">
                <form id="" method="post" action="{{ route('cms.create-restaurant') }}" enctype="multipart/form-data">
                    @method('post')
                    @csrf()
                    <div class="form-group row">
                        <div class="col-3">
                            <label>Restaurant Name: </label>
                        </div>
                        <div class="col-7">
                            <input type="text" name="res_name" required class="form-control">
                        </div>
                        <div class="col">

                            <input type="submit" name="submit" class="btn btn-sm btn-primary"  value="Add">
                        </div>
                    </div>
                </form>
                <table class="table table-striped">

                    <thead class="thead-light">
                        <th>#</th>
                        <th>Name</th>
                        <th>Action</th>
                    </thead>
                    <tbody>
                        @foreach(auth('hotel')->user()->restaurants as $index => $restaurant)
                        <tr>
                            <td>{{$index+1}}</td>
                            <td>{{$restaurant->name}}</td>
                            <td>
                                <a href="{{route('cms.delete-restaurant',['id'=>$restaurant->id])}}"
                                    class="btn btn-sm btn-danger">Delete</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="card mb-5">
            <div class="card-header">{{ __('Change password') }}</div>
            <div class="card-body">
                <form id="changePasswordForm" method="post"
                    action="{{ route('cms.hotels.reset-password', ['id' => auth('hotel')->user()->id]) }}"
                    enctype="multipart/form-data">
                    @method('put')
                    @csrf()
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group col-sm-12 col-md-4">
                                <label for="password">Old password</label>
                                <input class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}"
                                    id="password" name="password" type="password" placeholder="">
                                @if ($errors->has('password'))
                                <div class="invalid-feedback">*{{ $errors->first('password') }}</div>
                                @endif
                            </div>
                            <div class="form-group col-sm-12 col-md-4">
                                <label for="new_password">New password</label>
                                <input class="form-control {{ $errors->has('new_password') ? ' is-invalid' : '' }}"
                                    id="new_password" name="new_password" type="password" placeholder="">
                                @if ($errors->has('new_password'))
                                <div class="invalid-feedback">*{{ $errors->first('new_password') }}</div>
                                @endif
                            </div>
                            <div class="form-group col-sm-12 col-md-4">
                                <label for="new_password_confirmation">New password confirmation</label>
                                <input
                                    class="form-control {{ $errors->has('new_password_confirmation') ? ' is-invalid' : '' }}"
                                    id="new_password_confirmation" name="new_password_confirmation" type="password"
                                    placeholder="">
                                @if ($errors->has('new_password_confirmation'))
                                <div class="invalid-feedback">*{{ $errors->first('new_password_confirmation') }}</div>
                                @endif
                            </div>
                            <div class="form-group col-sm-12 col-md-4">
                                <button type="submit" class="btn btn-info pull-right">Change password</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="card">
            <div class="card-header">{{ __('Hotel features') }}</div>
            <div class="card-body">
                <table class="table table-responsive-sm">
                    <tbody>
                        <tr>
                            <td><b>Check in</b></td>
                            <td>
                                <label class="switch switch-pill switch-success">
                                    <input type="checkbox" class="switch-input" name="is_check_in_enabled"
                                        id="is_check_in_enabled"
                                        {{ $hotel->hotelFeature->is_check_in_enabled ? 'checked' : '' }}>
                                    <span class="switch-slider"></span>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Check out</b></td>
                            <td>
                                <label class="switch switch-pill switch-success">
                                    <input type="checkbox" class="switch-input" name="is_check_out_enabled"
                                        id="is_check_out_enabled"
                                        {{ $hotel->hotelFeature->is_check_out_enabled ? 'checked' : '' }}>
                                    <span class="switch-slider"></span>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Spa</b></td>
                            <td>
                                <label class="switch switch-pill switch-success">
                                    <input type="checkbox" class="switch-input" name="is_spa_enabled"
                                        id="is_spa_enabled"
                                        {{ $hotel->hotelFeature->is_spa_enabled || $hotel->hotelFeature->spa_treament || $hotel->hotelFeature->spa_room_service? 'checked' : '' }}>
                                    <span class="switch-slider"></span>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><b stlye="padding-left: 20px !important;">Spa Room Service</b></td>
                            <td>
                                <label class="switch switch-pill switch-success">
                                    <input type="checkbox" class="switch-input" name="spa_room_service"
                                        id="spa_room_service"
                                        {{$hotel->hotelFeature->spa_room_service ? 'checked' : '' }}>
                                    <span class="switch-slider"></span>
                                </label>
                            </td>
                        </tr>

                        <tr>
                            <td><b>Spa Treatment</b></td>
                            <td>
                                <label class="switch switch-pill switch-success">
                                    <input type="checkbox" class="switch-input" name="spa_treament" id="spa_treament"
                                        {{$hotel->hotelFeature->spa_treatment ? 'checked' : '' }}>
                                    <span class="switch-slider"></span>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Restaurant</b></td>
                            <td>
                                <label class="switch switch-pill switch-success">
                                    <input type="checkbox" class="switch-input" name="is_restaurant_enabled"
                                        id="is_restaurant_enabled"
                                        {{ $hotel->hotelFeature->is_restaurant_enabled ? 'checked' : '' }}>
                                    <span class="switch-slider"></span>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Concierge</b></td>
                            <td>
                                <label class="switch switch-pill switch-success">
                                    <input type="checkbox" class="switch-input" name="is_concierge_enabled"
                                        id="is_concierge_enabled"
                                        {{ $hotel->hotelFeature->is_concierge_enabled ? 'checked' : '' }}>
                                    <span class="switch-slider"></span>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Cleaning service</b></td>
                            <td>
                                <label class="switch switch-pill switch-success">
                                    <input type="checkbox" class="switch-input" name="is_cleaning_enabled"
                                        id="is_cleaning_enabled"
                                        {{ $hotel->hotelFeature->is_cleaning_enabled ? 'checked' : '' }}>
                                    <span class="switch-slider"></span>
                                </label>
                            </td>

                        </tr>
                        <tr>
                            <td><b>Experience</b></td>
                            <td>
                                <label class="switch switch-pill switch-success">
                                    <input type="checkbox" class="switch-input" name="is_experience" id="is_experience"
                                        {{ $hotel->hotelFeature->is_experience ? 'checked' : '' }}>
                                    <span class="switch-slider"></span>
                                </label>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="card">
            <div class="card-header">{{ __('Staff Members') }}</div>
            <div class="card-body">
                <div class="row">
                    <div class="col-5">
                        <h3>Add New Staff Member</h3>
                        <form method="post" action="{{route('cms.save_staff')}}">
                            @csrf()
                            @method('post')
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" name="name" id="name" placeholder="Enter name" class="form-control"
                                    required="true" autocomplete="off">

                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" name="email" id="email" placeholder="Enter email"
                                    class="form-control" required="true" autocomplete="off">

                            </div>
                            <div class="form-group">
                                <label for="name">Password</label>
                                <input type="password" name="password" id="password" placeholder="Enter password"
                                    class="form-control" required="true" autocomplete="off">
                            </div>
                            <input type="submit" name="submit" value="Save" class="btn btn-info pull-right">
                        </form>
                    </div>
                    <div class="col">
                        <table class="table table-striped">
                            <thead class="thead-light">
                                <th>Name</th>
                                <th>Email</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                <tr>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->email}}</td>

                                    <td>
                                        <a href="{{route('staff_permission.get',['id' => $user->id])}}"
                                            class="btn btn-sm btn-primary">Permissions</a>
                                        <a href="{{route('cms.delete_staff',['id' => $user->id])}}"
                                            class="btn btn-sm btn-danger">Delete</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@stop

@section('js')
<script>
    const url3 = '{!! route('ajax.cms.hotel-features.index') !!}';
        const id1 = '{!! auth('hotel')->user()->id !!}';
</script>
<script src="{{ asset('js/custom/cms/setting.js') }}"></script>

@stop