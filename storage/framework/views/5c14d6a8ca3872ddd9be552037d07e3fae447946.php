<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <meta name="description" content="<?php echo e(env('APP_DESCRIPTION')); ?>">
    <meta name="author" content="<?php echo e(env('APP_AUTHOR')); ?>">
    <meta name="keyword" content="<?php echo e(env('APP_KEYWORD')); ?>">
    <title><?php echo $__env->yieldContent('title'); ?> - <?php echo e(env('APP_NAME')); ?></title>

    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/custom-cms.css')); ?>" rel="stylesheet">
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
<?php echo $__env->make('cms.layouts.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="app-body">
    <?php echo $__env->make('cms.layouts.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <main class="main">
        <?php echo $__env->yieldContent('breadcrumb'); ?>
        <div class="container-fluid">
            <?php echo $__env->make('cms.layouts.flash-message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <div class="animated fadeIn">
                <div class="top-action-wrapper">
                    <?php echo $__env->yieldContent('top-action'); ?>
                </div>
                <?php echo $__env->yieldContent('content'); ?>
            </div>
        </div>
        <div class="chatsWrapper">
            <div class="chatGroupWrapper">
                <div class="chatGroupTextWrapper">
                    <span class="notify-bubble" id="chatCount">0</span>
                    <span class="chatGroupText" data-toggle="collapse" data-target="#collapseChatBodyGroup" aria-expanded="false" aria-controls="collapseChatBodyGroup" role="button">Chats</span>
                </div>
                <div class="chatGroupBodyWrapper collapse" id="collapseChatBodyGroup">
                    <ul id="chatGroupBodyWrapperUl">
                        <li class="listChatGroup301" data-chat_id="301" data-chat_name="Room 301">Room 301</li>
                    </ul>
                </div>
            </div>
            
                
                    
                    
                    
                
                
                    
                        
                            
                                
                            
                            
                                
                                
                            
                        
                        
                            
                                
                            
                            
                                
                                    
                                        
                                            
                                            
                                            
                                        
                                    
                                
                            
                        
                    
                    
                        
                            
                                
                                    
                                
                            
                            
                                
                            
                        
                    
                
            
        </div>
    </main>

    
    <audio controls class="d-none" id="audioNotificationSound">
        <source src="<?php echo e(asset('sounds/light.ogg')); ?>" type="audio/ogg">
        <source src="<?php echo e(asset('sounds/light.mp3')); ?>" type="audio/mp3">
    </audio>
</div>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha256-pasqAKBDmFT4eHoN2ndd6lN370kFiGUFyTiUHWhU7k8="
        crossorigin="anonymous">
</script>
<script src="<?php echo e(asset('js/vendor/SendBird.min.js')); ?>"></script>
<script>
    const adminHotel = <?php echo auth('hotel')->user();; ?>;
    const SENDBIRD_APP_ID = '<?php echo env('SENDBIRD_APP_ID'); ?>';
</script>
<script src="<?php echo e(asset('js/custom/cms/chat-new.js')); ?>"></script>
<script src="<?php echo e(asset('js/app.js')); ?>"></script>
<script>
    $('#sendEmailToSupport').click(function () {
        window.open('mailto:support@servrhotels.com');
    });
</script>

<?php echo $__env->yieldContent('js'); ?>
</body>
</html><?php /**PATH C:\Users\Syed Hasnain Haider\Desktop\backup\resources\views/cms/layouts/app.blade.php ENDPATH**/ ?>