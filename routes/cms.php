<?php

/*
|--------------------------------------------------------------------------
| Cms Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

Route::get('', function () {
    return view('welcome', [
        'title' => 'CMS ' . env('APP_NAME')
    ]);
});

Route::get('assign', function () {
    $permission_set = ["check_in","checkout","spa","spa_room_service","spa_treatment","restaurant","concierge","settings","experience"];
    foreach ($permission_set as $permission) {
        try {
            Permission::create(['name' => $permission, 'guard_name' => 'hotel_staff']);
        } catch (\Exception $ex) {
        }
    }
});

// Guest area
Route::get('', 'Cms\AuthController@loginForm');
Route::get('login', 'Cms\AuthController@loginForm')->name('cms.login.index');
Route::post('login', 'Cms\AuthController@login')->name('cms.do-login');

Route::get('staff_login', 'Cms\AuthController@loginForm')->name('cms.staff_login.index');
Route::post('staff_login', 'Cms\AuthController@staff_login')->name('cms.do-staff-login');

// Reset  password
Route::get('/forgot-password', 'Cms\ForgotPasswordController@showLinkRequestForm')->name('cms.forgot-password.index');
Route::post('/forgot-password/email', 'Cms\ForgotPasswordController@sendResetLinkEmail')->name('cms.forgot-password.action');
Route::get('/reset-password/{token}', 'Cms\ResetPasswordController@showResetForm')->name('cms.reset-password.index');
Route::post('/reset-password', 'Cms\ResetPasswordController@reset')->name('cms.reset-password.action');

// Hotel admin area
Route::group(['middleware' => 'hotel.admin'], function () {
    Route::get('logout', 'Cms\AuthController@logout')->name('cms.logout.index');
    Route::get('dashboard', 'Cms\DashboardController@index')->name('cms.dashboard.index');
    Route::resource('guests', 'Cms\GuestController', [
        'as' => 'cms',
        'only' => ['index']
    ]);
    Route::resource('appearances', 'Cms\AppearanceController', [
        'as' => 'cms',
        'only' => ['index']
    ]);
    Route::resource('hotels', 'Cms\HotelController', [
        'as' => 'cms',
        'only' => ['update']
    ]);
    Route::post('add-layout', 'Cms\HotelController@addLayout')->name('hotel.addHotelMobileLayout');
    Route::get('hotel-active-user', 'Cms\HotelController@getActiveUsers')->name('hotel.activeUser');
    Route::post('create-chat-group', 'Cms\HotelController@creatGroupChatChannel')->name('hotel.new-chat-group');
    Route::post('get-channel-url', 'Cms\HotelController@getUserChatChannel')->name('hotel.getChannel');
    Route::put('hotel/save-setting', 'Cms\HotelController@saveSetting')->name('cms.hotels.save-setting');
    Route::put('{hotel}/reset-password', 'Cms\HotelController@changePassword')->name('cms.hotels.reset-password');

    Route::resource('hotel-bookings', 'Cms\HotelBookingController', [
        'as' => 'cms',
        'only' => ['destroy', 'update']
    ]);

    Route::resource('restaurants', 'Cms\RestaurantController', [
        'as' => 'cms',
        'only' => ['show', 'update', 'store', 'destroy']
    ]);
    Route::post('restaurants/storecategory', 'Cms\RestaurantController@storeCategory')->name('cms.restaurants.storecategory');
    Route::get('delete-restaurant/{id}', 'Cms\RestaurantController@delete')->name('cms.delete-restaurant');
    Route::post('save-restaurant', 'Cms\RestaurantController@newRestaurant')->name('cms.create-restaurant');
    Route::resource('restaurant-galleries', 'Cms\RestaurantGalleryController', [
        'as' => 'cms',
        'only' => ['destroy']
    ]);
    Route::post('restaurants/storecategory', 'Cms\RestaurantController@storeCategory')->name('cms.restaurants.storecategory');

    Route::resource('restaurant-dishes', 'Cms\RestaurantDishController', [
        'as' => 'cms',
        'only' => ['store', 'update', 'destroy']
    ]);
    Route::resource('concierges', 'Cms\ConciergeController', [
        'as' => 'cms',
        'only' => ['index', 'update', 'store', 'destroy']
    ]);
    Route::get('laundary-menus', 'Cms\ConciergeController@showLaundaryMenus')->name('cms.laundary-menus.index');
    Route::put('laundary-update', 'Cms\ConciergeController@updateLaundary')->name('cms.update-laundary');
    Route::post('laundary-insert', 'Cms\ConciergeController@insertLaundary')->name('cms.insert-laundary');
    Route::delete('laundary-delete', 'Cms\ConciergeController@destroyLaundary')->name('cms.delete-laundary');
    Route::get('concierge-menus', 'Cms\ConciergeController@showConciergeMenus')->name('cms.concierge-menus.index');



    Route::group(['prefix' => 'experience'], function () {
        Route::get('/', 'Cms\ExperienceController@index')->name('cms.experience.index');
        Route::post('/', 'Cms\ExperienceController@store')->name('cms.experience.store');
        Route::get('/{id}', 'Cms\ExperienceController@get')->name('cms.experience.get');
        Route::post('/save_promotion_item', 'Cms\ExperienceController@uploadPromotionItemImage')->name('cms.experience.upload_promotion_item_image');
        Route::get('/delete_image/{id}', 'CMS\ExperienceController@deleteImage')->name('cms.experience.delete_image');
    });
    Route::get('/bill/{id}', 'Cms\BillController@index')->name('cms.bill');


    Route::resource('concierge-services-requests', 'Cms\ConciergeServiceRequestController', [
        'as' => 'cms',
        'only' => ['update']
    ]);
    Route::resource('restaurant-room-services', 'Cms\RestaurantRoomServiceController', [
        'as' => 'cms',
        'only' => ['update']
    ]);
    Route::resource('restaurant-reservations', 'Cms\RestaurantReservationController', [
        'as' => 'cms',
        'only' => ['update']
    ]);
    Route::resource('checkouts', 'Cms\CheckoutController', [
        'as' => 'cms',
        'only' => ['index', 'update']
    ]);

    Route::resource('spas', 'Cms\SpaController', [
        'as' => 'cms',
        'only' => ['show', 'update']
    ]);

    Route::resource('spa-galleries', 'Cms\SpaGalleryController', [
        'as' => 'cms',
        'only' => ['destroy']
    ]);

    Route::post('spa-treatments', 'Cms\AuthController@logout')->name('cms.logout.index');
    Route::post('save_staff', 'Cms\SettingController@save_staff')->name('cms.save_staff');
    Route::get('delete_staff/{id}', 'Cms\SettingController@delete_staff')->name('cms.delete_staff');
    Route::get('staff/{id}/permission', 'Cms\HotelStaffPermissionController@get')->name('staff_permission.get');
    Route::post('staff/{id}/permission', 'Cms\HotelStaffPermissionController@store')->name('staff_permission.store');


    Route::group(['prefix' => 'spa-treatments'], function () {
        Route::post('', 'Cms\SpaController@storeSpaTreatment')->name('cms.spa-treatments.store');
        Route::put('', 'Cms\SpaController@updateSpaTreatment')->name('cms.spa-treatments.update');
        
        Route::delete('', 'Cms\SpaController@destroySpaTreatment')->name('cms.spa-treatments.destroy');
    });
    Route::resource('spa-bookings', 'Cms\SpaBookingController', [
        'as' => 'cms',
        'only' => ['update']
    ]);
    Route::resource('settings', 'Cms\SettingController', [
        'as' => 'cms',
        'only' => ['index']
    ]);
    Route::resource('room-cleaning-requests', 'Cms\RoomCleaningController', [
        'as' => 'cms',
        'only' => ['update']
    ]);
    Route::resource('laundry-orders', 'Cms\LaundryOrderController', [
        'as' => 'cms',
        'only' => ['update']
    ]);

    // Datatables
    Route::group(['prefix' => 'data-tables'], function () {
        Route::get('guests', 'Cms\GuestController@indexDataTable')->name('data-tables.cms.guests');
    });

    Route::group(['prefix' => 'ajax'], function () {
        Route::get('guests', 'Cms\GuestController@ajaxGetGuest')->name('ajax.cms.guests.index');
        Route::get('guests/{hotelBookingId}', 'Cms\GuestController@ajaxGetGuestDetails')->name('ajax.cms.guests.show');
        Route::get('guests/mailbill/{hotelBookingId}/{email?}', 'Cms\GuestController@emailBill')->name('ajax.cms.guests.email_bill');
        Route::get('guests/printbill/{hotelBookingId}/{type}', 'Cms\GuestController@printBill')->name('ajax.cms.guests.print_bill');
        Route::get('restaurant-dishes', 'Cms\RestaurantDishController@ajaxGetDishes')->name('ajax.cms.restaurant-dishes.index');
        Route::get('restaurant-dishes/{dish}', 'Cms\RestaurantDishController@ajaxGetDishDetails')->name('ajax.cms.restaurant-dishes.show');
        Route::get('restaurant-category', 'Cms\RestaurantController@ajaxDeleteCategory')->name('ajax.cms.restaurant-category.delete');
        Route::post('/sort-gallery', 'Cms\RestaurantGalleryController@sortGallery')->name('ajax.cms.sort-gallery');
        Route::get('laundary-services/', 'Cms\ConciergeController@ajaxGetLaundaryServiceDetails')->name('ajax.cms.laundary-services.index');
        Route::get('laundary-services/{laundaryServiceid}', 'Cms\ConciergeController@ajaxGetLaundaryServiceDetails')->name('ajax.cms.laundary-services.show');

        Route::get('concierge-services', 'Cms\ConciergeController@ajaxGetConciergeService')->name('ajax.cms.concierge-services.index');
        Route::get('concierge-services/{conciergeServiceId}', 'Cms\ConciergeController@ajaxGetConciergeServiceDetails')->name('ajax.cms.concierge-services.show');

        Route::get('concierge-services-requests', 'Cms\ConciergeController@ajaxGetConciergeServiceRequest')->name('ajax.cms.concierge-services-requests.index');
        Route::get('concierge-services-requests/{conciergeServiceRequestId}', 'Cms\ConciergeController@ajaxGetConciergeServiceRequestDetails')->name('ajax.cms.concierge-services-requests.show');

        Route::get('restaurant-room-services', 'Cms\RestaurantRoomServiceController@ajaxGetRestaurantRoomServices')->name('ajax.cms.restaurant-room-services.index');
        Route::get('restaurant-room-services/{restaurantRoomService}', 'Cms\RestaurantRoomServiceController@ajaxGetRestaurantRoomService')->name('ajax.cms.restaurant-room-services.show');

        Route::get('restaurant-reservations', 'Cms\RestaurantReservationController@ajaxGetRestaurantReservations')->name('ajax.cms.restaurant-reservations.index');
        Route::get('restaurant-reservations/{restaurantReservation}', 'Cms\RestaurantReservationController@ajaxGetRestaurantReservation')->name('ajax.cms.restaurant-reservations.show');
        Route::get('restaurant-category', 'Cms\RestaurantController@ajaxDeleteCategory')->name('ajax.cms.restaurant-category.delete');
        Route::post('/sort-gallery', 'Cms\RestaurantGalleryController@sortGallery')->name('ajax.cms.sort-gallery');

        Route::get('spa-treatments', 'Cms\SpaController@ajaxGetSpaTreatments')->name('ajax.cms.spa-treatments.index');
        Route::get('spa-treatments/{spa_treatment}', 'Cms\SpaController@ajaxGetSpaTreatment')->name('ajax.cms.spa-treatments.show');

        Route::get('spa-bookings', 'Cms\SpaBookingController@ajaxGetSpaBookings')->name('ajax.cms.spa-bookings.index');
        Route::get('spa-bookings/{spa_booking}', 'Cms\SpaBookingController@ajaxGetSpaBooking')->name('ajax.cms.spa-bookings.show');

        Route::get('hotel-features', 'Admin\HotelFeatureController@ajaxGetHotelFeatures')->name('ajax.cms.hotel-features.index');
        Route::put('hotel-features/{hotel}', 'Admin\HotelFeatureController@ajaxUpdateHotelFeature')->name('ajax.cms.hotel-features.update');

        Route::get('get-restaurant-reservations', 'Cms\RestaurantReservationController@ajaxGetRestaurantReservation2');
        Route::get('get-spa-bookings', 'Cms\SpaBookingController@ajaxGetSpaBooking2');

        Route::get('is-room-number-not-used', 'Cms\HotelBookingController@ajaxIsRoomNumberNotUsed');
    });
});

Route::get('privacy-policy', function () {
    return view('web.privacy-policy.index');
});