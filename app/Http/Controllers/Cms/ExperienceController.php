<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Experience_Button;
use App\Services\ExperienceService;
use Auth;
use App\Models\Experience_Button_Image;

class ExperienceController extends Controller
{
    private $experienceService = null;
    public function __construct(ExperienceService $experienceService)
    {
        $this->experienceService = $experienceService;
    }
    public function index()
    {
        $hotel_id = auth('hotel')->user()->id;
        $buttons = Experience_Button::where('hotel_id', $hotel_id)->get();
        return view('cms.experience.index')->with([
            'buttons' => $buttons
        ]);
    }

    public function store(Request $request)
    {
        $request->validate(
            [
        'btnTitle' => 'required',
        'icon' => 'required',
        'btnType' => 'required'
        ],
            [
        'btnTitle.required' => 'Title is required',
        'btnType.required' => 'Button type is required',
    ]
        );

        $experience_button = null;
        if ($request->input('button_id') == "-1") {
            $experience_button = new Experience_Button;
        } else {
            $experience_button = Experience_Button::where('id', $request->input('button_id'))->first();
        }
        $experience_button->title = $request->input('btnTitle');
        $experience_button->icon = $request->input('icon');
        $experience_button->type = $request->input('btnType');
        
        $experience_button->hotel_id = auth('hotel')->user()->id;
        $type = $request->input('btnType');

        if ($type == "Promotion" || $type == "Custom" || $type == "Custom_Image") {
            $experience_button->data = $request->input('encoded');
        } else {
            $experience_button->data = "";
        }

        $experience_button->save();
        
        $array = explode(",", $request->input('images_id'));
        if (count($array) > 0) {
            Experience_Button_Image::whereIn('id', $array)->update(['experience_button_id' => $experience_button->id]);
        }
        return redirect()->back()->with([
            'success' => 'Button added successfull.'
        ]);
    }

    public function get($id)
    {
        $experience_button = Experience_Button::where('id', $id)->first();
        if (!$experience_button) {
            abort(404);
        }

        $experience_button_images = Experience_Button_Image::where('experience_button_id', $id)->get();
        $experience_button->images = $experience_button_images;
        return $experience_button;
    }

    public function uploadPromotionItemImage(Request $request)
    {
        //The ID of the button could be 0 or 1
        $button_id  = $request->input('button_id');
        if ($button_id < 1) {
            $button_id = null;
        }
        $request->validate(
            [ 'images.*' =>  'nullable|file|mimes:png|max:4096'],
            [ 'images.*.mimes' => 'Please upload only png file.',
                 'images.*.max' => 'Image is too large.'
                 ]
        );


        $images = $this->experienceService->uploadBtnImages($request, $button_id);
        return $images;
    }

    public function deleteImage($id)
    {
        Experience_Button_Image::where('id', $id)->delete();
        return "done";
    }
}