/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 19);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/custom/cms/experience.js":
/*!***********************************************!*\
  !*** ./resources/js/custom/cms/experience.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var addBtn = $("#addButton");
var addBtnModal = $(".addButtonModal");
var isImageValidated = false;
var selectedableImg = $(".selectable_image");
var selectBtnIcon = $("#selectBtnIconWrapper");
var btnSelectedImg = $("#btnSelectedImg");
var chooseIconModal = $(".chooseIconModal");
var selectedImgsWrapper = $("#selectedImgWrapper");
var cancelBtn = $("#cancelBtn"); //Form Inputs

var uploadImg = $("#btnImages");
var btnType = $("#btnType");
var saveBtn = $("#saveBtn");
var addBtnForm = $("#addBtnForm");
var imageUploadBtn = $("#imageUploadBtn"); //Promotions Element

var used = $("#used");
var maxLimit = $("#maxLimit");
var addNewPromotionModal = $(".addNewPromotionModal");
var promotion_title = $("#promotion_title"),
    promotion_amount = $("#promotion_amount"),
    promotion_description = $("#promotion_description"),
    promotion_code = $("#promotion_code"),
    promotion_url = $("#promotion_url"),
    promotion_image = $("#promotion_image"),
    promotion_images_wrapper = $("#promotion_images_wrapper");
promotion_item_id = $("#promotion_item_id");
var addPromotionItem = $("#addPromotionItem");
var addpromotionBtn = $("#addPromotion");
var promotion_items_wrapper = $("#promotion_items_wrapper");
var promotion_item_form = $("#promotion_item_form");
var promotion_image_preview = $(".promotion_image_preview"); //Custom Data

var promotion_items = [];
var uploaded_images = [5];
$(document).ready(function () {
  addBtnModal.appendTo("body");
  chooseIconModal.appendTo("body");
  addNewPromotionModal.appendTo("body");
  cancelBtn.click(function () {
    $(addBtnModal).modal("toggle");
    window.location.reload();
  });
  addBtn.click(function () {
    //uploaded_images = [];
    //$("#button_id").val("-1");
    $(addBtnModal).modal("toggle");
  });
  selectBtnIcon.click(function (event) {
    addBtnModal.modal("hide");
    chooseIconModal.modal("toggle");
  });
  selectedableImg.click(function (event) {
    addBtnModal.modal("show");
    chooseIconModal.modal("toggle");
    $("#selectedIcon_Name").val($(this).attr("data-imgname"));
    btnSelectedImg.attr("src", "/icons/" + $(this).attr("data-imgname") + ".png");
  });
  uploadImg.change(function (event) {
    readURL(this, selectedImgsWrapper, "button_image_preview");
  });
  btnType.change(function (event) {
    var val = btnType.val();
    $(".option").hide();

    if (val == "Promotion") {
      $("#promotion_data").show();
    } else if (val == "Custom") {
      $("#custom_data").show();
    } else if (val == "Custom_Image") {
      $("#custom_image_data").show();
    } else if (val == "Shuttle") {//$("#map_image_upload").show();
    } else if (val == "Map") {
      $("$map_image_2").show();
    }
  });
  imageUploadBtn.click(function (event) {
    btnImages.click();
  });
  promotion_image.change(function (event) {
    //Todo: Upload To Server and get stored id
    //Needs to be fixed

    /*var formData = new FormData(document.getElementById("promotion_item_form"));
    console.log("Form Data: ",formData);
    debugger;
    console.log("files: ",($(promotion_image))[0].files);
    if(promotion_item_id.val() == "-1"){
    formData.append("index", promotion_items.length);
    }else{
    formData.append("index", promotion_item_id.val());
    }
    //formData.append("files[]",($(promotion_image))[0].files);
    console.log("Form Data After: ",formData);
    $.ajax({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
            },
    type: 'POST',
    url: savePromotionItem,
    data: formData,
    processData: false,
    contentType: false,
    enctype: 'multipart/form-data',
    success: function(response){
    console.log(response.data);
    }
    });
    */
    if (validateExperienceImageSize) {
      readURL(this, promotion_images_wrapper, "promotion_image_preview");
    }
  });
  addPromotionItem.click(function (event) {
    promotion_item_id.val("-1");
    addNewPromotionModal.modal("toggle");
  });
  addpromotionBtn.click(function (event) {
    //Send the data to the server
    console.log("Hello"); // debugger;

    if (promotion_item_id.val() == "-1") {
      var _index = promotion_items.length;
      var html = "<tr>\n\t\t\t\t<td id=\"pitem_".concat(_index, "\">").concat(promotion_title.val(), "</td>\n\t\t\t\t<td><a href=\"#\"  class=\"btn btn-sm btn-primary promotion_item\" data-index=\"").concat(_index, "\" data-id=\"0\">Edit</a></td>\n\t\t\t</tr>\n\t\t\t");
      promotion_items.push(updatePromotionItem_Obj({}));
      promotion_items_wrapper.append(html);
    } else {
      //Todo update the existing item in the DB
      var _index2 = parseInt(promotion_item_id.val());

      var updatedObj = updatePromotionItem_Obj(promotion_items[_index2]);
      promotion_items[_index2] = updatedObj;
      $("#pitem_" + _index2).html(promotion_title.val());
    }

    clearPromotionItem_Form();
    clearFrom();
    addNewPromotionModal.modal("toggle");
  }); //Dynamic items handler

  $(document).on("click", ".promotion_item", function () {
    // debugger;
    //Todo: Show the selected images preview
    var index = parseInt($(this).attr("data-index"));
    var obj = promotion_items[index];
    promotion_item_id.val(index);
    promotion_title.val(obj.title);
    promotion_amount.val(obj.promotion_amount);
    promotion_description.val(obj.promotion_description);
    promotion_code.val(obj.promotion_code);
    promotion_url.val(obj.redeem_link);
    addNewPromotionModal.modal("toggle");
  });
  saveBtn.click(function (event) {
    // debugger;
    var btnTypeVal = btnType.val();
    var encodedJsonData = "";

    if (btnTypeVal == "Promotion") {
      encodedJsonData = JSON.stringify(updatePromotionItem_Obj({}));
    } else if (btnTypeVal == "Custom") {
      var text = $("#custom_data_txt").val();
      var title = $("#custom_button_title").val();
      var link = $("#custom_button_link").val();
      var obj = {
        sub_title: title,
        text: text,
        redeem_link: link
      };
      encodedJsonData = JSON.stringify(obj);
    } else if (btnTypeVal == "Custom_Image") {
      var _title = $("#custom_image_title").val();

      var _link = $("#custom_image_link").val();

      var obj = {
        sub_title: _title,
        redeem_link: _link
      };
      encodedJsonData = JSON.stringify(obj);
    }

    $("input[name=images_id]").val(uploaded_images.join(","));
    $("input[name=encoded]").val(encodedJsonData);
    addBtnForm.submit();
  });
  $(".imageWrapper").click(function () {
    // debugger;
    var index = parseInt($(this).attr("data-index"));
    var dbId = parseInt($(this).attr("data-id"));
    var imgSrc = $(this).children("img").attr("src");
    var element = $(this);
    Swal.fire({
      title: "Delete picture",
      type: "question",
      showCancelButton: true,
      confirmButtonColor: delete_button_color,
      cancelButtonColor: cancel_button_color,
      confirmButtonText: "Delete",
      reverseButtons: true,
      html: "\n\t\t\t\t<img src=\"".concat(imgSrc, "\" alt=\"Img\" class=\"w-100 h-25\">\n\t\t\t")
    }).then(function (result) {
      if (result.value) {
        console.log("url: ", deleteBtnImg + "/" + dbId);
        $.ajax({
          type: "GET",
          headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
          },
          url: deleteBtnImg + "/" + dbId,
          success: function success(response) {
            $(element).remove();
            uploaded_images.splice(index, 1);
          }
        });
      }
    });
  });
  $(".existing_btn").click(function () {
    var id = $(this).attr("data-id");
    $("#button_id").val(id);
    $.ajax({
      headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
      },
      type: "GET",
      url: endPoint + "/" + id,
      success: function success(response) {
        // debugger;
        addBtnModal.modal("toggle");
        btnType.val(response.type);
        btnType.trigger("change");
        $("input[name=icon]").val(response.icon);
        btnSelectedImg.attr("src", "/icons/" + response.icon + ".png");
        $("#selectedIcon_Name").val(response.icon);
        $("#btnTitle").val(response.title);

        if (response.type == "Promotion") {
          var obj = JSON.parse(response.data);
          promotion_title.val(obj.title);
          promotion_amount.val(obj.promotion_amount);
          promotion_description.val(obj.promotion_description);
          promotion_code.val(obj.promotion_code);
          promotion_url.val(obj.redeem_link);
        } else if (response.type == "Custom") {
          var obj = JSON.parse(response.data);
          $("#custom_button_title").val(obj.sub_title);
          $("#custom_data_txt").val(obj.text);
          $("#custom_button_link").val(obj.link);
        } else if (response.type == "Custom_Image") {
          var obj = JSON.parse(response.data);
          $("#custom_image_title").val(obj.sub_title);
          $("#custom_image_link").val(obj.link);
        }

        var indexStart = 0;
        clearSelectedImgs();

        for (image_index in response.images) {
          var image = response.images[image_index];
          uploaded_images.push(image.id);
          addImageInWrapper(image.path, {
            index: indexStart,
            id: image.id
          }, selectedImgsWrapper);
          indexStart++;
        }

        $("#images_id").val(uploaded_images.join(","));
      }
    });
  });
});

function updatePromotionItem_Obj(obj) {
  obj["title"] = promotion_title.val();
  obj["promotion_amount"] = promotion_amount.val();
  obj["promotion_description"] = promotion_description.val();
  obj["promotion_code"] = promotion_code.val();
  obj["redeem_link"] = promotion_url.val();
  return obj;
}

function clearPromotionItem_Form() {
  promotion_title.val("");
  promotion_amount.val("");
  promotion_description.val("");
  promotion_code.val("");
  promotion_url.val("");
  promotion_images_wrapper.html("");
}

function addImageInWrapper(src, props, wrapper) {
  //   debugger;
  var index = props.index ? 'data-index="' + props.index + '"' : " ";

  if ($(".newAdded:last").attr("data-index") > 2) {
    $("#selectedImgWrapper").addClass("imagesWrapperExperience");
  }

  var id = props.id ? 'data-id="' + props.id + '"' : "";
  wrapper.append('<div class="imageWrapper newAdded" ' + id + " " + index + '><img src="' + src + '" width="75" height="75"/></div>'); //selectedImgsWrapper.append(`<img src="${src}"  ${props.id ? 'data-id="'+props.id+'"' : ''} onclick="showImg(this)"`);
}

function clearSelectedImgs() {
  $(".imageWrapper.newAdded").remove();
}

function uploadImage(files) {
  // define data and connections
  var formData = new FormData($(addBtnForm)[0]);
  $.ajax({
    headers: {
      "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
    },
    type: "POST",
    url: savePromotionItem,
    data: formData,
    processData: false,
    contentType: false,
    enctype: "multipart/form-data",
    success: function success(response) {
      //TODO: append all those images into the container with their respective ID
      //   debugger;
      console.log("Response from image upload: ", response);
      var indexStart = uploaded_images.length;

      for (index in response) {
        // debugger;
        var image = response[index];
        addImageInWrapper(image.path, {
          index: indexStart,
          id: image.id
        }, selectedImgsWrapper);
        uploaded_images.push(response[index].id);
      }
    }
  });
}

function deleteImg() {}

function showImg(element) {}

function validateExperienceImageSize() {
  for (var i = 0; i < $("#btnImages").get(0).files.length; ++i) {
    var file1 = $("#btnImages").get(0).files[i].name;
    var file_size = $("#btnImages").get(0).files[i].size;

    if (file_size > 4097152) {
      $(".file_error").show();
      $("#saveBtn").attr("disabled", true);
      $("#imageUploadBtn").attr("data-status", "0");
      return false;
    } else {
      $(".file_error").hide();
      $("#saveBtn").attr("disabled", false);
      $("#imageUploadBtn").attr("data-status", "1");
      return true;
    }
  }
}

function validateImagesSize() {
  var fls = 0;
  console.log($("#btnImages").get(0).files);
  var imagesName = [];

  for (var i = 0; i < $("#btnImages").get(0).files.length; ++i) {
    var imageName = $("#btnImages").get(0).files[i].name;
    var file_size = $("#btnImages").get(0).files[i].size;
    console.log(file_size);

    if (file_size > 4097152) {
      var data = {};
      data.name = imageName;
      fls = fls + 1;
      imagesName.push(data);
    }
  }

  if (fls > 0) {
    $("#btnImages").val("");
    return {
      status: false,
      names: imagesName
    };
  } else {
    return {
      status: true,
      names: imagesName
    };
  }
}

function readURL(input, wrapper, imgClass) {
  //   debugger;
  if (input.files) {
    //clearSelectedImgs();
    // debugger;
    var resp = validateImagesSize();
    console.log(resp);
    jQuery.each(resp.names, function (i, val) {
      console.log(val.name);
      $(".restImagesErrors").show();
      $(".restImagesErrors").append("<li style=\"list-style:none;\">Error : <b>".concat(val.name, "</b> size is large.</li><br>"));
    });

    if (resp.status == true) {
      uploadImage(input.files);
      $(".restImagesErrors").hide();
    } // if (validateExperienceImageSize()) {
    //   uploadImage(input.files);
    // }

  }
}

$("#clearBtn").on("click", function () {
  document.getElementById("addBtnForm").reset();
  $(".imageWrapper img").attr("src", "");
  $(".imageWrapper img").hide();
  $("#selectedImageText img").attr("src", "");
  $("#selectedImageText img").romove();
  $(".restImagesErrors").css("display", "none");
  $("#selectedImageText").append('<img id="btnSelectedImg">');
});

/***/ }),

/***/ 19:
/*!*****************************************************!*\
  !*** multi ./resources/js/custom/cms/experience.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! E:\Xammp\htdocs\server_hotels\resources\js\custom\cms\experience.js */"./resources/js/custom/cms/experience.js");


/***/ })

/******/ });