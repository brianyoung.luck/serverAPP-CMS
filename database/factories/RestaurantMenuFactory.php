<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\RestaurantMenu;
use Faker\Generator as Faker;

$factory->define(RestaurantMenu::class, function (Faker $faker) {
    return [
        'name' => "Menu " . $faker->word,
        'description' => $faker->sentence(5),
    ];
});
