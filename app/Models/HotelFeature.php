<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HotelFeature extends Model
{
    protected $primaryKey = 'hotel_id';

    protected $fillable = [
        'hotel_id',
        'is_check_in_enabled',
        'is_check_out_enabled',
        'is_spa_enabled',
        'is_restaurant_enabled',
        'is_concierge_enabled',
        'is_cleaning_enabled',
        'spa_treatment',
        'spa_room_service',
          'is_experience'
    ];

    protected $casts = [
        'is_check_in_enabled' => 'boolean',
        'is_check_out_enabled' => 'boolean',
        'is_spa_enabled' => 'boolean',
        'is_restaurant_enabled' => 'boolean',
        'is_concierge_enabled' => 'boolean',
        'is_cleaning_enabled' => 'boolean',
        'spa_treatment' => 'boolean',
        'spa_room_service' => 'boolean',
        'is_experience' => 'boolean'
    ];
}