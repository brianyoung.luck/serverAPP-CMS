<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', function () {
    return 'Servr API';
});

/**
 * Hotels
 */
Route::group([
    'prefix' => '/hotels',
], function () {
    Route::get('/{code}', 'Api\HotelController@getByCode');
});

/**
 * Auth routes
 */
Route::group([
    'prefix' => '/auth',
], function () {
    Route::post('/login', 'Api\AuthController@login');

    // Need to be logged in but not required to be activated
    Route::group([
        'middleware' => [
            'auth:booking',
            'booking.active',
        ],
    ], function () {
        Route::get('/me', 'Api\AuthController@me');
        Route::post('/register-device-token', 'Api\AuthController@registerDeviceToken');
    });
});

/**
 * Hotel booking related
 */
Route::post('/bookings', 'Api\BookingController@checkIn');
Route::middleware('auth:booking', 'booking.active', 'booking.pending')->post('/late-check-out', 'Api\BookingController@lateCheckout');
Route::get('/experience/{hotel_id}', 'Api\ExperienceController@getAll')->name('experience');
Route::get('/experience/promotion/{hotel_id}', 'Api\ExperienceController@getPromotions')->name('experience_promotions');
Route::get('/experience/{hotel_id}/{button_id}', 'Api\ExperienceController@getButton')->name('experience_get');
/**
 * Restaurants
 */
Route::group([
    'prefix' => '/restaurants',
    'middleware' => [
        'auth:booking',
        'booking.active',
        'booking.pending',
    ],
], function () {
    Route::get('/', 'Api\RestaurantController@getAll');
    Route::get('/{restaurant}/dishes', 'Api\RestaurantController@getRestaurantDishes');
    Route::post('/{restaurant}/reservation', 'Api\RestaurantController@createReservation');
    Route::post('/{restaurant}/room-order', 'Api\RestaurantController@createRoomOrder');
});

/**
 * Restaurants
 */
Route::group([
    'prefix' => '/spas',
    'middleware' => [
        'auth:booking',
        'booking.active',
        'booking.pending',
    ],
], function () {
    Route::get('/', 'Api\SpaController@getAll');
    Route::get('/{spa}/treatments', 'Api\SpaController@getTreatments');
    Route::post('/{spa}/bookings', 'Api\SpaController@createBooking');
});

/**
 * Concierge service
 */
Route::group([
    'prefix' => '/concierge-services',
    'middleware' => [
        'auth:booking',
        'booking.active',
        'booking.pending',
    ],
], function () {
    Route::get('/', 'Api\ConciergeServiceController@getAll');
    Route::post('/', 'Api\ConciergeServiceController@createRequest');
});

/**
 * Laundry Services
 */
Route::group([
    'prefix' => '/laundries',
    'middleware' => [
        'auth:booking',
        'booking.active',
        'booking.pending',
    ],
], function () {
    Route::post('/', 'Api\LaundryOrderController@create');
});

/**
 * Room Cleaning Services
 */
Route::group([
    'prefix' => '/room-cleanings',
    'middleware' => [
        'auth:booking',
        'booking.active',
        'booking.pending',
    ],
], function () {
    Route::post('/', 'Api\RoomCleaningOrderController@create');
});

/**
 * All customer orders routes are here
 */
Route::group([
    'prefix' => '/orders',
    'middleware' => [
        'auth:booking',
        'booking.active',
        'booking.pending',
    ],
], function () {
    Route::get('/restaurant/reservations', 'Api\RestaurantController@getAllReservations');
    Route::get('/restaurant/room-orders', 'Api\RestaurantController@getAllRoomOrders');
    Route::get('/concierge-services', 'Api\ConciergeServiceController@getAllRequests');
    Route::get('/laundry-services', 'Api\ConciergeServiceController@getAllLaundary');
    Route::get('/spas', 'Api\SpaController@getAllRequests');
    Route::get('/laundries', 'Api\LaundryOrderController@getAllOrders');
    Route::get('/room-cleanings', 'Api\RoomCleaningOrderController@getAllOrders');
});