<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RestaurantDishCategory extends Model
{
    protected $fillable = [
        'name',
    ];

    public function dishes()
    {
        return $this->hasMany(RestaurantDish::class);
    }
}
