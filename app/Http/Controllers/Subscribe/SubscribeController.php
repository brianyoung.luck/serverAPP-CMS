<?php

namespace App\Http\Controllers\Subscribe;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Hotel;
use App\Models\HotelFeature;
use App\Models\Restaurant;
use App\Models\Spa;
use Srmklive\PayPal\Services\ExpressCheckout;
use Carbon\Carbon;

class SubscribeController extends Controller
{
    public function index()
    {
      return view('cms.paypal.subscribe-from-subscribe');
    }

    public function search(Request $req)
    {
      $req->validate([
        'code' => 'required|max:255',
      ]);

      try {
        $hotel = Hotel::where('code', $req->code)->firstOrFail();
        return view('cms.paypal.search-from-subscribe', compact('hotel'));
      } catch (\Exception $e) {
        return redirect()->back()->with('not_found', 'Hotel Code Not Found');
      }
    }

    public function store(Request $req)
    {
      switch ($req->room) {
        case '<20':
          $amount = 25;
          break;

        case '20-40':
          $amount = 50;
          break;

        case '50-99':
          $amount = 100;
          break;

        case '100-299':
          $amount = 190;
          break;

        case '300>':
          $amount = 300;
          break;

        default:
          $amount = 0;
          break;
      }

      $response = $this->checkout($req, $amount);

      // This will redirect user to PayPal
      return redirect($response['paypal_link']);
    }

    public function checkout($req, $amount)
    {
      $req->validate([
          'address1' => 'required|max:255',
          'address2' => 'required|max:255',
          'post_code' => 'required|max:255',
          'country' => 'required|max:255',
          'room' => 'required|max:255',
      ]);

      $provider = new ExpressCheckout;

      $hotel = Hotel::findOrFail($req->hotel_id);
      $hotel->fill($req->all());

      // set data for paypal
      $data = [];
      $data['items'] = [
          [
              'name'  => "Monthly Subscription",
              'price' => $amount,
              'qty'   => 1,
          ],
      ];
      $data['subscription_desc'] = "Monthly Subscription ".$hotel->name;
      $data['invoice_id'] = uniqId();
      $data['invoice_description'] = "Monthly Subscription ".$hotel->name;
      $data['return_url'] = route('cms.paypal.success');
      $data['cancel_url'] = route('subscribe.hotels.index');
      $total = 0;
      foreach ($data['items'] as $item) {
          $total += $item['price'] * $item['qty'];
      }
      $data['total'] = $total;

      // Use the following line when creating recurring payment profiles (subscriptions)
      $response = $provider->setExpressCheckout($data, true);      

      // simpan token ke database untuk tracking pembayaran
      $hotel->token = $response['TOKEN'];
      $hotel->save();

      // create HotelFeature
      $hotel_feature = HotelFeature::create([
        'hotel_id' => $hotel->id,
        'is_check_in_enabled' => '1',
        'is_check_out_enabled' => '1',
        'is_spa_enabled' => '1',
        'is_restaurant_enabled' => '1',
        'is_concierge_enabled' => '1',
        'is_cleaning_enabled' => '1',
      ]);

      // create restaurant
      $restaurant = Restaurant::create([
        'hotel_id' => $hotel->id,
        'name' => 'Restaurant',
        'logo_url' => 'https://marketplace.canva.com/MAC5oKacMGY/1/0/thumbnail_large-5/canva-black-with-utensils-icon-restaurant-logo-MAC5oKacMGY.jpg',
      ]);

      // create spa
      $spa = Spa::create([
        'hotel_id' => $hotel->id,
        'name' => 'Spa',
        'logo_url' => 'http://lorempixel.com/200/200',
      ]);

      return $response;
    }
}
