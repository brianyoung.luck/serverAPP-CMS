<?php if($message = Session::get('success')): ?>
<div class="alert alert-success alert-block" id="alert_success_session">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong><?php echo e($message); ?></strong>
</div>
<?php endif; ?>

<?php if($message = Session::get('error')): ?>
<div class="alert alert-danger alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong><?php echo e($message); ?></strong>
</div>
<?php endif; ?>

<?php if($message = Session::get('warning')): ?>
<div class="alert alert-warning alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
	<strong><?php echo e($message); ?></strong>
</div>
<?php endif; ?>

<?php if($message = Session::get('info')): ?>
<div class="alert alert-info alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
	<strong><?php echo e($message); ?></strong>
</div>
<?php endif; ?>

<?php if($message = Session::get('status')): ?>
	<div class="alert alert-success alert-block" id="alert_success_session">
		<button type="button" class="close" data-dismiss="alert">×</button>
		<strong><?php echo e($message); ?></strong>
	</div>
<?php endif; ?>

<?php if($errors->any()): ?>
<div class="alert alert-danger">
	<button type="button" class="close" data-dismiss="alert">×</button>	
	<strong><?php echo e(__('Please check the form below for errors')); ?></strong>
	
		
			
		
	
</div>
<?php endif; ?><?php /**PATH C:\Users\Syed Hasnain Haider\Desktop\backup\resources\views/cms/layouts/flash-message.blade.php ENDPATH**/ ?>