<?php

namespace App\Services;

use App\Models\HotelBooking;
use App\Models\LaundryOrder;

class LaundryOrderService
{
    public function createOrder(HotelBooking $booking, $data)
    {
        return LaundryOrder::create([
            'hotel_booking_id' => $booking->id,
            'type' => $data['type'],
        ]);
    }
}