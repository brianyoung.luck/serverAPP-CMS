<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMobileHotelLayoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mobile_hotel_layouts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('checkin_image');
            $table->longText('checkout_image');
            $table->longText('restaurant_image');
            $table->longText('spa_image');
            $table->longText('concierge_image');
            $table->longText('experience_image');
            $table->longText('photo_preview_url');
            $table->unsignedBigInteger('hotel_id')->default(0);
            $table->string('layout_name')->default('custom');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mobile_hotel_layouts');
    }
}