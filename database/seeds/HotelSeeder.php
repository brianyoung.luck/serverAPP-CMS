<?php

use App\Models\Hotel;
use App\Models\HotelFeature;
use Illuminate\Database\Seeder;

class HotelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $hotelAgus = Hotel::create([
            'code' => 'AGUS',
            'mobile_hotel_theme_id' => 1,
            'email' => 'agussuarya@gmail.com',
            'username' => 'agussuarya',
            'password' => bcrypt('secret'),
            'name' => 'Suarya Hotel',
            'description' => 'Hotel bintang 5.',
            'hotel_logo_ori' => 'https://www.designfreelogoonline.com/wp-content/uploads/2014/12/00546-Square-levels-logo-design-free-logos-online-02.png',
            'hotel_logo_lg' => 'https://www.designfreelogoonline.com/wp-content/uploads/2014/12/00546-Square-levels-logo-design-free-logos-online-02.png',
            'hotel_logo_md' => 'https://www.logaster.com/blog/wp-content/uploads/2018/05/LogoMakr.png',
            'hotel_logo_sm' => 'https://img.freepik.com/free-vector/polygonal-hexagon-logo_1061-517.jpg',
            'sendbird_user_id' => 'admin_hotel_1',
            'sendbird_access_token' => '91867eddc6cb0a082464e4c3669cbd34ea44d128',
        ]);

        HotelFeature::create([
            'hotel_id' => $hotelAgus->id,
        ]);

        factory(Hotel::class, 4)->create();
    }
}
