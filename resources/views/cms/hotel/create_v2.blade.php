<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ __('Register hotel') }} - {{ env('APP_NAME') }}</title>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom-cms.css') }}" rel="stylesheet">
</head>

<body class="register-hotel-wrapper">
    <div class="container-fluid p-0">
        <div class="header-nav-wrapper">
            <div class="icon-nav">
                <h1>Servr</h1>
            </div>
        </div>
        <div class="content-wrapper">
            <div class="row">
                <div class="col-md-5 content-left">
                    <div class="card card-text">
                        <div class="card-body">
                            <p>
                                Start your trial and join free for a month now <i class="icon-arrow-right icons"></i>
                            </p>
                        </div>
                    </div>
                    @if($no_trial)
                    <div class="alert alert-danger">You are not eligble for trial.</div>
                    @endif
                    <div class="card card-register">
                        <div class="card-body">
                            <form id="registerForm" class="form-horizontal" action="{{ route('cms.hotels.store') }}"
                                method="post">
                                @csrf()
                                @method('post')

                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <label class="col-form-label" for="name">Hotel name:</label>
                                        <div class="input-group">
                                            <input class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}"
                                                id="name" type="text" name="name"
                                                value="{{ old('name') ? old('name') : '' }}">
                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <label class="col-form-label" for="phone_number">Hotel phone number:</label>
                                        <div class="input-group">
                                            <input
                                                class="form-control {{ $errors->has('phone_number') ? ' is-invalid' : '' }}"
                                                id="phone_number" type="text" name="phone_number"
                                                value="{{ old('phone_number') ? old('phone_number') : '' }}">
                                            @if ($errors->has('phone_number'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('phone_number') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <label class="col-form-label" for="email">Hotel email:</label>
                                        <div class="input-group">
                                            <input class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}"
                                                id="email" type="email" name="email" placeholder="To be used to login"
                                                value="{{ old('email') ? old('email') : '' }}">
                                            @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <label class="col-form-label" for="password">Password:</label>
                                        <div class="input-group">
                                            <input
                                                class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}"
                                                id="password" type="password" name="password">
                                            <span class="fa fa-fw fa-eye-slash field-icon toggle-password"></span>
                                            @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <label class="col-form-label" for="email">Confirm Password:</label>
                                        <div class="input-group">
                                            <input
                                                class="form-control {{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}"
                                                id="password_confirmation" type="password" name="password_confirmation">
                                            <span
                                                class="fa fa-fw fa-eye-slash field-icon toggle-password-confirm"></span>
                                            @if ($errors->has('password_confirm'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <label class="col-form-label" for="email">Hotel code:</label>
                                        <div class="input-group">
                                            <input class="form-control {{ $errors->has('code') ? ' is-invalid' : '' }}"
                                                id="code" type="text" name="code"
                                                placeholder="To be used by guest to login"
                                                style="text-transform:uppercase"
                                                value="{{ old('code') ? old('code') : '' }}">
                                            @if ($errors->has('code'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('code') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <button class="btn btn-pill btn-block btn-primary" type="button"
                                            id="buttonRegister">{{ __('Register') }}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-7 content-right">
                    <div class="image-wrapper">
                        <img src="{{ asset('images/register_hotel/laptop_hd.png') }}" alt="device">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
        const url1 = '{!! route('cms.dashboard.index') !!}';
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
    <script src="{{ asset('js/custom/cms/hotel.js') }}"></script>
</body>

</html>