<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class HotelBooking extends Authenticatable implements JWTSubject
{
    use Notifiable;

    protected $fillable = [
        'hotel_id',
        'reference',
        'arrival_date',
        'departure_date',
        'late_check_out',
        'cardholder_name',
        'card_number',
        'card_expiry_date',
        'card_address',
        'late_check_out',
        'status',
        'phone_number',
        'room_number',
        'sendbird_user_id',
        'sendbird_access_token',
        'sendbird_channel_url',
    ];

    protected $dates = [
        'card_expiry_date',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [ 'exp' => $this->departure_date ];
    }

    public function passports()
    {
        return $this->hasMany(PassportPhoto::class);
    }

    /**
     * Scopes
     */

    /**
     * Filter by new arrivals
     * @param $query
     * @return mixed
     */
    public function scopeNewArrivals($query)
    {
        return $query->where('hotel_id', auth('hotel')->user()->id)
        ->where('status', 'pending');
    }

    public function scopeCurrentGuests($query)
    {
        return $query->where('hotel_id', auth('hotel')->user()->id)
            ->where('status', 'active');
    }

    public function scopeGetByPhoneNumber($query, $phoneNumber)
    {
        return $query->where('phone_number', $phoneNumber);
    }

    public function scopeActiveDate($query)
    {
        return $query->whereDate('arrival_date', '<=', now())
            ->whereDate('departure_date', '>=', now());
    }

    /**
     * Relations
     */

    public function passportPhotos()
    {
        return $this->hasMany(PassportPhoto::class, 'hotel_booking_id', 'id');
    }

    public function hotel()
    {
        return $this->belongsTo(Hotel::class);
    }

    public function restaurantReservations()
    {
        return $this->hasMany(RestaurantReservation::class);
    }

    public function restaurantRoomServices()
    {
        return $this->hasMany(RestaurantRoomService::class);
    }

    public function conciergeServiceRequests()
    {
        return $this->hasMany(ConciergeServiceRequest::class);
    }

    public function spaBookings()
    {
        return $this->hasMany(SpaBooking::class);
    }

    public function laundryOrders()
    {
        return $this->hasMany(LaundryOrder::class);
    }

    public function roomCleaningOrders()
    {
        return $this->hasMany(RoomCleaningOrder::class);
    }

    public function bill()
    {
        return $this->hasOne(BookingBill::class);
    }
}
