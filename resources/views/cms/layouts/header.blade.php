<header class="app-header navbar">
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        ☰
    </button>
    <a class="navbar-brand" href="{{ route('cms.dashboard.index') }}">
        {{--<img class="navbar-brand-full" src="{{ asset('images/logo/logo.png') }}" alt="Logo">--}}
        {{--<img class="navbar-brand-minimized" src="{{ asset('images/logo/logo.png') }}" alt="Logo">--}}
        <div class="navbar-brand-full">
            <h1>Servr</h1>
            <h2>Admin panel</h2>
        </div>
        <h1 class="navbar-brand-minimized"></h1>
    </a>
    {{--<button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">--}}
        {{--☰--}}
    {{--</button>--}}
    <ul class="nav navbar-nav ml-auto mr-3">
        <li class="nav-item">
            {{ auth('hotel')->user()->name }}
        </li>
        <li class="nav-item">
            {{  now()->format('M d, Y') }}
        </li>
    </ul>
</header>