/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 6);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/@babel/runtime/regenerator/index.js":
/*!**********************************************************!*\
  !*** ./node_modules/@babel/runtime/regenerator/index.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! regenerator-runtime */ "./node_modules/regenerator-runtime/runtime.js");


/***/ }),

/***/ "./node_modules/regenerator-runtime/runtime.js":
/*!*****************************************************!*\
  !*** ./node_modules/regenerator-runtime/runtime.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/**
 * Copyright (c) 2014-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

var runtime = (function (exports) {
  "use strict";

  var Op = Object.prototype;
  var hasOwn = Op.hasOwnProperty;
  var undefined; // More compressible than void 0.
  var $Symbol = typeof Symbol === "function" ? Symbol : {};
  var iteratorSymbol = $Symbol.iterator || "@@iterator";
  var asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator";
  var toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag";

  function wrap(innerFn, outerFn, self, tryLocsList) {
    // If outerFn provided and outerFn.prototype is a Generator, then outerFn.prototype instanceof Generator.
    var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator;
    var generator = Object.create(protoGenerator.prototype);
    var context = new Context(tryLocsList || []);

    // The ._invoke method unifies the implementations of the .next,
    // .throw, and .return methods.
    generator._invoke = makeInvokeMethod(innerFn, self, context);

    return generator;
  }
  exports.wrap = wrap;

  // Try/catch helper to minimize deoptimizations. Returns a completion
  // record like context.tryEntries[i].completion. This interface could
  // have been (and was previously) designed to take a closure to be
  // invoked without arguments, but in all the cases we care about we
  // already have an existing method we want to call, so there's no need
  // to create a new function object. We can even get away with assuming
  // the method takes exactly one argument, since that happens to be true
  // in every case, so we don't have to touch the arguments object. The
  // only additional allocation required is the completion record, which
  // has a stable shape and so hopefully should be cheap to allocate.
  function tryCatch(fn, obj, arg) {
    try {
      return { type: "normal", arg: fn.call(obj, arg) };
    } catch (err) {
      return { type: "throw", arg: err };
    }
  }

  var GenStateSuspendedStart = "suspendedStart";
  var GenStateSuspendedYield = "suspendedYield";
  var GenStateExecuting = "executing";
  var GenStateCompleted = "completed";

  // Returning this object from the innerFn has the same effect as
  // breaking out of the dispatch switch statement.
  var ContinueSentinel = {};

  // Dummy constructor functions that we use as the .constructor and
  // .constructor.prototype properties for functions that return Generator
  // objects. For full spec compliance, you may wish to configure your
  // minifier not to mangle the names of these two functions.
  function Generator() {}
  function GeneratorFunction() {}
  function GeneratorFunctionPrototype() {}

  // This is a polyfill for %IteratorPrototype% for environments that
  // don't natively support it.
  var IteratorPrototype = {};
  IteratorPrototype[iteratorSymbol] = function () {
    return this;
  };

  var getProto = Object.getPrototypeOf;
  var NativeIteratorPrototype = getProto && getProto(getProto(values([])));
  if (NativeIteratorPrototype &&
      NativeIteratorPrototype !== Op &&
      hasOwn.call(NativeIteratorPrototype, iteratorSymbol)) {
    // This environment has a native %IteratorPrototype%; use it instead
    // of the polyfill.
    IteratorPrototype = NativeIteratorPrototype;
  }

  var Gp = GeneratorFunctionPrototype.prototype =
    Generator.prototype = Object.create(IteratorPrototype);
  GeneratorFunction.prototype = Gp.constructor = GeneratorFunctionPrototype;
  GeneratorFunctionPrototype.constructor = GeneratorFunction;
  GeneratorFunctionPrototype[toStringTagSymbol] =
    GeneratorFunction.displayName = "GeneratorFunction";

  // Helper for defining the .next, .throw, and .return methods of the
  // Iterator interface in terms of a single ._invoke method.
  function defineIteratorMethods(prototype) {
    ["next", "throw", "return"].forEach(function(method) {
      prototype[method] = function(arg) {
        return this._invoke(method, arg);
      };
    });
  }

  exports.isGeneratorFunction = function(genFun) {
    var ctor = typeof genFun === "function" && genFun.constructor;
    return ctor
      ? ctor === GeneratorFunction ||
        // For the native GeneratorFunction constructor, the best we can
        // do is to check its .name property.
        (ctor.displayName || ctor.name) === "GeneratorFunction"
      : false;
  };

  exports.mark = function(genFun) {
    if (Object.setPrototypeOf) {
      Object.setPrototypeOf(genFun, GeneratorFunctionPrototype);
    } else {
      genFun.__proto__ = GeneratorFunctionPrototype;
      if (!(toStringTagSymbol in genFun)) {
        genFun[toStringTagSymbol] = "GeneratorFunction";
      }
    }
    genFun.prototype = Object.create(Gp);
    return genFun;
  };

  // Within the body of any async function, `await x` is transformed to
  // `yield regeneratorRuntime.awrap(x)`, so that the runtime can test
  // `hasOwn.call(value, "__await")` to determine if the yielded value is
  // meant to be awaited.
  exports.awrap = function(arg) {
    return { __await: arg };
  };

  function AsyncIterator(generator) {
    function invoke(method, arg, resolve, reject) {
      var record = tryCatch(generator[method], generator, arg);
      if (record.type === "throw") {
        reject(record.arg);
      } else {
        var result = record.arg;
        var value = result.value;
        if (value &&
            typeof value === "object" &&
            hasOwn.call(value, "__await")) {
          return Promise.resolve(value.__await).then(function(value) {
            invoke("next", value, resolve, reject);
          }, function(err) {
            invoke("throw", err, resolve, reject);
          });
        }

        return Promise.resolve(value).then(function(unwrapped) {
          // When a yielded Promise is resolved, its final value becomes
          // the .value of the Promise<{value,done}> result for the
          // current iteration.
          result.value = unwrapped;
          resolve(result);
        }, function(error) {
          // If a rejected Promise was yielded, throw the rejection back
          // into the async generator function so it can be handled there.
          return invoke("throw", error, resolve, reject);
        });
      }
    }

    var previousPromise;

    function enqueue(method, arg) {
      function callInvokeWithMethodAndArg() {
        return new Promise(function(resolve, reject) {
          invoke(method, arg, resolve, reject);
        });
      }

      return previousPromise =
        // If enqueue has been called before, then we want to wait until
        // all previous Promises have been resolved before calling invoke,
        // so that results are always delivered in the correct order. If
        // enqueue has not been called before, then it is important to
        // call invoke immediately, without waiting on a callback to fire,
        // so that the async generator function has the opportunity to do
        // any necessary setup in a predictable way. This predictability
        // is why the Promise constructor synchronously invokes its
        // executor callback, and why async functions synchronously
        // execute code before the first await. Since we implement simple
        // async functions in terms of async generators, it is especially
        // important to get this right, even though it requires care.
        previousPromise ? previousPromise.then(
          callInvokeWithMethodAndArg,
          // Avoid propagating failures to Promises returned by later
          // invocations of the iterator.
          callInvokeWithMethodAndArg
        ) : callInvokeWithMethodAndArg();
    }

    // Define the unified helper method that is used to implement .next,
    // .throw, and .return (see defineIteratorMethods).
    this._invoke = enqueue;
  }

  defineIteratorMethods(AsyncIterator.prototype);
  AsyncIterator.prototype[asyncIteratorSymbol] = function () {
    return this;
  };
  exports.AsyncIterator = AsyncIterator;

  // Note that simple async functions are implemented on top of
  // AsyncIterator objects; they just return a Promise for the value of
  // the final result produced by the iterator.
  exports.async = function(innerFn, outerFn, self, tryLocsList) {
    var iter = new AsyncIterator(
      wrap(innerFn, outerFn, self, tryLocsList)
    );

    return exports.isGeneratorFunction(outerFn)
      ? iter // If outerFn is a generator, return the full iterator.
      : iter.next().then(function(result) {
          return result.done ? result.value : iter.next();
        });
  };

  function makeInvokeMethod(innerFn, self, context) {
    var state = GenStateSuspendedStart;

    return function invoke(method, arg) {
      if (state === GenStateExecuting) {
        throw new Error("Generator is already running");
      }

      if (state === GenStateCompleted) {
        if (method === "throw") {
          throw arg;
        }

        // Be forgiving, per 25.3.3.3.3 of the spec:
        // https://people.mozilla.org/~jorendorff/es6-draft.html#sec-generatorresume
        return doneResult();
      }

      context.method = method;
      context.arg = arg;

      while (true) {
        var delegate = context.delegate;
        if (delegate) {
          var delegateResult = maybeInvokeDelegate(delegate, context);
          if (delegateResult) {
            if (delegateResult === ContinueSentinel) continue;
            return delegateResult;
          }
        }

        if (context.method === "next") {
          // Setting context._sent for legacy support of Babel's
          // function.sent implementation.
          context.sent = context._sent = context.arg;

        } else if (context.method === "throw") {
          if (state === GenStateSuspendedStart) {
            state = GenStateCompleted;
            throw context.arg;
          }

          context.dispatchException(context.arg);

        } else if (context.method === "return") {
          context.abrupt("return", context.arg);
        }

        state = GenStateExecuting;

        var record = tryCatch(innerFn, self, context);
        if (record.type === "normal") {
          // If an exception is thrown from innerFn, we leave state ===
          // GenStateExecuting and loop back for another invocation.
          state = context.done
            ? GenStateCompleted
            : GenStateSuspendedYield;

          if (record.arg === ContinueSentinel) {
            continue;
          }

          return {
            value: record.arg,
            done: context.done
          };

        } else if (record.type === "throw") {
          state = GenStateCompleted;
          // Dispatch the exception by looping back around to the
          // context.dispatchException(context.arg) call above.
          context.method = "throw";
          context.arg = record.arg;
        }
      }
    };
  }

  // Call delegate.iterator[context.method](context.arg) and handle the
  // result, either by returning a { value, done } result from the
  // delegate iterator, or by modifying context.method and context.arg,
  // setting context.delegate to null, and returning the ContinueSentinel.
  function maybeInvokeDelegate(delegate, context) {
    var method = delegate.iterator[context.method];
    if (method === undefined) {
      // A .throw or .return when the delegate iterator has no .throw
      // method always terminates the yield* loop.
      context.delegate = null;

      if (context.method === "throw") {
        // Note: ["return"] must be used for ES3 parsing compatibility.
        if (delegate.iterator["return"]) {
          // If the delegate iterator has a return method, give it a
          // chance to clean up.
          context.method = "return";
          context.arg = undefined;
          maybeInvokeDelegate(delegate, context);

          if (context.method === "throw") {
            // If maybeInvokeDelegate(context) changed context.method from
            // "return" to "throw", let that override the TypeError below.
            return ContinueSentinel;
          }
        }

        context.method = "throw";
        context.arg = new TypeError(
          "The iterator does not provide a 'throw' method");
      }

      return ContinueSentinel;
    }

    var record = tryCatch(method, delegate.iterator, context.arg);

    if (record.type === "throw") {
      context.method = "throw";
      context.arg = record.arg;
      context.delegate = null;
      return ContinueSentinel;
    }

    var info = record.arg;

    if (! info) {
      context.method = "throw";
      context.arg = new TypeError("iterator result is not an object");
      context.delegate = null;
      return ContinueSentinel;
    }

    if (info.done) {
      // Assign the result of the finished delegate to the temporary
      // variable specified by delegate.resultName (see delegateYield).
      context[delegate.resultName] = info.value;

      // Resume execution at the desired location (see delegateYield).
      context.next = delegate.nextLoc;

      // If context.method was "throw" but the delegate handled the
      // exception, let the outer generator proceed normally. If
      // context.method was "next", forget context.arg since it has been
      // "consumed" by the delegate iterator. If context.method was
      // "return", allow the original .return call to continue in the
      // outer generator.
      if (context.method !== "return") {
        context.method = "next";
        context.arg = undefined;
      }

    } else {
      // Re-yield the result returned by the delegate method.
      return info;
    }

    // The delegate iterator is finished, so forget it and continue with
    // the outer generator.
    context.delegate = null;
    return ContinueSentinel;
  }

  // Define Generator.prototype.{next,throw,return} in terms of the
  // unified ._invoke helper method.
  defineIteratorMethods(Gp);

  Gp[toStringTagSymbol] = "Generator";

  // A Generator should always return itself as the iterator object when the
  // @@iterator function is called on it. Some browsers' implementations of the
  // iterator prototype chain incorrectly implement this, causing the Generator
  // object to not be returned from this call. This ensures that doesn't happen.
  // See https://github.com/facebook/regenerator/issues/274 for more details.
  Gp[iteratorSymbol] = function() {
    return this;
  };

  Gp.toString = function() {
    return "[object Generator]";
  };

  function pushTryEntry(locs) {
    var entry = { tryLoc: locs[0] };

    if (1 in locs) {
      entry.catchLoc = locs[1];
    }

    if (2 in locs) {
      entry.finallyLoc = locs[2];
      entry.afterLoc = locs[3];
    }

    this.tryEntries.push(entry);
  }

  function resetTryEntry(entry) {
    var record = entry.completion || {};
    record.type = "normal";
    delete record.arg;
    entry.completion = record;
  }

  function Context(tryLocsList) {
    // The root entry object (effectively a try statement without a catch
    // or a finally block) gives us a place to store values thrown from
    // locations where there is no enclosing try statement.
    this.tryEntries = [{ tryLoc: "root" }];
    tryLocsList.forEach(pushTryEntry, this);
    this.reset(true);
  }

  exports.keys = function(object) {
    var keys = [];
    for (var key in object) {
      keys.push(key);
    }
    keys.reverse();

    // Rather than returning an object with a next method, we keep
    // things simple and return the next function itself.
    return function next() {
      while (keys.length) {
        var key = keys.pop();
        if (key in object) {
          next.value = key;
          next.done = false;
          return next;
        }
      }

      // To avoid creating an additional object, we just hang the .value
      // and .done properties off the next function object itself. This
      // also ensures that the minifier will not anonymize the function.
      next.done = true;
      return next;
    };
  };

  function values(iterable) {
    if (iterable) {
      var iteratorMethod = iterable[iteratorSymbol];
      if (iteratorMethod) {
        return iteratorMethod.call(iterable);
      }

      if (typeof iterable.next === "function") {
        return iterable;
      }

      if (!isNaN(iterable.length)) {
        var i = -1, next = function next() {
          while (++i < iterable.length) {
            if (hasOwn.call(iterable, i)) {
              next.value = iterable[i];
              next.done = false;
              return next;
            }
          }

          next.value = undefined;
          next.done = true;

          return next;
        };

        return next.next = next;
      }
    }

    // Return an iterator with no values.
    return { next: doneResult };
  }
  exports.values = values;

  function doneResult() {
    return { value: undefined, done: true };
  }

  Context.prototype = {
    constructor: Context,

    reset: function(skipTempReset) {
      this.prev = 0;
      this.next = 0;
      // Resetting context._sent for legacy support of Babel's
      // function.sent implementation.
      this.sent = this._sent = undefined;
      this.done = false;
      this.delegate = null;

      this.method = "next";
      this.arg = undefined;

      this.tryEntries.forEach(resetTryEntry);

      if (!skipTempReset) {
        for (var name in this) {
          // Not sure about the optimal order of these conditions:
          if (name.charAt(0) === "t" &&
              hasOwn.call(this, name) &&
              !isNaN(+name.slice(1))) {
            this[name] = undefined;
          }
        }
      }
    },

    stop: function() {
      this.done = true;

      var rootEntry = this.tryEntries[0];
      var rootRecord = rootEntry.completion;
      if (rootRecord.type === "throw") {
        throw rootRecord.arg;
      }

      return this.rval;
    },

    dispatchException: function(exception) {
      if (this.done) {
        throw exception;
      }

      var context = this;
      function handle(loc, caught) {
        record.type = "throw";
        record.arg = exception;
        context.next = loc;

        if (caught) {
          // If the dispatched exception was caught by a catch block,
          // then let that catch block handle the exception normally.
          context.method = "next";
          context.arg = undefined;
        }

        return !! caught;
      }

      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        var record = entry.completion;

        if (entry.tryLoc === "root") {
          // Exception thrown outside of any try block that could handle
          // it, so set the completion value of the entire function to
          // throw the exception.
          return handle("end");
        }

        if (entry.tryLoc <= this.prev) {
          var hasCatch = hasOwn.call(entry, "catchLoc");
          var hasFinally = hasOwn.call(entry, "finallyLoc");

          if (hasCatch && hasFinally) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            } else if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else if (hasCatch) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            }

          } else if (hasFinally) {
            if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else {
            throw new Error("try statement without catch or finally");
          }
        }
      }
    },

    abrupt: function(type, arg) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc <= this.prev &&
            hasOwn.call(entry, "finallyLoc") &&
            this.prev < entry.finallyLoc) {
          var finallyEntry = entry;
          break;
        }
      }

      if (finallyEntry &&
          (type === "break" ||
           type === "continue") &&
          finallyEntry.tryLoc <= arg &&
          arg <= finallyEntry.finallyLoc) {
        // Ignore the finally entry if control is not jumping to a
        // location outside the try/catch block.
        finallyEntry = null;
      }

      var record = finallyEntry ? finallyEntry.completion : {};
      record.type = type;
      record.arg = arg;

      if (finallyEntry) {
        this.method = "next";
        this.next = finallyEntry.finallyLoc;
        return ContinueSentinel;
      }

      return this.complete(record);
    },

    complete: function(record, afterLoc) {
      if (record.type === "throw") {
        throw record.arg;
      }

      if (record.type === "break" ||
          record.type === "continue") {
        this.next = record.arg;
      } else if (record.type === "return") {
        this.rval = this.arg = record.arg;
        this.method = "return";
        this.next = "end";
      } else if (record.type === "normal" && afterLoc) {
        this.next = afterLoc;
      }

      return ContinueSentinel;
    },

    finish: function(finallyLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.finallyLoc === finallyLoc) {
          this.complete(entry.completion, entry.afterLoc);
          resetTryEntry(entry);
          return ContinueSentinel;
        }
      }
    },

    "catch": function(tryLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc === tryLoc) {
          var record = entry.completion;
          if (record.type === "throw") {
            var thrown = record.arg;
            resetTryEntry(entry);
          }
          return thrown;
        }
      }

      // The context.catch method must only be called with a location
      // argument that corresponds to a known catch block.
      throw new Error("illegal catch attempt");
    },

    delegateYield: function(iterable, resultName, nextLoc) {
      this.delegate = {
        iterator: values(iterable),
        resultName: resultName,
        nextLoc: nextLoc
      };

      if (this.method === "next") {
        // Deliberately forget the last sent value so that we don't
        // accidentally pass it on to the delegate.
        this.arg = undefined;
      }

      return ContinueSentinel;
    }
  };

  // Regardless of whether this script is executing as a CommonJS module
  // or not, return the runtime object so that we can declare the variable
  // regeneratorRuntime in the outer scope, which allows this module to be
  // injected easily by `bin/regenerator --include-runtime script.js`.
  return exports;

}(
  // If this script is executing as a CommonJS module, use module.exports
  // as the regeneratorRuntime namespace. Otherwise create a new empty
  // object. Either way, the resulting object will be used to initialize
  // the regeneratorRuntime variable at the top of this file.
   true ? module.exports : undefined
));

try {
  regeneratorRuntime = runtime;
} catch (accidentalStrictMode) {
  // This module should not be running in strict mode, so the above
  // assignment should always work unless something is misconfigured. Just
  // in case runtime.js accidentally runs in strict mode, we can escape
  // strict mode using a global Function call. This could conceivably fail
  // if a Content Security Policy forbids using Function, but in that case
  // the proper solution is to fix the accidental strict mode problem. If
  // you've misconfigured your bundler to force strict mode and applied a
  // CSP to forbid Function, and you're not willing to fix either of those
  // problems, please detail your unique predicament in a GitHub issue.
  Function("r", "regeneratorRuntime = r")(runtime);
}


/***/ }),

/***/ "./resources/js/custom/cms/guest.js":
/*!******************************************!*\
  !*** ./resources/js/custom/cms/guest.js ***!
  \******************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var guestTable = $("#guestTable");
var optionsDateTime = {
  year: "numeric",
  month: "short",
  day: "numeric",
  hour: "2-digit",
  minute: "2-digit"
};
var deleteHotelBookingForm = $("#deleteHotelBookingForm");
var hotelBookingId = $("#hotelBookingId");
var hotelBookingIdUpdate = $("#hotelBookingIdUpdate");
var editHotelBookingForm = $("#editHotelBookingForm");
var guestProfileModal = $("#guestProfileModal");
var userProfileModal = $(".userProfileModal");
var guestName = $("#guestName");
var guestReference = $("#guestReference");
var guestArrivalDate = $("#guestArrivalDate");
var guestDepartureDate = $("#guestDepartureDate");
var guestCardExpiryDate = $("#guestCardExpiryDate");
var guestCardholderName = $("#guestCardholderName");
var guestCardAddress = $("#guestCardAddress");
var guestPassportEntriesWrapper = $("#guestPassportEntriesWrapper");
var newGuestName = $("#newGuestName");
var newGuestReference = $("#newGuestReference");
var newGuestArrivalDate = $("#newGuestArrivalDate");
var newGuestDepartureDate = $("#newGuestDepartureDate");
var newGuestCardExpiryDate = $("#newGuestCardExpiryDate");
var newGuestCardholderName = $("#newGuestCardholderName");
var newGuestCardAddress = $("#newGuestCardAddress");
var newGuestPassportEntriesWrapper = $("#newGuestPassportEntriesWrapper");
var declineBtn = $(".declineBtn");
var acceptBtn = $(".acceptBtn");
var checkoutBtn = $(".checkoutBtn");
var checkoutHotelBookingForm = $("#checkoutHotelBookingForm");
var hotelBookingIdCheckout = $("#hotelBookingIdCheckout");
var roomNumber = $("#roomNumber");
var lateCheckoutHotelBookingForm = $("#lateCheckoutHotelBookingForm");
var hotelBookingIdLateCheckout = $("#hotelBookingIdLateCheckout");
var statusLateCheckout = $("#statusLateCheckout");
var lateCheckoutBtn = $(".lateCheckoutBtn");
var guestCardNumber = $("#guestCardNumber");
var newGuestCardCardNumber = $("#newGuestCardCardNumber");
var userProfileText = $(".userProfileText");
var showModalType;
$(document).ready(function () {
  // guestTable.DataTable({
  //   lengthMenu: [[10, 25, 50, -1], ["10", "25", "50", "All"]],
  //   order: [[0, "desc"]],
  //   language: {
  //     searchPlaceholder: "Name/booking ref",
  //   },
  //   processing: true,
  //   serverSide: true,
  //   ajax: {
  //     url: url1,
  //     data: function(d) {},
  //   },
  //   columns: [
  //     {
  //       data: "cardholder_name",
  //       name: "cardholder_name",
  //       render: function(data) {
  //         return data;
  //       },
  //     },
  //     {
  //       data: "reference",
  //       name: "reference",
  //       render: function(data) {
  //         return data;
  //       },
  //     },
  //     {
  //       data: "room_number",
  //       name: "room_number",
  //       render: function(data) {
  //         return data ? data : "-";
  //       },
  //     },
  //     {
  //       data: "arrival_date",
  //       name: "arrival_date",
  //       render: function(data) {
  //         return new Date(data).toLocaleDateString("en-US", optionsDateTime);
  //       },
  //     },
  //     {
  //       data: "departure_date",
  //       name: "departure_date",
  //       render: function(data) {
  //         return new Date(data).toLocaleDateString("en-US", optionsDateTime);
  //       },
  //     },
  //     {
  //       data: "status",
  //       name: "status",
  //       render: function(data) {
  //         if (data == "active") {
  //           return '<span class="badge badge-success">Active</span>';
  //         } else {
  //           return '<span class="badge badge-warning">Checked out</span>';
  //         }
  //       },
  //     },
  //     {
  //       data: "id",
  //       name: "id",
  //       orderable: false,
  //       render: function(data) {
  //         return `
  //                     <a class="btn-action-table" data-id="${data}"><i class="icon-info icons" ></i></a>
  //                 `;
  //       },
  //     },
  //     {
  //       data: "reference",
  //       name: "reference",
  //       visible: false,
  //       orderable: false,
  //     },
  //   ],
  // });
  $(document).on("click", ".acceptBtn", function () {
    accept($(this).data("id"));
  });
  $(document).on("click", ".declineBtn", function () {
    decline($(this).data("id"));
  });
  $(document).on("click", ".checkoutBtn", function () {
    checkout($(this).data("id"));
  });
  $(document).on("click", ".lateCheckoutBtn", function () {
    lateCheckout($(this).data("id"));
  }); // $(document).on("click", ".btn-action-table", async function() { await showModalGuestDetails($(this).data('id')); });

  $(document).on("click", ".newArrivalWrapper",
  /*#__PURE__*/
  _asyncToGenerator(
  /*#__PURE__*/
  _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return showModalNewGuestDetails($(this).data("id"), "new_arrival");

          case 2:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  })));
  $(document).on("click", ".lateCheckoutWrapper",
  /*#__PURE__*/
  _asyncToGenerator(
  /*#__PURE__*/
  _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.next = 2;
            return showModalNewGuestDetails($(this).data("id"), "late_checkout");

          case 2:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, this);
  }))); // Append modal to body

  userProfileModal.appendTo("body");
  guestProfileModal.appendTo("body");
  userProfileModal.on("shown.bs.modal", function () {
    $(document).off("focusin.modal");
  });
  guestProfileModal.on("hidden.bs.modal", function (e) {
    // Change guest details to loading
    resetGuestDetails();
  });
  userProfileModal.on("hidden.bs.modal", function (e) {
    // Change guest details to loading
    resetNewGuestDetails();
  });
});

function accept(id) {
  if (showModalType === "late_checkout") {
    Swal.fire({
      title: "Are you sure?",
      type: "question",
      showCancelButton: true,
      confirmButtonColor: success_button_color,
      cancelButtonColor: cancel_button_color,
      confirmButtonText: "Accept",
      reverseButtons: true
    }).then(function (result) {
      if (result.value) {
        // Accept
        hotelBookingIdLateCheckout.val(id);
        statusLateCheckout.val("accepted");
        lateCheckoutHotelBookingForm.submit();
      }
    });
  } else {
    Swal.fire({
      title: "Are you sure?",
      type: "question",
      showCancelButton: true,
      confirmButtonColor: success_button_color,
      cancelButtonColor: cancel_button_color,
      confirmButtonText: "Accept",
      reverseButtons: true,
      input: "text",
      inputPlaceholder: "Enter room number",
      inputValue: "",
      inputValidator: function inputValidator(value) {
        if (!value) {
          return "You need to set room number.";
        }
      },
      showLoaderOnConfirm: true,
      preConfirm: function preConfirm(roomNumber) {
        return fetch("/ajax/is-room-number-not-used/?room_number=".concat(roomNumber)).then(function (response) {
          if (!response.ok) {
            throw new Error(response.statusText);
          }

          return response.json();
        }).then(function (myJson) {
          if (!myJson.status) {
            Swal.showValidationMessage("Room already checked in.");
          }
        })["catch"](function (error) {
          console.log(error);
        });
      },
      allowOutsideClick: function allowOutsideClick() {
        return !Swal.isLoading();
      }
    }).then(function (result) {
      if (result.value) {
        roomNumber.val(result.value);
        hotelBookingIdUpdate.val(id);
        editHotelBookingForm.submit();
      }
    });
  }
}

function decline(id) {
  if (showModalType === "late_checkout") {
    Swal.fire({
      title: "Are you sure?",
      type: "question",
      showCancelButton: true,
      confirmButtonColor: delete_button_color,
      cancelButtonColor: cancel_button_color,
      confirmButtonText: "Decline",
      reverseButtons: true
    }).then(function (result) {
      if (result.value && result.value === true) {
        // Reject
        hotelBookingIdLateCheckout.val(id);
        statusLateCheckout.val("rejected");
        lateCheckoutHotelBookingForm.submit();
      }
    });
  } else {
    Swal.fire({
      title: "Are you sure?",
      type: "question",
      showCancelButton: true,
      confirmButtonColor: delete_button_color,
      cancelButtonColor: cancel_button_color,
      confirmButtonText: "Decline",
      reverseButtons: true
    }).then(function (result) {
      if (result.value && result.value === true) {
        hotelBookingId.val(id);
        deleteHotelBookingForm.submit();
      }
    });
  }
}

function getGuestDetails(_x) {
  return _getGuestDetails.apply(this, arguments);
}

function _getGuestDetails() {
  _getGuestDetails = _asyncToGenerator(
  /*#__PURE__*/
  _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3(hotelBookingId) {
    var promise;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            promise = new Promise(function (resolve, reject) {
              $.ajax({
                headers: {
                  "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
                },
                type: "GET",
                url: url4 + "/" + hotelBookingId,
                contentType: "application/json",
                dataType: "json"
              }).done(function (response) {
                resolve(response);
              }).fail(function (error) {
                console.log(error);
                reject();
              });
            });
            _context3.prev = 1;
            _context3.next = 4;
            return promise;

          case 4:
            return _context3.abrupt("return", _context3.sent);

          case 7:
            _context3.prev = 7;
            _context3.t0 = _context3["catch"](1);
            return _context3.abrupt("return", {
              status: false,
              error: _context3.t0
            });

          case 10:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, null, [[1, 7]]);
  }));
  return _getGuestDetails.apply(this, arguments);
}

function fillGuestDetails(guestDetails) {
  $("#guestProfileModal .roomNumberText").html(guestDetails.room_number);
  guestName.html(guestDetails.cardholder_name);
  guestReference.html(guestDetails.reference);
  guestArrivalDate.html(dateTimeFormat(guestDetails.arrival_date));
  guestDepartureDate.html(dateTimeFormat(guestDetails.departure_date));
  guestCardExpiryDate.html(dateTimeFormat(guestDetails.card_expiry_date, "card_expiry_date"));
  guestCardholderName.html(guestDetails.cardholder_name);
  guestCardAddress.html(guestDetails.card_address);
  guestCardNumber.html(guestDetails.card_number);
  guestPassportEntriesWrapper.html("");
  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    for (var _iterator = guestDetails.passport_photos[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var passport_photo = _step.value;
      guestPassportEntriesWrapper.append("\n            <div class=\"passportEntryWrapper\">\n                <a href=\"".concat(passport_photo.url, "\" target=\"_blank\">\n                    <img src=\"").concat(passport_photo.url, "\" alt=\"Passport photo\">\n                </a>\n            </div>\n        "));
    }
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator["return"] != null) {
        _iterator["return"]();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }

  checkoutBtn.attr("data-id", guestDetails.id);
  lateCheckoutBtn.attr("data-id", guestDetails.id);

  if (guestDetails.late_check_out === "pending") {
    lateCheckoutBtn.addClass("d-inline");
    lateCheckoutBtn.removeClass("d-none");
  } else {
    lateCheckoutBtn.addClass("d-none");
    lateCheckoutBtn.removeClass("d-inline");
  }
}

function resetGuestDetails() {
  guestName.html("<i class=\"fa fa-refresh fa-spin fa-fw\"></i>");
  guestReference.html("<i class=\"fa fa-refresh fa-spin fa-fw\"></i>");
  guestArrivalDate.html("<i class=\"fa fa-refresh fa-spin fa-fw\"></i>");
  guestDepartureDate.html("<i class=\"fa fa-refresh fa-spin fa-fw\"></i>");
  guestCardExpiryDate.html("<i class=\"fa fa-refresh fa-spin fa-fw\"></i>");
  guestCardholderName.html("<i class=\"fa fa-refresh fa-spin fa-fw\"></i>");
  guestCardAddress.html("<i class=\"fa fa-refresh fa-spin fa-fw\"></i>");
  guestPassportEntriesWrapper.html("");
}

function showModalGuestDetails(_x2) {
  return _showModalGuestDetails.apply(this, arguments);
}

function _showModalGuestDetails() {
  _showModalGuestDetails = _asyncToGenerator(
  /*#__PURE__*/
  _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4(hotelBookingId) {
    var guestDetails;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            // Show modal
            guestProfileModal.modal("show"); // Call ajax to get guest details

            _context4.next = 3;
            return getGuestDetails(hotelBookingId);

          case 3:
            guestDetails = _context4.sent;
            // Show guest details
            fillGuestDetails(guestDetails.guestDetails);

          case 5:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4);
  }));
  return _showModalGuestDetails.apply(this, arguments);
}

function showModalNewGuestDetails(_x3, _x4) {
  return _showModalNewGuestDetails.apply(this, arguments);
}

function _showModalNewGuestDetails() {
  _showModalNewGuestDetails = _asyncToGenerator(
  /*#__PURE__*/
  _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee5(hotelBookingId, type) {
    var guestDetails;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            // Show modal
            userProfileModal.modal("show"); // Call ajax to get guest details

            _context5.next = 3;
            return getGuestDetails(hotelBookingId);

          case 3:
            guestDetails = _context5.sent;
            // Change show modal type
            showModalType = type; // Show guest details

            fillNewGuestDetails(guestDetails.guestDetails);

          case 6:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5);
  }));
  return _showModalNewGuestDetails.apply(this, arguments);
}

function fillNewGuestDetails(guestDetails) {
  if (showModalType === "late_checkout") {
    userProfileText.html("Late checkout:");
  } else {
    userProfileText.html("New arrival:");
  }

  newGuestName.html(guestDetails.cardholder_name);
  newGuestReference.html(guestDetails.reference ? guestDetails.reference : "-");
  newGuestArrivalDate.html(dateTimeFormat(guestDetails.arrival_date));
  newGuestDepartureDate.html(dateTimeFormat(guestDetails.departure_date));
  newGuestCardExpiryDate.html(dateTimeFormat(guestDetails.card_expiry_date, "card_expiry_date"));
  newGuestCardholderName.html(guestDetails.cardholder_name);
  newGuestCardAddress.html(guestDetails.card_address);
  newGuestCardCardNumber.html(guestDetails.card_number);
  newGuestPassportEntriesWrapper.html("");
  var _iteratorNormalCompletion2 = true;
  var _didIteratorError2 = false;
  var _iteratorError2 = undefined;

  try {
    for (var _iterator2 = guestDetails.passport_photos[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
      var passport_photo = _step2.value;
      newGuestPassportEntriesWrapper.append("\n            <div class=\"passportEntryWrapper\">\n                <a href=\"".concat(passport_photo.url, "\" target=\"_blank\">\n                    <img src=\"").concat(passport_photo.url, "\" alt=\"Passport photo\">\n                </a>\n            </div>\n        "));
    }
  } catch (err) {
    _didIteratorError2 = true;
    _iteratorError2 = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion2 && _iterator2["return"] != null) {
        _iterator2["return"]();
      }
    } finally {
      if (_didIteratorError2) {
        throw _iteratorError2;
      }
    }
  }

  declineBtn.attr("data-id", guestDetails.id);
  acceptBtn.attr("data-id", guestDetails.id);
}

function resetNewGuestDetails() {
  newGuestName.html("<i class=\"fa fa-refresh fa-spin fa-fw\"></i>");
  newGuestReference.html("<i class=\"fa fa-refresh fa-spin fa-fw\"></i>");
  newGuestArrivalDate.html("<i class=\"fa fa-refresh fa-spin fa-fw\"></i>");
  newGuestDepartureDate.html("<i class=\"fa fa-refresh fa-spin fa-fw\"></i>");
  newGuestCardExpiryDate.html("<i class=\"fa fa-refresh fa-spin fa-fw\"></i>");
  newGuestCardholderName.html("<i class=\"fa fa-refresh fa-spin fa-fw\"></i>");
  newGuestCardAddress.html("<i class=\"fa fa-refresh fa-spin fa-fw\"></i>");
  newGuestPassportEntriesWrapper.html("");
}

function checkout(id) {
  Swal.fire({
    title: "Are you sure?",
    type: "question",
    showCancelButton: true,
    confirmButtonColor: success_button_color,
    cancelButtonColor: cancel_button_color,
    confirmButtonText: "Checkout",
    reverseButtons: true
  }).then(function (result) {
    if (result.value && result.value === true) {
      hotelBookingIdCheckout.val(id);
      checkoutHotelBookingForm.submit();
    }
  });
}

function lateCheckout(id) {
  Swal.fire({
    title: "Approve late checkout?",
    type: "question",
    showCancelButton: true,
    confirmButtonColor: success_button_color,
    cancelButtonColor: cancel_button_color,
    confirmButtonText: "Accept",
    cancelButtonText: "Reject",
    reverseButtons: true
  }).then(function (result) {
    if (result.dismiss && result.dismiss === "cancel") {
      // Reject
      hotelBookingIdLateCheckout.val(id);
      statusLateCheckout.val("rejected");
      lateCheckoutHotelBookingForm.submit();
    } else if (result.value && result.value === true) {
      // Accept
      hotelBookingIdLateCheckout.val(id);
      statusLateCheckout.val("accepted");
      lateCheckoutHotelBookingForm.submit();
    }
  });
}

/***/ }),

/***/ 6:
/*!************************************************!*\
  !*** multi ./resources/js/custom/cms/guest.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! E:\Xammp\htdocs\server_hotels\resources\js\custom\cms\guest.js */"./resources/js/custom/cms/guest.js");


/***/ })

/******/ });