<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\RestaurantDish;
use Faker\Generator as Faker;

$factory->define(RestaurantDish::class, function (Faker $faker) {
    $dishNames = [
        'Cheese Pizza', 'Hamburger', 'Cheeseburger', 'Bacon Burger', 'Bacon Cheeseburger',
        'Little Hamburger', 'Little Cheeseburger', 'Little Bacon Burger', 'Little Bacon Cheeseburger',
        'Veggie Sandwich', 'Cheese Veggie Sandwich', 'Grilled Cheese',
        'Cheese Dog', 'Bacon Dog', 'Bacon Cheese Dog', 'Pasta', 'Potato',
    ];

    $adjectives = [
        'Hot',
        'Delicious',
        'Great',
        'Nice',
    ];

    $categoies = [
        'starter',
        'main',
        'dessert',
        'drink',
        'salad',
        'meat_and_fish',
    ];

    return [
        'name' => $faker->randomElement($adjectives) . " " . $faker->randomElement($dishNames),
        'price' => $faker->randomFloat(2, 1, 50),
        'category' => $faker->randomElement($categoies),
    ];
});
