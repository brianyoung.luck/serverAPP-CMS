<?php $__env->startSection('title', $spa->name); ?>

<?php $__env->startSection('content'); ?>
<div id="spaWrapper">
    <h3 class="mb-4">Spa: <span id="spaNameText"><?php echo e($spa->name); ?></span> &nbsp <i id="changeSpaName"
            class="icon-pencil icons"></i></h3>
    <div class="row">
        <div class="col-lg-4 col-md-5 col-12 spaLogoWrapper">
            
            <div class="alert alert-danger alert-dismissible fade show file_error messageAlert" role="alert"
                style="display: none;">
                <p></p>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="spaLogoFormWrapper">
                <div class="form-group row">
                    <label class="col-md-5 col-form-label">
                        Main logo
                    </label>
                    <div class="col-md-7">
                        <button class="btn btn-shadow" id="uploadFileSpaLogoBtn">Upload file</button>
                        <p class="text-muted mt-3">maximum size :4 MB</p>
                        <form id="editSpaForm" method="post" action="<?php echo e(route('cms.spas.update', ['id' => $spa->id])); ?>"
                            enctype="multipart/form-data">
                            <?php echo csrf_field(); ?>
                            <?php echo method_field('put'); ?>
                            <input type="file" name="logo" id="logo" class="d-none" accept="image/*">
                            <input type="hidden" name="updateType" id="updateType" value="">
                            <input type="hidden" name="spaNameUpdate" id="spaNameUpdate" value="">
                        </form>
                        <form id="deleteSpaGalleryForm" method="post"
                            action="<?php echo e(route('cms.spa-galleries.destroy', ['spa-gallery' => 0])); ?>"
                            enctype="multipart/form-data">
                            <?php echo csrf_field(); ?>
                            <?php echo method_field('delete'); ?>
                            <input type="hidden" name="spaGalleryId" id="spaGalleryId" value="0">
                            <input type="hidden" name="spaId" id="spaId" value="<?php echo e($spa->id); ?>">
                        </form>
                    </div>
                </div>
            </div>
            <div class="previewSpaLogo">
                <div class="row">
                    <div class="col-md-5">
                        <label>Preview</label>
                    </div>
                    <div class="col-md-7">
                        <div class="imgWrapper">
                            <img src="<?php echo e($spa->logo_url); ?>" alt="Logo">
                        </div>
                    </div>
                </div>
            </div>

            <div id="restaurantImagesFormWrapper">
                
                <div class="alert alert-danger alert-dismissible fade show restImagesErrors messageAlert" role="alert"
                    style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="form-group row">
                    <label class="col-md-5 col-form-label">
                        Spa images:
                    </label>
                    <div class="col-md-7">
                        <button class="btn btn-shadow" id="uploadFileSpaImagesBtn">Upload file</button>
                        <p class="text-muted mt-3">maximum size :4 MB</p>
                        <form id="editSpaImagesForm" method="post"
                            action="<?php echo e(route('cms.spas.update', ['id' => $spa->id])); ?>" enctype="multipart/form-data">
                            <?php echo csrf_field(); ?>
                            <?php echo method_field('put'); ?>
                            <input type="file" name="spaImages[]" id="spaImages" class="d-none" accept="image/*"
                                multiple>
                        </form>
                    </div>
                </div>
            </div>
            <div class="previewRestaurantImages">
                <div class="row">
                    <div class="col-md-5">
                        <label>Preview</label>
                    </div>
                    <div class="col-md-7">
                        <div class="imagesWrapper ui-sortable" style="white-space: nowrap;
                            overflow-x: scroll;
                            overflow-y: hidden;">
                            <?php $__currentLoopData = $galleries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gallery): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="imageWrapper ui-sortable-handle abc" data-sort_id="<?php echo e($gallery->sort_id); ?>" style="height: 110px;
                            margin: 10px 5px;
                            display: inline-block;
                            transition: transform 0.2s, -webkit-transform 0.2s;"
                                data-spa_gallery_id="<?php echo e($gallery->id); ?>"
                                data-spa_gallery_url="<?php echo e($gallery->image_url); ?>" id="<?php echo e($gallery->id); ?>">
                                <img style="width: 100%;
                                 object-fit: contain; vertical-align: middle;
                                 border-style: none;" src="<?php echo e($gallery->image_url); ?>" alt="Spa gallery">
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($galleries) == 0): ?>
                            <div class="imageWrapper">
                                <img src="<?php echo e(asset('images/hotel/img-placeholder.png')); ?>" alt="Restaurant gallery">
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-8 col-md-7 col-12 editMenuWrapper">
            <div class="row">
                <div class="col-lg-2 col-md-3 editMenuLabelWrapper">
                    <label>Edit menu:</label>
                </div>
                <div class="col-lg-10 col-md-9">
                    <div class="card">
                        <div class="starterWrapper d-inline-block">
                            <div class="d-inline-block">
                                <h4>Treatment:</h4>
                            </div>
                            <div class="d-inline-block pull-right">
                                <button class="btn btn-pill btn-primary" type="button" id="addMenuSpaBtn">
                                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>

                        <!-- Modal -->
                        <div class="modal fade addSpaMenuModal" tabindex="-1" role="dialog"
                            aria-labelledby="addSpaMenuModal" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <h4>Add menu:</h4>
                                        <hr />
                                        <form id="addSpaMenuForm" method="post"
                                            action="<?php echo e(route('cms.spa-treatments.store')); ?>"
                                            enctype="multipart/form-data">
                                            <?php echo csrf_field(); ?>
                                            <?php echo method_field('post'); ?>
                                            <input type="hidden" name="spa_id" value="<?php echo e($spa->id); ?>">

                                            <div class="form-group">
                                                <label for="name">Name</label>
                                                <input class="form-control" id="name" name="name" type="text">
                                            </div>
                                            <div class="form-group">
                                                <label for="description">Description</label>
                                                <textarea class="form-control" id="description" name="description"
                                                    rows="4" placeholder=""></textarea>
                                            </div>
                                            <div class="form-group">
                                                
                                                <div class="alert alert-danger alert-dismissible fade show add_file_error messageAlert"
                                                    role="alert" style="display: none;">
                                                    <p></p>
                                                    <button type="button" class="close" data-dismiss="alert"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <label for="image">Image</label>
                                                <input type="file" name="image" id="image"
                                                    class="form-control spaAddMenuImage">
                                            </div>
                                            <p class="text-muted mt-3">maximum size :4 MB</p>
                                            <div class="form-group">
                                                <label for="duration">Duration</label>
                                                <div class="input-group">
                                                    <input class="form-control" id="duration" name="duration"
                                                        type="number" min="5" step="5" value="5">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">Minute(s)</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="price">Price</label>
                                                <div class="input-prepend input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">$</span>
                                                    </div>
                                                    <input class="form-control" id="price" name="price" size="16"
                                                        type="number" step="0.01" min="0">
                                                </div>
                                            </div>
                                        </form>
                                        <button class="btn btn-pill btn-block btn-secondary btn-lg" type="button"
                                            id="storeNewSpaMenuBtn">Add</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade editSpaMenuModal" tabindex="-1" role="dialog"
                            aria-labelledby="editSpaMenuModal" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <h4>Edit menu:</h4>
                                        <hr />
                                        <form id="editSpaMenuForm" method="post"
                                            action="<?php echo e(route('cms.spa-treatments.update', ['spa_treatment' => 0])); ?>"
                                            enctype="multipart/form-data">
                                            <?php echo csrf_field(); ?>
                                            <?php echo method_field('put'); ?>
                                            <input type="hidden" id="spa_treatment_id" name="spa_treatment_id"
                                                value="0">
                                            <input type="hidden" id="spa_id" name="spa_id" value="<?php echo e($spa->id); ?>">

                                            <div class="form-group">
                                                <label for="nameUpdate">Name</label>
                                                <input class="form-control" id="nameUpdate" name="nameUpdate"
                                                    type="text">
                                            </div>
                                            <div class="form-group">
                                                <label for="descriptionUpdate">Description</label>
                                                <textarea class="form-control" id="descriptionUpdate"
                                                    name="descriptionUpdate" rows="4" placeholder=""></textarea>
                                            </div>
                                            <div class="form-group">
                                                <div class="alert alert-danger alert-dismissible fade show edit_file_error messageAlert"
                                                    role="alert" style="display: none;">
                                                    <p></p>
                                                    <button type="button" class="close" data-dismiss="alert"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <label for="image">Image</label>
                                                <input type="file" name="image" id="image" accept="image/*"
                                                    class="form-control spaEditMenuImage">
                                            </div>
                                            <p class="text-muted mt-3">maximum size :4 MB</p>
                                            <div class="form-group">
                                                <label for="durationUpdate">Duration</label>
                                                <div class="input-group">
                                                    <input class="form-control" id="durationUpdate"
                                                        name="durationUpdate" type="number" min="5" step="5" value="5">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">Minute(s)</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="priceUpdate">Price</label>
                                                <div class="input-prepend input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">$</span>
                                                    </div>
                                                    <input class="form-control" id="priceUpdate" name="priceUpdate"
                                                        size="16" type="number" step="0.01" min="0">
                                                </div>
                                            </div>
                                        </form>
                                        <form id="deleteSpaMenuForm" method="post"
                                            action="<?php echo e(route('cms.spa-treatments.destroy', ['spa' => 0])); ?>"
                                            enctype="multipart/form-data">
                                            <?php echo csrf_field(); ?>
                                            <?php echo method_field('delete'); ?>
                                            <input type="hidden" id="spa_treatment_id" name="spa_treatment_id"
                                                value="0">
                                            <input type="hidden" id="spa_id" name="spa_id" value="<?php echo e($spa->id); ?>">
                                        </form>

                                        <div class="row mt-4">
                                            <div class="col-6">
                                                <button class="btn btn-pill btn-block btn-danger btn-lg" type="button"
                                                    id="deleteSpaMenuBtn">Delete</button>
                                            </div>
                                            <div class="col-6">
                                                <button
                                                    class="btn btn-pill btn-block btn-secondary btn-lg spaMenuUpdateBtn"
                                                    type="button" id="updateSpaMenuBtn">Update</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="menusTableWrapper">
                            <table class="table table-responsive-sm menusTable">
                                <tbody>
                                    <?php $__currentLoopData = $spa->treatments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $treatment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr data-treatment_id="<?php echo e($treatment->id); ?>" class="menuTreatmentTr">
                                        <td><?php echo e($treatment->name); ?> <span
                                                class="pull-right">$<?php echo e($treatment->price); ?></span></td>
                                    </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade showDetailsSpaRequestModal" tabindex="-1" role="dialog"
        aria-labelledby="showDetailsSpaRequestModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <form id="updateSpaRequestForm" method="post"
                        action="<?php echo e(route('cms.spa-bookings.update', ['spa_booking' => 0])); ?>"
                        enctype="multipart/form-data">
                        <?php echo csrf_field(); ?>
                        <?php echo method_field('put'); ?>
                        <input type="hidden" name="spa_booking_id" id="spa_booking_id" value="0">
                        <input type="hidden" name="status" id="status" value="0">
                    </form>
                    <h4 class="mb-4" id="roomNoText">Room 101 - Booking details:</h4>
                    <div class="spaBookingDetailsUl">
                        <ul>
                            <li>
                                Type: Reservation
                            </li>
                            <li>
                                5 people
                            </li>
                            <li>
                                Booking date 21 August 2019 12:12
                            </li>
                        </ul>
                    </div>
                    <table class="table table-responsive-sm" id="spaBookingDetailsTable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Treatment</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>Lux 1</td>
                            </tr>
                        </tbody>
                    </table>

                    <div class="row mt-4">
                        <div class="col-6">
                            <button class="btn btn-pill btn-block btn-secondary btn-lg" type="button"
                                id="declineSpaBookingRequestBtn">Reject</button>
                        </div>
                        <div class="col-6">
                            <button class="btn btn-pill btn-block btn-success btn-lg" type="button"
                                id="acceptSpaBookingRequestBtn">Accept</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    <div class="col-12 bookingSpaCalendarWrapper mb-5">
        <h3>Bookings</h3>
        <div class="legendsWrapper">
            <div class="legendWrapper">
                <div class="color grey">
                </div>
                <div class="textWrapper">
                    <p>Requested</p>
                </div>
            </div>
            <div class="legendWrapper">
                <div class="color blue">
                </div>
                <div class="textWrapper">
                    <p>Accepted</p>
                </div>
            </div>
            <div class="legendWrapper">
                <div class="color red">
                </div>
                <div class="textWrapper">
                    <p>Rejected</p>
                </div>
            </div>
        </div>
        <div id='bookingSpaCalendar'></div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script>
    const url4 = '<?php echo route('ajax.cms.spa-treatments.index'); ?>';
        const url5 = '<?php echo route('ajax.cms.spa-bookings.index'); ?>';
        const url6 = '<?php echo route('ajax.cms.restaurant-reservations.index'); ?>';
        const id1 = '<?php echo $spa->id; ?>';
</script>
<script src="<?php echo e(asset('js/custom/cms/spa.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('cms.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\XAMP\htdocs\sevr_hotel\resources\views/cms/spas/show.blade.php ENDPATH**/ ?>