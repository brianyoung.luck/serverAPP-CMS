const togglePassword = $(".toggle-password");
const password = $("#password");
const togglePasswordConfirm = $(".toggle-password-confirm");
const passwordConfirm = $("#password_confirmation");
const buttonRegister = $("#buttonRegister");
const registerForm = $("#registerForm");

const username = $("#username");
const code = $("#code");
const name = $("#name");
const description = $("#description");
const email = $("#email");

$(document).ready(function() {
  buttonRegister.click(register);
  togglePassword.click(function(e) {
    changeIconPassword("password");
  });
  togglePasswordConfirm.click(function(e) {
    changeIconPassword("password_confirm");
  });

  code.keyup(function(e) {
    checkFormValidation(e);
  });
  username.keyup(function(e) {
    checkFormValidation(e);
  });
  name.keyup(function(e) {
    checkFormValidation(e);
  });
  description.keyup(function(e) {
    checkFormValidation(e);
  });
  email.keyup(function(e) {
    checkFormValidation(e);
  });
  password.keyup(function(e) {
    checkFormValidation(e);
  });
  passwordConfirm.keyup(function(e) {
    checkFormValidation(e);
  });
});

function register() {
  var type = buttonRegister.attr("data-type");
  if (type == "paypal") {
    Swal.fire({
      title: "Are you sure?",
      text:
        "Subscription is done with Paypal, Please note: Paypal will show you price of the monthly subscription but you will not be charged until your trial period ends If you are having issues with Paypal or do not have Paypal, please contact us at contact@servrhotels.com to arrange for payment via bank transfer.",
      type: "question",
      showCancelButton: true,
      confirmButtonColor: "#20a8d8",
      cancelButtonColor: cancel_button_color,
      confirmButtonText: "Ok",
      reverseButtons: true,
    }).then((result) => {
      if (result.value && result.value === true) {
        // Show loading
        showLoadingButton(true);
        // Submit
        registerForm.submit();
      }
    });
  } else {
    // Show loading
    showLoadingButton(true);
    // Submit
    registerForm.submit();
  }
  // // Show loading
  // showLoadingButton(true);

  // // Submit
  // registerForm.submit();
}

function showLoadingButton(is_show = true) {
  if (is_show === true) {
    buttonRegister
      .html(`<i class="fa fa-refresh fa-spin fa-fw"></i> Please wait...`)
      .attr("disabled", "disabled");
  } else {
    buttonRegister.html(`Login`).attr("disabled", false);
  }
}

function changeIconPassword(type) {
  if (type === "password") {
    if (password.attr("type") === "text") {
      password.attr("type", "password");
      togglePassword.addClass("fa-eye-slash");
      togglePassword.removeClass("fa-eye");
    } else if (password.attr("type") === "password") {
      password.attr("type", "text");
      togglePassword.removeClass("fa-eye-slash");
      togglePassword.addClass("fa-eye");
    }
  } else {
    if (passwordConfirm.attr("type") === "text") {
      passwordConfirm.attr("type", "password");
      togglePasswordConfirm.addClass("fa-eye-slash");
      togglePasswordConfirm.removeClass("fa-eye");
    } else if (passwordConfirm.attr("type") === "password") {
      passwordConfirm.attr("type", "text");
      togglePasswordConfirm.removeClass("fa-eye-slash");
      togglePasswordConfirm.addClass("fa-eye");
    }
  }
}

function isAllFormFilled() {
  let current = 0;
  const complete = 7;

  if (code.val() !== "") {
    current += 1;
  }
  if (username.val() !== "") {
    current += 1;
  }
  if (name.val() !== "") {
    current += 1;
  }
  if (description.val() !== "") {
    current += 1;
  }
  if (email.val() !== "") {
    current += 1;
  }
  if (password.val() !== "") {
    current += 1;
  }
  if (passwordConfirm.val() !== "") {
    current += 1;
  }

  return current === complete;
}

function checkFormValidation(e) {
  buttonRegister.prop("disabled", false);
  if (e.which === 13) {
    buttonRegister.click();
  }
  // if (isAllFormFilled() === true) {
  //     buttonRegister.prop('disabled', false);
  //     if(e.which === 13) {
  //         buttonRegister.click();
  //     }
  // } else {
  //     buttonRegister.prop('disabled', true);
  // }
}
