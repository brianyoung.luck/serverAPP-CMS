<?php

namespace App\Services;
use App\Models\HotelBooking;
use Carbon\Carbon;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthService
{
    public function issueToken(HotelBooking $booking)
    {
        return JWTAuth::customClaims([
            'exp' => Carbon::parse($booking->departure_date)->addHour()->setTime(23, 59, 59)->timestamp
        ])->fromUser($booking);
    }

    public function guestGetMe(HotelBooking $activeBooking)
    {
        return [
            'reference' => $activeBooking->reference,
            'arrival_date' => $activeBooking->arrival_date,
            'departure_date' => $activeBooking->departure_date,
            'cardholder_name' => $activeBooking->cardholder_name,
            'card_expiry_date' => $activeBooking->card_expiry_date->format('m/y'),
            'card_address' => $activeBooking->card_address,
            'phone_number' => $activeBooking->phone_number,
            'passport_photos' => $activeBooking->passports->pluck('url'),
            'sendbird_user_id' => $activeBooking->sendbird_user_id,
            'sendbird_access_token' => $activeBooking->sendbird_access_token,
            'sendbird_channel_url' => $activeBooking->sendbird_channel_url,
            'checkout_time' => $activeBooking->hotel->checkout_time,
            'late_checkout_time' => $activeBooking->hotel->late_checkout_time,
            'late_checkout_status' => $activeBooking->late_check_out,
            'status' => $activeBooking->status,
            'room_number' => $activeBooking->room_number
        ];
    }
}