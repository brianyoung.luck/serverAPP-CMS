<?php $__env->startSection('title', __('Login Admin')); ?>

<?php $__env->startSection('content'); ?>
    <div id="loginWrapper" class="row justify-content-center">
        <div class="col-xl-5 col-lg-6 col-md-8 col-sm-10">
            <div class="card-group">
                <div class="card p-4">
                    <div class="card-body">
                        <div class="admin-panel-text">
                            <h2><?php echo e(env('APP_NAME')); ?></h2>
                            <h3><?php echo e(__('Admin panel')); ?></h3>
                        </div>
                        <?php echo $__env->make('cms.layouts.flash-message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                        <form id="loginForm" class="form-horizontal" action="<?php echo e(route('admin.do-login')); ?>" method="post">
                            <?php echo csrf_field(); ?>
                            <?php echo method_field('post'); ?>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <input class="form-control <?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" id="email" type="email" name="email" placeholder="Email">
                                        <?php if($errors->has('email')): ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('email')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <input class="form-control <?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>" id="password" type="password" name="password" placeholder="Password">
                                        <span class="fa fa-fw fa-eye-slash field-icon toggle-password"></span>
                                        <?php if($errors->has('password')): ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('password')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <div class="row">
                            <div class="col-12">
                                <button class="btn btn-pill btn-block btn-primary" type="button" id="buttonLogin" disabled><?php echo e(__('Login')); ?></button>
                            </div>
                        </div>
                        <div id="forgotPasswordLinkWrapper">
                            <a href="<?php echo e(route('admin.forgot-password.index')); ?>"><?php echo e(__('Forgot password?')); ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script>
        const url1 = '<?php echo route('cms.dashboard.index'); ?>';
    </script>
    <script src="<?php echo e(asset('js/custom/cms/login.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.auth', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Syed Hasnain Haider\Desktop\backup\resources\views/admin/auth/login/index.blade.php ENDPATH**/ ?>