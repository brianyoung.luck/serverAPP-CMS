<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <title><?php echo e(__('Register hotel')); ?> - <?php echo e(env('APP_NAME')); ?></title>

    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/custom-cms.css')); ?>" rel="stylesheet">
</head>
<body class="register-hotel-wrapper">
<div class="container-fluid p-0">
   <div class="header-nav-wrapper">
       <div class="icon-nav">
           <h1>Servr</h1>
       </div>
   </div>
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-5 content-left">
                <div class="card card-text">
                    <div class="card-body">
                        <p>
                            Start your trial and join free for a month now <i class="icon-arrow-right icons"></i>
                        </p>
                    </div>
                </div>
                <?php if($no_trial): ?>
                    <div class="alert alert-danger">You are not eligble for trial.</div>
                <?php endif; ?>
                <div class="card card-register">
                    <div class="card-body">
                        <form id="registerForm" class="form-horizontal" action="<?php echo e(route('cms.hotels.store')); ?>" method="post">
                            <?php echo csrf_field(); ?>
                            <?php echo method_field('post'); ?>

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label class="col-form-label" for="name">Hotel name:</label>
                                    <div class="input-group">
                                        <input class="form-control <?php echo e($errors->has('name') ? ' is-invalid' : ''); ?>" id="name" type="text" name="name" value="<?php echo e(old('name') ? old('name') : ''); ?>">
                                        <?php if($errors->has('name')): ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('name')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label class="col-form-label" for="phone_number">Hotel phone number:</label>
                                    <div class="input-group">
                                        <input class="form-control <?php echo e($errors->has('phone_number') ? ' is-invalid' : ''); ?>" id="phone_number" type="text" name="phone_number" value="<?php echo e(old('phone_number') ? old('phone_number') : ''); ?>">
                                        <?php if($errors->has('phone_number')): ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('phone_number')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label class="col-form-label" for="email">Hotel email:</label>
                                    <div class="input-group">
                                        <input class="form-control <?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" id="email" type="email" name="email" placeholder="To be used to login" value="<?php echo e(old('email') ? old('email') : ''); ?>">
                                        <?php if($errors->has('email')): ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('email')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label class="col-form-label" for="password">Password:</label>
                                    <div class="input-group">
                                        <input class="form-control <?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>" id="password" type="password" name="password">
                                        <span class="fa fa-fw fa-eye-slash field-icon toggle-password"></span>
                                        <?php if($errors->has('password')): ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('password')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label class="col-form-label" for="email">Confirm Password:</label>
                                    <div class="input-group">
                                        <input class="form-control <?php echo e($errors->has('password_confirmation') ? ' is-invalid' : ''); ?>" id="password_confirmation" type="password" name="password_confirmation">
                                        <span class="fa fa-fw fa-eye-slash field-icon toggle-password-confirm"></span>
                                        <?php if($errors->has('password_confirm')): ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('password_confirmation')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label class="col-form-label" for="email">Hotel code:</label>
                                    <div class="input-group">
                                        <input class="form-control <?php echo e($errors->has('code') ? ' is-invalid' : ''); ?>" id="code" type="text" name="code" placeholder="To be used by guest to login" style="text-transform:uppercase" value="<?php echo e(old('code') ? old('code') : ''); ?>">
                                        <?php if($errors->has('code')): ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('code')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <button class="btn btn-pill btn-block btn-primary" type="button" id="buttonRegister"><?php echo e(__('Register')); ?></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-7 content-right">
                <div class="image-wrapper">
                    <img src="<?php echo e(asset('images/register_hotel/laptop_hd.png')); ?>" alt="device">
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo e(asset('js/app.js')); ?>"></script>
<script>
    const url1 = '<?php echo route('cms.dashboard.index'); ?>';
</script>
<script src="<?php echo e(asset('js/custom/cms/hotel.js')); ?>"></script></body>
</html><?php /**PATH E:\Xammp\htdocs\server_hotels\resources\views/cms/hotel/create_v2.blade.php ENDPATH**/ ?>