<?php

namespace App\Http\Resources\Customer;

use Illuminate\Http\Resources\Json\JsonResource;

class ConciergeServiceRequestResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'services' => ConciergeServiceResource::collection($this->whenLoaded('services')),
            'datetime' => $this->created_at,
            'status' => $this->status,
        ];
    }
}
