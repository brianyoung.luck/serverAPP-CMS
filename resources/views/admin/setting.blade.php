@extends('admin.layouts.app')

@section('title', __('Staff'))

@section('content')

    <div id="dashboardWrapper">
    	<div class="row">
    		<div class="col-12">
    			<div class="card-header">{{ __('System Wide Settings') }}</div>
                 <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <h3>System Wide Setting</h3>
                            <form method="post" action="{{route('settings.store')}}">
                                @csrf()
                                @method('post')

                                @foreach($settings as $setting)
                                    <label for="{{$setting->id}}">{{$labels[$setting->key]}}</label>
                                    <input type="text" class="form-control" name="{{$setting->id}}" value="{{$setting->value}}">
                                @endforeach

                                
                                <input type="submit" name="submit" value="Save" class="btn btn-info pull-right mt-3">
                            </form>
                        </div>
                        
                    </div>
                    
               
    	</div>
    </div>
@stop

@section('js')

@stop