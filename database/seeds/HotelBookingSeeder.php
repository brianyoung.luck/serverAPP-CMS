<?php

use App\Models\Hotel;
use App\Models\HotelBooking;
use App\Models\User;
use Illuminate\Database\Seeder;

class HotelBookingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $hotels = Hotel::all();
        foreach ($hotels as $hotel)
        {
            factory(HotelBooking::class)->create([
                'hotel_id' => $hotel->id,
            ]);
        }
    }
}
