@extends('cms.layouts.app')

@section('title', __('Checkout'))

@section('content')
<div id="checkoutsWrapper">
    @inject('helperService', 'App\Services\HelperService')

    <div class="row">
        <h4>
            Checkout time: {{ $helperService::dateTimeFormat(auth('hotel')->user()->checkout_time, 'g:i A') }}
            <button class="btn btn-shadow ml-2" id="changeCheckoutTimeBtn">Change</button>
        </h4>
    </div>
    <div class="row">
        <h4>
            Late checkout time: {{ $helperService::dateTimeFormat(auth('hotel')->user()->late_checkout_time, 'g:i A') }}
            <button class="btn btn-shadow ml-2" id="changeLateCheckoutTimeBtn">Change</button>
        </h4>
    </div>
    <form id="editHotelDetailsForm" method="post"
        action="{{ route('cms.hotels.update', ['id' => auth('hotel')->user()->id]) }}" enctype="multipart/form-data">
        @csrf()
        @method('put')
        <input type="hidden" name="checkout_time" id="checkout_time" value="{{ auth('hotel')->user()->checkout_time }}">
        <input type="hidden" name="late_checkout_time" id="late_checkout_time"
            value="{{ auth('hotel')->user()->late_checkout_time }}">
    </form>
    <div class="row">
        <h4>
            Guests:
        </h4>
        <form id="checkoutHotelBookingForm" method="post"
            action="{{ route('cms.checkouts.update', ['hotel-booking' => 0]) }}">
            @method('put')
            @csrf()
            <input type="hidden" name="hotelBookingId" id="hotelBookingIdCheckout" value="0">
            <input type="hidden" name="updateType" id="updateType" value="checkout">
        </form>
        <form id="lateCheckoutHotelBookingForm" method="post"
            action="{{ route('cms.checkouts.update', ['hotel-booking' => 0]) }}">
            @method('put')
            @csrf()
            <input type="hidden" name="hotelBookingId" id="hotelBookingIdLateCheckout" value="0">
            <input type="hidden" name="updateType" id="updateType" value="lateCheckout">
            <input type="hidden" name="statusLateCheckout" id="statusLateCheckout" value="accepted">
        </form>
        <div class="col-12 p-2">
            <div class="card">
                <table class="table table-responsive-sm table-striped" id="guestTable">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Booking ref#</th>
                            <th>Price ref#</th>
                            <th>Arrival date</th>
                            <th>Departure date</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
                <!-- Modal -->
                <div class="modal fade guestProfileModal" id="guestProfileModal" tabindex="-1" role="dialog"
                    aria-labelledby="guestProfileModal" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <h4>User profile:</h4>
                                <p>Passport entries</p>
                                <div class="passportEntriesWrapper" id="guestPassportEntriesWrapper">
                                    <div class="passportEntryWrapper">
                                        <a href="#" target="_blank">
                                            <img src="" alt="Passport photo">
                                        </a>
                                    </div>
                                </div>
                                <p class="msgText text-white mt-3 text-center shadow"></p>
                                <div class="row userProfileDetails">
                                    @inject('helperService', 'App\Services\HelperService')
                                    <div class="col-12 col-md-6">
                                        <p>
                                            Name: <span id="guestName" class="copyText"><i
                                                    class="fa fa-refresh fa-spin fa-fw"></i></span><br />
                                            Date of Birth: -<br />
                                            Booking reference: <span id="guestReference" class="copyText"><i
                                                    class="fa fa-refresh fa-spin fa-fw"></i></span><br />
                                            Arrival date:<span id="guestArrivalDate" class="copyText"><i
                                                    class="fa fa-refresh fa-spin fa-fw"></i></span><br />
                                            Departure date: <span id="guestDepartureDate" class="copyText"><i
                                                    class="fa fa-refresh fa-spin fa-fw"></i></span><br />
                                        </p>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <p>
                                            Credit card number: <span id="guestCardNumber" class="copyText"><i
                                                    class="fa fa-refresh fa-spin fa-fw"></i></span><br />
                                            Exp. date: <span id="guestCardExpiryDate" class="copyText"><i
                                                    class="fa fa-refresh fa-spin fa-fw"></i></span><br />
                                            Name on card: <span id="guestCardholderName" class="copyText"><i
                                                    class="fa fa-refresh fa-spin fa-fw"></i></span><br />
                                            Address: <span id="guestCardAddress" class="copyText"><i
                                                    class="fa fa-refresh fa-spin fa-fw"></i></span><br />
                                        </p>
                                        <div>
                                            <button class="btn btn-pill btn-warning lateCheckoutBtn d-inline"
                                                data-id="0" type="button" aria-pressed="true">Late</button>
                                            <button class="btn btn-pill btn-success checkoutBtn" data-id="0"
                                                type="button" aria-pressed="true">Checkout</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-3 p-3 d-none">
                                    <div class="col">
                                        <div class="sigWrapper">
                                            <h4 style="color: khaki;">Customer Signature Optional</h4>
                                            <canvas style="border: 3px dotted khaki;"></canvas>

                                        </div>
                                        <button class="btn btn-pill btn-primary" id="clearSig" data-id="0" type="button"
                                            aria-pressed="true">Clear</button>

                                    </div>
                                </div>
                                <div class="row mt-3 p-3 bookingBillWrapper">
                                    <h4>Bill details</h4>
                                    <table class="table table-responsive-sm table-striped bookingBillTable">
                                        <thead>
                                            <tr>
                                                <th>Service</th>
                                                <th>From</th>
                                                <th>Price</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <h5 class="totalBillPrice w-100 text-right">Total price 123</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- Dishes list modal  --}}
                    {{-- <div class="modal fade DishesListModal" id="DishesListModal" tabindex="-1" role="dialog" aria-labelledby="DishesListModal" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <div class="row mt-3 p-3 bookingBillWrapper">
                                        <h4>Dishes Detail</h4>
                                        <table class="table table-responsive-sm table-striped DishesListModal">
                                            <thead>
                </div>
                {{-- Dishes list modal  --}}
                    <div class="modal fade DishesListModal" id="DishesListModal" tabindex="-1" role="dialog"
                        aria-labelledby="DishesListModal" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <div class="row mt-3 p-3 bookingBillWrapper">
                                        <h4>Dishes Detail</h4>
                                        <table class="table table-responsive-sm table-striped DishesListModal">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Image</th>
                                                    <th>Name</th>
                                                    <th>Price</th>
                                                    <th>Qty</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            </tbody>
                                        </table>

                                        <h5 class=" w-100 text-right">Total price 123</h5>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
    <input type="hidden" name="getHotelId" value="0" data-id="0" class="getHotelId" id="getHotelId">
</div>
@stop

@section('js')
<script>
    const url1 = '{!! route('data-tables.cms.guests') !!}';
        const url2 = '{!! route('cms.guests.index') !!}';
        const url3 = '{!! route('data-tables.cms.guests') !!}';
        const url4 = '{!! route('ajax.cms.guests.index') !!}';
        const val1 = '{!! auth('hotel')->user()->checkout_time !!}';
        const val2 = '{!! auth('hotel')->user()->late_checkout_time !!}';
        const printUrl = '{!! url('ajax/guests/printbill/') !!}';
        const redirectType='checkouts';
</script>
<script src="{{ asset('js/custom/cms/checkout.js') }}"></script>
<script>
    async function openItemPopUp(type) {
        const hotelBookingId=$('#getHotelId').attr('data-id');
        const guestDetails =await getGuestDetails(hotelBookingId);
        const bookingBill=guestDetails.bookingBill;
        console.log(bookingBill);
        var html='';
        for (const item of bookingBill.items) {
   
        if (item.billable_type === "App\\Models\\RestaurantRoomService") {
            $('.itemDeatil'+item.id).html('');
        for(const dish of item.billable.dishes){
            html+=`<div class="col-md-6">
            <span>${dish.name}</span>
        </div>
        <div class="col-md-6">
            <span> ${dish.pivot.price*dish.pivot.qty}</span>
        </div>`;
        // $('#restaurantRoomService').collapse();
        }
   
        $('.itemDeatil'+item.id).html(html);
        } else if (item.billable_type === "App\\Models\\SpaBooking") {
            $('.itemDeatil'+item.id).html('');
        for(const treatment of item.billable.treatments){
      
        $('.itemDeatil'+item.id).append(`
        <div class="col-md-6">
            <span>${treatment.name}</span>
        </div>
        <div class="col-md-6">
            <span>${treatment.pivot.price}</span>
        </div>`);
        // $('#spaRoomService' +item.id).collapse();
        }
        }
        // i++;
        }
        
        $(type).addClass("show");
        }

        async function getGuestDetails(hotelBookingId) {
        let promise = new Promise(function(resolve, reject) {
        $.ajax({
        headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        type: "GET",
        url: url4 + "/" + hotelBookingId,
        contentType: "application/json",
        dataType: "json",
        })
        .done(function(response) {
        resolve(response);
        })
        .fail(function(error) {
        console.log(error);
        reject();
        });
        });
        
        try {
        return await promise;
        } catch (err) {
        return {
        status: false,
        error: err,
        };
        }
        }
</script>
@stop