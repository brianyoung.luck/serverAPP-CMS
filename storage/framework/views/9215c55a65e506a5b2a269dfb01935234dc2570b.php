<?php $__env->startSection('title', __('Forgot Password')); ?>

<?php $__env->startSection('content'); ?>
    <div id="forgotPasswordWrapper" class="row justify-content-center">
        <div class="col-xl-5 col-lg-6 col-md-8 col-sm-10">
            <div class="card-group">
                <div class="card p-4">
                    <div class="card-body">
                        <div class="admin-panel-text">
                            <h2><?php echo e(env('APP_NAME')); ?></h2>
                            <h3><?php echo e(__('Forgot Password')); ?></h3>
                        </div>
                        <?php echo $__env->make('cms.layouts.flash-message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                        <form id="forgotPasswordForm" class="form-horizontal" action="<?php echo e(route('cms.forgot-password.action')); ?>" method="post">
                            <?php echo csrf_field(); ?>
                            <?php echo method_field('post'); ?>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <input class="form-control <?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" id="email" type="email" name="email" placeholder="Email">
                                        <?php if($errors->has('email')): ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('email')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <div class="row">
                            <div class="col-12">
                                <button class="btn btn-pill btn-block btn-primary" type="button" id="buttonForgot" disabled><?php echo e(__('Send reset link')); ?></button>
                            </div>
                        </div>
                        <div id="loginLinkWrapper">
                            <a href="<?php echo e(route('cms.login.index')); ?>">
                                <i class="icons cui-arrow-left"></i>
                                <?php echo e(__('Login')); ?>

                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script>

    </script>
    <script src="<?php echo e(asset('js/custom/cms/forgot-password.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('cms.layouts.auth', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/servrhotels.com/resources/views/cms/forgot-password/index.blade.php ENDPATH**/ ?>