<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <title><?php echo e(__('Subscribe hotel')); ?> - <?php echo e(env('APP_NAME')); ?></title>

    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/custom-cms.css')); ?>" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo e(asset('horizontal-select/src/ui-choose.css')); ?>">
</head>
<body class="register-hotel-wrapper">
<div class="container-fluid p-0">
   <div class="header-nav-wrapper">
       <div class="icon-nav">
           <h1>Servr</h1>
       </div>
   </div>
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-5 content-left">
                <div class="card card-text">
                    <div class="card-body">
                        <p>
                            Start your trial and join free for a month now <i class="icon-arrow-right icons"></i>
                        </p>
                    </div>
                </div>

                <div class="card card-register">
                    <div class="card-body">
                        <form id="registerForm" class="form-horizontal" action="<?php echo e(route('subscribe.hotels.search')); ?>" method="post">
                            <?php echo csrf_field(); ?>
                            <?php echo method_field('post'); ?>

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label class="col-form-label" for="email">Hotel code:</label>
                                    <div class="input-group">
                                        <input class="form-control <?php if(session('not_found')): ?> is-invalid <?php endif; ?> <?php echo e($errors->has('code') ? ' is-invalid' : ''); ?>" id="code" type="text" name="code" placeholder="Hotel Code" style="text-transform:uppercase" value="<?php echo e(old('code') ? old('code') : ''); ?>">
                                        <?php if($errors->has('code')): ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('code')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                        <?php if(session('not_found')): ?>
                                          <span class="invalid-feedback" role="alert">
                                              <strong><?php echo e(session('not_found')); ?></strong>
                                          </span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <button class="btn btn-pill btn-block btn-primary" type="button" id="buttonRegister"><?php echo e(__('Submit')); ?></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-7 content-right">
                <div class="image-wrapper">
                    <img src="<?php echo e(asset('images/register_hotel/laptop_hd.png')); ?>" alt="device">
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo e(asset('js/app.js')); ?>"></script>
<script>
    const url1 = '<?php echo route('cms.dashboard.index'); ?>';
</script>
<script src="<?php echo e(asset('js/custom/cms/hotel.js')); ?>"></script></body>
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="<?php echo e(asset('horizontal-select/src/ui-choose.js')); ?>"></script>
<script type="text/javascript">
$('.ui-choose').ui_choose();
</script>
</html>
<?php /**PATH /var/www/servrhotels.com/resources/views/cms/paypal/subscribe-from-subscribe.blade.php ENDPATH**/ ?>