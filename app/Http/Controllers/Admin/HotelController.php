<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StoreHotelRequest;
use App\Models\Hotel;
use App\Models\HotelFeature;
use App\Models\PassportPhoto;
use App\Models\Spa;
use App\Services\HotelBookingService;
use App\Services\HotelService;
use App\Services\SendbirdService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Redirect;

class HotelController extends Controller
{
    private $hotelService;

    public function __construct(HotelService $hotelService)
    {
        $this->hotelService = $hotelService;
    }

    public function index()
    {
        return view('admin.hotel.index');
    }

    /**
     * Process data tables ajax request.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function indexDataTable(Request $request)
    {
        // Create query
        $query = Hotel::query();

        return Datatables::of($query)
            ->addIndexColumn()
            ->make(true);
    }

    public function show(Hotel $hotel)
    {
        $hotel->load('hotelFeature');
        if (!$hotel->hotelFeature) {
            // Create hotel feature
            HotelFeature::create([
                'hotel_id' => $hotel->id,
                'is_check_in_enabled' => true,
                'is_check_out_enabled' => true,
                'is_spa_enabled' => true,
                'is_restaurant_enabled' => true,
                'is_concierge_enabled' => true,
                'is_cleaning_enabled' => true,
                'spa_treatment' => true,
                'spa_room_service' => true,
                'is_experience' => true
            ]);
            $hotel->load('hotelFeature');

            // Create mobile layout
            MobileLayout::create([
                'hotel_id' => $hotel->id,
                'mobile_hotel_theme_id' => 1,
                'mobile_hotel_icon_id' => 1,
            ]);

            if (!$hotel->sendbird_user_id) {
                $sendbirdService = new SendbirdService();
                $sendbirdHotel = $sendbirdService->createHotelUser($hotel);
                $hotel->sendbird_user_id = $sendbirdHotel->user_id;
                $hotel->sendbird_access_token = $sendbirdHotel->access_token;
                $hotel->save();
            }
        }

        return view('admin.hotel.show', [
            'hotel' => $hotel,
        ]);
    }

    /**
     * @param Request $request
     * @param Hotel $hotel
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function update(Request $request, Hotel $hotel)
    {
        $updateType = $request->get('type', 'renew');
        $updateMonthTime  =$request->get('extendmonthTime');
        switch ($updateType) {
            case 'inactive':
                $hotel->is_active = false;
                $hotel->save();
                return redirect()->route('admin.hotels.show', ['hotel' => $hotel->id])->with('success', 'Inactive success.');

                break;
            default:
                $expiredAt = Carbon::create($hotel->expired_at);

                if ($expiredAt->isPast()) {
                    $hotel->expired_at = now()->addMonth($updateMonthTime);
                } else {
                    $hotel->expired_at = $expiredAt->addMonth($updateMonthTime);
                }
                $hotel->is_active = true;

                $hotel->save();
                return redirect()->route('admin.hotels.show', ['hotel' => $hotel->id])->with('success', 'Renew success.');
                break;
        }
    }

    public function create()
    {
        return view('admin.hotel.create');
    }

    public function store(StoreHotelRequest $request)
    {
        $sendbirdService = new SendbirdService();
        $data = $request->only([
            'code',
            'email',
            'username',
            'password',
            'name',
            'phone_number',
        ]);

        $hotel = null;
        DB::transaction(function () use ($data, &$hotel, $sendbirdService) {
            $hotel = $this->hotelService->store($data);
            $sendbirdHotel = $sendbirdService->createHotelUser($hotel);
            $hotel->sendbird_user_id = $sendbirdHotel->user_id;
            $hotel->sendbird_access_token = $sendbirdHotel->access_token;
            $hotel->save();

            HotelFeature::create([
                'hotel_id' => $hotel->id,
                'is_check_in_enabled' => true,
                'is_check_out_enabled' => true,
                'is_spa_enabled' => true,
                'is_restaurant_enabled' => true,
                'is_concierge_enabled' => true,
                'is_cleaning_enabled' => true,
                'spa_treatment' => true,
                'spa_room_service' => true,
                'is_experience' => true
            ]);

            $spa = Spa::firstOrCreate(
                ['hotel_id' => $hotel->id],
                ['name' => 'Your spa', 'logo_url' => 'https://servrhotels.s3-ap-southeast-1.amazonaws.com/spas/default.png']
            );
        });
        return redirect()->route('admin.hotels.show', ['hotel' => $hotel->id])->with('success', 'Create hotel success.');

        // For demo only
        // Create 1 checkin request
        $bookingData = [
            'hotel_id' => $hotel->id,
            'arrival_date' => now()->addDays(3)->toDateTimeString(),
            'departure_date' => now()->addDays(5)->toDateTimeString(),
            'cardholder_name' => 'John Doe',
            'card_expiry_date' => '2024-12-31',
            'card_number' => '4242424242424242',
            'card_address' => 'Bali',
            'phone_number' => '+628123456789',
            'reference' => null,
        ];
        $hotelBookingService = new HotelBookingService();
        $booking = $hotelBookingService->create($bookingData);
        PassportPhoto::create([
            'hotel_booking_id' => $booking->id,
            'url' => 'https://upload.wikimedia.org/wikipedia/en/4/44/Interpolpassportpage.png',
        ]);
        PassportPhoto::create([
            'hotel_booking_id' => $booking->id,
            'url' => 'https://upload.wikimedia.org/wikipedia/commons/e/e3/New_Zealand_Diplomatic_Passport_Biodata_Page.png',
        ]);
        $sendbirdUser = $sendbirdService->createBookingUser($booking);
        $sendbirdChannel = $sendbirdService->createChatChannel($booking);
        $booking->sendbird_user_id = $sendbirdUser->user_id;
        $booking->sendbird_access_token = $sendbirdUser->access_token;
        $booking->sendbird_channel_url = $sendbirdChannel->channel_url;
        $booking->save();

        return redirect()->route('admin.hotels.show', ['hotel' => $hotel->id])->with('success', 'Create hotel success.');
    }

    public function destroy(Request $request)
    {
        $hotel = Hotel::where('id', $request->id)->delete();
        $id = $request->id;
        
        return Redirect::back()->with('success', 'Staff member added successfully.');
    }
}