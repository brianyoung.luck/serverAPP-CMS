<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ __('Subscribe hotel') }} - {{ env('APP_NAME') }}</title>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom-cms.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('horizontal-select/src/ui-choose.css') }}">
</head>
<body class="register-hotel-wrapper">
<div class="container-fluid p-0">
   <div class="header-nav-wrapper">
       <div class="icon-nav">
           <h1>Servr</h1>
       </div>
   </div>
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-5 content-left">
                <div class="card card-text">
                    <div class="card-body">
                        <p>
                            Start your trial and join free for a month now <i class="icon-arrow-right icons"></i>
                        </p>
                    </div>
                </div>

                <div class="card card-register">
                    <div class="card-body">
                        <form id="registerForm" class="form-horizontal" action="{{ route('cms.hotels.search') }}" method="post">
                            @csrf()
                            @method('post')

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label class="col-form-label" for="email">Hotel code:</label>
                                    <div class="input-group">
                                        <input class="form-control @if (session('not_found')) is-invalid @endif {{ $errors->has('code') ? ' is-invalid' : '' }}" id="code" type="text" name="code" placeholder="Hotel Code" style="text-transform:uppercase" value="{{ old('code') ? old('code') : '' }}">
                                        @if ($errors->has('code'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('code') }}</strong>
                                            </span>
                                        @endif
                                        @if (session('not_found'))
                                          <span class="invalid-feedback" role="alert">
                                              <strong>{{ session('not_found') }}</strong>
                                          </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <button class="btn btn-pill btn-block btn-primary" type="button" id="buttonRegister">{{ __('Submit') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-7 content-right">
                <div class="image-wrapper">
                    <img src="{{ asset('images/register_hotel/laptop_hd.png') }}" alt="device">
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('js/app.js') }}"></script>
<script>
    const url1 = '{!! route('cms.dashboard.index') !!}';
</script>
<script src="{{ asset('js/custom/cms/hotel.js') }}"></script></body>
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="{{ asset('horizontal-select/src/ui-choose.js') }}"></script>
<script type="text/javascript">
$('.ui-choose').ui_choose();
</script>
</html>
