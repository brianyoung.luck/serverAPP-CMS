<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterHotelTableForOptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hotels',function(Blueprint $table){
            $table->float('vat',8,2)->default(0.0);
            $table->float('service_charges',8,2)->default(0.0);
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hotels', function(Blueprint $table){
            $table->dropColumn('vat');
            $table->dropColumn('service_charges');
        }); 
    }
}