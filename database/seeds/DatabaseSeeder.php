<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(MobileHotelThemeSeeder::class);
        $this->call(MobileHotelLayoutSeeder::class);
        $this->call(HotelSeeder::class);
//        $this->call(MobileLayoutSeeder::class);
        $this->call(HotelBookingSeeder::class);
        $this->call(PassportPhotoSeeder::class);
        $this->call(RestaurantSeeder::class);
        $this->call(ConciergeServiceSeeder::class);
        $this->call(SpaSeeder::class);
    }
}