<?php

namespace App\Console\Commands;

use App\Models\HotelBooking;
use App\Services\HotelBookingService;
use Illuminate\Console\Command;

class AutoCheckOut extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'booking:checkout';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Auto Check Out Booking';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $hotelBookingService = new HotelBookingService();

        HotelBooking::where('departure_date', '<=', now()->subHour())->chunk(100, function ($bookings) use ($hotelBookingService) {
            foreach ($bookings as $booking) {
                $hotelBookingService->update($booking, ['status' => 'checked_out']);
            }
        });
    }
}
