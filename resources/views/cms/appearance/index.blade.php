@extends('cms.layouts.app')

@section('title', __('Appearances'))
@section ('styles')
<style>
    .modal-backdrop {
    position: fixed;
    top: 0;
    left: 0;
    z-index: -1!important;
    width: 100vw;
    height: 100vh;
    background-color: #000;
}


</style>
@endsection
@section('content')

<div id="appearancesWrapper">
    <div class="row">
        <div class="col-12 col-sm-12 col-md-6 mainLogoWrapper">
            <div id="mainLogoFormWrapper">
                <div class="form-group row">
                    <label class="col-md-4 col-form-label" for="mainLogoTooltip">
                        Main logo
                        <i class="icon-info icons" id="mainLogoTooltip" data-toggle="tooltip" data-placement="top"
                            title="" data-original-title="This will be shown many times throughout the app"></i>
                    </label>
                    <div class="col-md-8">
                        <p class="alert alert-danger text-center logo_file_error" style="display: none;">Image is
                            too large.</p>
                        <button class="btn btn-shadow" id="uploadFileBtn">Upload file</button>
                        <p class="text-muted mt-3">maximum size :4 MB</p>
                        <form id="editHotelDetailsForm" method="post"
                            action="{{ route('cms.hotels.update', ['id' => auth('hotel')->user()->id]) }}"
                            enctype="multipart/form-data">
                            @csrf()
                            @method('put')
                            <input type="file" name="logo" id="logo" class="d-none" accept="image/*">
                            <input type="hidden" name="name" id="name" value="{{ $hotel->name }}">
                        </form>
                    </div>
                </div>
            </div>

            <div class="previewMainLogo">
                <div class="row">
                    <div class="col-md-4">
                        <label>Preview</label>
                    </div>
                    <div class="col-md-8">
                        <div class="imgWrapper">
                            <img src="{{ $hotel->hotel_logo_ori }}" alt="Logo">
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-12 col-sm-12 col-md-6 hotelNameWrapper">
            <div id="hotelNameFormWrapper">
                <div class="form-group row">
                    <label class="col-md-4 col-form-label" for="disabled-input">
                        Hotel name
                    </label>
                    <div class="col-md-8">
                        <button class="btn btn-shadow" id="editHotelNameBtn">Enter here</button>
                    </div>
                </div>
            </div>
            <div class="previewHotelName">
                <div class="row">
                    <div class="col-md-4">
                        <label>Preview</label>
                    </div>
                    <div class="col-md-8">
                        <h4>{{ $hotel->name }}</h4>
                    </div>
                </div>

            </div>

        </div>
    </div>
    {{-- THEME ROW --}}
    <div class="row">
        <h4>
            Themes
        </h4>
        <form id="editMobileHotelThemeForm" method="post"
            action="{{ route('cms.hotels.update', ['id' => auth('hotel')->user()->id]) }}">
            @method('put')
            @csrf()
            <input type="hidden" name="mobile_hotel_theme_id" id="mobileHotelThemeIdUpdate" value="0">
        </form>
        <div class="col-12 themesWrapper">
            @foreach($mobileHotelThemes as $mobileHotelTheme)
            @if($hotel->mobile_hotel_theme_id == $mobileHotelTheme->id)
            <div class="themeWrapper active" data-id="{{ $mobileHotelTheme->id }}">
                <img src="{{ $mobileHotelTheme->photo_preview_url }}" alt="Theme {{ $mobileHotelTheme->id }}">
            </div>
            @else
            <div class="themeWrapper" data-id="{{ $mobileHotelTheme->id }}">
                <img src="{{ $mobileHotelTheme->photo_preview_url }}" alt="Theme {{ $mobileHotelTheme->id }}">
            </div>
            @endif
            @endforeach
        </div>
    </div>
    {{-- LAYOUT ROW --}}
    <div class="row">

        <h4>
            Layouts
        </h4>
        <button class="btn btn-primary btnAddCustomLayout ml-lg-5">Add Layout</button>
        <form id="editMobileHotelLayoutForm" method="post"
            action="{{ route('cms.hotels.update', ['id' => auth('hotel')->user()->id]) }}">
            @method('put')
            @csrf()
            <input type="hidden" name="mobile_hotel_layout_id" id="mobileHotelLayoutIdUpdate" value="0">
        </form>
        <div class="col-12 layoutsWrapper">
            {{-- <button class="btn btn-danger" style="position: absolute;">Delete</button> --}}
            @foreach($mobileHotelLayouts as $mobileHotelLayout)
            @if($hotel->mobile_hotel_layout_id == $mobileHotelLayout->id)
            <div class="layoutWrapper active" data-id="{{ $mobileHotelLayout->id }}">
                <img src="{{ $mobileHotelLayout->photo_preview_url }}" alt="Layout {{ $mobileHotelLayout->id }}">
            </div>
            @else
            <div class="layoutWrapper" data-id="{{ $mobileHotelLayout->id }}">
                <img src="{{ $mobileHotelLayout->photo_preview_url }}" alt="Layout {{ $mobileHotelLayout->id }}">
            </div>
            @endif
            @endforeach
            <!-- Modal -->
            <div class="modal fade" id="customLayoutModal" tabindex="-1" role="dialog"
                aria-labelledby="customLayoutModal" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="customLayoutModalLabel">Add Layout</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form method="POST" action="{{ route('hotel.addHotelMobileLayout') }}" id="saveLayoutImagesForm"
                            enctype="multipart/form-data">
                            @csrf
                            <div class="modal-body customLayoutModalBody">

                                <div class="form-group">
                                    <p class="alert alert-danger text-center file_error" style="display: none;">Image is
                                        too large.</p>
                                    <label for="checkin_image">Layout Name</label>
                                    <input type="text" class="form-control" placeholder="layout name" id="layout_name"
                                        name="layout_name">
                                </div>
                                <div class="form-group">
                                    <label for="checkin_image">Checkin Image</label>
                                    <input type="file" class="form-control" data-status="false" id="checkin_image"
                                        name="checkin_image" onchange="validateImageSize(this)"
                                        aria-describedby="emailHelp">
                                    <span class="text-danger checkin_image_error" style="display: none;">Checkin image
                                        is large.</span>
                                </div>
                                <div class="form-group">
                                    <label for="checkout_image">Checkout Image</label>
                                    <input type="file" class="form-control" data-status="false" id="checkout_image"
                                        name="checkout_image" onchange="validateImageSize(this)"
                                        aria-describedby="emailHelp">
                                    <span class="text-danger checkout_image_error" style="display: none;">Checkout image
                                        is large.</span>
                                </div>
                                <div class="form-group">
                                    <label for="checkin_image">Restaurant Image</label>
                                    <input type="file" class="form-control" data-status="false" id="restaurant_image"
                                        onchange="validateImageSize(this)" name="restaurant_image"
                                        aria-describedby="emailHelp">
                                    <span class="text-danger restaurant_image_error" style="display: none;">Restaurant
                                        image is large.</span>
                                </div>
                                <div class="form-group">
                                    <label for="checkin_image">Spa Image</label>
                                    <input type="file" class="form-control" id="spa_image" data-status="false"
                                        name="spa_image" onchange="validateImageSize(this)"
                                        aria-describedby="emailHelp">
                                    <span class="text-danger spa_image_error" style="display: none;">Spa image is
                                        large.</span>
                                </div>
                                <div class="form-group">
                                    <label for="checkin_image">Concierge Image</label>
                                    <input type="file" class="form-control" id="concierge_image" data-status="false"
                                        name="concierge_image" onchange="validateImageSize(this)"
                                        aria-describedby="emailHelp">
                                    <span class="text-danger concierge_image_error" style="display: none;">Concierge
                                        image is large.</span>
                                </div>
                                <div class="form-group">
                                    <label for="checkin_image">Experience Image</label>
                                    <input type="file" class="form-control" data-status="false" id="experience_image"
                                        onchange="validateImageSize(this)" name="experience_image"
                                        aria-describedby="emailHelp">
                                    <span class="text-danger experience_image_error" style="display: none;">Experience
                                        image is large.</span>
                                </div>
                                <div class="form-group">
                                    <label for="checkin_image">Photo Preview Image</label>
                                    <input type="file" required class="form-control" data-status="false"
                                        id="photo_preview_image" onchange="validateImageSize(this)"
                                        name="photo_preview_image" aria-describedby="emailHelp">
                                    <span class="text-danger photo_preview_image_error" style="display: none;">Photo
                                        Preview Image is large.</span>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary saveLayoutImagesBtn">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>


</div>
@stop

@section('js')
<script>
    const hotelName = `{{ $hotel->name }}`;
</script>
<script src="{{ asset('js/custom/cms/appearance.js') }}"></script>
<script>
    function validateImageSize(image) {
        $(".file_error").hide();   
    var file_size = $(image)[0].files[0].size;
    console.log(file_size);
    if (file_size > 4097152) {
        $(image).attr('data-status',false);
    $("."+$(image).attr('id')+"_error").show();
    $('.saveLayoutImagesBtn').attr('disabled',true);
    return false;
    }else{
        $(image).attr('data-status',true);
    $('.saveLayoutImagesBtn').attr('disabled',false);
  $("."+$(image).attr('id')+"_error").hide();
    return true;
    }
    }
</script>
@stop