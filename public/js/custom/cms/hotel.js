/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 9);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/custom/cms/hotel.js":
/*!******************************************!*\
  !*** ./resources/js/custom/cms/hotel.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var togglePassword = $(".toggle-password");
var password = $("#password");
var togglePasswordConfirm = $(".toggle-password-confirm");
var passwordConfirm = $("#password_confirmation");
var buttonRegister = $("#buttonRegister");
var registerForm = $("#registerForm");
var username = $("#username");
var code = $("#code");
var name = $("#name");
var description = $("#description");
var email = $("#email");
$(document).ready(function () {
  buttonRegister.click(register);
  togglePassword.click(function (e) {
    changeIconPassword("password");
  });
  togglePasswordConfirm.click(function (e) {
    changeIconPassword("password_confirm");
  });
  code.keyup(function (e) {
    checkFormValidation(e);
  });
  username.keyup(function (e) {
    checkFormValidation(e);
  });
  name.keyup(function (e) {
    checkFormValidation(e);
  });
  description.keyup(function (e) {
    checkFormValidation(e);
  });
  email.keyup(function (e) {
    checkFormValidation(e);
  });
  password.keyup(function (e) {
    checkFormValidation(e);
  });
  passwordConfirm.keyup(function (e) {
    checkFormValidation(e);
  });
});

function register() {
  var type = buttonRegister.attr("data-type");

  if (type == "paypal") {
    Swal.fire({
      title: "Are you sure?",
      text: "Subscription is done with Paypal, Please note: Paypal will show you price of the monthly subscription but you will not be charged until your trial period ends If you are having issues with Paypal or do not have Paypal, please contact us at contact@servrhotels.com to arrange for payment via bank transfer.",
      type: "question",
      showCancelButton: true,
      confirmButtonColor: "#20a8d8",
      cancelButtonColor: cancel_button_color,
      confirmButtonText: "Ok",
      reverseButtons: true
    }).then(function (result) {
      if (result.value && result.value === true) {
        // Show loading
        showLoadingButton(true); // Submit

        registerForm.submit();
      }
    });
  } else {
    // Show loading
    showLoadingButton(true); // Submit

    registerForm.submit();
  } // // Show loading
  // showLoadingButton(true);
  // // Submit
  // registerForm.submit();

}

function showLoadingButton() {
  var is_show = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

  if (is_show === true) {
    buttonRegister.html("<i class=\"fa fa-refresh fa-spin fa-fw\"></i> Please wait...").attr("disabled", "disabled");
  } else {
    buttonRegister.html("Login").attr("disabled", false);
  }
}

function changeIconPassword(type) {
  if (type === "password") {
    if (password.attr("type") === "text") {
      password.attr("type", "password");
      togglePassword.addClass("fa-eye-slash");
      togglePassword.removeClass("fa-eye");
    } else if (password.attr("type") === "password") {
      password.attr("type", "text");
      togglePassword.removeClass("fa-eye-slash");
      togglePassword.addClass("fa-eye");
    }
  } else {
    if (passwordConfirm.attr("type") === "text") {
      passwordConfirm.attr("type", "password");
      togglePasswordConfirm.addClass("fa-eye-slash");
      togglePasswordConfirm.removeClass("fa-eye");
    } else if (passwordConfirm.attr("type") === "password") {
      passwordConfirm.attr("type", "text");
      togglePasswordConfirm.removeClass("fa-eye-slash");
      togglePasswordConfirm.addClass("fa-eye");
    }
  }
}

function isAllFormFilled() {
  var current = 0;
  var complete = 7;

  if (code.val() !== "") {
    current += 1;
  }

  if (username.val() !== "") {
    current += 1;
  }

  if (name.val() !== "") {
    current += 1;
  }

  if (description.val() !== "") {
    current += 1;
  }

  if (email.val() !== "") {
    current += 1;
  }

  if (password.val() !== "") {
    current += 1;
  }

  if (passwordConfirm.val() !== "") {
    current += 1;
  }

  return current === complete;
}

function checkFormValidation(e) {
  buttonRegister.prop("disabled", false);

  if (e.which === 13) {
    buttonRegister.click();
  } // if (isAllFormFilled() === true) {
  //     buttonRegister.prop('disabled', false);
  //     if(e.which === 13) {
  //         buttonRegister.click();
  //     }
  // } else {
  //     buttonRegister.prop('disabled', true);
  // }

}

/***/ }),

/***/ 9:
/*!************************************************!*\
  !*** multi ./resources/js/custom/cms/hotel.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! E:\Xammp\htdocs\server_hotels\resources\js\custom\cms\hotel.js */"./resources/js/custom/cms/hotel.js");


/***/ })

/******/ });