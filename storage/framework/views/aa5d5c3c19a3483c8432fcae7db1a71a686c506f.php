<?php $__env->startSection('title', __('Hotel details')); ?>

<?php $__env->startSection('content'); ?>
    <div class="row hotelDetailsWrapper">
        <div class="col-12">
            <div class="card">
                <div class="card-header"><?php echo e(__('Hotel details')); ?></div>
                <div class="card-body">
                    <form id="updateHotelForm" method="post" action="<?php echo e(route('admin.hotels.update', ['hotel' => $hotel->id])); ?>">
                        <?php echo method_field('put'); ?>
                        <?php echo csrf_field(); ?>
                        <input type="hidden" name="type" id="typeUpdateHotel" value="renew">
                    </form>
                    <table class="table table-responsive-sm table-striped" id="hotelDetailsTable">
                        <tbody>
                        <tr>
                            <td><b>Hotel name</b></td>
                            <td><?php echo e($hotel->name); ?></td>
                        </tr>
                        <tr>
                            <td><b>Description</b></td>
                            <td><?php echo e($hotel->description); ?></td>
                        </tr>
                        <tr>
                            <td><b>Code</b></td>
                            <td><?php echo e($hotel->code); ?></td>
                        </tr>
                        <tr>
                            <td><b>Email</b></td>
                            <td><?php echo e($hotel->email); ?></td>
                        </tr>
                        <tr>
                            <td><b>Checkout time</b></td>
                            <td><?php echo e($hotel->checkout_time); ?></td>
                        </tr>
                        <tr>
                            <td><b>Late checkout time</b></td>
                            <td><?php echo e($hotel->late_checkout_time); ?></td>
                        </tr>

                        <tr>
                            <td><b>Created at</b></td>
                            <td><?php echo e($hotel->created_at); ?></td>
                        </tr>
                        <tr>
                            <td><b>Status</b></td>
                            <td>
                                <?php if($hotel->is_active): ?>
                                    <span class="badge badge-pill badge-success">Active</span>
                                <?php else: ?>
                                    <span class="badge badge-pill badge-warning">Inactive</span>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Expired at</b></td>
                            <td>
                                <?php echo e(\App\Services\HelperService::dateTimeFormat($hotel->expired_at)); ?>

                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <button class="btn btn-success" type="button" id="renewBtn">
                                    <i class="nav-icon icon-refresh"></i>&nbsp
                                    <?php echo e(__('Renew 1 month')); ?>

                                </button>
                                <button class="btn btn-success" type="button" id="extendTrial">
                                    <i class="nav-icon icon-refresh"></i>&nbsp
                                    <?php echo e(__('Extend Trial 1 month')); ?>

                                </button>
                                <?php if($hotel->is_active): ?>
                                    <button class="btn btn-danger" type="button" id="deleteHotelBtn">
                                        <i class="nav-icon icon-trash"></i>&nbsp
                                        <?php echo e(__('Inactive')); ?>

                                    </button>
                                <?php endif; ?>
                                <a class="btn btn-danger" type="button" id="deleteHotelButton" href="<?php echo e(route('admin.hotel.destroy',['id'=>$hotel->id])); ?>">
                                        <i class="nav-icon icon-trash"></i>&nbsp
                                        <?php echo e(__('Delete')); ?>

                                </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-12">
            <div class="card">
                <div class="card-header"><?php echo e(__('Hotel features')); ?></div>
                <div class="card-body">
                    <form id="updateHotelFeatureForm" method="post" action="<?php echo e(route('admin.hotel-features.update', ['hotel' => $hotel->id])); ?>">
                        <?php echo method_field('put'); ?>
                        <?php echo csrf_field(); ?>
                    </form>
                    <table class="table table-responsive-sm table-striped" id="hotelDetailsTable">
                        <tbody>
                        <tr>
                            <td><b>Check in</b></td>
                            <td>
                                <label class="switch switch-pill switch-success">
                                    <input type="checkbox" class="switch-input" name="is_check_in_enabled" id="is_check_in_enabled" <?php echo e($hotel->hotelFeature->is_check_in_enabled ? 'checked' : ''); ?>>
                                    <span class="switch-slider"></span>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Check out</b></td>
                            <td>
                                <label class="switch switch-pill switch-success">
                                    <input type="checkbox" class="switch-input" name="is_check_out_enabled" id="is_check_out_enabled" <?php echo e($hotel->hotelFeature->is_check_out_enabled ? 'checked' : ''); ?>>
                                    <span class="switch-slider"></span>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Spa</b></td>
                            <td>
                                <label class="switch switch-pill switch-success">
                                    <input type="checkbox" class="switch-input" name="is_spa_enabled" id="is_spa_enabled" <?php echo e($hotel->hotelFeature->is_spa_enabled || $hotel->hotelFeature->spa_treament || $hotel->hotelFeature->spa_room_service? 'checked' : ''); ?>>
                                    <span class="switch-slider"></span>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><b stlye="padding-left: 20px !important;">Spa Room Service</b></td>
                            <td>
                                <label class="switch switch-pill switch-success">
                                    <input type="checkbox" class="switch-input" name="spa_room_service" id="spa_room_service" <?php echo e($hotel->hotelFeature->spa_room_service ? 'checked' : ''); ?>>
                                    <span class="switch-slider"></span>
                                </label>
                            </td>
                        </tr>

                        <tr>
                            <td><b >Spa Treatment</b></td>
                            <td>
                                <label class="switch switch-pill switch-success">
                                    <input type="checkbox" class="switch-input" name="spa_treament" id="spa_treament" <?php echo e($hotel->hotelFeature->spa_treatment ? 'checked' : ''); ?>>
                                    <span class="switch-slider"></span>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Restaurant</b></td>
                            <td>
                                <label class="switch switch-pill switch-success">
                                    <input type="checkbox" class="switch-input" name="is_restaurant_enabled" id="is_restaurant_enabled" <?php echo e($hotel->hotelFeature->is_restaurant_enabled ? 'checked' : ''); ?>>
                                    <span class="switch-slider"></span>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Concierge</b></td>
                            <td>
                                <label class="switch switch-pill switch-success">
                                    <input type="checkbox" class="switch-input" name="is_concierge_enabled" id="is_concierge_enabled" <?php echo e($hotel->hotelFeature->is_concierge_enabled ? 'checked' : ''); ?>>
                                    <span class="switch-slider"></span>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Cleaning service</b></td>
                            <td>
                                <label class="switch switch-pill switch-success">
                                    <input type="checkbox" class="switch-input" name="is_cleaning_enabled" id="is_cleaning_enabled" <?php echo e($hotel->hotelFeature->is_cleaning_enabled ? 'checked' : ''); ?>>
                                    <span class="switch-slider"></span>
                                </label>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script>
        const url1 = '<?php echo route('data-tables.admin.hotels'); ?>';
        const url2 = '<?php echo route('data-tables.admin.hotels'); ?>';
        const url3 = '<?php echo route('ajax.admin.hotel-features.index'); ?>';
        const id1 = '<?php echo $hotel->id; ?>';

    </script>
    <script src="<?php echo e(asset('js/custom/admin/hotel.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Syed Hasnain Haider\Desktop\backup\resources\views/admin/hotel/show.blade.php ENDPATH**/ ?>