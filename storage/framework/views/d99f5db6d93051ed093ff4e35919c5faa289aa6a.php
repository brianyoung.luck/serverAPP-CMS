<?php $__env->startSection('title', __('Appearances')); ?>

<?php $__env->startSection('content'); ?>
    <div id="appearancesWrapper" >
        <div class="row">
            <div class="col-12 col-sm-12 col-md-6 mainLogoWrapper">
                <div id="mainLogoFormWrapper">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label" for="mainLogoTooltip">
                            Main logo
                            <i class="icon-info icons" id="mainLogoTooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="This will be shown many times throughout the app"></i>
                        </label>
                        <div class="col-md-8">
                            <button class="btn btn-shadow" id="uploadFileBtn">Upload file</button>
                            <form id="editHotelDetailsForm" method="post" action="<?php echo e(route('cms.hotels.update', ['id' => auth('hotel')->user()->id])); ?>" enctype="multipart/form-data">
                                <?php echo csrf_field(); ?>
                                <?php echo method_field('put'); ?>
                                <input type="file" name="logo" id="logo" class="d-none" accept="image/*">
                                <input type="hidden" name="name" id="name" value="<?php echo e($hotel->name); ?>">
                            </form>
                        </div>
                    </div>
                </div>

                <div class="previewMainLogo">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Preview</label>
                        </div>
                        <div class="col-md-8">
                            <div class="imgWrapper">
                                <img src="<?php echo e($hotel->hotel_logo_ori); ?>" alt="Logo">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-6 hotelNameWrapper">
                <div id="hotelNameFormWrapper">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label" for="disabled-input">
                            Hotel name
                        </label>
                        <div class="col-md-8">
                            <button class="btn btn-shadow" id="editHotelNameBtn">Enter here</button>
                        </div>
                    </div>
                </div>
                <div class="previewHotelName">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Preview</label>
                        </div>
                        <div class="col-md-8">
                            <h4><?php echo e($hotel->name); ?></h4>
                        </div>
                    </div>

                </div>

            </div>
        </div>
        <div class="row">
            <h4>
                Themes
            </h4>
            <form id="editMobileHotelThemeForm" method="post" action="<?php echo e(route('cms.hotels.update', ['id' => auth('hotel')->user()->id])); ?>">
                <?php echo method_field('put'); ?>
                <?php echo csrf_field(); ?>
                <input type="hidden" name="mobile_hotel_theme_id" id="mobileHotelThemeIdUpdate" value="0">
            </form>
            <div class="col-12 themesWrapper">
                <?php $__currentLoopData = $mobileHotelThemes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mobileHotelTheme): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($hotel->mobile_hotel_theme_id == $mobileHotelTheme->id): ?>
                        <div class="themeWrapper active" data-id="<?php echo e($mobileHotelTheme->id); ?>">
                            <img src="<?php echo e($mobileHotelTheme->photo_preview_url); ?>" alt="Theme <?php echo e($mobileHotelTheme->id); ?>">
                        </div>
                    <?php else: ?>
                        <div class="themeWrapper" data-id="<?php echo e($mobileHotelTheme->id); ?>">
                            <img src="<?php echo e($mobileHotelTheme->photo_preview_url); ?>" alt="Theme <?php echo e($mobileHotelTheme->id); ?>">
                        </div>
                    <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                
                    
                
                
                    
                
                
                    
                
                
                    
                
                
                    
                
                
                    
                
                
                    
                
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script>
        const hotelName = `<?php echo e($hotel->name); ?>`;
    </script>
    <script src="<?php echo e(asset('js/custom/cms/appearance.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('cms.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Syed Hasnain Haider\Desktop\backup\resources\views/cms/appearance/index.blade.php ENDPATH**/ ?>