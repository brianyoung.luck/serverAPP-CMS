<?php

namespace App\Http\Controllers\Cms;

use App\Models\ConciergeServiceRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ConciergeServiceRequestController extends Controller
{
    public function update(Request $request)
    {
        ConciergeServiceRequest::where('id', $request->get('concierge_service_request_id', 0))
            ->update([
                'status' => $request->get('status', 'done')
            ]);

        return redirect()->route('cms.concierges.index')->with('success', 'Success');
    }
}