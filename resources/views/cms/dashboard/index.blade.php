@extends('cms.layouts.app')

@section('title', __('Dashboard'))
@php 
$user = auth('hotel_staff')->user();
@endphp

@section('content')
    <div id="dashboardWrapper" >
        <div class="row">
            <div class="col-12 col-sm-4 col-md-3 col-lg-3 col-xl-3 colBtnMenuList">
                <a class="btn btn-block btn-secondary btnMenuList" href="{{ route('cms.appearances.index') }}">
                    <img src="{{ asset('images/hotel/appearance_icon@3x.png') }}" alt="appearance img">
                    <p>Appearance</p>
                </a>
            </div>
            @if (auth('hotel')->user()->feature->is_restaurant_enabled && (!$user || $user->can('restaurant')))
                <div class="col-12 col-sm-4 col-md-3 col-lg-3 col-xl-3 colBtnMenuList">
                    <a class="btn btn-block btn-secondary btnMenuList" data-toggle="modal" data-target="#restaurantSelectModal">
                        <img src="{{ asset('images/hotel/restaurant_icon@3x.png') }}" alt="restaurant img">
                        <p>Restaurant</p>
                    </a>
                </div>
            @endif
            @if (auth('hotel')->user()->feature->is_spa_enabled && (!$user || $user->can('spa')))
                <div class="col-12 col-sm-4 col-md-3 col-lg-3 col-xl-3 colBtnMenuList">
                    @php
                        $spa = auth('hotel')->user()->spas->first();
                    @endphp
                    <a class="btn btn-block btn-secondary btnMenuList" href="{{ route('cms.spas.show', ['spa' => $spa->id]) }}">
                        <img src="{{ asset('images/hotel/spa_icon@3x.png') }}" alt="spa img">
                        <p>Spa</p>
                    </a>
                </div>
            @endif
            @if (auth('hotel')->user()->feature->is_check_in_enabled && (!$user || $user->can('checkin_in')))
                <div class="col-12 col-sm-4 col-md-3 col-lg-3 col-xl-3 colBtnMenuList">
                    <a class="btn btn-block btn-secondary btnMenuList" href="{{ route('cms.guests.index') }}">
                        <img src="{{ asset('images/hotel/check_in_icon@3x.png') }}" alt="guests img">
                        <p>Guests</p>
                    </a>
                </div>
            @endif
            @if ((!$user || $user->can('experience')))
                <div class="col-12 col-sm-4 col-md-3 col-lg-3 col-xl-3 colBtnMenuList">
                    <a class="btn btn-block btn-secondary btnMenuList" href="{{ route('cms.experience.index') }}">
                        <img src="{{ asset('images/hotel/check_out_icon@3x.png') }}" alt="checkout img">
                        <p>Experience</p>
                    </a>
                </div>
            @endif
            @if ($hotel->feature->is_concierge_enabled && (!$user || $user->can('concierge')))
                <div class="col-12 col-sm-4 col-md-3 col-lg-3 col-xl-3 colBtnMenuList">
                    <a class="btn btn-block btn-secondary btnMenuList" href="{{ route('cms.concierges.index') }}">
                        <img src="{{ asset('images/hotel/concierge_icon@3x.png') }}" alt="concierge img">
                        <p>Concierge</p>
                    </a>
                </div>
            @endif
            @if((!$user || $user->can('settings')))
            <div class="col-12 col-sm-4 col-md-3 col-lg-3 col-xl-3 colBtnMenuList">
                <a class="btn btn-block btn-secondary btnMenuList" href="{{ route('cms.settings.index') }}">
                    <img src="{{ asset('images/hotel/settings_icon@3x.png') }}" alt="settings img">
                    <p>Settings</p>
                </a>
            </div>
            @endif
            <div class="col-12 col-sm-4 col-md-3 col-lg-3 col-xl-3 colBtnMenuList">
                <a class="btn btn-block btn-secondary btnMenuList" id="btnSupportMailTo">
                    <img src="{{ asset('images/hotel/help_icon@3x.png') }}" alt="support img">
                    <p>Support</p>
                </a>
            </div>
        </div>
    </div>

    <div class="modal fade restaurantSelectModal" id="restaurantSelectModal" tabindex="-1" role="dialog" aria-labelledby="restaurantSelectModal" aria-hidden="true">
        <div class="modal-dialog modal-md modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    @foreach ($hotel->restaurants as $restaurant)
                        <a href="{{ route('cms.restaurants.show', ['restaurant' => $restaurant->id]) }}" class="btn btn-pill btn-block btn-primary">{{ $restaurant->name }}</a>
                    @endforeach
                    @if (count($hotel->restaurants) < 3)
                       <button class="btn btn-pill btn-block btn-secondary mt-2" id="addNewRestaurantBtn">{{ __('Create new') }}</button>
                    @endif
                </div>
            </div>
        </div>
    </div>

@stop

@section('js')
    <script src="{{ asset('js/custom/cms/dashboard.js') }}"></script>
@stop