<?php

namespace App\Services;

use App\Models\Hotel;
use App\Models\MobileHotelLayout;
use App\Models\MobileLayout;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Arr;

class HotelService
{
    public function getByCode($code)
    {
        $hotel = Hotel::where('code', $code)
            ->with(['mobileHotelTheme', 'hotelFeature'])
            ->first();

        if (!$hotel) {
            throw new ModelNotFoundException();
        }

        return $hotel;
    }

    /**
     * Update hotel details
     * @param Hotel $hotel
     * @param $data
     * @return bool
     */
    public function update(Hotel $hotel, $data)
    {
        $hotel->fill($data);
        return $hotel->save();
    }

    /**
     * Update hotel logo
     * @param Hotel $hotel
     * @param $file
     */
    public function updateLogo(Hotel $hotel, $file)
    {
        // Upload file
        $fileStorageService = new FileStorageService(env('FILESYSTEM_DRIVER_SERVR_FILES'));
        $url = $fileStorageService->uploadFile($file, 'hotel-logos-input/250x250-_200x200-_150x150', $hotel->id . '.png');

        // Update hotel details
        $hotel->hotel_logo_ori = $url;
        $hotel->hotel_logo_lg = env('AWS_URL_SERVR_FILES') . '/hotel-logos/250x250/' . $hotel->id . '.png';
        $hotel->hotel_logo_md = env('AWS_URL_SERVR_FILES') . '/hotel-logos/200x200/' . $hotel->id . '.png';
        $hotel->hotel_logo_sm = env('AWS_URL_SERVR_FILES') . '/hotel-logos/150x150/' . $hotel->id . '.png';
        $hotel->save();
    }
    public function updateMobileHotelLayoutImages($request)
    {
        $hotel=auth('hotel')->user();
        // $layout=new MobileHotelLayout;
        $layout=new MobileHotelLayout;
        $layout->checkin_image = $this->getLayoutImageUrl($request->file('checkin_image'), 'checkin_image', $hotel->id);
        $layout->checkout_image = $this->getLayoutImageUrl($request->file('checkout_image'), 'checkout_image', $hotel->id);
        $layout->restaurant_image = $this->getLayoutImageUrl($request->file('restaurant_image'), 'restaurant_image', $hotel->id);
        $layout->spa_image = $this->getLayoutImageUrl($request->file('spa_image'), 'spa_image', $hotel->id);
        $layout->concierge_image = $this->getLayoutImageUrl($request->file('concierge_image'), 'concierge_image', $hotel->id);
        $layout->experience_image = $this->getLayoutImageUrl($request->file('experience_image'), 'experience_image', $hotel->id);
        $layout->photo_preview_url = $this->getLayoutImageUrl($request->file('photo_preview_image'), 'preview', $hotel->id);
        $layout->hotel_id=$hotel->id;
        $layout->layout_name=$request->layout_name;
        if ($layout->save()) {
            return ['success'=>true,'msg'=>'success'];
        } else {
            return ['success'=>false,'msg'=>'false'];
        }
    }

    public function getLayoutImageUrl($file, $name, $hotel_id)
    {
        // $fileStorageService = new FileStorageService(env('FILESYSTEM_DRIVER_SERVR_FILES'));
        // return   $url = $fileStorageService->uploadFile($file, 'hotel-logos-input/250x250-_200x200-_150x150', $hotel_id.$name . '.png');

        $fileStorageService = new FileStorageService(env('FILESYSTEM_DRIVER'));
      
        $filename = HelperService::generateRandomString(8) . '.' .$fileStorageService->getExtension($file);
        // Upload file
        return   $url = $fileStorageService->uploadFile($file, 'luxury_themes', $filename);
    //    $fileStorageService = new FileStorageService(env('FILESYSTEM_DRIVER'));
    //    $ext = $fileStorageService->getExtension($file);
    //    $directory = 'spas';
    //    $filename = $hotel_id . '.' . $ext;
    //    return  $url = $fileStorageService->uploadFile($file, $directory, $filename);
    }
    public function store($data)
    {
        $data['code'] = strtoupper($data['code']);
        $data['mobile_hotel_theme_id'] = 1; // Default theme
        $data['password'] = bcrypt(data_get($data, 'password'));
        $data['hotel_logo_ori'] = asset('images/logo/logo.png');
        $data['hotel_logo_lg'] = asset('images/logo/logo.png');
        $data['hotel_logo_md'] = asset('images/logo/logo.png');
        $data['hotel_logo_sm'] = asset('images/logo/logo.png');
        $data['is_active'] = false;
        $data['expired_at'] = now();
        $data['username'] = Arr::get($data, 'username');
        if ($data['username'] == null) {
            $data['username'] = Arr::get($data, 'code');
        }
        $data['description'] = 'Phone number ' . Arr::get($data, 'phone_number');
        $hotel = Hotel::create($data);
//        $hotel->username = Arr::get($data, 'code') . '' . $hotel->id;
//        $hotel->save();
        return  $hotel;
    }
}