<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Hotel;
use App\Models\HotelFeature;
use App\Models\Restaurant;
use App\Models\Spa;
use Srmklive\PayPal\Services\ExpressCheckout;
use Carbon\Carbon;
use App\Models\RestaurantRoomService;
use App\Models\RoomCleaningOrder;
use Illuminate\Support\Facades\DB;


class BillController extends Controller
{

    public function index($booking_id){
        $key = "hotel_booking_id";
        $sum = 0.0;

        //$restaurant_room_services = RestaurantRoomService::where([[$key,'=',$book_id],['status','=','confirmed']])->get();
        //$sum += $restaurant_room_services->sum('total_price');
        $data = DB::select(DB::raw("select rds.price, rds.qty,rd.name from restaurant_room_services rrs 
                                    inner join restaurant_dish_restaurant_room_service rds on rrs.id = rds.restaurant_room_service_id
                                    inner join restaurant_dishes rd on rds.restaurant_dish_id = rd.id
                                    where rrs.hotel_booking_id = ".$booking_id));
        $hotel_settings = Hotel::where('id', auth('hotel')->user()->id)->first();
        
        $currency = $hotel_settings['currency'];
        $vat = $hotel_settings['vat'];
        $service_charges = $hotel_settings['service_charges'];

        return array(
            'data' => $data,
            'currency' => $currency,
            'vat' => $vat,
            'service_charges' => $service_charges
        );

    }

}