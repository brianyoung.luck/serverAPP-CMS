<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\HotelCheckIn;
use App\Services\AuthService;
use App\Services\HotelBookingService;
use App\Http\Controllers\Controller;
use App\Services\SendbirdService;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Auth;

class BookingController extends Controller
{
    protected $hotelBookingService;

    public function __construct(HotelBookingService $hotelBookingService)
    {
        $this->hotelBookingService = $hotelBookingService;
    }

    public function checkIn(HotelCheckIn $request, AuthService $authService, SendbirdService $sendbirdService)
    {
        $data = DB::transaction(function () use ($request, $authService, $sendbirdService) {
            $bookingData = $request->validated();

            // Cast to carbon
            $bookingData['card_expiry_date'] = Carbon::createFromFormat('m/y', $bookingData['card_expiry_date'])->endOfMonth();

            // Save to database
            $booking = $this->hotelBookingService->create($bookingData);

            // Upload passport
            foreach ($request->file('passport_photos') as $passport_photo) {
                $this->hotelBookingService->savePassportPhoto($booking, $passport_photo);
            }

            // Create sendbird id
            $sendbirdUser = $sendbirdService->createBookingUser($booking);

            // Create channel
            $sendbirdChannel = $sendbirdService->createChatChannel($booking);

            // Save sendbird additional data into database
            $booking->sendbird_user_id = $sendbirdUser->user_id;
            $booking->sendbird_access_token = $sendbirdUser->access_token;
            $booking->sendbird_channel_url = $sendbirdChannel->channel_url;
            $booking->save();

            // Notification via sendbird
            $sendbirdService->sendAdminMessage($booking, [
                'text' => 'New check in request received',
                'payload' => [
                    'type' => 'check_in_received',
                    'id' => $booking->id,
                ],
            ]);
            
            // Issue Token
            $token = $authService->issueToken($booking);
            $booking = $authService->guestGetMe($booking);

            return [
                'token' => $token,
                'booking' => $booking,
            ];
        });

        return response()->json([
            'message' => 'Check in success',
            'data' => $data,
        ], 201);
    }

    public function lateCheckOut(SendbirdService $sendbirdService)
    {
        $booking = Auth::user();

        DB::transaction(function () use ($booking) {
            $booking->late_check_out = 'pending';
            $booking->save();
        });

        // Notification to admin
        $sendbirdService->sendAdminMessage($booking, [
            'text' => 'Late check out has been requested',
            'payload' => [
                'type' => 'late_check_out',
                'id' => $booking->id,
            ],
        ]);

        return response()->json([
            'message' => 'Request late checkout success',
        ], 201);
    }
}