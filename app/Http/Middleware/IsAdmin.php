<?php

namespace App\Http\Middleware;

use Closure;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Check if admin logged in or not
        if (!auth('admin')->check()) {
            return redirect()->route('admin.login.index');
        }

        return $next($request);
    }
}
