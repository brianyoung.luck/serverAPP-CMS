<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHotelIdLayoutNameToMobileHotelLayoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mobile_hotel_layouts', function (Blueprint $table) {
            $table->unsignedBigInteger('hotel_id')->default(0);
            $table->string('layout_name')->default('custom');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mobile_hotel_layouts', function (Blueprint $table) {
            //
        });
    }
}