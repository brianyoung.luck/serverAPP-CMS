<?php

namespace App\Http\Controllers\Cms;

use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\StoreHotelRequest;
use App\Http\Requests\UpdateHotelRequest;
use App\Models\Hotel;
use App\Models\HotelBooking;
use App\Services\AuthService;
use App\Services\HotelService;
use App\Services\HelperService;
use App\Services\FileStorageService;
use App\Services\SendbirdService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests\MobileHotelLayoutRequest;

class HotelController extends Controller
{
    private $hotelService;
    public function __construct(HotelService $hotelService)
    {
        $this->hotelService = $hotelService;
    }

    public function update(UpdateHotelRequest $request, Hotel $hotel)
    {
        // Get data
        $data = $request->only([
            'name',
            'checkout_time',
            'late_checkout_time',
            'mobile_hotel_theme_id',
            'mobile_hotel_layout_id',
        ]);
        $logo = $request->file('logo');

        if (data_get($data, 'name') != null) {
            // Update hotel details
            $this->hotelService->update($hotel, $data);
        } elseif (data_get($data, 'checkout_time') || data_get($data, 'late_checkout_time')) {
            // Update hotel checkout time & late checkout time
            $data = $request->only([
                'checkout_time',
                'late_checkout_time',
            ]);
            $this->hotelService->update($hotel, $data);

            return redirect()->route('cms.checkouts.index')->with('success', 'Update time success.');
        }

        if ($logo) {
            // Update logo
            $this->hotelService->updateLogo($hotel, $logo);
        }

        if ($mobileHotelThemeId = data_get($data, 'mobile_hotel_theme_id')) {
            // Update theme
            $this->hotelService->update($hotel, [
                'mobile_hotel_theme_id' => $mobileHotelThemeId,
            ]);
        }
        if ($mobileHotelLayoutId = data_get($data, 'mobile_hotel_layout_id')) {
            // Update theme
            $this->hotelService->update($hotel, [
                'mobile_hotel_layout_id' => $mobileHotelLayoutId,
            ]);
            return redirect()->route('cms.appearances.index')->with('success', 'Change layout success.');
        }
      

        return redirect()->route('cms.appearances.index')->with('success', 'Update hotel success.');
    }

    public function create()
    {
        //return view('cms.hotel.create');
        $no_trial = false;
        if (session()->has('no_trial')) {
            $no_trial = true;
        }
        return view(
            'cms.hotel.create_v2',
            [
                'no_trial' => $no_trial
            ]
        );
    }

    public function store(StoreHotelRequest $request)
    {
        $data = $request->only([
            'code',
            'email',
            'password',
            'name',
            'phone_number'
        ]);

        $hotel = $this->hotelService->store($data);
        if ($hotel) {
            return redirect()->route('cms.paypal.create')->with('hotel_id', $hotel->id);
            // return redirect()->route('cms.hotels.success');
        }
    }
    public function getActiveUsers(HotelBooking $booking)
    {
        $activeUsers  =$booking->CurrentGuests()->get();
        if ($activeUsers) {
            return response()->json(
                ['success'=>true,'msg'=>'active users list','data'=>$activeUsers]
            );
        } else {
            return response()->json(['success'=>true,'msg'=>'no user found']);
        }
    }
    public function getUserChatChannel(Request $request)
    {
        
        $user_ids =$request->get('sendBird_ids');
        foreach($user_ids as $ids){
            $getSendBirdInfo[] = HotelBooking::where('sendbird_user_id',$ids)->select('room_number as chat_name','sendbird_channel_url as chat_id ')->first();
        } 
            // foreach($getSendBirdInfo as $info){
            //     $info['chat_id'] =$info->sendbird_channel_url;
            //     $info['chat_name'] =$info->room_number;
            // }
        if ($getSendBirdInfo) {
            return response()->json(
                ['success'=>true,'msg'=>'Get Channel info','data'=>$getSendBirdInfo]);
        } else {
            return response()->json(['success'=>false,'msg'=>'no channelFound']);
        }
    }

    public function addLayout(MobileHotelLayoutRequest $request)
    {
        $response=  $this->hotelService->updateMobileHotelLayoutImages($request);
        if ($response['success']) {

            return redirect()->route('cms.appearances.index')->with('success', 'Added layout success.');
        } else {
            return redirect()->route('cms.appearances.index')->with('error', 'Error in adding layout.');
        }
    }
    public function successResponse()
    {
        return view('cms.hotel.response', [
            'response' => 'Register hotel success. Please wait for admin confirmation.',
        ]);
    }

    public function changePassword(ChangePasswordRequest $request)
    {
        $hotel = auth('hotel')->user();
        if (Hash::check($request->get('password'), $hotel->password)) {
            $hotel->password = bcrypt($request->get('new_password'));
            $hotel->save();
            return redirect()->back()->with('success', 'Change password success.');
        } else {
            return redirect()->back()->with('error', 'Incorrect password.');
        }
    }

    public function saveSetting(Request $request)
    {
        $setting = Hotel::where('id', auth('hotel')->user()->id)->first();
        $setting->currency = $request->input('currency');
        $setting->vat = $request->input('vat');
        $setting->service_charges = $request->input('service_charges');
        $setting->save();
        return redirect()->back()->with('success', 'Setting saved success.');
    }
}