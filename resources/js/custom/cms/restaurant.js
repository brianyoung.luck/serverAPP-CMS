import { Calendar } from "@fullcalendar/core";
import dayGridPlugin from "@fullcalendar/daygrid";
import interactionPlugin from "@fullcalendar/interaction"; // for selectable
import timeGridPlugin from "@fullcalendar/timegrid";
import SignaturePad from "signature_pad";

const uploadFileRestaurantLogoBtn = $("#uploadFileRestaurantLogoBtn");
const logo = $("#logo");
const editRestaurantForm = $("#editRestaurantForm");
const uploadFileRestaurantImagesBtn = $("#uploadFileRestaurantImagesBtn");
const restaurantImages = $("#restaurantImages");
const editRestaurantImagesForm = $("#editRestaurantImagesForm");
const addMenuRestaurantBtn = $("#addMenuRestaurantBtn");
const addRestaurantMenuModal = $(".addRestaurantMenuModal");
const storeNewRestaurantMenuBtn = $("#storeNewRestaurantMenuBtn");
const addRestaurantMenuForm = $("#addRestaurantMenuForm");
const editRestaurantMenuModal = $(".editRestaurantMenuModal");
const addCategoryModal = $(".addCategoryModal");
const addCategoryForm = $("#addCategoryForm");
const storeNewCategory = $("#storeNewCategory");
const nameUpdate = $("#nameUpdate");
const descriptionUpdate = $("#descriptionUpdate");
const priceUpdate = $("#priceUpdate");
const dishId = $("#dish_id");
const dishIdDelete = $("#dish_id_delete");
const updateRestaurantMenuBtn = $("#updateRestaurantMenuBtn");
const editRestaurantMenuForm = $("#editRestaurantMenuForm");
const deleteRestaurantMenuBtn = $("#deleteRestaurantMenuBtn");
const deleteRestaurantMenuForm = $("#deleteRestaurantMenuForm");
const showDetailsOrderModal = $(".showDetailsOrderModal");
const selectCategoryDish = $("#selectCategoryDish");
const categoryUpdate = $("#categoryUpdate");
const declineRestaurantOrderRoomBtn = $("#declineRestaurantOrderRoomBtn");
const processRestaurantServiceRequestConfirmBtn = $(
  "#processRestaurantServiceRequestConfirmBtn"
);
const processRestaurantServiceRequestPreparingBtn = $(
  "#processRestaurantServiceRequestPreparingBtn"
);
const processRestaurantServiceRequestOnTheWayBtn = $(
  "#processRestaurantServiceRequestOnTheWayBtn"
);
const processRestaurantServiceRequestDoneBtn = $(
  "#processRestaurantServiceRequestDoneBtn"
);

const updateRestaurantRoomServiceForm = $("#updateRestaurantRoomServiceForm");
const updateRestaurantReservationForm = $("#updateRestaurantReservationForm");
const rejectRestaurantReservationBtn = $("#rejectRestaurantReservationBtn");
const acceptRestaurantReservationBtn = $("#acceptRestaurantReservationBtn");
const showDetailsReservationModal = $(".showDetailsReservationModal");
const changeRestaurantName = $("#changeRestaurantName");
const deleteRestaurantBtn = $("#deleteRestaurantBtn");
const restaurantNameText = $("#restaurantNameText");
const updateType = $("#updateType");
const restaurantNameUpdate = $("#restaurantNameUpdate");
const deleteRestaurantForm = $("#deleteRestaurantForm");
var canvas = document.querySelector("canvas");
var clearSig = $("#clearSig");
var refreshRequired = false;

let currentRestaurantRoomServiceId = 0;
let currentRestaurantReservationId = 0;

$(document).ready(function() {
  var signaturePad = new SignaturePad(canvas);
  clearSig.click(function() {
    signaturePad.clear();
  });
  // Append modal to body
  addRestaurantMenuModal.appendTo("body");
  editRestaurantMenuModal.appendTo("body");
  showDetailsOrderModal.appendTo("body");
  showDetailsReservationModal.appendTo("body");

  addCategoryModal.appendTo("body");
  showDetailsOrderModal.on("hidden.bs.modal", function() {
    if (refreshRequired) {
      window.location.reload();
    }
  });

  uploadFileRestaurantLogoBtn.click(function() {
    logo.trigger("click");
  });
  uploadFileRestaurantImagesBtn.click(function() {
    restaurantImages.trigger("click");
  });

  logo.change(function() {
    editRestaurantLogo();
  });
  restaurantImages.change(function() {
    editRestaurantImages();
  });
  addMenuRestaurantBtn.click(function() {
    addRestaurantMenu();
  });
  function get() {
    $.ajax({
      headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
      },
      type: "GET",
      url: url4 + "/" + $("#selectCategoryDish").val(),
      contentType: "application/json",
      dataType: "json",
    })
      .done(function(response) {
        console.log(response);
      })
      .fail(function(error) {
        console.log(error);
      });
  }
  storeNewRestaurantMenuBtn.click(function() {
    if (validateDishMenuImageSize("add")) {
      storeRestaurantMenu();
    }
  });
  $(document).on("click", ".menuDishTr", async function() {
    await showModalDishRestaurant($(this).data("dish_id"));
  });
  $(document).on("click", ".orderWrapper", async function() {
    await showModalOrderDetails($(this).data("order_details_id"));
  });
  $(document).on("click", ".reservationWrapper", async function() {
    await showModalReservationDetails($(this).data("reservation_id"));
  });
  updateRestaurantMenuBtn.click(function() {
    if (validateDishMenuImageSize("edit")) {
      updateRestaurantMenu();
    }
  });
  deleteRestaurantMenuBtn.click(function() {
    confirmDeleteDishRestaurant();
  });
  selectCategoryDish.change(async function() {
    await getDishes(selectCategoryDish.val());
  });

  processRestaurantServiceRequestConfirmBtn.click(function() {
    confirmRestaurantRoomService("confirmed", "Confirm");
  });
  processRestaurantServiceRequestPreparingBtn.click(function() {
    preparingRestaurantRoomService("preparing", "Preparing");
  });
  processRestaurantServiceRequestOnTheWayBtn.click(function() {
    onTheWayRestaurantRoomService("on_the_way", "On the way");
  });
  processRestaurantServiceRequestDoneBtn.click(function() {
    doneRestaurantRoomService("done", "Done");
  });
  declineRestaurantOrderRoomBtn.click(function() {
    declineRestaurantRoomService();
  });

  acceptRestaurantReservationBtn.click(function() {
    acceptRestaurantReservation();
  });
  rejectRestaurantReservationBtn.click(function() {
    rejectRestaurantReservation();
  });

  changeRestaurantName.click(function() {
    changeRestaurantNameAction();
  });
  deleteRestaurantBtn.click(function() {
    deleteRestaurantAction();
  });

  storeNewCategory.click(function() {
    addCategoryForm.submit();
  });
  $(".del-cat").click(function() {
    deleteCategory($(this).attr("data-catid"));
  });

  initRestaurantReservationCalendar();

  $(document).on("click", ".imageWrapper", function() {
    deleteRestaurantGalleryAction($(this));
  });
});

function editRestaurantLogo() {
  if (validateLogoImageSize()) {
    editRestaurantForm.submit();
  }
}

function editRestaurantImages() {
  var resp = validateImagesSize();
  console.log(resp);
  jQuery.each(resp.names, function(i, val) {
    console.log(val.name);
    $(".restImagesErrors").show();
    $(".restImagesErrors").append(
      `<li style="list-style:none;">Error : <b>${
        val.name
      }</b> size is large.</li><br>`
    );
  });

  if (resp.status == true) {
    editRestaurantImagesForm.submit();
    $(".restImagesErrors").hide();
  }
}

function addRestaurantMenu() {
  // Show modal
  // getCategoryMunues();

  addRestaurantMenuModal.modal("show");
  const selectedMenue = $("#selectCategoryDish").val();
  console.log(selectedMenue);
  if (selectedMenue != "all") {
    $("select").val(selectedMenue);
  }
}

function storeRestaurantMenu() {
  storeNewRestaurantMenuBtn.attr("disabled", true);
  if (validateDishMenuImageSize("add")) {
    addRestaurantMenuForm.submit();
  }
}

async function showModalDishRestaurant(dishId) {
  // Show modal
  editRestaurantMenuModal.modal("show");

  // Call ajax to get data
  const restaurantDish = await getRestaurantDishDetails(dishId);

  // Show guest details
  if (restaurantDish.status) {
    fillRestaurantDishDetails(restaurantDish.dishDetails);
  }
}

async function getRestaurantDishDetails(dishId) {
  let promise = new Promise(function(resolve, reject) {
    $.ajax({
      headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
      },
      type: "GET",
      url: url4 + "/" + dishId,
      contentType: "application/json",
      dataType: "json",
    })
      .done(function(response) {
        resolve(response);
      })
      .fail(function(error) {
        console.log(error);
        reject();
      });
  });

  try {
    return await promise;
  } catch (err) {
    return {
      status: false,
      error: err,
    };
  }
}

function fillRestaurantDishDetails(restaurantDishDetails) {
  categoryUpdate.val(restaurantDishDetails.category);
  nameUpdate.val(restaurantDishDetails.name);
  descriptionUpdate.html(restaurantDishDetails.description);
  priceUpdate.val(restaurantDishDetails.price);
  dishId.val(restaurantDishDetails.id);
  dishIdDelete.val(restaurantDishDetails.id);
}

function updateRestaurantMenu() {
  updateRestaurantMenuBtn.attr("disabled", true);
  editRestaurantMenuForm.submit();
}

function confirmDeleteDishRestaurant(dishId) {
  Swal.fire({
    title: "Are you sure?",
    type: "question",
    showCancelButton: true,
    confirmButtonColor: delete_button_color,
    cancelButtonColor: cancel_button_color,
    confirmButtonText: "Delete",
    reverseButtons: true,
  }).then((result) => {
    if (result.value && result.value === true) {
      deleteRestaurantMenuForm.submit();
    }
  });
}

async function showModalOrderDetails(orderDetailId) {
  // Show modal
  showDetailsOrderModal.modal("show");

  // Call ajax to get data
  const orderDetails = await getOrderDetails(orderDetailId);

  // Clear table
  $(".showDetailsOrderModal #ordersTable tbody").html(``);

  if (orderDetails.status) {
    console.log(orderDetails);
    const data = orderDetails.restaurantRoomService;
    // Save id
    currentRestaurantRoomServiceId = data.id;

    // Show order details
    $(".showDetailsOrderModal #roomText").html(
      `${
        data.hotel_booking.room_number ? data.hotel_booking.room_number : ""
      } - Order details: <br> Order Number: ${currentRestaurantRoomServiceId}`
    );

    data.dishes.forEach((dish, index) => {
      $(".showDetailsOrderModal #ordersTable tbody").append(`
                <tr>
                    <td>${index + 1}</td>
                    <td>${dish.name}</td>
                    <td>${dish.pivot.qty}</td>
                    <td>${dish.pivot.note ? dish.pivot.note : "-"}</td>
                    <td>$${dish.pivot.price}</td>
                </tr>
            `);
      console.log(data.status);
      if (data.status == "done" || data.status == "confirm") {
        console.log("if");

        $(".rowBtns").hide();
        $("#mySign").hide();
        $("#clearSig").hide();
        $(".showDetailsOrderModal .sigDev .signature").html(``);
        $(".sigDev").html(`
      <div class="signature">
        ${data.signature ? '<img src="' + data.signature + '" />' : ""}
      </div>`);
      } else if (data.status != "done") {
        $(".rowBtns").show();
        console.log("else");
        $(".sigDev").html(``);
        $(".sigDev .signature").remove();
        $("#mySign").show();
        $("#clearSig").show();
        $(".showDetailsOrderModal .sigDev .signature").html(``);
      }
      // var ctx = canvas.getContext("2d");

      // var image = new Image();
      // image.src = data.signature;
      // image.onload = function() {
      //   ctx.drawImage(image, 0, 0);
      // };
    });
    /*
        // show button action
        switch (data.status) {

            case 'pending':
                declineRestaurantOrderRoomBtn.removeClass('d-none');
                processRestaurantServiceRequestConfirmBtn.removeClass('d-none');
                processRestaurantServiceRequestPreparingBtn.addClass('d-none');
                processRestaurantServiceRequestOnTheWayBtn.addClass('d-none');
                processRestaurantServiceRequestDoneBtn.addClass('d-none');
                break;
            case 'confirmed':
                declineRestaurantOrderRoomBtn.removeClass('d-none');
                processRestaurantServiceRequestConfirmBtn.addClass('d-none');
                processRestaurantServiceRequestPreparingBtn.removeClass('d-none');
                processRestaurantServiceRequestOnTheWayBtn.addClass('d-none');
                processRestaurantServiceRequestDoneBtn.addClass('d-none');
                break;
            case 'preparing':
                declineRestaurantOrderRoomBtn.removeClass('d-none');
                processRestaurantServiceRequestConfirmBtn.addClass('d-none');
                processRestaurantServiceRequestPreparingBtn.addClass('d-none');
                processRestaurantServiceRequestOnTheWayBtn.removeClass('d-none');
                processRestaurantServiceRequestDoneBtn.addClass('d-none');
                break;
            case 'on_the_way':
                declineRestaurantOrderRoomBtn.removeClass('d-none');
                processRestaurantServiceRequestConfirmBtn.addClass('d-none');
                processRestaurantServiceRequestPreparingBtn.addClass('d-none');
                processRestaurantServiceRequestOnTheWayBtn.addClass('d-none');
                processRestaurantServiceRequestDoneBtn.removeClass('d-none');
                break;
            default:
                declineRestaurantOrderRoomBtn.addClass('d-none');
                processRestaurantServiceRequestConfirmBtn.addClass('d-none');
                processRestaurantServiceRequestPreparingBtn.addClass('d-none');
                processRestaurantServiceRequestOnTheWayBtn.addClass('d-none');
                processRestaurantServiceRequestDoneBtn.addClass('d-none');
        }
        */
  } else {
    currentRestaurantRoomServiceId = 0;
  }
}

async function getOrderDetails(orderDetailId) {
  let promise = new Promise(function(resolve, reject) {
    $.ajax({
      headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
      },
      type: "GET",
      url: url5 + "/" + orderDetailId,
      contentType: "application/json",
      dataType: "json",
    })
      .done(function(response) {
        resolve(response);
      })
      .fail(function(error) {
        console.log(error);
        reject();
      });
  });

  try {
    return await promise;
  } catch (err) {
    return {
      status: false,
      error: err,
    };
  }
}

async function getDishes(category) {
  const tbody = $(".menusTable tbody");
  // Clear html
  tbody.html("");

  // Call ajax to get data
  const dishes = await getDishesFromAjax(category);

  // Show data
  if (dishes.status) {
    for (const dish of dishes.dishes) {
      tbody.append(`
                 <tr data-dish_id="${dish.id}" class="menuDishTr">
                    <td>${dish.name} <span class="pull-right">${
        dish.price
      }</span></td>
                </tr>
            `);
    }
  }
}

async function getDishesFromAjax(category) {
  let promise = new Promise(function(resolve, reject) {
    $.ajax({
      headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
      },
      type: "GET",
      url: url4,
      data: {
        category: category,
        restaurant_id: id1,
      },
      contentType: "application/json",
      dataType: "json",
    })
      .done(function(response) {
        resolve(response);
      })
      .fail(function(error) {
        console.log(error);
        reject();
      });
  });

  try {
    return await promise;
  } catch (err) {
    return {
      status: false,
      error: err,
    };
  }
}

function confirmRestaurantRoomService(status, text) {
  Swal.fire({
    title: "Are you sure?",
    type: "question",
    showCancelButton: true,
    confirmButtonColor: success_button_color,
    cancelButtonColor: cancel_button_color,
    confirmButtonText: text,
    reverseButtons: true,
  }).then((result) => {
    if (result.value && result.value === true) {
      $("#updateRestaurantRoomServiceForm #restaurant_room_services_id").val(
        currentRestaurantRoomServiceId
      );
      $("#updateRestaurantRoomServiceForm #status").val(status);
      //updateRestaurantRoomServiceForm.submit();
      refreshRequired = true;
      $.ajax({
        type: "POST",
        headers: {
          "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        url: urlRoomServiceUpdate,
        data: updateRestaurantRoomServiceForm.serialize(),
        dataType: "application/json",
        success: function() {
          $("#processRestaurantServiceRequestConfirmBtn").addClass("d-none");
          $("#processRestaurantServiceRequestPreparingBtn").removeClass(
            "d-none"
          );
        },
        error: function() {
          $("#processRestaurantServiceRequestConfirmBtn").addClass("d-none");
          $("#processRestaurantServiceRequestPreparingBtn").removeClass(
            "d-none"
          );
        },
      });
    }
  });
}

function preparingRestaurantRoomService() {
  Swal.fire({
    title: "Are you sure?",
    type: "question",
    showCancelButton: true,
    confirmButtonColor: success_button_color,
    cancelButtonColor: cancel_button_color,
    confirmButtonText: "Preparing",
    reverseButtons: true,
  }).then((result) => {
    if (result.value && result.value === true) {
      $("#updateRestaurantRoomServiceForm #restaurant_room_services_id").val(
        currentRestaurantRoomServiceId
      );
      $("#updateRestaurantRoomServiceForm #status").val("preparing");
      //updateRestaurantRoomServiceForm.submit();
      //updateRestaurantRoomServiceForm.submit();
      refreshRequired = true;
      $.ajax({
        type: "POST",
        url: urlRoomServiceUpdate,
        data: updateRestaurantRoomServiceForm.serialize(),
        success: function() {
          $("#processRestaurantServiceRequestOnTheWayBtn").removeClass(
            "d-none"
          );
          $("#processRestaurantServiceRequestPreparingBtn").addClass("d-none");
        },
        error: function() {
          $("#processRestaurantServiceRequestOnTheWayBtn").removeClass(
            "d-none"
          );
          $("#processRestaurantServiceRequestPreparingBtn").addClass("d-none");
        },
      });
    }
  });
}

function onTheWayRestaurantRoomService() {
  Swal.fire({
    title: "Are you sure?",
    type: "question",
    showCancelButton: true,
    confirmButtonColor: success_button_color,
    cancelButtonColor: cancel_button_color,
    confirmButtonText: "On the way",
    reverseButtons: true,
  }).then((result) => {
    if (result.value && result.value === true) {
      $("#updateRestaurantRoomServiceForm #restaurant_room_services_id").val(
        currentRestaurantRoomServiceId
      );
      $("#updateRestaurantRoomServiceForm #status").val("on_the_way");
      //updateRestaurantRoomServiceForm.submit();
      //updateRestaurantRoomServiceForm.submit();
      refreshRequired = true;
      $.ajax({
        type: "POST",
        url: urlRoomServiceUpdate,
        data: updateRestaurantRoomServiceForm.serialize(),
        success: function() {
          $("#processRestaurantServiceRequestOnTheWayBtn").addClass("d-none");
          $("#processRestaurantServiceRequestDoneBtn").removeClass("d-none");
        },
        error: function() {
          $("#processRestaurantServiceRequestOnTheWayBtn").addClass("d-none");
          $("#processRestaurantServiceRequestDoneBtn").removeClass("d-none");
        },
      });
    }
  });
}

function doneRestaurantRoomService() {
  Swal.fire({
    title: "Are you sure?",
    type: "question",
    showCancelButton: true,
    confirmButtonColor: success_button_color,
    cancelButtonColor: cancel_button_color,
    confirmButtonText: "Done",
    reverseButtons: true,
  }).then((result) => {
    if (result.value && result.value === true) {
      $("#updateRestaurantRoomServiceForm #restaurant_room_services_id").val(
        currentRestaurantRoomServiceId
      );
      $("#updateRestaurantRoomServiceForm #status").val("done");
      //updateRestaurantRoomServiceForm.submit();
      //updateRestaurantRoomServiceForm.submit();
      var canvas = document.getElementById("mySign");
      var dataUrl = canvas.toDataURL();
      $("#updateRestaurantRoomServiceForm #signature").val(dataUrl);
      // console.log(dataUrl);
      refreshRequired = true;
      $.ajax({
        type: "POST",
        headers: {
          "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        url: urlRoomServiceUpdate,
        data: updateRestaurantRoomServiceForm.serialize(),
        success: function(resp) {
          // console.log(resp);
          window.location.reload();
        },
        error: function(error) {
          // console.log(error);
          window.location.reload();
        },
      });
    }
  });
}

function declineRestaurantRoomService() {
  Swal.fire({
    title: "Are you sure?",
    type: "question",
    showCancelButton: true,
    confirmButtonColor: delete_button_color,
    cancelButtonColor: cancel_button_color,
    confirmButtonText: "Reject",
    reverseButtons: true,
  }).then((result) => {
    if (result.value && result.value === true) {
      $("#updateRestaurantRoomServiceForm #restaurant_room_services_id").val(
        currentRestaurantRoomServiceId
      );
      $("#updateRestaurantRoomServiceForm #status").val("rejected");
      updateRestaurantRoomServiceForm.submit();
    }
  });
}

async function showModalReservationDetails(reservationId) {
  // Show modal
  showDetailsReservationModal.modal("show");

  // Call ajax to get data
  const reservationDetails = await getReservationDetails(reservationId);

  // Show details
  currentRestaurantReservationId = 0;
  if (reservationDetails.status) {
    const data = reservationDetails.restaurantReservation;
    currentRestaurantReservationId = data.id;

    $(".showDetailsReservationModal #roomText").html(
      `${data.hotel_booking.room_number}`
    );
    $(".showDetailsReservationModal #reservationTable tbody").html(`
            <tr>
                <td>Room</td>
                <td>${data.hotel_booking.room_number}</td>
            </tr>
            <tr>
                <td>People number</td>
                <td>${data.people_number}</td>
            </tr>
            <tr>
                <td>Booking date</td>
                <td>${dateTimeFormat(data.booking_date)}</td>
            </tr>
        `);

    if (data.status === "pending") {
      rejectRestaurantReservationBtn.removeClass("d-none");
      acceptRestaurantReservationBtn.removeClass("d-none");
    } else {
      rejectRestaurantReservationBtn.addClass("d-none");
      acceptRestaurantReservationBtn.addClass("d-none");
    }
  }
}

async function getReservationDetails(reservationId) {
  let promise = new Promise(function(resolve, reject) {
    $.ajax({
      headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
      },
      type: "GET",
      url: url6 + "/" + reservationId,
      contentType: "application/json",
      dataType: "json",
    })
      .done(function(response) {
        resolve(response);
      })
      .fail(function(error) {
        console.log(error);
        reject();
      });
  });

  try {
    return await promise;
  } catch (err) {
    return {
      status: false,
      error: err,
    };
  }
}

function acceptRestaurantReservation() {
  Swal.fire({
    title: "Are you sure?",
    type: "question",
    showCancelButton: true,
    confirmButtonColor: success_button_color,
    cancelButtonColor: cancel_button_color,
    confirmButtonText: "Accept",
    reverseButtons: true,
  }).then((result) => {
    if (result.value && result.value === true) {
      $("#updateRestaurantReservationForm #restaurant_reservation_id").val(
        currentRestaurantReservationId
      );
      $("#updateRestaurantReservationForm #status").val("accepted");
      updateRestaurantReservationForm.submit();
    }
  });
}

function rejectRestaurantReservation() {
  Swal.fire({
    title: "Are you sure?",
    type: "question",
    showCancelButton: true,
    confirmButtonColor: delete_button_color,
    cancelButtonColor: cancel_button_color,
    confirmButtonText: "Reject",
    reverseButtons: true,
  }).then((result) => {
    if (result.value && result.value === true) {
      $("#updateRestaurantReservationForm #restaurant_reservation_id").val(
        currentRestaurantReservationId
      );
      $("#updateRestaurantReservationForm #status").val("rejected");
      updateRestaurantReservationForm.submit();
    }
  });
}

function changeRestaurantNameAction() {
  Swal.fire({
    title: "Change restaurant name",
    type: "question",
    showCancelButton: true,
    confirmButtonColor: success_button_color,
    cancelButtonColor: cancel_button_color,
    confirmButtonText: "Save",
    reverseButtons: true,
    input: "text",
    inputValue: $.trim(restaurantNameText.html()),
    inputValidator: (value) => {
      if (!value) {
        return "You need to write something!";
      }
    },
  }).then((result) => {
    if (result.value) {
      console.log(result);
      updateType.val("changeRestaurantName");
      restaurantNameUpdate.val(result.value);
      editRestaurantForm.submit();
    }
  });
}

function deleteRestaurantAction() {
  Swal.fire({
    title: "Delete restaurant",
    type: "question",
    showCancelButton: true,
    confirmButtonColor: delete_button_color,
    cancelButtonColor: cancel_button_color,
    confirmButtonText: "Delete",
    reverseButtons: true,
  }).then((result) => {
    if (result.value) {
      deleteRestaurantForm.submit();
    }
  });
}

function initRestaurantReservationCalendar() {
  const calendarEl = document.getElementById("reservationRestaurantCalendar");

  const calendar = new Calendar(calendarEl, {
    plugins: [interactionPlugin, dayGridPlugin, timeGridPlugin],
    defaultView: "dayGridMonth",
    selectable: true,
    navLinks: true,
    // displayEventEnd: true,
    eventLimit: true,
    displayEventTime: true,
    header: {
      right: "prev,next today",
      left: "title",
      center: "",
      // right:  'dayGridMonth,timeGridWeek,timeGridDay'
    },
    eventClick: async function(info) {
      // console.log(info.event.start);
      // console.log(info.event.id);
      await showModalReservationDetails(info.event.id);
    },
    eventSources: [
      // your event source
      {
        url: "/ajax/get-restaurant-reservations",
        method: "GET",
        extraParams: {
          restaurant_id: id1,
          status: "pending",
        },
        failure: function() {
          alert("There was an error while fetching events!");
        },
        backgroundColor: "#6d6d6d",
        borderColor: "#6d6d6d",
        textColor: "#000000",
      },
      {
        url: "/ajax/get-restaurant-reservations",
        method: "GET",
        extraParams: {
          restaurant_id: id1,
          status: "accepted",
        },
        failure: function() {
          alert("There was an error while fetching events!");
        },
        backgroundColor: "#5ab9ea",
        borderColor: "#5ab9ea",
        textColor: "#ffffff",
      },
      {
        url: "/ajax/get-restaurant-reservations",
        method: "GET",
        extraParams: {
          restaurant_id: id1,
          status: "rejected",
        },
        failure: function() {
          alert("There was an error while fetching events!");
        },
        backgroundColor: "#e3342f",
        borderColor: "#e3342f",
        textColor: "#ffffff",
      },
    ],
    eventTimeFormat: {
      hour: "2-digit",
      minute: "2-digit",
      hour12: true,
    },
    timeZone: "UTC",
  });

  calendar.render();
}

function deleteRestaurantGalleryAction(element) {
  const restaurantGalleryId = element.data("restaurant_gallery_id");
  const restaurantGalleryUrl = element.data("restaurant_gallery_url");

  Swal.fire({
    title: "Delete picture",
    type: "question",
    showCancelButton: true,
    confirmButtonColor: delete_button_color,
    cancelButtonColor: cancel_button_color,
    confirmButtonText: "Delete",
    reverseButtons: true,
    html: `
            <img src="${restaurantGalleryUrl}" alt="Img" class="w-100 h-25">
        `,
  }).then((result) => {
    if (result.value) {
      $("#deleteRestaurantGalleryForm #restaurantGalleryId").val(
        restaurantGalleryId
      );
      $("#deleteRestaurantGalleryForm").submit();
    }
  });
}
function deleteCategory(id) {
  $.ajax({
    type: "GET",
    url: delCatUrl + "?id=" + id,
    success: function() {
      window.location.reload();
    },
  });
}

// function getCategoryMunues() {
//   var selectedCat = $("#selectCategoryDish").val();
//   $.ajax({
//     headers: {
//       "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
//     },
//     type: "GET",
//     url: url6 + "/" + reservationId,
//     contentType: "application/json",
//     dataType: "json",
//   })
//     .done(function(response) {
//       console.log(response);
//     })
//     .fail(function(error) {
//       console.log(error);
//     });
// }

function validateLogoImageSize() {
  // $("#file_error").html("");
  var file_size = $("#logo")[0].files[0].size;
  if (file_size > 4097152) {
    $(".logo_file_error").show();
    $(".logo_file_error p").html("Image is too large.");
    return false;
  }
  $(".logo_file_error").hide();
  return true;
}

function validateImagesSize() {
  var fls = 0;
  console.log($("#restaurantImages").get(0).files);
  var imagesName = [];
  for (var i = 0; i < $("#restaurantImages").get(0).files.length; ++i) {
    var imageName = $("#restaurantImages").get(0).files[i].name;
    var file_size = $("#restaurantImages").get(0).files[i].size;
    console.log(file_size);
    if (file_size > 4097152) {
      var data = {};
      data.name = imageName;
      fls = fls + 1;
      imagesName.push(data);
    }
  }
  if (fls > 0) {
    $("#restaurantImages").val("");
    return { status: false, names: imagesName };
    return false;
  } else {
    return { status: true, names: imagesName };
  }
}

$(".restaurantAddMenuImage").change(function() {
  validateDishMenuImageSize("add");
});
$(".restaurantEditMenuImage").change(function() {
  validateDishMenuImageSize("edit");
});

function validateDishMenuImageSize(type) {
  if (type == "edit") {
    var file_size = $(".restaurantEditMenuImage")[0].files[0].size;
  } else {
    var file_size = $(".restaurantAddMenuImage")[0].files[0].size;
  }
  if (file_size > 4097152) {
    if (type == "edit") {
      $(".edit_file_error").show();
      $(".edit_file_error p").html("Image is too large.");
    } else {
      $(".add_file_error").show();
      $(".add_file_error p").html("Image is too large.");
    }
    return false;
  } else {
    if (type == "edit") {
      $(".edit_file_error").hide();
    } else {
      $(".add_file_error").hide();
    }
    return true;
  }
}
