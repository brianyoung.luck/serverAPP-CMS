/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/custom/cms/login.js":
/*!******************************************!*\
  !*** ./resources/js/custom/cms/login.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var togglePassword = $(".toggle-password");
var password = $("#password");
var buttonLogin = $("#buttonLogin");
var loginForm = $("#loginForm");
var username = $("#username");
var btnAppStore = $(".btnAppStore");
var btnPlayStore = $(".btnPlayStore");
$(document).ready(function () {
  buttonLogin.click(login);
  togglePassword.click(changeIconPassword);
  username.keyup(function (e) {
    checkFormValidation(e);
  });
  password.keyup(function (e) {
    checkFormValidation(e);
  });
});
btnAppStore.click(function () {
  window.location.href = "https://apps.apple.com/us/app/servr/id1473118642?ls=1";
});
btnPlayStore.click(function () {
  window.location.href = "https://play.google.com/store/apps/details?id=com.servr";
});

function login() {
  // Show loading
  showLoadingButton(true); // Submit

  loginForm.submit();
}

function showLoadingButton() {
  var is_show = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

  if (is_show === true) {
    buttonLogin.html("<i class=\"fa fa-refresh fa-spin fa-fw\"></i> Please wait...").attr("disabled", "disabled");
  } else {
    buttonLogin.html("Login").attr("disabled", false);
  }
}

function changeIconPassword() {
  if (password.attr("type") === "text") {
    password.attr("type", "password");
    togglePassword.addClass("fa-eye-slash");
    togglePassword.removeClass("fa-eye");
  } else if (password.attr("type") === "password") {
    password.attr("type", "text");
    togglePassword.removeClass("fa-eye-slash");
    togglePassword.addClass("fa-eye");
  }
}

function isAllFormFilled() {
  var current = 0;
  var complete = 2;

  if (username.val() !== "") {
    current += 1;
  }

  if (password.val() !== "") {
    current += 1;
  }

  return current === complete;
}

function checkFormValidation(e) {
  if (isAllFormFilled() === true) {
    buttonLogin.prop("disabled", false);

    if (e.which === 13) {
      buttonLogin.click();
    }
  } else {
    buttonLogin.prop("disabled", true);
  }
}

/***/ }),

/***/ 3:
/*!************************************************!*\
  !*** multi ./resources/js/custom/cms/login.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! E:\Xammp\htdocs\server_hotels\resources\js\custom\cms\login.js */"./resources/js/custom/cms/login.js");


/***/ })

/******/ });