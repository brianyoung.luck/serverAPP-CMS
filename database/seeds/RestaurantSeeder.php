<?php

use App\Models\Hotel;
use App\Models\RestaurantDishCategory;
use Illuminate\Database\Seeder;
use App\Models\Restaurant;
use App\Models\RestaurantDish;

class RestaurantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $hotels = Hotel::all();

        foreach ($hotels as $hotel)
        {
            factory(Restaurant::class, 3)->create(['hotel_id' => $hotel->id])->each(function ($restaurant) {
                RestaurantDishCategory::insert([
                    [
                        'name' => 'Starters',
                    ],
                    [
                        'name' => 'Mains',
                    ],
                    [
                        'name' => 'Sides',
                    ],
                    [
                        'name' => 'Desserts',
                    ],
                    [
                        'name' => 'Drinks',
                    ],
                ]);
                factory(RestaurantDish::class, 20)->create(['restaurant_id' => $restaurant->id])->each(function($dish) use ($restaurant) {
                    $dish->categories()->attach(RestaurantDishCategory::inRandomOrder()->take(1)->get());
                });
            });
        }
    }
}
