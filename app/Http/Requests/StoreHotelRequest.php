<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreHotelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => ['required', Rule::unique('hotels')],
            'email' => ['required', Rule::unique('hotels')],
            'username' => ['nullable', Rule::unique('hotels')],
            'name' => 'required|string|max:100',
            'phone_number' => 'required|string',
            'password' => 'required|confirmed',
        ];
    }
}
