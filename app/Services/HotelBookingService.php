<?php

namespace App\Services;

use App\Models\HotelBooking;
use App\Models\PassportPhoto;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class HotelBookingService
{
    public function create($data)
    {
        $booking = HotelBooking::create([
            'hotel_id' => $data['hotel_id'],
            'arrival_date' => $data['arrival_date'],
            'departure_date' => $data['departure_date'],
            'cardholder_name' => $data['cardholder_name'],
            'card_number' => $data['card_number'],
            'card_expiry_date' => $data['card_expiry_date'],
            'card_address' => $data['card_address'],
            'phone_number' => $data['phone_number'],
            'reference' => $data['reference'],
        ]);

        return $booking;
    }

    public function savePassportPhoto(HotelBooking $booking, $photo)
    {
        $imageName = $photo->getClientOriginalName().'-'.now()->timestamp.'.'.$photo->getClientOriginalExtension();
        $image = $photo;
        $path = 'p/'.$booking->id.'-'.$imageName;

        Storage::disk('s3')->put($path, file_get_contents($image));
        $url = Storage::disk('s3')->url($path);

        // Save to the database
        PassportPhoto::create([
            'hotel_booking_id' => $booking->id,
            'url' => $url,
        ]);
    }

    public function setReference(HotelBooking $hotelBooking)
    {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 2; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        $tmp = HotelBooking::where('id', $hotelBooking->id)
            ->select(DB::raw("id, CONCAT( '$randomString', LPAD(id,5,'0')) AS reference"))
            ->first();

        return data_get($tmp, 'reference');
    }

    public function update(HotelBooking $hotelBooking, $data)
    {
        $hotelBooking->fill($data);
        $hotelBooking->save();

        if (array_key_exists('room_number', $data)) {
            $sendBirdService = new SendbirdService();
            $sendBirdService->updateChatChannel($hotelBooking);

            // Notification via sendbird
            $sendBirdService->sendAdminMessage($hotelBooking, [
                'text' => 'Check In request has been accepted',
                'payload' => [
                    'type' => 'check_in_accepted',
                    'id' => $hotelBooking->id,
                ],
            ]);
        }

        return true;
    }
}
