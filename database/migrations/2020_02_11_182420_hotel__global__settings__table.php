<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Setting;

class HotelGlobalSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function(Blueprint $table){
            $table->bigIncrements('id');
            $table->string('key');
            $table->string('value');
        });
        $setting = new Setting;
        $setting->key = "max_promotions_basic";
        $setting->value = "5";
        $setting->save();

        $setting = new Setting;
        $setting->key = "max_promotions_premium";
        $setting->value = "25";
        $setting->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
