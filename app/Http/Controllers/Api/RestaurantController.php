<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\CustomForbiddenException;
use App\Http\Requests\CreateRestaurantReservationRequest;
use App\Http\Requests\CreateRestaurantRoomServiceRequest;
use App\Http\Resources\Customer\RestaurantReservationResource;
use App\Http\Resources\Customer\RestaurantReservationResourceCollection;
use App\Http\Resources\Customer\RestaurantResourceCollection;
use App\Http\Resources\Customer\RestaurantRoomOrderResource;
use App\Http\Resources\Customer\RestaurantRoomOrderResourceCollection;
use App\Models\Restaurant;
use App\Models\RestaurantDish;
use App\Models\RestaurantDishCategory;
use App\Services\RestaurantService;
use App\Http\Controllers\Controller;
use App\Services\SendbirdService;
use Auth;

class RestaurantController extends Controller
{
    protected $restaurantService;

    public function __construct(RestaurantService $restaurantService)
    {
        $this->restaurantService = $restaurantService;
    }

    public function getAll()
    {
        $result = Auth::user()->hotel->restaurants->load('restaurantGalleries');

        return (new RestaurantResourceCollection($result))
            ->additional([
                'message' => 'Get restaurants success'
            ])
            ->response()
            ->setStatusCode(200);
    }

    public function getRestaurantDishes(Restaurant $restaurant)
    {
        if ($restaurant->hotel_id !== Auth::user()->hotel->id) {
            throw new CustomForbiddenException('Forbidden Access');
        }

        // Get data
        //$dishes = $this->restaurantService->getRestaurantDishes($restaurant);
        $categories = RestaurantDishCategory::where('hotel_id',Auth::user()->hotel->id)->get();
        $dishes = RestaurantDish::where('restaurant_id',$restaurant->id)->get();
        //$categoriesResp = $dishes->pluck('name');
        $finalResp = [];
        foreach($categories as $category){
            $obj = new \stdClass();
            $obj->name = $category->name;
            $obj->dishes = $dishes->filter(function($value, $key) use ($category){
           
                return (int)$value->category == $category->id;
            })->all();
            array_push($finalResp,$obj);
        }
        return $finalResp;
    }

    public function getAllReservations()
    {
        $bookings = Auth::user()->restaurantReservations;

        return (new RestaurantReservationResourceCollection($bookings))
            ->additional([
                'message' => 'Get restaurant reservations success'
            ])
            ->response()
            ->setStatusCode(200);
    }

    public function createReservation(Restaurant $restaurant, CreateRestaurantReservationRequest $request, SendbirdService $sendbirdService)
    {
        if ($restaurant->hotel_id !== Auth::user()->hotel->id) {
            throw new CustomForbiddenException('Forbidden Access');
        }
        $booking = Auth::user();

        $validated = $request->validated();
        $validated['restaurant_id'] = $restaurant->id;

        $result = $this->restaurantService->createRestaurantReservation(Auth::user(), $validated);

        // Notification to admin
        $sendbirdService->sendAdminMessage($booking, [
            'text' => 'Restaurant reservation has been requested',
            'payload' => [
                'type' => 'restaurant_reservation',
                'id' => $result->id,
            ],
        ]);

        return (new RestaurantReservationResource($result))
            ->additional([
                'message' => 'Success, your reservation request has been sent'
            ])
            ->response()
            ->setStatusCode(201);
    }

    public function getAllRoomOrders()
    {
        $result = Auth::user()->restaurantRoomServices->load(['dishes', 'restaurant']);

        return (new RestaurantRoomOrderResourceCollection($result))
            ->additional([
                'message' => 'Create room service order success'
            ])
            ->response()
            ->setStatusCode(201);
    }

    public function createRoomOrder(Restaurant $restaurant, CreateRestaurantRoomServiceRequest $request, SendbirdService $sendbirdService)
    {
        if ($restaurant->hotel_id !== Auth::user()->hotel->id) {
            throw new CustomForbiddenException('Forbidden Access');
        }
        $booking = Auth::user();

        $validated = $request->validated();
        $validated['restaurant_id'] = $restaurant->id;

        $result = $this->restaurantService->createRestaurantRoomOrder(Auth::user(), $validated);

        // Notification to admin
        $sendbirdService->sendAdminMessage($booking, [
            'text' => 'Restaurant room order has been requested',
            'payload' => [
                'type' => 'restaurant_room_order',
                'id' => $result->id,
            ],
        ]);

        return (new RestaurantRoomOrderResource($result))
            ->additional([
                'message' => 'Create room service order success'
            ])
            ->response()
            ->setStatusCode(201);
    }
}