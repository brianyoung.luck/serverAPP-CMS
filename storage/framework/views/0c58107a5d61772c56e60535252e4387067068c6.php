<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <meta name="description" content="<?php echo e(env('APP_DESCRIPTION')); ?>">
    <meta name="author" content="<?php echo e(env('APP_AUTHOR')); ?>">
    <meta name="keyword" content="<?php echo e(env('APP_KEYWORD')); ?>">
    <title><?php echo $__env->yieldContent('title'); ?> - <?php echo e(env('APP_NAME')); ?></title>

    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/custom-admin.css')); ?>" rel="stylesheet">
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
<?php echo $__env->make('admin.layouts.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="app-body">
    <?php echo $__env->make('admin.layouts.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <main class="main">
        <?php echo $__env->yieldContent('breadcrumb'); ?>
        <div class="container-fluid">
            <?php echo $__env->make('admin.layouts.flash-message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <div class="animated fadeIn">
                <div class="top-action-wrapper">
                    <?php echo $__env->yieldContent('top-action'); ?>
                </div>
                <?php echo $__env->yieldContent('content'); ?>
            </div>
        </div>
    </main>
</div>

<footer class="app-footer">
    <div class="ml-auto">
        <a href="#"><?php echo e(env('APP_NAME')); ?></a>
        <span>&copy; <?php echo e(__('2019')); ?></span>
    </div>
</footer>
<script src="<?php echo e(asset('js/app.js')); ?>"></script>
<?php echo $__env->yieldContent('js'); ?>
</body>
</html><?php /**PATH C:\Users\Syed Hasnain Haider\Desktop\backup\resources\views/admin/layouts/app.blade.php ENDPATH**/ ?>