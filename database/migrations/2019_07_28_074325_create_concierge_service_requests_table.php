<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConciergeServiceRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('concierge_service_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('hotel_booking_id');
            $table->enum('status', [
                'pending',
                'confirmed',
                'preparing',
                'on_the_way',
                'rejected',
                'cancelled',
                'done',
            ])->default('pending');
            $table->timestamps();

            // Foreign Keys
            $table->foreign('hotel_booking_id')
                ->references('id')
                ->on('hotel_bookings')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('concierge_service_requests');
    }
}
