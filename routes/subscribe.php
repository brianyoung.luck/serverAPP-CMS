<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Register hotel
Route::get('', 'Subscribe\SubscribeController@index')->name('subscribe.hotels.index');
Route::post('', 'Subscribe\SubscribeController@search')->name('subscribe.hotels.search');
Route::post('/hotels/store', 'Subscribe\SubscribeController@store')->name('subscribe.hotels.store');
