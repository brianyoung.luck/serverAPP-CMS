<?php

namespace App\Http\Middleware;

use App\Exceptions\CustomForbiddenException;
use Closure;
use Auth;

class IsBookingPending
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->status == 'pending') {
            throw new CustomForbiddenException('You are not allowed to access this feature yet.');
        }

        return $next($request);
    }
}
