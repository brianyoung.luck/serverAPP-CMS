<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test', 'Cms\PaypalController@test');
// Register hotel
Route::get('', 'Cms\HotelController@create')->name('cms.hotels.create');
Route::get('success', 'Cms\HotelController@successResponse')->name('cms.hotels.success');
Route::post('', 'Cms\HotelController@store')->name('cms.hotels.store');

// Subscribe
Route::get('/subscribe', function() {
  return view('cms.paypal.subscribe');
})->name('cms.code.search');
Route::post('/search', 'Cms\PaypalController@search')->name('cms.hotels.search');

// Paypal
Route::get('/paypal-register', function() {
  return view('cms.paypal.create');
})->name('cms.paypal.create');
Route::post('/paypal-store', 'Cms\PaypalController@store')->name('cms.paypal.store');
Route::get('/paypal-success', 'Cms\PaypalController@success')->name('cms.paypal.success');
Route::post('/paypal-notification', 'Cms\PaypalController@notification')->name('cms.paypal.notification');
