<?php

namespace App\Http\Controllers\Admin;

use App\Models\HotelFeature;
use App\Models\Spa;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HotelFeatureController extends Controller
{
    public function update(Request $request, $hotelId)
    {
        dd($hotelId);
    }

    public function ajaxGetHotelFeatures(Request $request)
    {
        return [
            'status' => true,
        ];
    }

    public function ajaxUpdateHotelFeature(Request $request, $hotelId)
    {

        
        // Get details
        $hotelFeature = HotelFeature::where('hotel_id', $hotelId)
            ->first();
        if ($hotelFeature) {
            $hotelFeature->fill([
                'is_check_in_enabled' => $request->get('is_check_in_enabled', true),
                'is_check_out_enabled' => $request->get('is_check_out_enabled', true),
                'is_spa_enabled' => $request->get('is_spa_enabled', true),
                'is_restaurant_enabled' => $request->get('is_restaurant_enabled', true),
                'is_concierge_enabled' => $request->get('is_concierge_enabled', true),
                'is_cleaning_enabled' => $request->get('is_cleaning_enabled', true),
                'spa_treatment' => $request->get('spa_treament', true),
                'spa_room_service' => $request->get('spa_room_service', true),
                'is_experience' => $request->get('is_experience', true)

            ]);
            
            $hotelFeature->save();

            if ($hotelFeature->is_spa_enabled) {
                $spa = Spa::firstOrCreate(
                    ['hotel_id' => $hotelFeature->hotel_id],
                    ['name' => 'Your spa', 'logo_url' => 'https://servrhotels.s3-ap-southeast-1.amazonaws.com/spas/default.png']
                );
            }
        }

        return [
            'status' => true
        ];
    }
}