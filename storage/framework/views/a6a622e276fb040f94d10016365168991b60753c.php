<?php $__env->startSection('title', __('Staff')); ?>

<?php $__env->startSection('content'); ?>

    <div id="dashboardWrapper">
    	<div class="row">
    		<div class="col-12">
    			<div class="card-header"><?php echo e(__('System Wide Settings')); ?></div>
                 <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <h3>System Wide Setting</h3>
                            <form method="post" action="<?php echo e(route('settings.store')); ?>">
                                <?php echo csrf_field(); ?>
                                <?php echo method_field('post'); ?>

                                <?php $__currentLoopData = $settings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $setting): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <label for="<?php echo e($setting->id); ?>"><?php echo e($labels[$setting->key]); ?></label>
                                    <input type="text" class="form-control" name="<?php echo e($setting->id); ?>" value="<?php echo e($setting->value); ?>">
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                
                                <input type="submit" name="submit" value="Save" class="btn btn-info pull-right mt-3">
                            </form>
                        </div>
                        
                    </div>
                    
               
    	</div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Syed Hasnain Haider\Desktop\backup\resources\views/admin/setting.blade.php ENDPATH**/ ?>