<?php

namespace App\Http\Resources\Customer;

use Illuminate\Http\Resources\Json\JsonResource;

class SpaBookingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'spa_id' => $this->spa_id,
            'total_price' => $this->total_price,
            'booking_type' => $this->type,
            'people_number' => $this->people_number,
            'booking_date' => $this->booking_date,
            'treatments' => SpaTreatmentResource::collection($this->whenLoaded('treatments')),
            'status' => $this->status,
        ];
    }
}
