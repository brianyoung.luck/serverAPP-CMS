const restaurantSelectModal = $('#restaurantSelectModal');
const addNewRestaurantBtn = $('#addNewRestaurantBtn');
const createRestaurantForm = $('#createRestaurantForm');
const btnSupportMailTo = $('#btnSupportMailTo');

$(document).ready(function() {
    // Append modal to body
    restaurantSelectModal.appendTo("body");

    addNewRestaurantBtn.click(function () {
        Swal.fire({
            title: 'Are you sure?',
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: success_button_color,
            cancelButtonColor: cancel_button_color,
            confirmButtonText: 'Create new',
            reverseButtons: true,
        }).then((result) => {
            if (result.value && result.value === true) {
                createRestaurantForm.submit();
            }
        });
    });

    btnSupportMailTo.click(function () {
        window.open('mailto:support@servhotels.com');
    });
});