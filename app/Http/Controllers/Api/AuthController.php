<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\UserLogin;
use App\Http\Requests\UserRegister;
use App\Models\HotelBooking;
use App\Services\AuthService;
use App\Services\UserService;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Facades\JWTAuth;
use Firebase\Auth\Token\Verifier;
use Auth;

class AuthController extends Controller
{
    protected $authService;

    public function __construct()
    {
        return auth()->shouldUse('api_user');
    }

    public function register(UserRegister $request, UserService $userService)
    {
        $userData = $request->validated();

        $user = $userService->create($userData);

        return response()->json([
            'message' => 'Register user success',
            'data' => $user,
        ], 201);
    }

    public function me(AuthService $authService)
    {
        $data = $authService->guestGetMe(Auth::user());

        return response()->json([
            'message' => 'Get me success.',
            'data' => $data,
        ]);
    }

    public function login(UserLogin $request, AuthService $authService)
    {
        // Firebase verifier
        $verifier = new Verifier(env('FIREBASE_PROJECT_ID'));

        try {
            // Verify token
            $verifiedIdToken = $verifier->verifyIdToken($request->token);

            // Decode token to get firebase id
            $phoneNumber = $verifiedIdToken->getClaim('phone_number');

            // Find booking based on firebase id
            $booking = HotelBooking::getByPhoneNumber($phoneNumber)->activeDate()->currentGuests()->first();

            $authService->guestGetMe($booking);

            // Save user if not found
            if (!$booking) {
                return response()->json([
                    'message' => 'Unauthenticated',
                ], 401);
            }

            // JWT-Auth token to be use
            $token = JWTAuth::fromUser($booking);
        } catch (\Firebase\Auth\Token\Exception\ExpiredToken $e) {
            return response()->json([
                'message' => $e->getMessage(),
            ], 401);
        } catch (\Firebase\Auth\Token\Exception\IssuedInTheFuture $e) {
            return response()->json([
                'message' => $e->getMessage(),
            ], 401);
        } catch (\Firebase\Auth\Token\Exception\InvalidToken $e) {
            return response()->json([
                'message' => $e->getMessage(),
            ], 401);
        }

        return response()->json([
            'message' => 'Login success',
            'data' => [
                'token' => $token,
                'booking' => $booking,
            ]
        ], 200);
    }

    public function refresh()
    {
        $token = auth()->refresh();

        return response()->json([
            'message' => 'Login success',
            'data' => [
                'token_type' => 'Bearer',
                'access_token' => $token,
                'expires_in' => auth()->factory()->getTTL() * 60,
            ]
        ], 200);
    }

    public function logout()
    {
        auth()->logout();

        return response()->json([
            'message' => 'Logout success'
        ], 200);
    }
}

