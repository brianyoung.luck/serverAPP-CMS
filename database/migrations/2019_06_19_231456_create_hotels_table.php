<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotels', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('mobile_hotel_theme_id')->nullable();
            $table->unsignedBigInteger('mobile_hotel_layout_id')->nullable();
            $table->string('code')->unique();
            $table->string('email')->unique();
            $table->string('username')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->string('name');
            $table->string('hotel_logo_ori');
            $table->string('hotel_logo_lg');
            $table->string('hotel_logo_md');
            $table->string('hotel_logo_sm');
            $table->string('sendbird_user_id')->nullable();
            $table->string('sendbird_access_token')->nullable();
            $table->string('sendbird_channel_url')->nullable();
            $table->text('description')->nullable();
            $table->boolean('is_active')->default(true);
            $table->dateTime('expired_at')->default(now()->addDays(14));
            $table->time('checkout_time')->default('10:00:00');
            $table->time('late_checkout_time')->default('14:00:00');
            $table->timestamps();

//            // Foreign Key
//            $table->foreign('mobile_hotel_theme_id')
//                ->references('id')
//                ->on('mobile_hotel_themes')
//                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotels');
    }
}