const isCheckInEnabled = $("#is_check_in_enabled");
const isCheckOutEnabled = $("#is_check_out_enabled");
const isSpaEnabled = $("#is_spa_enabled");
const isRestaurantEnabled = $("#is_restaurant_enabled");
const isConciergeEnabled = $("#is_concierge_enabled");
const isCleaningEnabled = $("#is_cleaning_enabled");
const isExperienceEnabled = $("#is_experience");

const spa_treatment_enable = $("#spa_treament");
const spa_room_service = $("#spa_room_service");
$(document).ready(function() {
  isCheckInEnabled.change(async function() {
    await updateHotelFeature();
  });
  isCheckOutEnabled.change(async function() {
    await updateHotelFeature();
  });
  isSpaEnabled.change(async function() {
    await updateHotelFeature();
  });
  isRestaurantEnabled.change(async function() {
    await updateHotelFeature();
  });
  isConciergeEnabled.change(async function() {
    await updateHotelFeature();
  });
  isCleaningEnabled.change(async function() {
    await updateHotelFeature();
  });
  isExperienceEnabled.change(async function() {
    await updateHotelFeature();
  });
  spa_treatment_enable.change(async function() {
    console.log("SPA Room Service: ", $(spa_room_service).is(":checked"));
    console.log("SPA Treatment: ", $(spa_treatment_enable).is(":checked"));
    if (
      !spa_room_service.is(":checked") &&
      !spa_treatment_enable.is(":checked")
    ) {
      if ($(is_spa_enabled).is(":checked")) {
        $(is_spa_enabled).trigger("click");
      }
    } else {
      if (!$(is_spa_enabled).is(":checked")) {
        $(is_spa_enabled).trigger("click");
      }
    }

    await updateHotelFeature();
  });
  spa_room_service.change(async function() {
    if (
      !spa_room_service.is(":checked") &&
      !spa_treatment_enable.is(":checked")
    ) {
      if ($(is_spa_enabled).is(":checked")) {
        $(is_spa_enabled).trigger("click");
      }
    } else {
      if (!$(is_spa_enabled).is(":checked")) {
        $(is_spa_enabled).trigger("click");
      }
    }
    await updateHotelFeature();
  });
});

async function updateHotelFeature() {
  let promise = new Promise(function(resolve, reject) {
    $.ajax({
      headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
      },
      type: "PUT",
      url: url3 + "/" + id1,
      contentType: "application/json",
      dataType: "json",
      data: JSON.stringify({
        is_check_in_enabled: isCheckInEnabled.is(":checked"),
        is_check_out_enabled: isCheckOutEnabled.is(":checked"),
        is_spa_enabled: isSpaEnabled.is(":checked"),
        is_restaurant_enabled: isRestaurantEnabled.is(":checked"),
        is_concierge_enabled: isConciergeEnabled.is(":checked"),
        is_cleaning_enabled: isCleaningEnabled.is(":checked"),
        is_experience: isExperienceEnabled.is(":checked"),
        spa_treament: spa_treatment_enable.is(":checked"),
        spa_room_service: spa_room_service.is(":checked"),
      }),
    })
      .done(function(response) {
        resolve(response);
      })
      .fail(function(error) {
        console.log(error);
        reject();
      });
  });

  try {
    return await promise;
  } catch (err) {
    return {
      status: false,
      error: err,
    };
  }
}
