<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantDishRestaurantDishCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_dish_restaurant_dish_category', function (Blueprint $table) {
            $table->unsignedBigInteger('restaurant_dish_id');
            $table->unsignedBigInteger('restaurant_dish_category_id');

            // Foreign Keys
            $table->foreign('restaurant_dish_id', 'restaurant_dish_id_fk')
                ->references('id')
                ->on('restaurant_dishes')
                ->onDelete('restrict');
            $table->foreign('restaurant_dish_category_id', 'restaurant_dish_category_id_fk')
                ->references('id')
                ->on('restaurant_dish_categories')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant_dish_restaurant_dish_category');
    }
}
