<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Hotel;
use Faker\Generator as Faker;

$factory->define(Hotel::class, function (Faker $faker) {
    return [
        'mobile_hotel_theme_id' => 1,
        'code' => strtoupper($faker->unique()->lexify('????')),
        'email' => $faker->unique()->email,
        'username' => $faker->unique()->userName,
        'password' => bcrypt('secret'),
        'name' => $faker->company . ' Hotel',
        'description' => $faker->words($faker->randomElement([1, 2, 3]), true),
        'hotel_logo_ori' => 'https://www.designfreelogoonline.com/wp-content/uploads/2014/12/00546-Square-levels-logo-design-free-logos-online-02.png',
        'hotel_logo_lg' => 'https://www.designfreelogoonline.com/wp-content/uploads/2014/12/00546-Square-levels-logo-design-free-logos-online-02.png',
        'hotel_logo_md' => 'https://www.logaster.com/blog/wp-content/uploads/2018/05/LogoMakr.png',
        'hotel_logo_sm' => 'https://img.freepik.com/free-vector/polygonal-hexagon-logo_1061-517.jpg',
    ];
});
