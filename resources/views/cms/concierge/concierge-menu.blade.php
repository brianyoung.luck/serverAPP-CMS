@extends('cms.layouts.app')

@section('title', 'Concierge')

@section('content')
<div id="conciergeWrapper">
    <div class="row">
        <div class="col-md-6 col-12">
            {{-- 
            <a href="{{ url('concierges') }}" class="btn btn-lg btn-primary">
            Back</a> --}}
            <h3 class="mb-4"><a href="{{ url('concierges') }}"><i class="fa fa-arrow-left text-dark"
                        style="font-size: 18px !important;"></i></a>
                &nbspConcierge menu:</h3>
            <div class="row">
                <div class="col-12 editConciergeServiceWrapper">
                    <div class="row">
                        <div class="col-12 editConciergeServiceLabelWrapper mb-1">
                            <label>Edit items:</label>
                        </div>
                        <div class="col-12">
                            <div class="card">
                                <div class="starterWrapper d-inline-block">
                                    <div class="d-inline-block">
                                        {{--<h4>Starters:</h4>--}}
                                    </div>
                                    <div class="d-inline-block pull-right">
                                        <button class="btn btn-pill btn-primary" type="button"
                                            id="addConciergeServiceBtn">
                                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                </div>

                                <!-- Modal -->
                                <div class="modal fade addConciergeServiceModal" tabindex="-1" role="dialog"
                                    aria-labelledby="addConciergeServiceModal" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <h4>Add menu:</h4>
                                                <hr />
                                                <form id="addConciergeServiceForm" method="post"
                                                    action="{{ route('cms.concierges.store') }}"
                                                    enctype="multipart/form-data">
                                                    @csrf()
                                                    @method('post')

                                                    <div class="form-group">
                                                        <label for="name">Name</label>
                                                        <input class="form-control" id="name" name="name" type="text">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="description">Description</label>
                                                        <textarea class="form-control" id="description"
                                                            name="description" rows="4" placeholder=""></textarea>
                                                    </div>
                                                </form>
                                                <button class="btn btn-pill btn-block btn-secondary btn-lg"
                                                    type="button" id="storeNewConciergeServiceBtn">Add</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade editConciergeServiceModal" tabindex="-1" role="dialog"
                                    aria-labelledby="editConciergeServiceModal" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <h4>Edit menu:</h4>
                                                <hr />
                                                <form id="editConciergeServiceForm" method="post"
                                                    action="{{ route('cms.concierges.update', ['concierge' => 0]) }}"
                                                    enctype="multipart/form-data">
                                                    @csrf()
                                                    @method('put')
                                                    <input type="hidden" name="concierge_service_id"
                                                        id="concierge_service_id" value="0">

                                                    <div class="form-group">
                                                        <label for="nameUpdate">Name</label>
                                                        <input class="form-control" id="nameUpdate" name="nameUpdate"
                                                            type="text">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="descriptionUpdate">Description</label>
                                                        <textarea class="form-control" id="descriptionUpdate"
                                                            name="descriptionUpdate" rows="4" placeholder=""></textarea>
                                                    </div>
                                                </form>
                                                <form id="deleteConciergeServiceForm" method="post"
                                                    action="{{ route('cms.concierges.destroy', ['concierge' => 0]) }}"
                                                    enctype="multipart/form-data">
                                                    @csrf()
                                                    @method('delete')
                                                    <input type="hidden" name="concierge_service_id"
                                                        id="concierge_service_id" value="0">
                                                </form>

                                                <div class="row mt-4">
                                                    <div class="col-6">
                                                        <button class="btn btn-pill btn-block btn-danger btn-lg"
                                                            type="button" id="deleteConciergeServiceBtn">Delete</button>
                                                    </div>
                                                    <div class="col-6">
                                                        <button class="btn btn-pill btn-block btn-secondary btn-lg"
                                                            type="button" id="updateConciergeServiceBtn">Update</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="conciergeServicesTableWrapper">
                                    <table class="table table-responsive-sm conciergeServicesTable">
                                        <tbody>
                                            @foreach ($conciergeServices as $conciergeService)
                                            <tr data-concierge_service_id="{{ $conciergeService->id }}"
                                                class="menuConciergeServiceTr">
                                                <td>{{ $conciergeService->name }}</td>

                                            </tr>

                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
<script>
    const url4 = '{!! route('ajax.cms.concierge-services-requests.index') !!}';
    const url5 = '{!! route('ajax.cms.concierge-services.index') !!}';
</script>
@section('js')
<script src="{{ asset('js/custom/cms/concierge.js') }}"></script>
@stop