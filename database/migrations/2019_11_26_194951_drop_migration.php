<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('restaurant_dishes',function(Blueprint $table){
            /*DB::statement('

            ');
            $table->dropColumn('category');
            $table->integer('category_id');
            */
        });
        Schema::table('restaurant_dish_categories',function(Blueprint $table){
            DB::statement('alter table restaurant_dish_categories modify column hotel_id int null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('restaurant_dishes',function(Blueprint $table){
            $table->string('category');
            $table->dropColumn('category_id');
        });
        
    }
}
