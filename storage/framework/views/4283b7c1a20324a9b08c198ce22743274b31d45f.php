<?php $__env->startSection('title', __('Hotels')); ?>

<?php $__env->startSection('content'); ?>
    <div id="createHotelWrapper" >
        <div class="row">
            <h4>
                Create new hotel
            </h4>
            <div class="col-12 p-2">
                <form id="createHotelForm" class="form-horizontal" action="<?php echo e(route('admin.hotels.store')); ?>" method="post">
                    <?php echo csrf_field(); ?>
                    <?php echo method_field('post'); ?>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <input class="form-control <?php echo e($errors->has('name') ? ' is-invalid' : ''); ?>" id="name" type="text" name="name" placeholder="Hotel name" value="<?php echo e(old('name') ? old('name') : ''); ?>">
                                <?php if($errors->has('name')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('name')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <input class="form-control <?php echo e($errors->has('code') ? ' is-invalid' : ''); ?>" id="code" type="text" name="code" placeholder="Hotel code" value="<?php echo e(old('code') ? old('code') : ''); ?>">
                                <?php if($errors->has('code')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('code')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <input class="form-control <?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" id="email" type="email" name="email" placeholder="Email" value="<?php echo e(old('email') ? old('email') : ''); ?>">
                                <?php if($errors->has('email')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <input class="form-control <?php echo e($errors->has('username') ? ' is-invalid' : ''); ?>" id="username" type="text" name="username" placeholder="Username" value="<?php echo e(old('username') ? old('username') : ''); ?>">
                                <?php if($errors->has('username')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('username')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <input class="form-control <?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>" id="password" type="password" name="password" placeholder="Password">
                                <?php if($errors->has('password')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('password')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <input class="form-control <?php echo e($errors->has('password_confirmation') ? ' is-invalid' : ''); ?>" id="password_confirmation" type="password" name="password_confirmation" placeholder="Password confirm.">
                                <?php if($errors->has('password')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('password_confirmation')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <input class="form-control <?php echo e($errors->has('phone_number') ? ' is-invalid' : ''); ?>" id="phone_number" type="text" name="phone_number" placeholder="Phone number" value="<?php echo e(old('phone_number') ? old('phone_number') : ''); ?>">
                                <?php if($errors->has('phone_number')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('phone_number')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-md-6">
                        <button class="btn btn-pill btn-block btn-primary" type="button" id="buttonCreateHotel"><?php echo e(__('Create')); ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script>
        const url1 = '<?php echo route('data-tables.admin.hotels'); ?>';
        const url2 = '<?php echo route('admin.hotels.index'); ?>';

    </script>
    <script src="<?php echo e(asset('js/custom/admin/hotel.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/servrhotels.com/resources/views/admin/hotel/create.blade.php ENDPATH**/ ?>