<?php $__env->startSection('title', __('Guests')); ?>

<?php $__env->startSection('content'); ?>
<div id="guestsWrapper">
    <div class="row">
        <h4>
            New arrivals: <span class="badge badge-primary badge-pill"><?php echo e($newArrivals->count()); ?></span>
        </h4>
        <?php $helperService = app('App\Services\HelperService'); ?>

        <form id="deleteHotelBookingForm" method="post"
            action="<?php echo e(route('cms.hotel-bookings.destroy', ['hotel-booking' => 0])); ?>">
            <?php echo method_field('delete'); ?>
            <?php echo csrf_field(); ?>
            <input type="hidden" name="hotelBookingId" id="hotelBookingId" value="0">
        </form>
        <form id="editHotelBookingForm" method="post"
            action="<?php echo e(route('cms.hotel-bookings.update', ['hotel-booking' => 0])); ?>">
            <?php echo method_field('put'); ?>
            <?php echo csrf_field(); ?>
            <input type="hidden" name="hotelBookingId" id="hotelBookingIdUpdate" value="0">
            <input type="hidden" name="roomNumber" id="roomNumber" value="0">
        </form>
        <div class="col-12 newArrivalsWrapper">
            <?php $__currentLoopData = $newArrivals; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $newArrival): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="newArrivalWrapper" data-id="<?php echo e($newArrival->id); ?>">
                <div>
                    <div class="userFullNameWrapper">
                        <p class="userFullName"><?php echo e($newArrival->cardholder_name); ?></p>
                        <?php if($newArrival->passportPhotos->count() > 1): ?>
                        <p class="counter">+<?php echo e($newArrival->passportPhotos->count() - 1); ?></p>
                        <?php endif; ?>
                    </div>
                    <p class="bookingRefText">
                        Booking ref#:
                    </p>
                    <p class="bookingRef">
                        <?php echo e($newArrival->reference ? $newArrival->reference : '-'); ?>

                    </p>
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
        <!-- Modal -->
        <div class="modal fade userProfileModal" tabindex="-1" role="dialog" aria-labelledby="userProfileModal"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <h4 class="userProfileText">User profile:</h4>
                        <p>Passport entries</p>
                        <div class="passportEntriesWrapper" id="newGuestPassportEntriesWrapper">
                            <div class="passportEntryWrapper">
                                <a href="#" target="_blank">
                                    <img src="" alt="Passport photo">
                                </a>
                            </div>
                        </div>

                        <div class="row userProfileDetails">
                            <?php $helperService = app('App\Services\HelperService'); ?>
                            <div class="col-12 col-md-6">
                                <p>
                                    Name: <span id="newGuestName"><i
                                            class="fa fa-refresh fa-spin fa-fw"></i></span><br />
                                    Date of Birth: -<br />
                                    Booking reference: <span id="newGuestReference"><i
                                            class="fa fa-refresh fa-spin fa-fw"></i></span><br />
                                    Arrival date: <span id="newGuestArrivalDate"><i
                                            class="fa fa-refresh fa-spin fa-fw"></i></span><br />
                                    Departure date: <span id="newGuestDepartureDate"><i
                                            class="fa fa-refresh fa-spin fa-fw"></i></span><br />
                                </p>
                            </div>
                            <div class="col-12 col-md-6">
                                <p>
                                    Credit card number: <span id="newGuestCardCardNumber"><i
                                            class="fa fa-refresh fa-spin fa-fw"></i></span><br />
                                    Exp. date: <span id="newGuestCardExpiryDate"><i
                                            class="fa fa-refresh fa-spin fa-fw"></i></span><br />
                                    Name on card: <span id="newGuestCardholderName"><i
                                            class="fa fa-refresh fa-spin fa-fw"></i></span><br />
                                    Address: <span id="newGuestCardAddress"><i
                                            class="fa fa-refresh fa-spin fa-fw"></i></span><br />
                                </p>
                                <div>
                                    <div>
                                        <button class="btn btn-pill btn-outline-danger declineBtn" data-id="0"
                                            type="button">Decline</button>
                                        <button class="btn btn-pill btn-success acceptBtn" data-id="0" type="button"
                                            aria-pressed="true">Accept</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <h4>
            Checkout time: <?php echo e($helperService::dateTimeFormat(auth('hotel')->user()->checkout_time, 'g:i A')); ?>

            <button class="btn btn-shadow ml-2" id="changeCheckoutTimeBtn">Change</button>
        </h4>
    </div>
    <div class="row">
        <h4>
            Late checkout time: <?php echo e($helperService::dateTimeFormat(auth('hotel')->user()->late_checkout_time, 'g:i A')); ?>

            <button class="btn btn-shadow ml-2" id="changeLateCheckoutTimeBtn">Change</button>
        </h4>
    </div>
</div>
<form id="editHotelDetailsForm" method="post"
    action="<?php echo e(route('cms.hotels.update', ['id' => auth('hotel')->user()->id])); ?>" enctype="multipart/form-data">
    <?php echo csrf_field(); ?>
    <?php echo method_field('put'); ?>
    <input type="hidden" name="checkout_time" id="checkout_time" value="<?php echo e(auth('hotel')->user()->checkout_time); ?>">
    <input type="hidden" name="late_checkout_time" id="late_checkout_time"
        value="<?php echo e(auth('hotel')->user()->late_checkout_time); ?>">
</form>
<div class="row">
    <h4>
        Late checkouts: <span class="badge badge-primary badge-pill"><?php echo e($lateCheckouts->count()); ?></span>
    </h4>
    <div class="col-12 lateCheckoutsWrapper shadow">
        <?php $__currentLoopData = $lateCheckouts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lateCheckout): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="lateCheckoutWrapper" data-id="<?php echo e($lateCheckout->id); ?>">
            <div>
                <div class="userFullNameWrapper">
                    <p class="userFullName"><?php echo e($lateCheckout->cardholder_name); ?></p>
                    <?php if($lateCheckout->passportPhotos->count() > 1): ?>
                    <p class="counter">+<?php echo e($lateCheckout->passportPhotos->count() - 1); ?></p>
                    <?php endif; ?>
                </div>
                <p class="bookingRefText">
                    Booking ref#:
                </p>
                <p class="bookingRef">
                    <?php echo e($lateCheckout->reference ? $lateCheckout->reference : '-'); ?>

                </p>
            </div>
        </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
</div>

<div class="row mt-2">
    <h4>
        Guests:
    </h4>
    <form id="checkoutHotelBookingForm" method="post"
        action="<?php echo e(route('cms.hotel-bookings.update', ['hotel-booking' => 0])); ?>">
        <?php echo method_field('put'); ?>
        <?php echo csrf_field(); ?>
        <input type="hidden" name="hotelBookingId" id="hotelBookingIdCheckout" value="0">
        <input type="hidden" name="updateType" id="updateType" value="checkout">
    </form>
    <form id="lateCheckoutHotelBookingForm" method="post"
        action="<?php echo e(route('cms.hotel-bookings.update', ['hotel-booking' => 0])); ?>">
        <?php echo method_field('put'); ?>
        <?php echo csrf_field(); ?>
        <input type="hidden" name="hotelBookingId" id="hotelBookingIdLateCheckout" value="0">
        <input type="hidden" name="updateType" id="updateType" value="lateCheckout">
        <input type="hidden" name="statusLateCheckout" id="statusLateCheckout" value="accepted">
    </form>
    <div class="col-12 p-2">
        <div class="card">
            <table class="table table-responsive-sm table-striped" id="guestTable">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Booking Ref#</th>
                        <th>Room number</th>
                        <th>Arrival date</th>
                        <th>Departure date</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
            </table>
            <!-- Modal -->
            <div class="modal fade guestProfileModal focus" style="" id="guestProfileModal" tabindex="-1" role="dialog"
                aria-labelledby="guestProfileModal" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <h4 class="roomNumberText">Room x</h4>
                            <p>Passport entries</p>
                            <div class="passportEntriesWrapper" id="guestPassportEntriesWrapper">
                                <div class="passportEntryWrapper">
                                    <a href="#" target="_blank">
                                        <img src="" alt="Passport photo">
                                    </a>
                                </div>
                            </div>

                            <div class="row userProfileDetails">
                                <?php $helperService = app('App\Services\HelperService'); ?>
                                <div class="col-12 col-md-6">
                                    <p>
                                        Name: <span id="guestName"><i
                                                class="fa fa-refresh fa-spin fa-fw"></i></span><br />
                                        Date of Birth: -<br />
                                        Booking reference: <span id="guestReference"><i
                                                class="fa fa-refresh fa-spin fa-fw"></i></span><br />
                                        Arrival date: <span id="guestArrivalDate"><i
                                                class="fa fa-refresh fa-spin fa-fw"></i></span><br />
                                        Departure date: <span id="guestDepartureDate"><i
                                                class="fa fa-refresh fa-spin fa-fw"></i></span><br />
                                    </p>
                                </div>
                                <div class="col-12 col-md-6">
                                    <p>
                                        Credit card number: <span id="guestCardNumber"><i
                                                class="fa fa-refresh fa-spin fa-fw"></i></span><br />
                                        Exp. date: <span id="guestCardExpiryDate"><i
                                                class="fa fa-refresh fa-spin fa-fw"></i></span><br />
                                        Name on card: <span id="guestCardholderName"><i
                                                class="fa fa-refresh fa-spin fa-fw"></i></span><br />
                                        Address: <span id="guestCardAddress"><i
                                                class="fa fa-refresh fa-spin fa-fw"></i></span><br />
                                    </p>
                                    <div>
                                        <button class="btn btn-pill btn-warning lateCheckoutBtn d-inline" data-id="0"
                                            type="button" aria-pressed="true">Late</button>
                                        <button class="btn btn-pill btn-success checkoutBtn" data-id="0" type="button"
                                            aria-pressed="true">Checkout</button>
                                    </div>
                                </div>
                                <div class="row mt-3 p-3">
                                    
                                    <div class="col">
                                        <div class="sigWrapper">
                                            <h4 style="color: #5CB9EA; display:none;">Customer Signature Optional</h4>
                                            <canvas style="border: 3px dotted #5CB9EA; display:none;"></canvas>

                                        </div>
                                        <button class="btn btn-pill btn-primary d-none" id="clearSig" data-id="0"
                                            type="button" aria-pressed="true">Clear</button>

                                    </div>
                                </div>
                                <div class="row mt-3 p-3 bookingBillWrapper">
                                    <h4>Bill details</h4>
                                    <table class="table table-responsive-sm table-striped bookingBillTable">
                                        <thead>
                                            <tr>
                                                <th>Service</th>
                                                <th>From</th>
                                                <th>Price</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </tbody>

                                    </table>

                                    <h5 class="totalVatPrice w-100 text-right">VAT: 123</h5>
                                    <h5 class="totalServiceCharges w-100 text-right">Service Charges: 123</h5>
                                    <h5 class="totalBillPrice w-100 text-right">Total price 123</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal -->
            <div class="modal fade exampleModal" id="exampleModal" tabindex="0" role="dialog" data-backdrop="false"
                aria-labelledby="exampleModalLabel" aria-hidden="true" style="margin-top:55px;">
                <div class="modal-dialog" role="document">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary">Save changes</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

 




<input type="hidden" name="getHotelId" value="0" data-id="0" class="getHotelId" id="getHotelId">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script>
    const url1 = '<?php echo route('data-tables.cms.guests'); ?>';
        const url2 = '<?php echo route('cms.guests.index'); ?>';
        const url3 = '<?php echo route('data-tables.cms.guests'); ?>';
        const url4 = '<?php echo route('ajax.cms.guests.index'); ?>';
</script>
<script>
    const curl1 = '<?php echo route('data-tables.cms.guests'); ?>';
        const curl2 = '<?php echo route('cms.guests.index'); ?>';
        const curl3 = '<?php echo route('data-tables.cms.guests'); ?>';
        const curl4 = '<?php echo route('ajax.cms.guests.index'); ?>';
        const val1 = '<?php echo auth('hotel')->user()->checkout_time; ?>';
        const val2 = '<?php echo auth('hotel')->user()->late_checkout_time; ?>';
</script>
<script src="<?php echo e(asset('js/custom/cms/checkout.js')); ?>"></script>
<script src="<?php echo e(asset('js/custom/cms/guest.js')); ?>"></script>
<script>
//     var isMyPopoverShown = false;
// async function myPop(element) {
//     const hotelBookingId=$('#getHotelId').attr('data-id');
//     const guestDetails =await  getGuestDetails(hotelBookingId);
//     const bookingBill=guestDetails.bookingBill;
//     $("#popover-content").html('');
// $("#popover-content").append(`
// <div class="row">
//     <div class="col-lg-8"><h5 class="text-left">Name</h5></div>
//     <div class="col-lg-4"><h5 class="text-center">Price</h5></div>'
// </div>
// `);
// for (const item of bookingBill.items) {
// if (item.billable_type === "App\\Models\\RestaurantRoomService") {
//     for(const dish of item.billable.dishes){
// $("#popover-content").append(`<div class="row">
//     <div class="col-lg-8 mb-1">${dish.name}</div>
//     <div class="col-lg-4 text-center mb-1">${dish.price}</div>
// </div>
// `);
//     }
// } else if (item.billable_type === "App\\Models\\SpaBooking") {
// for(const treatment of item.billable.treatments){
//     $("#popover-content").append(`<div class="row">
//         <div class="col-lg-8 mb-1">${treatment.name}</div>
//         <div class="col-lg-4 text-center mb-1">${treatment.name}</div>
//     </div>
//     `);
//     }
// }
// // i++;
// }


// if (isMyPopoverShown === false) {
// $(element).popover({
// html: true,
// content: function() {
// $("#popover-content").html(
// '<div class="row"><div class="col-lg-6"><h5 class="text-center">Name</h5></div><div class="col-lg-6"><h5 class="text-center">Price</h5></div></div>\
// <div class="data-row" id="data-row"></div>'
// );
// return $("#popover-content").html();
// },
// });
// }
// $(element).popover("toggle");
// }

async function getGuestDetails(hotelBookingId) {
let promise = new Promise(function(resolve, reject) {
$.ajax({
headers: {
"X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
},
type: "GET",
url: url4 + "/" + hotelBookingId,
contentType: "application/json",
dataType: "json",
})
.done(function(response) {
resolve(response);
})
.fail(function(error) {
console.log(error);
reject();
});
});

try {
return await promise;
} catch (err) {
return {
status: false,
error: err,
};
}
}
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('cms.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\sevr_hotel\resources\views/cms/guest/index.blade.php ENDPATH**/ ?>