<?php

namespace App\Services;

use App\Models\BookingBill;
use App\Models\HotelBooking;
use App\Models\RestaurantRoomService;
use App\Models\SpaBooking;

class BookingBillService
{
    public function create(HotelBooking $hotelBooking)
    {
        $bookingBillTotal = 0;

        $bookingBill = BookingBill::create([
            'hotel_booking_id' => $hotelBooking->id,
        ]);

        // Restaurant Room Order
        $restaurantRoomServices = $hotelBooking->restaurantRoomServices()->status('done')->get();
        foreach($restaurantRoomServices as $restaurantRoomService) {
            $bookingBill->items()->create([
                'billable_type' => RestaurantRoomService::class,
                'billable_id' => $restaurantRoomService->id,
            ]);
            $bookingBillTotal += $restaurantRoomService->total_price;
        }

        // Spa Order
        $spaBookings = $hotelBooking->spaBookings()->status('accepted')->get();
        foreach($spaBookings as $spaBooking) {
            $bookingBill->items()->create([
                'billable_type' => SpaBooking::class,
                'billable_id' => $spaBooking->id,
            ]);
            $bookingBillTotal += $spaBooking->total_price;
        }

        // Save the calculated total
        $bookingBill->update([
            'total' => $bookingBillTotal
        ]);

        return $bookingBill;
    }

    public function addToBill($billable)
    {
        // Get bill first
        $bookingBill = $billable->hotelBooking->bill;

        if (!$bookingBill) {
            $bookingBill = BookingBill::create([
                'hotel_booking_id' => $billable->hotelBooking->id,
                'total' => 0,
            ]);
        }

        // Create billable
        $bookingBill->items()->create([
            'billable_type' => get_class($billable),
            'billable_id' => $billable->id,
        ]);

        // Calculate total
        $bookingBill->total += $billable->total_price;
        $bookingBill->save();

        return $bookingBill;
    }
}