<?php $__env->startSection('title', __('Reset Password')); ?>

<?php $__env->startSection('content'); ?>
    <div id="resetPasswordWrapper" class="row justify-content-center">
        <div class="col-xl-5 col-lg-6 col-md-8 col-sm-10">
            <div class="card-group">
                <div class="card p-4">
                    <div class="card-body">
                        <div class="admin-panel-text">
                            <h2><?php echo e(env('APP_NAME')); ?></h2>
                            <h3><?php echo e(__('Reset Password')); ?></h3>
                        </div>
                        <form id="resetPasswordForm" class="form-horizontal" action="<?php echo e(route('admin.reset-password.action')); ?>" method="post">
                            <?php echo csrf_field(); ?>
                            <?php echo method_field('post'); ?>
                            <input type="hidden" name="token" value="<?php echo e($token); ?>">
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <input class="form-control <?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" id="email" type="email" name="email" placeholder="Email">
                                        <?php if($errors->has('email')): ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('email')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <input class="form-control <?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>" id="password" type="password" name="password" placeholder="New password">
                                        <?php if($errors->has('password')): ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('password')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <input class="form-control <?php echo e($errors->has('password_confirmation') ? ' is-invalid' : ''); ?>" id="password_confirmation" type="password" name="password_confirmation" placeholder="New password confirm.">
                                        <?php if($errors->has('password_confirmation')): ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('password_confirmation')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <div class="row">
                            <div class="col-12">
                                <button class="btn btn-pill btn-block btn-primary" type="button" id="buttonReset" disabled><?php echo e(__('Reset password')); ?></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script>

    </script>
    <script src="<?php echo e(asset('js/custom/admin/reset-password.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.auth', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/servrhotels.com/resources/views/admin/reset-password/index.blade.php ENDPATH**/ ?>