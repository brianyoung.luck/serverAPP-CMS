<header class="app-header navbar">
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        ☰
    </button>
    <a class="navbar-brand" href="<?php echo e(route('cms.dashboard.index')); ?>">
        
        
        <div class="navbar-brand-full">
            <h1>Servr</h1>
            <h2>Admin panel</h2>
        </div>
        <h1 class="navbar-brand-minimized"></h1>
    </a>
    
        
    
    <ul class="nav navbar-nav ml-auto mr-3">
        <li class="nav-item">
            <?php echo e(auth('admin')->user()->name); ?>

        </li>
        <li class="nav-item">
            <?php echo e(now()->format('M d, Y')); ?>

        </li>
    </ul>
</header><?php /**PATH D:\XAMP\htdocs\sevr_hotel\resources\views/admin/layouts/header.blade.php ENDPATH**/ ?>