const mainLogoTooltip = $("#mainLogoTooltip");
const uploadFileBtn = $("#uploadFileBtn");
const logo = $("#logo");
const editHotelDetailsForm = $("#editHotelDetailsForm");
const updateHotelNameBtn = $("#updateHotelNameBtn");
const editHotelNameBtn = $("#editHotelNameBtn");
const name = $("#name");
const mobileHotelThemeIdUpdate = $("#mobileHotelThemeIdUpdate");
const editMobileHotelThemeForm = $("#editMobileHotelThemeForm");
const mobileHotelLayoutIdUpdate = $("#mobileHotelLayoutIdUpdate");
const editMobileHotelLayoutForm = $("#editMobileHotelLayoutForm");

$(document).ready(function() {
  mainLogoTooltip.tooltip();
  uploadFileBtn.click(function() {
    logo.trigger("click");
  });
  updateHotelNameBtn.click(function() {
    editHotelLogo();
  });
  logo.change(function() {
    if (validateLogoImageSize()) {
      editHotelLogo();
    }
  });
  editHotelNameBtn.click(function() {
    editHotelName();
  });

  $(document).on("click", ".themeWrapper", function() {
    changeTheme($(this).data("id"));
  });

  $(document).on("click", ".layoutWrapper", function() {
    changeLayout($(this).data("id"));
  });
});

function editHotelLogo() {
  editHotelDetailsForm.submit();
}

function editHotelName() {
  Swal.fire({
    title: "Input hotel name",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: primary_color,
    cancelButtonColor: cancel_button_color,
    confirmButtonText: "Save",
    reverseButtons: true,
    input: "text",
    inputValue: hotelName,
    inputValidator: (value) => {
      if (!value) {
        return "Please input hotel name!.";
      }
    },
  }).then((result) => {
    if (result.value) {
      name.val(result.value);
      editHotelDetailsForm.submit();
    }
  });
}

function changeTheme(themeId) {
  mobileHotelThemeIdUpdate.val(themeId);
  editMobileHotelThemeForm.submit();
}

function changeLayout(layoutId) {
  mobileHotelLayoutIdUpdate.val(layoutId);
  editMobileHotelLayoutForm.submit();
  $(".file_error").hide();
}

$(document).on("click", ".btnAddCustomLayout", function() {
  $("#customLayoutModal").modal("show");
});

$(".saveLayoutImagesBtn").click(function() {
  var checkin_image = $("#checkin_image").attr("data-status");
  var checkout_image = $("#checkout_image").attr("data-status");
  var restaurant_image = $("#restaurant_image").attr("data-status");
  var spa_image = $("#spa_image").attr("data-status");
  var concierge_image = $("#concierge_image").attr("data-status");
  var experience_image = $("#experience_image").attr("data-status");
  var photo_preview_image = $("#photo_preview_image").attr("data-status");
  if (
    $("#checkin_image").val() &&
    $("#checkout_image").val() &&
    $("#restaurant_image").val() &&
    $("#spa_image").val() &&
    $("#concierge_image").val() &&
    $("#experience_image").val() &&
    $("#photo_preview_image").val() &&
    $("#layout_name").val()
  ) {
    if (
      checkin_image == "true" &&
      checkout_image == "true" &&
      restaurant_image == "true" &&
      spa_image == "true" &&
      concierge_image == "true" &&
      experience_image == "true" &&
      photo_preview_image == "true"
    ) {
      $(".file_error").hide();
      $("#saveLayoutImagesForm").submit();
    } else {
      $(".file_error").show();
    }
  } else {
    $(".file_error").show();
    $(".file_error").html("Please fill the all fields.");
  }
});

function validateLogoImageSize() {
  $(".logo_file_error").hide();
  var file_size = $("#logo")[0].files[0].size;
  if (file_size > 4097152) {
    $(".logo_file_error").show();
    console.log("err");
    return false;
  } else {
    $(".logo_file_error").hide();
    return true;
  }
}
