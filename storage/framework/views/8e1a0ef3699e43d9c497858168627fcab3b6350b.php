<?php $__env->startSection('title', 'Concierge'); ?>

<?php $__env->startSection('content'); ?>
<div id="conciergeWrapper">
    <div class="row">
        <div class="col-md-6 col-12">
            <!-- Modal -->
            <div class="modal fade showDetailsConciergeServiceRequestModal" tabindex="-1" role="dialog"
                aria-labelledby="showDetailsConciergeServiceRequestModal" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <form id="updateConciergeServiceRequestForm" method="post"
                                action="<?php echo e(route('cms.concierge-services-requests.update', ['conciergeServicesRequest' => 0])); ?>"
                                enctype="multipart/form-data">
                                <?php echo csrf_field(); ?>
                                <?php echo method_field('put'); ?>
                                <input type="hidden" name="concierge_service_request_id"
                                    id="concierge_service_request_id" value="0">
                                <input type="hidden" name="status" id="status" value="0">
                            </form>
                            <h4 class="mb-4" id="roomNoText">Room 101 - Request details:</h4>
                            <table class="table table-responsive-sm" id="conciergeServiceRequestDetailsTable">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Item</th>
                                        <th>Qty</th>
                                        <th>Note</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Toothbrush</td>
                                        <td>1</td>
                                        <td>-</td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="row mt-4">
                                <div class="offset-lg-1 col-lg-3 col-sm-6 col-xs-12">
                                    <button class="btn btn-pill btn-block btn-danger btn-lg reject"
                                        style="margin-top:0px !important;" type="button"
                                        id="declineConciergeServiceRequestBtn">Reject</button>
                                </div>
                                
                                <div class="col-lg-3 col-sm-6 col-xs-12 mt-xs-1 mt-sm-1">
                                    <button class="btn btn-pill btn-block btn-success btn-lg accept" type="button"
                                        id="processConciergeServiceRequestConfirmBtn">Accept</button>
                                </div>
                                
                                
                                
                                <div class="col-lg-3 col-sm-6 col-xs-12 mt-xs-1 mt-sm-1">
                                    <button class="btn btn-pill btn-block btn-success btn-lg complete" type="button"
                                        id="processConciergeServiceRequestDoneBtn">Completed</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            
            <div class="row">
                <div class="col-12 conciergeServiceRequestPendingWrapper">
                    <div class="conciergeTextWrapper">
                        <div class="conciergeRequestTextWrapper">
                            <h3 class="mb-4">Concierge requests:</h3>
                        </div>
                        <div class="editMenuConciergeWrapper">
                            <a class="btn btn-pill btn-primary" id="editMenuConciergeBtn"
                                href="<?php echo e(route('cms.concierge-menus.index')); ?>">
                                Edit concierge menu
                            </a>
                        </div>
                    </div>
                    <div class="row orderItemsWrapper">
                        <?php $__currentLoopData = $conciergeServicePendingRequests; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $conciergeServiceRequest): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="column col-md-4 col-sm-4 col-6">
                            <div class="orderWrapper"
                                data-concierge_service_request_id="<?php echo e($conciergeServiceRequest->id); ?>">
                                <div class="roomTextWrapper">
                                    <h4><?php echo e(data_get($conciergeServiceRequest, 'hotelBooking.room_number', 'Room -')); ?>

                                    </h4>
                                </div>
                                <div class="conciergeServiceRequestWrapper">
                                    <ul>
                                        <?php for($i=0, $j=1; $i<count($conciergeServiceRequest->conciergeServices); $i++,
                                            $j++): ?>
                                            <?php if($j < 4): ?> <li><?php echo e($conciergeServiceRequest->conciergeServices[$i]->name); ?>

                                                </li>
                                                <?php elseif(count($conciergeServiceRequest->conciergeServices) == 4): ?>
                                                <li><?php echo e($conciergeServiceRequest->conciergeServices[$i]->name); ?></li>
                                                <?php else: ?>
                                                <li>More...</li>
                                                <?php break; ?>
                                                <?php endif; ?>
                                                <?php endfor; ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>

                <div class="col-12 laundryRequestPendingWrapper">
                    <!-- Modal -->
                    <div class="modal fade showLaundaryRequestModal" data-order-id="0" role="dialog"
                        data-backdrop="false" style="margin-top: 106px;" aria-labelledby="showLaundaryRequestModal"
                        aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <h4 class="mb-4 text-center">Are you sure?</h4>
                                    <div class="row mt-4">
                                        <div class="offset-lg-1 col-lg-3 col-sm-6 col-xs-12">
                                            <button class="btn btn-pill btn-block btn-danger btn-lg reject"
                                                style="margin-top:0px !important;" type="button"
                                                id="reject">Reject</button>
                                        </div>
                                        <div class="col-lg-3 col-sm-6 col-xs-12 mt-xs-1 mt-sm-1">
                                            <button class="btn btn-pill btn-block btn-success btn-lg accept"
                                                type="button" id="accept">Accept</button>
                                        </div>
                                        <div class="col-lg-3 col-sm-6 col-xs-12 mt-xs-1 mt-sm-1">
                                            <button class="btn btn-pill btn-block btn-success btn-lg complete"
                                                type="button" id="complete">Completed</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-8">
                            <h3 class="mb-4">Laundry requests:</h3>
                        </div>
                        <div class="col">
                            <div class="editMenuConciergeWrapper float-right">
                                <a class="btn btn-pill btn-primary"
                                    style="background-color: #e7e7e7; box-shadow: 1px 2px 10px -5px rgba(0, 0, 0, 0.63); border:none; color: #6d6d6d; padding-left: 20px; padding-right: 20px; width: 166.83px;"
                                    id="" href="<?php echo e(route('cms.laundary-menus.index')); ?>">
                                    Edit laundry menu
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row orderItemsWrapper">
                        <form id="updateLaundryOrderForm" method="post"
                            action="<?php echo e(route('cms.laundry-orders.update', ['laundry_order' => 0])); ?>"
                            enctype="multipart/form-data">
                            <?php echo csrf_field(); ?>
                            <?php echo method_field('put'); ?>
                            <input type="hidden" name="laundry_orders_id" id="laundry_orders_id" value="0">
                            <input type="hidden" name="status" id="status" value="0">
                        </form>
                        <?php $__currentLoopData = $laundryRequests; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $laundryRequest): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="column col-md-4 col-sm-4 col-6">
                            <div class="laundryRequestWrapper" data-laundry_request_id="<?php echo e($laundryRequest->id); ?>">
                                <div class="roomTextWrapper">
                                    <h4><?php echo e(data_get($laundryRequest, 'hotelBooking.room_number', 'Room -')); ?>

                                    </h4>
                                </div>
                                <div class="conciergeServiceRequestWrapper">
                                    <ul>
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        

                                        <li><?php echo e($laundryRequest->type); ?></li>
                                        
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-12">
            <div id="laundryRoomCleaningWrapper">
                <div class="row">
                    <div class="col-12 roomCleaningRequestPendingWrapper">
                        <h3 class="mb-4">Room cleaning requests:</h3>
                        <div class="row">
                            <div class="col-12 roomCleaningOrdersWrapper">
                                <form id="updateRoomCleaningRequestForm" method="post"
                                    action="<?php echo e(route('cms.room-cleaning-requests.update', ['roomCleaningRequests' => 0])); ?>"
                                    enctype="multipart/form-data">
                                    <?php echo csrf_field(); ?>
                                    <?php echo method_field('put'); ?>
                                    <input type="hidden" name="room_cleaning_request_id" id="room_cleaning_request_id"
                                        value="0">
                                    <input type="hidden" name="room_cleaning_request_status"
                                        id="room_cleaning_request_status" value="0">
                                </form>
                                <?php $__currentLoopData = $roomCleaningRequests; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $roomCleaningRequest): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="roomCleaningOrderWrapper">
                                    <div class="roomText">
                                        <?php echo e(data_get($roomCleaningRequest, 'hotelBooking.room_number', 'Room -')); ?>

                                    </div>
                                    <div class="btnStatusWrapper">
                                        <div class="btnWrapper">
                                            <button
                                                class="btn <?php echo e(data_get($roomCleaningRequest, 'status', 'completed') == 'pending' ? 'btn-info' : 'btn-secondary'); ?>"
                                                type="button">Requested</button>
                                        </div>
                                        <div class="btnWrapper border-left border-right">
                                            <button
                                                class="btn <?php echo e(data_get($roomCleaningRequest, 'status', 'completed') == 'confirmed' ? 'btn-info' : 'btn-secondary'); ?> btnConfirmRoomCleaningRequest"
                                                type="button"
                                                data-room_cleaning_request_id="<?php echo e($roomCleaningRequest->id); ?>">Accepted</button>
                                        </div>
                                        <div class="btnWrapper">
                                            <button
                                                class="btn <?php echo e(data_get($roomCleaningRequest, 'status', 'completed') == 'completed' || data_get($roomCleaningRequest, 'status', 'completed') == 'rejected' ? 'btn-info' : 'btn-secondary'); ?> btnFinishRoomCleaningRequest"
                                                type="button"
                                                data-room_cleaning_request_id="<?php echo e($roomCleaningRequest->id); ?>">Completed</button>
                                        </div>
                                    </div>
                                </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<?php $__env->stopSection(); ?>
<script>
    
    
    
    const url4 = '<?php echo route('ajax.cms.concierge-services-requests.index'); ?>';
    const url5 = '<?php echo route('ajax.cms.concierge-services.index'); ?>';
</script>
<?php $__env->startSection('js'); ?>
<script src="<?php echo e(asset('js/custom/cms/concierge.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('cms.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\XAMP\htdocs\sevr_hotel\resources\views/cms/concierge/index.blade.php ENDPATH**/ ?>