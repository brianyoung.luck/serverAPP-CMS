<?php

namespace App\Http\Controllers\Cms;

use App\Models\LaundryOrder;
use App\Services\SendbirdService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LaundryOrderController extends Controller
{
    public function update(Request $request, SendbirdService $sendbirdService)
    {
        $laundryOrder = LaundryOrder::where('id', $request->get('laundry_orders_id', 0))
            ->whereHas('hotelBooking', function (Builder $query) {
                $query->where('hotel_id', auth('hotel')->user()->id);
            })
            ->first();
        if (!$laundryOrder) {
            abort(404);
        }

        $laundryOrder->status = $request->get('status');
        $laundryOrder->save();
        return redirect()->route('cms.concierges.index')->with('success', 'Success.');

        // Notification via sendbird
        return     $sendbirdService->sendAdminMessage($laundryOrder->hotelBooking, [
            'text' => 'Your laundry order is ' . ucwords(str_replace("_", " ", $laundryOrder->status)),
            'payload' => [
                'type' => 'restaurant_room_order',
                'id' => $laundryOrder->id,
            ],
        ]);

        return redirect()->route('cms.concierges.index')->with('success', 'Success.');
    }
}