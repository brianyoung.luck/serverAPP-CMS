@extends('admin.layouts.app')

@section('title', __('Hotels'))

@section('content')
    <div id="createHotelWrapper" >
        <div class="row">
            <h4>
                Create new hotel
            </h4>
            <div class="col-12 p-2">
                <form id="createHotelForm" class="form-horizontal" action="{{ route('admin.hotels.store') }}" method="post">
                    @csrf()
                    @method('post')
                    <div class="form-group row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <input class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" type="text" name="name" placeholder="Hotel name" value="{{ old('name') ? old('name') : '' }}">
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <input class="form-control {{ $errors->has('code') ? ' is-invalid' : '' }}" id="code" type="text" name="code" placeholder="Hotel code" value="{{ old('code') ? old('code') : '' }}">
                                @if ($errors->has('code'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <input class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" type="email" name="email" placeholder="Email" value="{{ old('email') ? old('email') : '' }}">
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <input class="form-control {{ $errors->has('username') ? ' is-invalid' : '' }}" id="username" type="text" name="username" placeholder="Username" value="{{ old('username') ? old('username') : '' }}">
                                @if ($errors->has('username'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <input class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" id="password" type="password" name="password" placeholder="Password">
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <input class="form-control {{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" id="password_confirmation" type="password" name="password_confirmation" placeholder="Password confirm.">
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <input class="form-control {{ $errors->has('phone_number') ? ' is-invalid' : '' }}" id="phone_number" type="text" name="phone_number" placeholder="Phone number" value="{{ old('phone_number') ? old('phone_number') : '' }}">
                                @if ($errors->has('phone_number'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-md-6">
                        <button class="btn btn-pill btn-block btn-primary" type="button" id="buttonCreateHotel">{{ __('Create') }}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>
        const url1 = '{!! route('data-tables.admin.hotels') !!}';
        const url2 = '{!! route('admin.hotels.index') !!}';

    </script>
    <script src="{{ asset('js/custom/admin/hotel.js') }}"></script>
@stop