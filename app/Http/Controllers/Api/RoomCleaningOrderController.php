<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\Customer\RoomCleaningOrderResource;
use App\Http\Resources\Customer\RoomCleaningOrderResourceCollection;
use App\Services\RoomCleaningOrderService;
use App\Http\Controllers\Controller;
use App\Services\SendbirdService;
use Auth;

class RoomCleaningOrderController extends Controller
{
    protected $roomCleaningOrderService;

    public function __construct(RoomCleaningOrderService $roomCleaningOrderService)
    {
        $this->roomCleaningOrderService = $roomCleaningOrderService;
    }

    public function create(SendbirdService $sendbirdService)
    {
        $booking = Auth::user();

        $result = $this->roomCleaningOrderService->createOrder($booking);

        // Notification to admin
        $sendbirdService->sendAdminMessage($booking, [
            'text' => 'Room cleaning service has been requested',
            'payload' => [
                'type' => 'room_cleaning',
                'id' => $result->id,
            ],
        ]);

        return (new RoomCleaningOrderResource($result))
            ->additional([
                'message' => 'Create room cleaning order success'
            ])
            ->response()
            ->setStatusCode(201);
    }

    public function getAllOrders()
    {
        $result = Auth::user()->roomCleaningOrders;

        return (new RoomCleaningOrderResourceCollection($result))
            ->additional([
                'message' => 'Get all room cleaning orders success'
            ])
            ->response()
            ->setStatusCode(201);
    }
}
