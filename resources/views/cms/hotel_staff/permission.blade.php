@extends('cms.layouts.app')

@section('title', __('User Permissions'))

@section('content')
    <div class="row hotelSettingsWrapper">
       
        <div class="col-12">
            <div class="card">
                <div class="card-header">{{ __('Hotel features') }}</div>
                <div class="card-body">
                <form action="{{route('staff_permission.store', ['id' => $user_id])}}" method="post">
                @csrf()
                    <table class="table table-responsive-sm">
                        <tbody>
                        <tr>
                            <td><b>Check in</b></td>
                            <td>
                                <label class="switch switch-pill switch-success">
                                    <input type="checkbox" class="switch-input" name="permission[]" value="check_in" id="is_check_in_enabled" {{  in_array('check_in',$permissions) ? 'checked' : '' }}>
                                    <span class="switch-slider"></span>
                                </label>
                            </td>
                        </tr>
                        
                        <tr>
                            <td><b>Check out</b></td>
                            <td>
                                <label class="switch switch-pill switch-success">
                                    <input type="checkbox" class="switch-input" name="permission[]" value="checkout" id="is_check_out_enabled" {{ in_array('checkout' ,$permissions) ? 'checked' : '' }}>
                                    <span class="switch-slider"></span>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Spa</b></td>
                            <td>
                                <label class="switch switch-pill switch-success">
                                    <input type="checkbox" class="switch-input" name="permission[]" value="spa" id="is_spa_enabled" {{ in_array('spa', $permissions) ? 'checked' : '' }}>
                                    <span class="switch-slider"></span>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><b stlye="padding-left: 20px !important;">Spa Room Service</b></td>
                            <td>
                                <label class="switch switch-pill switch-success">
                                    <input type="checkbox" class="switch-input" name="permission[]" value="spa_room_service" id="spa_room_service" {{ in_array('spa_room_service', $permissions) ? 'checked' : '' }}>
                                    <span class="switch-slider"></span>
                                </label>
                            </td>
                        </tr>

                        <tr>
                            <td><b >Spa Treatment</b></td>
                            <td>
                                <label class="switch switch-pill switch-success">
                                    <input type="checkbox" class="switch-input" name="permission[]" value="spa_treatment" id="spa_treament" {{in_array('spa_treatment', $permissions) ? 'checked' : '' }}>
                                    <span class="switch-slider"></span>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Restaurant</b></td>
                            <td>
                                <label class="switch switch-pill switch-success">
                                    <input type="checkbox" class="switch-input" name="permission[]" value="restaurant" id="is_restaurant_enabled" {{ in_array('restaurant', $permissions) ? 'checked' : '' }}>
                                    <span class="switch-slider"></span>
                                </label>
                            </td>
                        </tr>
                        
                        <tr>
                            <td><b>Experience</b></td>
                            <td>
                                <label class="switch switch-pill switch-success">
                                    <input type="checkbox" class="switch-input" name="permission[]" value="experience" id="is_experience_enabled" {{ in_array('experience', $permissions) ? 'checked' : '' }}>
                                    <span class="switch-slider"></span>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Concierge</b></td>
                            <td>
                                <label class="switch switch-pill switch-success">
                                    <input type="checkbox" class="switch-input" name="permission[]" value="concierge" id="is_concierge_enabled" {{ in_array('concierge', $permissions) ? 'checked' : '' }}>
                                    <span class="switch-slider"></span>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Settings</b></td>
                            <td>
                                <label class="switch switch-pill switch-success">
                                    <input type="checkbox" class="switch-input" name="permission[]" value="settings" id="is_concierge_enabled" {{ in_array('settings', $permissions) ? 'checked' : '' }}>
                                    <span class="switch-slider"></span>
                                </label>
                            </td>
                        </tr>
                        
                        
                        </tbody>
                    </table>
                <div class="float-right">
                    <input type="submit" class="btn btn-primary" value="Save" >
                </div>
                </form>
                </div>
            </div>
        </div>
        
    </div>
@stop

