<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        // For API
        if ($request->expectsJson()) {
            if ($exception instanceof AuthorizationException) {
                throw new CustomAuthorizationException(
                    'This action is unauthorized.',
                    [
                        'unauthorized' => [
                            'This action is unauthorized.'
                        ]
                    ]
                );
            } elseif ($exception instanceof ModelNotFoundException) {
                return response()->json(
                    [
                        'message' => 'The requested data is not available',
                    ],
                    404
                );
            } elseif ($exception instanceof BadRequestHttpException) {
                return response()->json(
                    [
                        'message' => $exception->getMessage(),
                    ],
                    $exception->getStatusCode()
                );
            }
        }

        if ($exception instanceof \Symfony\Component\HttpFoundation\File\Exception\FileException) {
        // create a validator and validate to throw a new ValidationException
        return Validator::make($request->all(), [
            'image' => 'file|size:4096',
        ])->validate();
    }

        return parent::render($request, $exception);
    }
}
