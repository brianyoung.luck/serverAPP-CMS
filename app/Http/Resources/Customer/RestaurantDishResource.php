<?php

namespace App\Http\Resources\Customer;

use Illuminate\Http\Resources\Json\JsonResource;

class RestaurantDishResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'description' => $this->description,
            'price' => $this->price,
            'price' => $this->whenPivotLoaded('restaurant_dish_restaurant_room_service', function () {
                return $this->pivot->price;
            }),
            'qty' => $this->whenPivotLoaded('restaurant_dish_restaurant_room_service', function () {
                return $this->pivot->qty;
            }),
            'note' => $this->whenPivotLoaded('restaurant_dish_restaurant_room_service', function () {
                return $this->pivot->note;
            }),
        ];
    }
}
