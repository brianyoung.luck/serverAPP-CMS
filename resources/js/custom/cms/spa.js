import { Calendar } from "@fullcalendar/core";
import dayGridPlugin from "@fullcalendar/daygrid";
import interactionPlugin from "@fullcalendar/interaction"; // for selectable
import timeGridPlugin from "@fullcalendar/timegrid";

const uploadFileSpaLogoBtn = $("#uploadFileSpaLogoBtn");
const logo = $("#logo");
const editSpaForm = $("#editSpaForm");
const uploadFileSpaImagesBtn = $("#uploadFileSpaImagesBtn");
const spaImages = $("#spaImages");
const editSpaImagesForm = $("#editSpaImagesForm");
const addMenuSpaBtn = $("#addMenuSpaBtn");
const addSpaMenuModal = $(".addSpaMenuModal");
const showDetailsSpaRequestModal = $(".showDetailsSpaRequestModal");
const storeNewSpaMenuBtn = $("#storeNewSpaMenuBtn");
const addSpaMenuForm = $("#addSpaMenuForm");
const editSpaMenuModal = $(".editSpaMenuModal");
const changeSpaName = $("#changeSpaName");

const nameUpdate = $("#nameUpdate");
const descriptionUpdate = $("#descriptionUpdate");
const priceUpdate = $("#priceUpdate");
const treatmentId = $("#treatment_id");
const treatmentIdDelete = $("#treatment_id_delete");
const updateSpaMenuBtn = $("#updateSpaMenuBtn");
const editSpaMenuForm = $("#editSpaMenuForm");
const deleteSpaMenuBtn = $("#deleteSpaMenuBtn");
const deleteSpaMenuForm = $("#deleteSpaMenuForm");
const showDetailsOrderModal = $(".showDetailsOrderModal");
const spaNameText = $("#spaNameText");
const updateType = $("#updateType");
const spaNameUpdate = $("#spaNameUpdate");
const durationUpdate = $("#durationUpdate");
const spaTreatmentIdMenuUpdate = $("#editSpaMenuForm #spa_treatment_id");
const spaTreatmentIdMenuDelete = $("#deleteSpaMenuForm #spa_treatment_id");

const acceptSpaBookingRequestBtn = $("#acceptSpaBookingRequestBtn");
const declineSpaBookingRequestBtn = $("#declineSpaBookingRequestBtn");
const updateSpaRequestForm = $("#updateSpaRequestForm");

let currentSpaBookingId = 0;

$(document).ready(function() {
  // $(".abc").click(function() {
  //   alert();
  // });
  // Append modal to body
  addSpaMenuModal.appendTo("body");
  editSpaMenuModal.appendTo("body");
  showDetailsSpaRequestModal.appendTo("body");

  uploadFileSpaLogoBtn.click(function() {
    logo.trigger("click");
  });
  uploadFileSpaImagesBtn.click(function() {
    spaImages.trigger("click");
  });

  $(".abc").click(function() {
    const spaGalleryId = $(this).data("spa_gallery_id");
    const spaGalleryUrl = $(this).data("spa_gallery_url");

    Swal.fire({
      title: "Delete picture",
      type: "question",
      showCancelButton: true,
      confirmButtonColor: delete_button_color,
      cancelButtonColor: cancel_button_color,
      confirmButtonText: "Delete",
      reverseButtons: true,
      html: `
            <img src="${spaGalleryUrl}" alt="Img" class="w-100 h-25">
        `,
    }).then((result) => {
      if (result.value) {
        $("#deleteSpaGalleryForm #spaGalleryId").val(spaGalleryId);
        $("#deleteSpaGalleryForm").submit();
      }
    });
  });

  logo.change(function() {
    editSpaLogo();
  });
  spaImages.change(function() {
    editSpaImages();
  });
  addMenuSpaBtn.click(function() {
    addSpaMenu();
  });
  storeNewSpaMenuBtn.click(function() {
    if (validateSpaMenuImageSize("add")) {
      storeSpaMenu();
    }
  });
  $(document).on("click", ".menuTreatmentTr", async function() {
    await showModalTreatmentSpa($(this).data("treatment_id"));
  });
  updateSpaMenuBtn.click(function() {
    if (validateSpaMenuImageSize("edit")) {
      updateSpaMenu();
    }
  });
  deleteSpaMenuBtn.click(function() {
    confirmDeleteTreatmentSpa();
  });

  changeSpaName.click(function() {
    changeSpaNameAction();
  });
  $(document).on("click", ".orderWrapper", async function() {
    await showModalOrderDetails($(this).data("spa_booking_id"));
  });
  declineSpaBookingRequestBtn.click(function() {
    declineSpaBookingRequest();
  });
  acceptSpaBookingRequestBtn.click(function() {
    acceptSpaBookingRequest();
  });

  initBookingSpaCalendar();
});

function editSpaLogo() {
  if (validateSpaImageSize()) {
    editSpaForm.submit();
  }
}

function editSpaImages() {
  var resp = validateImagesSize();
  console.log(resp);
  jQuery.each(resp.names, function(i, val) {
    console.log(val.name);
    $(".restImagesErrors").show();
    $(".restImagesErrors").append(
      `<li style="list-style:none;">Error : <b>${
        val.name
      }</b> size is large.</li><br>`
    );
  });

  if (resp.status == true) {
    editSpaImagesForm.submit();
    $(".restImagesErrors").hide();
  }
}

function addSpaMenu() {
  // Show modal
  addSpaMenuModal.modal("show");
}

function storeSpaMenu() {
  storeNewSpaMenuBtn.attr("disabled", true);
  addSpaMenuForm.submit();
}

async function showModalTreatmentSpa(spaTreatmentId) {
  // Show modal
  editSpaMenuModal.modal("show");

  // Call ajax to get data
  const spaTreatment = await getSpaTreatmentDetails(spaTreatmentId);

  // Show guest details
  if (spaTreatment.status) {
    fillSpaTreatmentDetails(spaTreatment.spaTreatmentDetails);
  }
}

async function getSpaTreatmentDetails(spaTreatmentId) {
  let promise = new Promise(function(resolve, reject) {
    $.ajax({
      headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
      },
      type: "GET",
      url: url4 + "/" + spaTreatmentId,
      contentType: "application/json",
      dataType: "json",
    })
      .done(function(response) {
        resolve(response);
      })
      .fail(function(error) {
        console.log(error);
        reject();
      });
  });

  try {
    return await promise;
  } catch (err) {
    return {
      status: false,
      error: err,
    };
  }
}

function fillSpaTreatmentDetails(spaTreatmentDetails) {
  nameUpdate.val(spaTreatmentDetails.name);
  descriptionUpdate.html(spaTreatmentDetails.description);
  durationUpdate.val(spaTreatmentDetails.duration_alt);
  priceUpdate.val(spaTreatmentDetails.price);
  spaTreatmentIdMenuUpdate.val(spaTreatmentDetails.id);
  spaTreatmentIdMenuDelete.val(spaTreatmentDetails.id);
}

function updateSpaMenu() {
  updateSpaMenuBtn.attr("disabled", true);
  editSpaMenuForm.submit();
}

function confirmDeleteTreatmentSpa() {
  Swal.fire({
    title: "Are you sure?",
    type: "question",
    showCancelButton: true,
    confirmButtonColor: delete_button_color,
    cancelButtonColor: cancel_button_color,
    confirmButtonText: "Delete",
    reverseButtons: true,
  }).then((result) => {
    if (result.value && result.value === true) {
      deleteSpaMenuForm.submit();
    }
  });
}

async function showModalOrderDetails(spaBookingId) {
  // Show modal
  showDetailsSpaRequestModal.modal("show");

  // Call ajax to get data
  const orderDetails = await getOrderDetails(spaBookingId);

  // Clear table
  $(".showDetailsSpaRequestModal #spaBookingDetailsTable tbody").html(``);
  const spaBookingDetailsUl = $(
    ".showDetailsSpaRequestModal .spaBookingDetailsUl"
  );
  spaBookingDetailsUl.html(``);

  if (orderDetails.status) {
    const data = orderDetails.spaBooking;

    // Save id
    currentSpaBookingId = data.id;

    // Show order details
    let labelStatus = "";
    if (data.status === "pending") {
      labelStatus = `<span class="badge badge-warning">Pending</span>`;
    } else if (data.status === "accepted") {
      labelStatus = `<span class="badge badge-success">Accepted</span>`;
    } else if (data.status === "rejected") {
      labelStatus = `<span class="badge badge-danger">Rejected</span>`;
    }
    $(".showDetailsSpaRequestModal #roomNoText").html(`
            ${data.hotel_booking.room_number} - Booking detailsss: 
            ${labelStatus}
        `);

    // Show data
    spaBookingDetailsUl.html(`
            <ul>
                <li>
                    Type: ${
                      data.type === "normal_reservation"
                        ? "Reservation"
                        : "Room service"
                    }
                </li>
                <li>
                    ${data.people_number} people
                </li>
                <li>
                    Booking date ${data.booking_date}
                </li>
            </ul>
        `);

    data.spa_treatments.forEach((treatment, index) => {
      $(".showDetailsSpaRequestModal #spaBookingDetailsTable tbody").append(`
                <tr>
                    <td>${index + 1}</td>
                    <td>${treatment.name}</td>
                </tr>
            `);
    });

    // show button action
    switch (data.status) {
      case "pending":
        declineSpaBookingRequestBtn.removeClass("d-none");
        acceptSpaBookingRequestBtn.removeClass("d-none");
        break;
      default:
        declineSpaBookingRequestBtn.addClass("d-none");
        acceptSpaBookingRequestBtn.addClass("d-none");
    }
  } else {
    currentSpaBookingId = 0;
  }
}

async function getOrderDetails(spaBookingId) {
  let promise = new Promise(function(resolve, reject) {
    $.ajax({
      headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
      },
      type: "GET",
      url: url5 + "/" + spaBookingId,
      contentType: "application/json",
      dataType: "json",
    })
      .done(function(response) {
        resolve(response);
      })
      .fail(function(error) {
        console.log(error);
        reject();
      });
  });

  try {
    return await promise;
  } catch (err) {
    return {
      status: false,
      error: err,
    };
  }
}

async function getDishes(category) {
  // Clear html
  $(".menusTable tbody").html("");

  // Call ajax to get data
  const dishes = await getDishesFromAjax(category);

  // Show data
  if (dishes.status) {
    for (const dish of dishes.dishes) {
      $(".menusTable tbody").append(`
                 <tr data-dish_id="${dish.id}" class="menuDishTr">
                    <td>${dish.name} <span class="pull-right">${
        dish.price
      }</span></td>
                </tr>
            `);
    }
  }
}

async function getDishesFromAjax(category) {
  let promise = new Promise(function(resolve, reject) {
    $.ajax({
      headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
      },
      type: "GET",
      url: url4,
      data: {
        category: category,
      },
      contentType: "application/json",
      dataType: "json",
    })
      .done(function(response) {
        resolve(response);
      })
      .fail(function(error) {
        console.log(error);
        reject();
      });
  });

  try {
    return await promise;
  } catch (err) {
    return {
      status: false,
      error: err,
    };
  }
}

async function getReservationDetails(reservationId) {
  let promise = new Promise(function(resolve, reject) {
    $.ajax({
      headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
      },
      type: "GET",
      url: url6 + "/" + reservationId,
      contentType: "application/json",
      dataType: "json",
    })
      .done(function(response) {
        resolve(response);
      })
      .fail(function(error) {
        console.log(error);
        reject();
      });
  });

  try {
    return await promise;
  } catch (err) {
    return {
      status: false,
      error: err,
    };
  }
}

function changeSpaNameAction() {
  Swal.fire({
    title: "Change spa name",
    type: "question",
    showCancelButton: true,
    confirmButtonColor: success_button_color,
    cancelButtonColor: cancel_button_color,
    confirmButtonText: "Save",
    reverseButtons: true,
    input: "text",
    inputValue: spaNameText.html(),
    inputValidator: (value) => {
      if (!value) {
        return "You need to write something!";
      }
    },
  }).then((result) => {
    if (result.value) {
      console.log(result);
      updateType.val("changeSpaName");
      spaNameUpdate.val(result.value);
      editSpaForm.submit();
    }
  });
}

function declineSpaBookingRequest() {
  Swal.fire({
    title: "Are you sure?",
    type: "question",
    showCancelButton: true,
    confirmButtonColor: delete_button_color,
    cancelButtonColor: cancel_button_color,
    confirmButtonText: "Reject",
    reverseButtons: true,
  }).then((result) => {
    if (result.value && result.value === true) {
      $("#updateSpaRequestForm #spa_booking_id").val(currentSpaBookingId);
      $("#updateSpaRequestForm #status").val("rejected");
      updateSpaRequestForm.submit();
    }
  });
}

function acceptSpaBookingRequest() {
  Swal.fire({
    title: "Are you sure?",
    type: "question",
    showCancelButton: true,
    confirmButtonColor: success_button_color,
    cancelButtonColor: cancel_button_color,
    confirmButtonText: "Accept",
    reverseButtons: true,
  }).then((result) => {
    if (result.value && result.value === true) {
      $("#updateSpaRequestForm #spa_booking_id").val(currentSpaBookingId);
      $("#updateSpaRequestForm #status").val("accepted");
      updateSpaRequestForm.submit();
    }
  });
}

function initBookingSpaCalendar() {
  const calendarEl = document.getElementById("bookingSpaCalendar");

  const calendar = new Calendar(calendarEl, {
    plugins: [interactionPlugin, dayGridPlugin, timeGridPlugin],
    defaultView: "dayGridMonth",
    selectable: true,
    navLinks: true,
    // displayEventEnd: true,
    eventLimit: true,
    displayEventTime: true,
    header: {
      right: "prev,next today",
      left: "title",
      center: "",
      // right:  'dayGridMonth,timeGridWeek,timeGridDay'
    },
    eventClick: async function(info) {
      // console.log(info.event.start);
      // console.log(info.event.id);
      await showModalOrderDetails(info.event.id);
    },
    eventSources: [
      // your event source
      {
        url: "/ajax/get-spa-bookings",
        method: "GET",
        extraParams: {
          spa_id: id1,
          status: "pending",
        },
        failure: function() {
          alert("There was an error while fetching events!");
        },
        backgroundColor: "#6d6d6d",
        borderColor: "#6d6d6d",
        textColor: "#000000",
      },
      {
        url: "/ajax/get-spa-bookings",
        method: "GET",
        extraParams: {
          spa_id: id1,
          status: "accepted",
        },
        failure: function() {
          alert("There was an error while fetching events!");
        },
        backgroundColor: "#5ab9ea",
        borderColor: "#5ab9ea",
        textColor: "#ffffff",
      },
      {
        url: "/ajax/get-spa-bookings",
        method: "GET",
        extraParams: {
          spa_id: id1,
          status: "rejected",
        },
        failure: function() {
          alert("There was an error while fetching events!");
        },
        backgroundColor: "#f86c6b",
        borderColor: "#f86c6b",
        textColor: "#ffffff",
      },
    ],
    eventTimeFormat: {
      hour: "2-digit",
      minute: "2-digit",
      hour12: true,
    },
    timeZone: "UTC",
  });

  calendar.render();
}

$(".spaAddMenuImage").change(function() {
  validateSpaMenuImageSize("add");
});
$(".spaEditMenuImage").change(function() {
  validateSpaMenuImageSize("edit");
});

function validateSpaImageSize() {
  var file_size = $("#logo")[0].files[0].size;
  if (file_size > 4097152) {
    $(".file_error").show();
    $(".file_error p").html("Image is too large.");
    return false;
  }
  $(".file_error").hide();
  return true;
}
function validateSpaMenuImageSize(type) {
  if (type == "edit") {
    var file_size = $(".spaEditMenuImage")[0].files[0].size;
  } else {
    var file_size = $(".spaAddMenuImage")[0].files[0].size;
  }
  if (file_size > 4097152) {
    if (type == "edit") {
      $(".edit_file_error").show();
      $(".edit_file_error p").html("Image is too large.");
    } else {
      $(".add_file_error").show();
      $(".add_file_error p").html("Image is too large.");
    }
    return false;
  } else {
    if (type == "edit") {
      $(".edit_file_error").hide();
    } else {
      $(".add_file_error").hide();
    }
    return true;
  }
}

function validateImagesSize() {
  var fls = 0;
  console.log($("#spaImages").get(0).files);
  var imagesName = [];
  for (var i = 0; i < $("#spaImages").get(0).files.length; ++i) {
    var imageName = $("#spaImages").get(0).files[i].name;
    var file_size = $("#spaImages").get(0).files[i].size;
    console.log(file_size);
    if (file_size > 4097152) {
      var data = {};
      data.name = imageName;
      fls = fls + 1;
      imagesName.push(data);
    }
  }
  if (fls > 0) {
    $("#spaImages").val("");
    return { status: false, names: imagesName };
    return false;
  } else {
    return { status: true, names: imagesName };
  }
}
