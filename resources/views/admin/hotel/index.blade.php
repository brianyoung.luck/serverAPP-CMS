@extends('admin.layouts.app')

@section('title', __('Hotels'))

@section('content')
    <div id="hotelsWrapper" >
        <div class="row">
            <h4>
                New hotel
            </h4>
            <div class="col-12 p-2">
                <a class="btn mb-4 btn-shadow" href="{{ route('admin.hotels.create') }}">Create new hotel</a>
            </div>
        </div>
        <div class="row">
            <h4>
                Hotels:
            </h4>
            <div class="col-12 p-2">
                <div class="card">
                    <table class="table table-responsive-sm table-striped" id="hotelTable">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Status</th>
                            <th>Expired at</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>
        const url1 = '{!! route('data-tables.admin.hotels') !!}';
        const url2 = '{!! route('admin.hotels.index') !!}';

    </script>
    <script src="{{ asset('js/custom/admin/hotel.js') }}"></script>
@stop