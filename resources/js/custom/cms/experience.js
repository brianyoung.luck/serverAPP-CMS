const addBtn = $("#addButton");
const addBtnModal = $(".addButtonModal");
var isImageValidated = false;
const selectedableImg = $(".selectable_image");
const selectBtnIcon = $("#selectBtnIconWrapper");
const btnSelectedImg = $("#btnSelectedImg");
const chooseIconModal = $(".chooseIconModal");
var selectedImgsWrapper = $("#selectedImgWrapper");
const cancelBtn = $("#cancelBtn");
//Form Inputs
const uploadImg = $("#btnImages");
const btnType = $("#btnType");
const saveBtn = $("#saveBtn");
const addBtnForm = $("#addBtnForm");
const imageUploadBtn = $("#imageUploadBtn");

//Promotions Element
const used = $("#used");
const maxLimit = $("#maxLimit");
const addNewPromotionModal = $(".addNewPromotionModal");
const promotion_title = $("#promotion_title"),
  promotion_amount = $("#promotion_amount"),
  promotion_description = $("#promotion_description"),
  promotion_code = $("#promotion_code"),
  promotion_url = $("#promotion_url"),
  promotion_image = $("#promotion_image"),
  promotion_images_wrapper = $("#promotion_images_wrapper");
promotion_item_id = $("#promotion_item_id");
const addPromotionItem = $("#addPromotionItem");
const addpromotionBtn = $("#addPromotion");
const promotion_items_wrapper = $("#promotion_items_wrapper");
const promotion_item_form = $("#promotion_item_form");
const promotion_image_preview = $(".promotion_image_preview");
//Custom Data

var promotion_items = [];
var uploaded_images = [5];

$(document).ready(function() {
  addBtnModal.appendTo("body");
  chooseIconModal.appendTo("body");
  addNewPromotionModal.appendTo("body");
  cancelBtn.click(function() {
    $(addBtnModal).modal("toggle");
    window.location.reload();
  });
  addBtn.click(function() {
    //uploaded_images = [];
    //$("#button_id").val("-1");
    $(addBtnModal).modal("toggle");
  });

  selectBtnIcon.click(function(event) {
    addBtnModal.modal("hide");
    chooseIconModal.modal("toggle");
  });

  selectedableImg.click(function(event) {
    addBtnModal.modal("show");
    chooseIconModal.modal("toggle");
    $("#selectedIcon_Name").val($(this).attr("data-imgname"));
    btnSelectedImg.attr(
      "src",
      "/icons/" + $(this).attr("data-imgname") + ".png"
    );
  });
  uploadImg.change(function(event) {
    readURL(this, selectedImgsWrapper, "button_image_preview");
  });

  btnType.change(function(event) {
    let val = btnType.val();
    $(".option").hide();
    if (val == "Promotion") {
      $("#promotion_data").show();
    } else if (val == "Custom") {
      $("#custom_data").show();
    } else if (val == "Custom_Image") {
      $("#custom_image_data").show();
    } else if (val == "Shuttle") {
      //$("#map_image_upload").show();
    } else if (val == "Map") {
      $("$map_image_2").show();
    }
  });

  imageUploadBtn.click(function(event) {
    btnImages.click();
  });

  promotion_image.change(function(event) {
    //Todo: Upload To Server and get stored id
    //Needs to be fixed
    /*var formData = new FormData(document.getElementById("promotion_item_form"));
		console.log("Form Data: ",formData);
		debugger;
		console.log("files: ",($(promotion_image))[0].files);
		if(promotion_item_id.val() == "-1"){
			formData.append("index", promotion_items.length);
		}else{
			formData.append("index", promotion_item_id.val());
		}
		//formData.append("files[]",($(promotion_image))[0].files);
		console.log("Form Data After: ",formData);
		$.ajax({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
            },
			type: 'POST',
			url: savePromotionItem,
			data: formData,
			processData: false,
			contentType: false,
			enctype: 'multipart/form-data',
			success: function(response){
				console.log(response.data);
			}
		});
    */
    if (validateExperienceImageSize) {
      readURL(this, promotion_images_wrapper, "promotion_image_preview");
    }
  });

  addPromotionItem.click(function(event) {
    promotion_item_id.val("-1");
    addNewPromotionModal.modal("toggle");
  });

  addpromotionBtn.click(function(event) {
    //Send the data to the server
    console.log("Hello");
    // debugger;
    if (promotion_item_id.val() == "-1") {
      let index = promotion_items.length;
      var html = `<tr>
				<td id="pitem_${index}">${promotion_title.val()}</td>
				<td><a href="#"  class="btn btn-sm btn-primary promotion_item" data-index="${index}" data-id="0">Edit</a></td>
			</tr>
			`;
      promotion_items.push(updatePromotionItem_Obj({}));
      promotion_items_wrapper.append(html);
    } else {
      //Todo update the existing item in the DB
      let index = parseInt(promotion_item_id.val());
      var updatedObj = updatePromotionItem_Obj(promotion_items[index]);
      promotion_items[index] = updatedObj;
      $("#pitem_" + index).html(promotion_title.val());
    }
    clearPromotionItem_Form();
    clearFrom();
    addNewPromotionModal.modal("toggle");
  });

  //Dynamic items handler
  $(document).on("click", ".promotion_item", function() {
    // debugger;
    //Todo: Show the selected images preview
    let index = parseInt($(this).attr("data-index"));
    var obj = promotion_items[index];
    promotion_item_id.val(index);
    promotion_title.val(obj.title);
    promotion_amount.val(obj.promotion_amount);
    promotion_description.val(obj.promotion_description);
    promotion_code.val(obj.promotion_code);
    promotion_url.val(obj.redeem_link);
    addNewPromotionModal.modal("toggle");
  });

  saveBtn.click(function(event) {
    // debugger;
    let btnTypeVal = btnType.val();
    var encodedJsonData = "";
    if (btnTypeVal == "Promotion") {
      encodedJsonData = JSON.stringify(updatePromotionItem_Obj({}));
    } else if (btnTypeVal == "Custom") {
      let text = $("#custom_data_txt").val();
      let title = $("#custom_button_title").val();
      let link = $("#custom_button_link").val();

      var obj = {
        sub_title: title,
        text: text,
        redeem_link: link,
      };
      encodedJsonData = JSON.stringify(obj);
    } else if (btnTypeVal == "Custom_Image") {
      let title = $("#custom_image_title").val();
      let link = $("#custom_image_link").val();

      var obj = {
        sub_title: title,
        redeem_link: link,
      };
      encodedJsonData = JSON.stringify(obj);
    }
    $("input[name=images_id]").val(uploaded_images.join(","));
    $("input[name=encoded]").val(encodedJsonData);
    addBtnForm.submit();
  });
  $(".imageWrapper").click(function() {
    // debugger;
    let index = parseInt($(this).attr("data-index"));
    let dbId = parseInt($(this).attr("data-id"));
    let imgSrc = $(this)
      .children("img")
      .attr("src");
    let element = $(this);
    Swal.fire({
      title: "Delete picture",
      type: "question",
      showCancelButton: true,
      confirmButtonColor: delete_button_color,
      cancelButtonColor: cancel_button_color,
      confirmButtonText: "Delete",
      reverseButtons: true,
      html: `
				<img src="${imgSrc}" alt="Img" class="w-100 h-25">
			`,
    }).then((result) => {
      if (result.value) {
        console.log("url: ", deleteBtnImg + "/" + dbId);
        $.ajax({
          type: "GET",
          headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
          },
          url: deleteBtnImg + "/" + dbId,
          success: function(response) {
            $(element).remove();
            uploaded_images.splice(index, 1);
          },
        });
      }
    });
  });

  $(".existing_btn").click(function() {
    let id = $(this).attr("data-id");
    $("#button_id").val(id);

    $.ajax({
      headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
      },
      type: "GET",
      url: endPoint + "/" + id,
      success: function(response) {
        // debugger;
        addBtnModal.modal("toggle");
        btnType.val(response.type);
        btnType.trigger("change");

        $("input[name=icon]").val(response.icon);
        btnSelectedImg.attr("src", "/icons/" + response.icon + ".png");
        $("#selectedIcon_Name").val(response.icon);
        $("#btnTitle").val(response.title);

        if (response.type == "Promotion") {
          var obj = JSON.parse(response.data);
          promotion_title.val(obj.title);
          promotion_amount.val(obj.promotion_amount);
          promotion_description.val(obj.promotion_description);
          promotion_code.val(obj.promotion_code);
          promotion_url.val(obj.redeem_link);
        } else if (response.type == "Custom") {
          var obj = JSON.parse(response.data);
          $("#custom_button_title").val(obj.sub_title);
          $("#custom_data_txt").val(obj.text);
          $("#custom_button_link").val(obj.link);
        } else if (response.type == "Custom_Image") {
          var obj = JSON.parse(response.data);
          $("#custom_image_title").val(obj.sub_title);
          $("#custom_image_link").val(obj.link);
        }
        let indexStart = 0;
        clearSelectedImgs();
        for (image_index in response.images) {
          let image = response.images[image_index];
          uploaded_images.push(image.id);
          addImageInWrapper(
            image.path,
            { index: indexStart, id: image.id },
            selectedImgsWrapper
          );
          indexStart++;
        }
        $("#images_id").val(uploaded_images.join(","));
      },
    });
  });
});

function updatePromotionItem_Obj(obj) {
  obj["title"] = promotion_title.val();
  obj["promotion_amount"] = promotion_amount.val();
  obj["promotion_description"] = promotion_description.val();
  obj["promotion_code"] = promotion_code.val();
  obj["redeem_link"] = promotion_url.val();
  return obj;
}

function clearPromotionItem_Form() {
  promotion_title.val("");
  promotion_amount.val("");
  promotion_description.val("");
  promotion_code.val("");
  promotion_url.val("");
  promotion_images_wrapper.html("");
}

function addImageInWrapper(src, props, wrapper) {
  //   debugger;
  let index = props.index ? 'data-index="' + props.index + '"' : " ";
  if ($(".newAdded:last").attr("data-index") > 2) {
    $("#selectedImgWrapper").addClass("imagesWrapperExperience");
  }
  let id = props.id ? 'data-id="' + props.id + '"' : "";

  wrapper.append(
    '<div class="imageWrapper newAdded" ' +
      id +
      " " +
      index +
      '><img src="' +
      src +
      '" width="75" height="75"/></div>'
  );
  //selectedImgsWrapper.append(`<img src="${src}"  ${props.id ? 'data-id="'+props.id+'"' : ''} onclick="showImg(this)"`);
}
function clearSelectedImgs() {
  $(".imageWrapper.newAdded").remove();
}

function uploadImage(files) {
  // define data and connections
  var formData = new FormData($(addBtnForm)[0]);
  $.ajax({
    headers: {
      "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
    },
    type: "POST",
    url: savePromotionItem,
    data: formData,
    processData: false,
    contentType: false,
    enctype: "multipart/form-data",
    success: function(response) {
      //TODO: append all those images into the container with their respective ID
      //   debugger;
      console.log("Response from image upload: ", response);
      let indexStart = uploaded_images.length;
      for (index in response) {
        // debugger;
        let image = response[index];
        addImageInWrapper(
          image.path,
          { index: indexStart, id: image.id },
          selectedImgsWrapper
        );
        uploaded_images.push(response[index].id);
      }
    },
  });
}

function deleteImg() {}

function showImg(element) {}

function validateExperienceImageSize() {
  for (var i = 0; i < $("#btnImages").get(0).files.length; ++i) {
    var file1 = $("#btnImages").get(0).files[i].name;
    var file_size = $("#btnImages").get(0).files[i].size;
    if (file_size > 4097152) {
      $(".file_error").show();
      $("#saveBtn").attr("disabled", true);
      $("#imageUploadBtn").attr("data-status", "0");
      return false;
    } else {
      $(".file_error").hide();
      $("#saveBtn").attr("disabled", false);
      $("#imageUploadBtn").attr("data-status", "1");
      return true;
    }
  }
}

function validateImagesSize() {
  var fls = 0;
  console.log($("#btnImages").get(0).files);
  var imagesName = [];
  for (var i = 0; i < $("#btnImages").get(0).files.length; ++i) {
    var imageName = $("#btnImages").get(0).files[i].name;
    var file_size = $("#btnImages").get(0).files[i].size;
    console.log(file_size);
    if (file_size > 4097152) {
      var data = {};
      data.name = imageName;
      fls = fls + 1;
      imagesName.push(data);
    }
  }
  if (fls > 0) {
    $("#btnImages").val("");
    return { status: false, names: imagesName };
  } else {
    return { status: true, names: imagesName };
  }
}

function readURL(input, wrapper, imgClass) {
  //   debugger;
  if (input.files) {
    //clearSelectedImgs();
    // debugger;

    var resp = validateImagesSize();
    console.log(resp);
    jQuery.each(resp.names, function(i, val) {
      console.log(val.name);
      $(".restImagesErrors").show();
      $(".restImagesErrors").append(
        `<li style="list-style:none;">Error : <b>${
          val.name
        }</b> size is large.</li><br>`
      );
    });

    if (resp.status == true) {
      uploadImage(input.files);
      $(".restImagesErrors").hide();
    }

    // if (validateExperienceImageSize()) {
    //   uploadImage(input.files);
    // }
  }
}

$("#clearBtn").on("click", function() {
  document.getElementById("addBtnForm").reset();

  $(".imageWrapper img").attr("src", "");
  $(".imageWrapper img").hide();
  $("#selectedImageText img").attr("src", "");
  $("#selectedImageText img").romove();
  $(".restImagesErrors").css("display", "none");
  $("#selectedImageText").append('<img id="btnSelectedImg">');
});
