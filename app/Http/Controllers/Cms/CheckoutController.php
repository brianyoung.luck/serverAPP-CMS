<?php

namespace App\Http\Controllers\Cms;

use App\Models\HotelBooking;
use App\Services\HotelBookingService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CheckoutController extends Controller
{
    private $hotelBookingService;

    public function __construct(HotelBookingService $hotelBookingService)
    {
        $this->hotelBookingService = $hotelBookingService;
    }

    public function index()
    {
        return view('cms.checkout.index');
    }

    public function update(Request $request)
    {
        $hotelBookingId = $request->get('hotelBookingId', 0);
        $hotelBooking = HotelBooking::findOrFail($hotelBookingId);

        switch ($request->get('updateType', 'update')) {
            case 'checkout':
                $this->hotelBookingService->update($hotelBooking, [
                    'status' => 'checked_out',
                ]);
                return redirect()->route('cms.checkouts.index')->with('success', 'Checkout guest success.');
                break;
            case 'lateCheckout':
                $this->hotelBookingService->update($hotelBooking, [
                    'late_check_out' => $request->get('statusLateCheckout'),
                ]);
                return redirect()->route('cms.checkouts.index')->with('success', 'Late checkout guest success.');
                break;
            default:
        }
    }
}
