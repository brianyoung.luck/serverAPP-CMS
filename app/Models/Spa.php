<?php

namespace App\Models;

use App\Services\FileStorageService;
use Illuminate\Database\Eloquent\Model;

class Spa extends Model
{
    protected $fillable = [
        'hotel_id',
        'name',
        'logo_url',
    ];

    /**
     * Getter
     */

    public function getLogoUrlAttribute($value)
    {
        $fileStorageService = new FileStorageService();
        $storageServiceName = $fileStorageService->getStorageServiceName($value);
        switch ($storageServiceName) {
            case 's3':
                return $fileStorageService->getTemporaryUrl($value);
                break;
            default:
                return $value;
        }
    }

    public function treatments()
    {
        return $this->hasMany(SpaTreatment::class);
    }

    public function spaGalleries()
    {
        return $this->hasMany(spaGallery::class, 'spa_id', 'id');
    }
}