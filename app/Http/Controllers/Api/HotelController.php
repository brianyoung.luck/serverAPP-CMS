<?php

namespace App\Http\Controllers\Api;

use App\Services\HotelService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MobileHotelLayout;

class HotelController extends Controller
{
    protected $hotelService;

    public function __construct(HotelService $hotelService)
    {
        $this->hotelService = $hotelService;
    }

    public function getByCode(Request $request)
    {
        $hotelCode = $request->code;

        $hotel = $this->hotelService->getByCode($hotelCode);
        // if (!$hotel->mobile_hotel_layout_id) {
        //     $hotel->mobile_hotel_layout_id=1;
        //     $hotel->save();
        // }
        $mobileHotelTheme = $hotel->mobileHotelTheme;
        $mobileHotelLayout = MobileHotelLayout::where('hotel_id', $hotel->id)->orWhere('hotel_id', 0)->get();
        $hotelFeature = $hotel->hotelFeature;

        return response()->json([
            'message' => 'Get hotel success',
            'data' => [
                'id' => $hotel->id,
                'code' => $hotel->code,
                'name' => $hotel->name,
                'description' => $hotel->description,
                'hotel_logo_lg' => $hotel->hotel_logo_lg,
                'hotel_logo_md' => $hotel->hotel_logo_md,
                'hotel_logo_sm' => $hotel->hotel_logo_sm,
                'currency' => $hotel->currency,
                'category' => $hotel->category,
                'layout' => [
                    'theme' => [
                        'primary_color' => $mobileHotelTheme->primary_color,
                        'secondary_color' => $mobileHotelTheme->secondary_color,
                    ],
                    'icons' => [
                        'check_in_color' => $mobileHotelTheme->check_in_color,
                        'check_out_color' => $mobileHotelTheme->check_out_color,
                        'restaurant_color' => $mobileHotelTheme->restaurant_color,
                        'spa_color' => $mobileHotelTheme->spa_color,
                        'concierge_color' => $mobileHotelTheme->concierge_color,
                        'cleaning_color' => $mobileHotelTheme->cleaning_color,
                    ],
                ],
                'mobile_hotel_layouts' =>$mobileHotelLayout,
                'hotel_features' => [
                    'is_check_in_enabled' => $hotelFeature->is_check_in_enabled,
                    'is_check_out_enabled' => $hotelFeature->is_check_out_enabled,
                    'is_spa_enabled' => $hotelFeature->is_spa_enabled,
                    'is_restaurant_enabled' => $hotelFeature->is_restaurant_enabled,
                    'is_concierge_enabled' => $hotelFeature->is_concierge_enabled,
                    'is_cleaning_enabled' => $hotelFeature->is_cleaning_enabled,
                    'is_spa_treatment' => $hotelFeature->spa_treatment,
                    'is_spa_room_service' => $hotelFeature->spa_room_service,
                    'is_experience' => $hotelFeature->is_experience
                ],
            ],
        ]);
    }
}