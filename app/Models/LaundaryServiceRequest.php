<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConciergeServiceRequest extends Model
{
    protected $fillable = [
        'hotel_booking_id',
    ];

    public function services()
    {
        return $this->belongsToMany(LaundaryService::class)->withPivot(['qty', 'note']);
    }

    public function hotelBooking()
    {
        return $this->belongsTo(HotelBooking::class, 'hotel_booking_id', 'id');
    }

    public function conciergeServices()
    {
        return $this->belongsToMany(LaundaryService::class)->withPivot(['qty', 'note']);
    }
}