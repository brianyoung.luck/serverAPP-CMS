<?php

namespace App\Http\Controllers\Cms;

use App\Models\SpaBooking;
use App\Services\BookingBillService;
use App\Services\SendbirdService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class SpaBookingController extends Controller
{
    public function ajaxGetSpaBookings(Request $request)
    {
        return [
            'status' => true,
        ];
    }

    public function ajaxGetSpaBooking(Request $request, $spaBookingId)
    {
        // Get details
        $spaBooking = SpaBooking::where('id', $spaBookingId)
            ->with([
                'hotelBooking',
                'spaTreatments',
            ])
            ->first();
        if (!$spaBooking) {
            return [
                'status' => false,
                'spaBooking' => null,
            ];
        }

        return [
            'status' => true,
            'spaBooking' => $spaBooking,
        ];
    }

    public function update(Request $request, $spaBookingId, SendbirdService $sendbirdService, BookingBillService $bookingBillService)
    {
        $status = $request->get('status', 'rejected');

        $spaBooking = SpaBooking::where('id', $request->get('spa_booking_id', 0))
            ->first();

        DB::transaction(function () use ($spaBooking, $status, $bookingBillService) {
            $spaBooking->status = $status;
            $spaBooking->save();

            if ($spaBooking->status == 'accepted') {
                $bookingBillService->addToBill($spaBooking);
            }
        });

        // Notification via sendbird
        if ($spaBooking->status == 'accepted') {
            $sendbirdService->sendAdminMessage($spaBooking->hotelBooking, [
                'text' => 'Your spa reservation has been accepted.',
                'payload' => [
                    'type' => 'spa_booking_accepted',
                    'id' => $spaBooking->id,
                ],
            ]);
        }

        return redirect()->route('cms.spas.show', ['spa' => $spaBooking->spa_id])->with('success', 'Success.');
    }

    public function ajaxGetSpaBooking2(Request $request)
    {
        $status = $request->query('status');
        $spaId = $request->query('spa_id');
        $startDate = Carbon::parse($request->query('start'));
        $endDate = Carbon::parse($request->query('end'));

        $spaBookings = SpaBooking::where('spa_id', $spaId)
            ->where('status', $status)
            ->where('booking_date', '>=', $startDate->toDateTimeString())
            ->where('booking_date', '<=', $endDate->toDateTimeString())
            ->with([
                'hotelBooking',
            ])
            ->get();

        $result = [];
        foreach ($spaBookings as $spaBooking) {
            array_push($result, [
                'id' => $spaBooking->id,
                'title' => data_get($spaBooking->hotelBooking, 'room_number', ''),
                'start' => Carbon::create($spaBooking->booking_date),
                'end' => Carbon::create($spaBooking->booking_date)->addSecond(),
                'allDay' => false
            ]);
        }

        return $result;
    }
}
