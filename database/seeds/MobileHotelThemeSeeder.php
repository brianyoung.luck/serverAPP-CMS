<?php

use App\Models\MobileHotelTheme;
use Illuminate\Database\Seeder;

class MobileHotelThemeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Default
        MobileHotelTheme::create([
            'primary_color' => '#72D7FF',
            'secondary_color' => '#5AB9EA',
            'check_in_color' => '#379683',
            'check_out_color' => '#557A95',
            'restaurant_color' => '#557A95',
            'spa_color' => '#B1A296',
            'concierge_color' => '#5D5C61',
            'cleaning_color' => '#7395AE',
            'photo_preview_url' => 'https://servrhotels.s3-ap-southeast-1.amazonaws.com/appearances/themes/1.png',
        ]);

        // Theme 1
        MobileHotelTheme::create([
            'primary_color' => '#5D5C61',
            'secondary_color' => '#7395AE',
            'check_in_color' => '#379683',
            'check_out_color' => '#557A95',
            'restaurant_color' => '#557A95',
            'spa_color' => '#B1A296',
            'concierge_color' => '#5D5C61',
            'cleaning_color' => '#7395AE',
            'photo_preview_url' => 'https://servrhotels.s3-ap-southeast-1.amazonaws.com/appearances/themes/theme1.png',
        ]);

        // Theme 2
        MobileHotelTheme::create([
            'primary_color' => '#9A1750',
            'secondary_color' => '#9A1750',
            'check_in_color' => '#5D001E',
            'check_out_color' => '#E3E2DF',
            'restaurant_color' => '#E3AFBC',
            'spa_color' => '#9A1750',
            'concierge_color' => '#EE4C7C',
            'cleaning_color' => '#9A1750',
            'photo_preview_url' => 'https://servrhotels.s3-ap-southeast-1.amazonaws.com/appearances/themes/theme2.png',
        ]);

        // Theme 3
        MobileHotelTheme::create([
            'primary_color' => '#C0B283',
            'secondary_color' => '#C0B283',
            'check_in_color' => '#DCD0C0',
            'check_out_color' => '#F4F4F4',
            'restaurant_color' => '#373737',
            'spa_color' => '#F3C68E',
            'concierge_color' => '#F3C68E',
            'cleaning_color' => '#727272',
            'photo_preview_url' => 'https://servrhotels.s3-ap-southeast-1.amazonaws.com/appearances/themes/theme3.png',
        ]);
    }
}
