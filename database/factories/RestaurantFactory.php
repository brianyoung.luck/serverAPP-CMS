<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Restaurant;
use Faker\Generator as Faker;

$factory->define(Restaurant::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'logo_url' => 'https://marketplace.canva.com/MAC5oKacMGY/1/0/thumbnail_large-5/canva-black-with-utensils-icon-restaurant-logo-MAC5oKacMGY.jpg',
    ];
});
