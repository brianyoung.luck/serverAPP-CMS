<?php

namespace App\Http\Controllers\Cms;

use App\Services\SpaService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SpaGallery;
use Response;
use Illuminate\Support\Facades\Input;

class SpaGalleryController extends Controller
{
    private $restaurantService;

    public function __construct(SpaService $spaService)
    {
        $this->spaService = $spaService;
    }

    public function destroy(Request $request)
    {
        $this->spaService->deleteSpaGallery($request->get('spaGalleryId', 0));
        return redirect()->route('cms.spas.show', ['spa' => $request->get('spaId', 0)])->with('success', 'Image deleted success.');
    }

    public function sortGallery()
    {
        $val = Input::get('sorted');
        $i = 0;
        foreach ($val as $id) {
            $gallery = SpaGallery::where('id', $id)->first();
            $gallery->sort_id = $i++;
            $gallery->save();
        }
        
        return "done";
    }
}