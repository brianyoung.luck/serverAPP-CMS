<?php

namespace App\Models;

use App\Services\FileStorageService;
use Illuminate\Database\Eloquent\Model;


class RestaurantGallery extends Model
{
    protected $fillable = [
        'restaurant_id',
        'image_url',
    ];

    /**
     * Getter
     */

    public function getImageUrlAttribute($value)
    {
        return $this->getS3Url($value);
    }

    /**
     * Create presign url
     * @param $value
     * @return string|null
     */
    private function getS3Url($value)
    {
        $fileStorageService = new FileStorageService(env('FILESYSTEM_DRIVER'));
        $storageServiceName = $fileStorageService->getStorageServiceName($value);
        switch ($storageServiceName) {
            case 's3':
                return $fileStorageService->getTemporaryUrl($value);
                break;
            default:
                return $value;
        }
    }
}
