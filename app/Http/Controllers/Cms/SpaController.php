<?php

namespace App\Http\Controllers\Cms;

use App\Http\Requests\StoreSpaTreatmentRequest;
use App\Models\Spa;
use App\Models\SpaGallery;

use App\Models\SpaBooking;
use App\Models\SpaTreatment;
use App\Services\FileStorageService;
use App\Services\SpaService;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class SpaController extends Controller
{
    private $spaService;

    public function __construct(SpaService $spaService)
    {
        $this->spaService = $spaService;
    }

    public function show($spaId)
    {
        $spa = $this->spaService->getSpa($spaId);

//        $pendingSpaBookings = SpaBooking::where('status', 'pending')
//            ->whereHas('hotelBooking', function (Builder $query) {
//                $query->where('hotel_id', auth('hotel')->user()->id);
//            })
//            ->with([
//                'hotelBooking',
//                'spaTreatments',
//            ])
//            ->get();
//        $todaySpaBookings = SpaBooking::where('status', 'accepted')
//            ->whereHas('hotelBooking', function (Builder $query) {
//                $query->where('hotel_id', auth('hotel')->user()->id);
//            })
//            ->whereDate('booking_date', now())
//            ->where('booking_date', '>', now()->toDateTimeString())
//            ->with([
//                'hotelBooking',
//                'spaTreatments',
//            ])
//            ->orderBy('booking_date', 'asc')
//            ->get();
//        $doneSpaBookings = SpaBooking::whereHas('hotelBooking', function (Builder $query) {
//                $query->where('hotel_id', auth('hotel')->user()->id);
//            })
//            ->where(function ($query) {
//                // Accept and pas
//                $query->where('status', 'accepted')
//                    ->where('booking_date', '<', now()->toDateTimeString());
//            })
//            ->orWhere(function ($query) {
//                // All reject
//                $query->where('status', 'rejected');
//            })
//            ->with([
//                'hotelBooking',
//                'spaTreatments',
//            ])
//            ->orderBy('booking_date', 'desc')
//            ->get();
        $gallery=SpaGallery::where('spa_id', $spaId)->get();
        return view('cms.spas.show', [
            'spa' => $spa,
            'galleries'=>$gallery
//            'pendingSpaBookings' => $pendingSpaBookings,
//            'todaySpaBookings' => $todaySpaBookings,
//            'doneSpaBookings' => $doneSpaBookings,
        ]);
    }

    public function update(Request $request, $spaId)
    {
        $updateType = $request->get('updateType');
        switch ($updateType) {
            case 'changeSpaName':
                $spa = $this->spaService->updateSpa($spaId, [
                    'name' => $request->get('spaNameUpdate'),
                ]);
                return redirect()->route('cms.spas.show', ['spa' => $spa->id])->with('success', 'Update spa success.');
                break;
        }

        // Upload Spa images
        $spaImages = $request->file('spaImages');
        if ($spaImages) {
            // Validate Restaurant Image
            $request->validate(
                [ 'spaImages.*' =>  'nullable|file|mimes:png,jpg,jpeg|max:4096'],
                [ 'spaImages.*.mimes' => 'Please upload only png or jpg file.',
                 'spaImages.*.max' => 'Image is too large.'
                 ]
            );

            $this->spaService->updateSpaImages($spaId, $spaImages);
        }

        // Update logo if image included
        $logoFile = $request->file('logo');
        if ($logoFile) {
            // Validate Image
            $request->validate(
                [ 'logo' =>  'nullable|file|mimes:png,jpg,jpeg|max:4096'],
                [ 'logo.mimes' => 'Please upload only png or jpg file.',
                 'logo.max' => 'Image is too large.'
                 ]
            );
            $this->spaService->updateLogo($spaId, $logoFile);
        }
        return redirect()->route('cms.spas.show', ['spa' => $spaId])->with('success', 'Update logo success.');
    }

    

    public function storeSpaTreatment(StoreSpaTreatmentRequest $request)
    {
        // return $request->all();
        $time = $request->get('duration');
        $hours = floor($time / 60);
        $minutes = $time % 60;
        $data = $request->only([
            'spa_id',
            'name',
            'description',
            'price',
        ]);
        $data['duration'] = $hours . ':' . $minutes;
        $image = $request->file('image');
        if ($image) {
            $request->validate(
                [ 'image' =>  'nullable|file|mimes:png,jpg,jpeg|max:4096'],
                [ 'image.mimes' => 'Please upload only png or jpg file.',
                 'image.max' => 'Image is too large.'
                 ]
            );
        }

   
        // Store restaurant menu
        $this->spaService->storeSpaTreatment($data, $image);

        // $this->spaService->storeSpaTreatment($data);

        return redirect()->route('cms.spas.show', ['spa' =>$request->get('spa_id')])->with('success', 'Add menu success.');
    }

    public function updateSpaTreatment(Request $request)
    {
        $time = $request->get('durationUpdate');
        $hours = floor($time / 60);
        $minutes = $time % 60;
        $image = $request->file('image');
        // Validate Imgae
        if ($image) {
            $request->validate(
                [ 'image' =>  'nullable|file|mimes:png,jpg,jpeg|max:4096'],
                [ 'image.mimes' => 'Please upload only png or jpg file.',
                 'image.max' => 'Image is too large.'
                 ]
            );
        }
        $this->spaService->updateSpaTreatment($request->get('spa_treatment_id'), [
            'name' => $request->get('nameUpdate'),
            'description' => $request->get('descriptionUpdate'),
            'duration' => $hours . ':' . $minutes,
            'price' => $request->get('priceUpdate'),
        ], $image);

        return redirect()->route('cms.spas.show', ['spa' =>$request->get('spa_id')])->with('success', 'Update menu success.');
    }

    public function destroySpaTreatment(Request $request)
    {
        $this->spaService->destroySpaTreatment($request->get('spa_treatment_id'));

        return redirect()->route('cms.spas.show', ['spa' =>$request->get('spa_id')])->with('success', 'Delete menu success.');
    }

    public function ajaxGetSpaTreatments(Request $request)
    {
        return [
            'status' => true,
        ];
    }

    public function ajaxGetSpaTreatment(Request $request, $spaTreatmentId)
    {
        // Get guest details
        $treatment = SpaTreatment::where('id', $spaTreatmentId)
            ->whereHas('spa', function (Builder $query) {
                $query->where('hotel_id', auth('hotel')->user()->id);
            })
            ->first();
        if (!$treatment) {
            return [
                'status' => false,
                'spaTreatmentDetails' => null,
            ];
        }

        // Parse time to duration
        $hours = Carbon::parse($treatment->duration)->hour;
        $minutes = Carbon::parse($treatment->duration)->minute;
        $treatment->duration_alt = ($hours * 60) + $minutes;

        return [
            'status' => true,
            'spaTreatmentDetails' => $treatment,
        ];
    }
}